package at.moki.selenium.cucumber;

import at.cucumber.dbunit.bdd.glue.CucumberDbunitGlueDe;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.de.Angenommen;
import cucumber.api.java.de.Dann;
import cucumber.api.java.de.Und;
import cucumber.api.java.de.Wenn;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import cucumber.runtime.arquillian.api.Glues;
import org.apache.commons.lang3.SystemUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author Green Arrow
 * @date 27.07.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(CukeSpace.class)
@Features({
        "src/test/resources/features/loginFrontend.feature",
        "src/test/resources/features/krankenpflegerinFrontend.feature"
})
@Glues({SeleniumCucumberIT.class, CucumberDbunitGlueDe.class})
public class SeleniumCucumberIT {

    private WebDriver webDriver;

    @BeforeClass
    public static void globalInit() {
        if (SystemUtils.IS_OS_LINUX) {
            System.setProperty("webdriver.gecko.driver", Objects.requireNonNull(SeleniumCucumberIT.class.getClassLoader()
                    .getResource("geckodriver")).getFile());
        } else if (SystemUtils.IS_OS_WINDOWS) {
            System.setProperty("webdriver.gecko.driver", Objects.requireNonNull(SeleniumCucumberIT.class.getClassLoader()
                    .getResource("geckodriver-v0.24.0-win64.exe")).getFile());
        } else {
            throw new IllegalStateException("Mac OS X is certainly not supported by THIS application!!! -> choose GNU/Linux!");
        }
    }

    @Before
    public void init() {
        this.webDriver = new FirefoxDriver();
        this.webDriver.manage().window().maximize();
    }

    @After
    public void destroyWebDriver() {
        this.webDriver.quit();
    }

    @Dann("wird auf die Seite (.*) verlinkt")
    public void wirdAufDiePageVerlinkt(String page) {
        Assert.assertEquals(page, this.webDriver.getCurrentUrl());
    }

    @Angenommen("^die Textbox (.*) hat den Wert (.*)$")
    public void dasFeldTxtUsername_textbox_HatDenWertAdmin(String inputElement, String value) {
        this.webDriver.findElement(By.id(inputElement)).sendKeys(value);
    }

    @Wenn("^auf den Button (.*) geklickt wird$")
    public void aufDenButtonBtnLogin_button_GeklicktWird(String buttonElement) throws InterruptedException {
        Thread.sleep(500);
        this.webDriver.findElement(By.id(buttonElement)).click();
    }

    @Angenommen("^die Applikation befindet sich auf der Seite (.*)$")
    public void dieApplikationBefindetSichAufDerSeite(String url) {
        this.webDriver.get(url);
    }

    @Und("^Enter gedrueckt wird$")
    public void enterGedruecktWird() {
        webDriver.switchTo().activeElement().sendKeys(Keys.ENTER);
    }
}
