# language: de

Funktionalität: Dieses Feature File ueberprueft ob ein gueltiger User sich einloggen kann

  Grundlage:
    Angenommen der Treiber ist org.mariadb.jdbc.Driver
    Und die URL ist jdbc:mysql://localhost:3306/mokidb
    Und der Username ist moki
    Und das Passwort ist 288f3d45-e741-4a4c-ace4-13841f1ea50c
    Und folgendes SQL Script wird ausgefuehrt /sql/mokidb.sql

  Szenario: Es loggt sich ein gueltiger User ein
    Angenommen die Applikation befindet sich auf der Seite http://localhost:8080/moki-frontend/login.zul
    Und die Textbox txtUsername_textbox_20 hat den Wert admin
    Und die Textbox txtPassword_textbox_25 hat den Wert admin
    Wenn auf den Button btnLogin_button_29 geklickt wird
    Dann wird auf die Seite http://localhost:8080/moki-frontend/content/index.zul verlinkt

  Szenario: Der Username ist falsch
    Angenommen die Applikation befindet sich auf der Seite http://localhost:8080/moki-frontend/login.zul
    Und die Textbox txtUsername_textbox_20 hat den Wert Grace
    Und die Textbox txtPassword_textbox_25 hat den Wert admin
    Wenn auf den Button btnLogin_button_29 geklickt wird
    Dann wird auf die Seite http://localhost:8080/moki-frontend/login.zul verlinkt

  Szenario: Das Passwort ist falsch
    Angenommen die Applikation befindet sich auf der Seite http://localhost:8080/moki-frontend/login.zul
    Und die Textbox txtUsername_textbox_20 hat den Wert admin
    Und die Textbox txtPassword_textbox_25 hat den Wert Grace
    Wenn auf den Button btnLogin_button_29 geklickt wird
    Dann wird auf die Seite http://localhost:8080/moki-frontend/login.zul verlinkt