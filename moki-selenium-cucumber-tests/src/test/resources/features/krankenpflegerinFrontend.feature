# language: de

Funktionalität: dieses feature file ist ein test fuer dbunit

  Grundlage:
    Angenommen der Treiber ist org.mariadb.jdbc.Driver
    Und die URL ist jdbc:mysql://localhost:3306/mokidb
    Und der Username ist moki
    Und das Passwort ist 288f3d45-e741-4a4c-ace4-13841f1ea50c
    Und folgendes SQL Script wird ausgefuehrt /sql/mokidb.sql

  Szenario: Es wird eine neue Krankenpflegerin angelegt
    Angenommen die Applikation befindet sich auf der Seite http://localhost:8080/moki-frontend/login.zul
    Und die Textbox txtUsername_textbox_20 hat den Wert admin
    Und die Textbox txtPassword_textbox_25 hat den Wert admin
    Wenn auf den Button btnLogin_button_29 geklickt wird
    Dann wird auf die Seite http://localhost:8080/moki-frontend/content/index.zul verlinkt
    Wenn auf den Button menuAdministration_menu_23 geklickt wird
    Und auf den Button mNeuerEintrag_menu_25 geklickt wird
    Und auf den Button miEntryKrankenpflegerin_menuitem_27 geklickt wird
    Und die Textbox txtUsername_textbox_64 hat den Wert anja
    Und die Textbox txtVorname_textbox_71 hat den Wert Anja
    Und die Textbox txtNachname_textbox_75 hat den Wert K.
    Wenn auf den Button btnSave_button_98 geklickt wird
    Dann wird auf die Seite http://localhost:8080/moki-frontend/content/index.zul verlinkt


  Szenario: Es soll ueberprueft werden ob eine Krankenpflegerin gesucht werden kann
    Angenommen die Applikation befindet sich auf der Seite http://localhost:8080/moki-frontend/login.zul
    Und die Textbox txtUsername_textbox_20 hat den Wert admin
    Und die Textbox txtPassword_textbox_25 hat den Wert admin
    Wenn auf den Button btnLogin_button_29 geklickt wird
    Dann wird auf die Seite http://localhost:8080/moki-frontend/content/index.zul verlinkt
    Wenn auf den Button menuAdministration_menu_23 geklickt wird
    Und auf den Button mNeuerEintrag_menu_25 geklickt wird
    Und auf den Button miEntryKrankenpflegerin_menuitem_27 geklickt wird
    Und die Textbox txtUsername_textbox_64 hat den Wert anja
    Und die Textbox txtVorname_textbox_71 hat den Wert Anja
    Und die Textbox txtNachname_textbox_75 hat den Wert K.
    Wenn auf den Button btnSave_button_98 geklickt wird
    Dann wird auf die Seite http://localhost:8080/moki-frontend/content/index.zul verlinkt
    Wenn Enter gedrueckt wird
    Und auf den Button miLogout_menuitem_41 geklickt wird
    Dann die Applikation befindet sich auf der Seite http://localhost:8080/moki-frontend/login.zul
    Und die Textbox txtUsername_textbox_20 hat den Wert anja
    Und die Textbox txtPassword_textbox_25 hat den Wert s3cret
    Wenn auf den Button btnLogin_button_29 geklickt wird
    Und auf den Button menuProfile_menu_7 geklickt wird
    Und auf den Button menuUserLoginData_menu_9 geklickt wird
    Und auf den Button miUserLoginUsername_menuitem_11 geklickt wird
    Und die Textbox txtUsernameOld_textbox_64 hat den Wert anja
    Und die Textbox txtUsernameNew_textbox_69 hat den Wert edith
    Und die Textbox txtUsernameNewConfirmation_textbox_74 hat den Wert edith
    Wenn auf den Button btnUpdateUsername_button_76 geklickt wird
    Wenn Enter gedrueckt wird
    Wenn auf den Button miLogout_menuitem_41 geklickt wird
    Und die Textbox txtUsername_textbox_20 hat den Wert edith
    Und die Textbox txtPassword_textbox_25 hat den Wert s3cret
    Wenn auf den Button btnLogin_button_29 geklickt wird
    Dann wird auf die Seite http://localhost:8080/moki-frontend/content/index.zul verlinkt