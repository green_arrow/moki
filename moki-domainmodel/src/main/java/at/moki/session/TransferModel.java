package at.moki.session;

import at.moki.domainmodel.klientin.KlientIn;
import at.moki.domainmodel.klientin.KlientInExcelContainerModel;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * created by Green Arrow on 25.05.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
@SessionScoped
public class TransferModel implements Serializable {

    private static final long serialVersionUID = -1401397064109381224L;

    private KlientIn klientInOrgProt;

    private KlientIn klientInSearch;

    private String klientInTaetigkeitsfeld;

    private List<KlientInExcelContainerModel> klientInExcelContainerModelList;

    public void clearKlientInSearch() {
        this.klientInSearch = null;
    }

    public void clearAll() {
        this.klientInOrgProt = null;
        this.klientInSearch = null;
        this.klientInTaetigkeitsfeld = null;
        this.klientInExcelContainerModelList = null;
    }

    public KlientIn getKlientInSearch() {
        return klientInSearch;
    }

    public void setKlientInSearch(KlientIn klientInSearch) {
        this.klientInSearch = klientInSearch;
    }

    public KlientIn getKlientInOrgProt() {
        return klientInOrgProt;
    }

    public void setKlientInOrgProt(KlientIn klientInOrgProt) {
        this.klientInOrgProt = klientInOrgProt;
    }

    public String getKlientInTaetigkeitsfeld() {
        return klientInTaetigkeitsfeld;
    }

    public void setKlientInTaetigkeitsfeld(String klientInTaetigkeitsfeld) {
        this.klientInTaetigkeitsfeld = klientInTaetigkeitsfeld;
    }

    public List<KlientInExcelContainerModel> getKlientInExcelContainerModelList() {
        return klientInExcelContainerModelList;
    }

    public void setKlientInExcelContainerModelList(List<KlientInExcelContainerModel> klientInExcelContainerModelList) {
        this.klientInExcelContainerModelList = klientInExcelContainerModelList;
    }
}
