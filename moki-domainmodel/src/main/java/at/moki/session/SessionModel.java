package at.moki.session;

import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;
import at.moki.domainmodel.user.MokiUser;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Green Arrow on 19.01.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
@SessionScoped
public class SessionModel implements Serializable {

    private static final long serialVersionUID = -6556733534660498591L;

    private MokiUser mokiUser;

    private Map<String, Integer> userSperrList = new HashMap<>();

    private Krankenpflegerin krankenpflegerin;

    public MokiUser getMokiUser() {
        return mokiUser;
    }

    public Map<String, Integer> getUserSperrList() {
        return userSperrList;
    }

    public void setMokiUser(MokiUser mokiUser) {
        this.mokiUser = mokiUser;
    }

    public Krankenpflegerin getKrankenpflegerin() {
        return krankenpflegerin;
    }

    public void setKrankenpflegerin(Krankenpflegerin krankenpflegerin) {
        this.krankenpflegerin = krankenpflegerin;
    }

    public void clearAll() {
        this.mokiUser = null;
        this.krankenpflegerin = null;
        this.userSperrList = null;
    }
}
