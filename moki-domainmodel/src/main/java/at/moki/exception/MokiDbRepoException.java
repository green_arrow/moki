package at.moki.exception;

/**
 * Created by Green Arrow on 06.01.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class MokiDbRepoException extends RuntimeException {

    private static final long serialVersionUID = -7345948339817178407L;

    public MokiDbRepoException() {

    }

    public MokiDbRepoException(String message) {
        super(message);
    }

    public MokiDbRepoException(Throwable cause) {
        super(cause);
    }

    public MokiDbRepoException(String message, Throwable cause) {
        super(message, cause);
    }
}
