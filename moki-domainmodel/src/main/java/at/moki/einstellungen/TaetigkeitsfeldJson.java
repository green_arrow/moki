package at.moki.einstellungen;

import java.util.List;

/**
 * @author Green Arrow
 * @date 08.08.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class TaetigkeitsfeldJson {
    private String taetigkietsfeld;
    private String orgprot;
    private String stammblatt;
    private List<String> blattList;


    public String getTaetigkietsfeld() {
        return taetigkietsfeld;
    }

    public void setTaetigkietsfeld(String taetigkietsfeld) {
        this.taetigkietsfeld = taetigkietsfeld;
    }

    public String getOrgprot() {
        return orgprot;
    }

    public void setOrgprot(String orgprot) {
        this.orgprot = orgprot;
    }

    public String getStammblatt() {
        return stammblatt;
    }

    public void setStammblatt(String stammblatt) {
        this.stammblatt = stammblatt;
    }

    public List<String> getBlattList() {
        return blattList;
    }

    public void setBlattList(List<String> blattList) {
        this.blattList = blattList;
    }
}
