package at.moki.domainmodel.betreuungsstunden;

/**
 * Created by Green Arrow on 25.02.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class Betreuungsstunden {

    private String id;
    private int jahr;
    private String stunden;
    private String klientInId;

    public Betreuungsstunden() {
        // empty constructor
    }

    public Betreuungsstunden(String id, int jahr, String stunden, String klientInId) {
        this.id = id;
        this.jahr = jahr;
        this.stunden = stunden;
        this.klientInId = klientInId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getJahr() {
        return jahr;
    }

    public void setJahr(int jahr) {
        this.jahr = jahr;
    }

    public String getStunden() {
        return stunden;
    }

    public void setStunden(String stunden) {
        this.stunden = stunden;
    }

    public String getKlientInId() {
        return klientInId;
    }

    public void setKlientInId(String klientInId) {
        this.klientInId = klientInId;
    }
}
