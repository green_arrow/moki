package at.moki.domainmodel.klientin;

import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.domainmodel.finanzierung.Finanzierung;

import java.util.List;

/**
 * @author Green Arrow
 * @date 15.08.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class KJH {

    private KlientIn klientIn;
    private KlientIn zweitesKind;
    private KlientIn drittesKind;
    private KlientIn viertesKind;
    private List<Betreuungsstunden> betreuungsstundenKlientIn;
    private List<Betreuungsstunden> betreuungsstundenZweitesKind;
    private List<Betreuungsstunden> betreuungsstundenDrittesKind;
    private List<Betreuungsstunden> betreuungsstundenViertesKind;
    private List<Finanzierung> finanzierungList;

    public KlientIn getKlientIn() {
        return klientIn;
    }

    public void setKlientIn(KlientIn klientIn) {
        this.klientIn = klientIn;
    }

    public KlientIn getZweitesKind() {
        return zweitesKind;
    }

    public void setZweitesKind(KlientIn zweitesKind) {
        this.zweitesKind = zweitesKind;
    }

    public KlientIn getDrittesKind() {
        return drittesKind;
    }

    public void setDrittesKind(KlientIn drittesKind) {
        this.drittesKind = drittesKind;
    }

    public KlientIn getViertesKind() {
        return viertesKind;
    }

    public void setViertesKind(KlientIn viertesKind) {
        this.viertesKind = viertesKind;
    }

    public List<Betreuungsstunden> getBetreuungsstundenKlientIn() {
        return betreuungsstundenKlientIn;
    }

    public void setBetreuungsstundenKlientIn(List<Betreuungsstunden> betreuungsstundenKlientIn) {
        this.betreuungsstundenKlientIn = betreuungsstundenKlientIn;
    }

    public List<Betreuungsstunden> getBetreuungsstundenZweitesKind() {
        return betreuungsstundenZweitesKind;
    }

    public void setBetreuungsstundenZweitesKind(List<Betreuungsstunden> betreuungsstundenZweitesKind) {
        this.betreuungsstundenZweitesKind = betreuungsstundenZweitesKind;
    }

    public List<Betreuungsstunden> getBetreuungsstundenDrittesKind() {
        return betreuungsstundenDrittesKind;
    }

    public void setBetreuungsstundenDrittesKind(List<Betreuungsstunden> betreuungsstundenDrittesKind) {
        this.betreuungsstundenDrittesKind = betreuungsstundenDrittesKind;
    }

    public List<Betreuungsstunden> getBetreuungsstundenViertesKind() {
        return betreuungsstundenViertesKind;
    }

    public void setBetreuungsstundenViertesKind(List<Betreuungsstunden> betreuungsstundenViertesKind) {
        this.betreuungsstundenViertesKind = betreuungsstundenViertesKind;
    }

    public List<Finanzierung> getFinanzierungList() {
        return finanzierungList;
    }

    public void setFinanzierungList(List<Finanzierung> finanzierungList) {
        this.finanzierungList = finanzierungList;
    }
}
