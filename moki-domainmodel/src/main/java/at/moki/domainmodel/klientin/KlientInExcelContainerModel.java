package at.moki.domainmodel.klientin;

import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.domainmodel.finanzierung.Finanzierung;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;

import java.util.List;

/**
 * Created by Green Arrow on 01.09.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class KlientInExcelContainerModel {

    private Krankenpflegerin krankenpflegerin;

    private KlientIn klientIn;

    private List<Betreuungsstunden> betreuungsstundenList;

    private List<Finanzierung> finanzierungList;

    public Krankenpflegerin getKrankenpflegerin() {
        return krankenpflegerin;
    }

    public void setKrankenpflegerin(Krankenpflegerin krankenpflegerin) {
        this.krankenpflegerin = krankenpflegerin;
    }

    public KlientIn getKlientIn() {
        return klientIn;
    }

    public void setKlientIn(KlientIn klientIn) {
        this.klientIn = klientIn;
    }

    public List<Betreuungsstunden> getBetreuungsstundenList() {
        return betreuungsstundenList;
    }

    public void setBetreuungsstundenList(List<Betreuungsstunden> betreuungsstundenList) {
        this.betreuungsstundenList = betreuungsstundenList;
    }

    public List<Finanzierung> getFinanzierungList() {
        return finanzierungList;
    }

    public void setFinanzierungList(List<Finanzierung> finanzierungList) {
        this.finanzierungList = finanzierungList;
    }
}
