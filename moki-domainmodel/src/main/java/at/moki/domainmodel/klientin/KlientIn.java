package at.moki.domainmodel.klientin;

import java.time.LocalDate;

/**
 * Created by Green Arrow on 13.02.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class KlientIn {

    private String id;
    private String klientenNummer;
    private String vorname;
    private String nachname;
    private Geschlecht geschlecht;
    private LocalDate geburtsdatum;
    private LocalDate sterbedatum;
    private String taetigkeitsfeldId;
    private String taetigkeitsfeld;
    private String taetigkeitsfeldNummer;
    private String weitereKontaktdaten;
    private String verrechnungId;
    private String verrechnungNummer;
    private String spende;
    private String werkvertragsNummer;
    private String impacctGruppe;
    private String pflegestufe;
    private String sozialversicherungsnummer;
    private String sozialversicherungstraeger;
    private String diagnose;
    private String diagnoseschluessel;
    private boolean mutterVertreterin;
    private String mutterName;
    private LocalDate mutterGeburtsdatum;
    private String mutterTelefonnummer;
    private boolean vaterVertreter;
    private String vaterName;
    private LocalDate vaterGeburtsdatum;
    private String vaterTelefonnummer;
    private boolean sonstigeVertreter;
    private String sonstigeName;
    private LocalDate sonstigeGeburtsdatum;
    private String sonstigeTelefonnummer;
    private String strasse;
    private String mutterEmail;
    private String ort;
    private String vaterEmail;
    private boolean hauptwohnsitz;
    private String zuweisendeStelleKH;
    private String zuweisendeStelleKJH;
    private String zuweisendeStelleSonstige;
    private String kinderfacharztName;
    private String kinderfacharztTelefonnummer;
    private String betreuendeKrankenpflegerin;
    private String weitereKrankenpflegerinnen;
    private LocalDate betreuungsbeginn;
    private LocalDate betreuungsende;
    private String kmProHb;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKlientenNummer() {
        return klientenNummer;
    }

    public void setKlientenNummer(String klientenNummer) {
        this.klientenNummer = klientenNummer;
    }

    public String getVerrechnungId() {
        return verrechnungId;
    }

    public void setVerrechnungId(String verrechnungId) {
        this.verrechnungId = verrechnungId;
    }

    public String getVerrechnungNummer() {
        return verrechnungNummer;
    }

    public void setVerrechnungNummer(String verrechnungNummer) {
        this.verrechnungNummer = verrechnungNummer;
    }

    public String getTaetigkeitsfeldId() {
        return taetigkeitsfeldId;
    }

    public void setTaetigkeitsfeldId(String taetigkeitsfeldId) {
        this.taetigkeitsfeldId = taetigkeitsfeldId;
    }

    public String getTaetigkeitsfeld() {
        return taetigkeitsfeld;
    }

    public void setTaetigkeitsfeld(String taetigkeitsfeld) {
        this.taetigkeitsfeld = taetigkeitsfeld;
    }

    public String getTaetigkeitsfeldNummer() {
        return taetigkeitsfeldNummer;
    }

    public void setTaetigkeitsfeldNummer(String taetigkeitsfeldNummer) {
        this.taetigkeitsfeldNummer = taetigkeitsfeldNummer;
    }

    public String getWeitereKontaktdaten() {
        return weitereKontaktdaten;
    }

    public void setWeitereKontaktdaten(String weitereKontaktdaten) {
        this.weitereKontaktdaten = weitereKontaktdaten;
    }

    public String getSpende() {
        return spende;
    }

    public void setSpende(String spende) {
        this.spende = spende;
    }

    public String getWerkvertragsNummer() {
        return werkvertragsNummer;
    }

    public void setWerkvertragsNummer(String werkvertragsNummer) {
        this.werkvertragsNummer = werkvertragsNummer;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public Geschlecht getGeschlecht() {
        return geschlecht;
    }

    public void setGeschlecht(Geschlecht geschlecht) {
        this.geschlecht = geschlecht;
    }

    public String getImpacctGruppe() {
        return impacctGruppe;
    }

    public void setImpacctGruppe(String impacctGruppe) {
        this.impacctGruppe = impacctGruppe;
    }

    public String getPflegestufe() {
        return pflegestufe;
    }

    public void setPflegestufe(String pflegestufe) {
        this.pflegestufe = pflegestufe;
    }

    public String getSozialversicherungsnummer() {
        return sozialversicherungsnummer;
    }

    public void setSozialversicherungsnummer(String sozialversicherungsnummer) {
        this.sozialversicherungsnummer = sozialversicherungsnummer;
    }

    public LocalDate getGeburtsdatum() {
        return geburtsdatum;
    }

    public void setGeburtsdatum(LocalDate geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    public String getSozialversicherungstraeger() {
        return sozialversicherungstraeger;
    }

    public void setSozialversicherungstraeger(String sozialversicherungstraeger) {
        this.sozialversicherungstraeger = sozialversicherungstraeger;
    }

    public String getDiagnose() {
        return diagnose;
    }

    public void setDiagnose(String diagnose) {
        this.diagnose = diagnose;
    }

    public String getDiagnoseschluessel() {
        return diagnoseschluessel;
    }

    public void setDiagnoseschluessel(String diagnoseschluessel) {
        this.diagnoseschluessel = diagnoseschluessel;
    }

    public LocalDate getSterbedatum() {
        return sterbedatum;
    }

    public void setSterbedatum(LocalDate sterbedatum) {
        this.sterbedatum = sterbedatum;
    }

    public boolean isMutterVertreterin() {
        return mutterVertreterin;
    }

    public void setMutterVertreterin(boolean mutterVertreterin) {
        this.mutterVertreterin = mutterVertreterin;
    }

    public String getMutterName() {
        return mutterName;
    }

    public void setMutterName(String mutterName) {
        this.mutterName = mutterName;
    }

    public LocalDate getMutterGeburtsdatum() {
        return mutterGeburtsdatum;
    }

    public void setMutterGeburtsdatum(LocalDate mutterGeburtsdatum) {
        this.mutterGeburtsdatum = mutterGeburtsdatum;
    }

    public String getMutterTelefonnummer() {
        return mutterTelefonnummer;
    }

    public void setMutterTelefonnummer(String mutterTelefonnummer) {
        this.mutterTelefonnummer = mutterTelefonnummer;
    }

    public boolean isVaterVertreter() {
        return vaterVertreter;
    }

    public void setVaterVertreter(boolean vaterVertreter) {
        this.vaterVertreter = vaterVertreter;
    }

    public String getVaterName() {
        return vaterName;
    }

    public void setVaterName(String vaterName) {
        this.vaterName = vaterName;
    }

    public LocalDate getVaterGeburtsdatum() {
        return vaterGeburtsdatum;
    }

    public void setVaterGeburtsdatum(LocalDate vaterGeburtsdatum) {
        this.vaterGeburtsdatum = vaterGeburtsdatum;
    }

    public String getVaterTelefonnummer() {
        return vaterTelefonnummer;
    }

    public void setVaterTelefonnummer(String vaterTelefonnummer) {
        this.vaterTelefonnummer = vaterTelefonnummer;
    }

    public boolean isSonstigeVertreter() {
        return sonstigeVertreter;
    }

    public void setSonstigeVertreter(boolean sonstigeVertreter) {
        this.sonstigeVertreter = sonstigeVertreter;
    }

    public String getSonstigeName() {
        return sonstigeName;
    }

    public void setSonstigeName(String sonstigeName) {
        this.sonstigeName = sonstigeName;
    }

    public LocalDate getSonstigeGeburtsdatum() {
        return sonstigeGeburtsdatum;
    }

    public void setSonstigeGeburtsdatum(LocalDate sonstigeGeburtsdatum) {
        this.sonstigeGeburtsdatum = sonstigeGeburtsdatum;
    }

    public String getSonstigeTelefonnummer() {
        return sonstigeTelefonnummer;
    }

    public void setSonstigeTelefonnummer(String sonstigeTelefonnummer) {
        this.sonstigeTelefonnummer = sonstigeTelefonnummer;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getMutterEmail() {
        return mutterEmail;
    }

    public void setMutterEmail(String mutterEmail) {
        this.mutterEmail = mutterEmail;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getVaterEmail() {
        return vaterEmail;
    }

    public void setVaterEmail(String vaterEmail) {
        this.vaterEmail = vaterEmail;
    }

    public boolean isHauptwohnsitz() {
        return hauptwohnsitz;
    }

    public void setHauptwohnsitz(boolean hauptwohnsitz) {
        this.hauptwohnsitz = hauptwohnsitz;
    }

    public String getZuweisendeStelleKH() {
        return zuweisendeStelleKH;
    }

    public void setZuweisendeStelleKH(String zuweisendeStelleKH) {
        this.zuweisendeStelleKH = zuweisendeStelleKH;
    }

    public String getZuweisendeStelleKJH() {
        return zuweisendeStelleKJH;
    }

    public void setZuweisendeStelleKJH(String zuweisendeStelleKJH) {
        this.zuweisendeStelleKJH = zuweisendeStelleKJH;
    }

    public String getZuweisendeStelleSonstige() {
        return zuweisendeStelleSonstige;
    }

    public void setZuweisendeStelleSonstige(String zuweisendeStelleSonstige) {
        this.zuweisendeStelleSonstige = zuweisendeStelleSonstige;
    }

    public String getKinderfacharztName() {
        return kinderfacharztName;
    }

    public void setKinderfacharztName(String kinderfacharztName) {
        this.kinderfacharztName = kinderfacharztName;
    }

    public String getKinderfacharztTelefonnummer() {
        return kinderfacharztTelefonnummer;
    }

    public void setKinderfacharztTelefonnummer(String kinderfacharztTelefonnummer) {
        this.kinderfacharztTelefonnummer = kinderfacharztTelefonnummer;
    }

    public String getBetreuendeKrankenpflegerin() {
        return betreuendeKrankenpflegerin;
    }

    public void setBetreuendeKrankenpflegerin(String betreuendeKrankenpflegerin) {
        this.betreuendeKrankenpflegerin = betreuendeKrankenpflegerin;
    }

    public String getWeitereKrankenpflegerinnen() {
        return weitereKrankenpflegerinnen;
    }

    public void setWeitereKrankenpflegerinnen(String weitereKrankenpflegerinnen) {
        this.weitereKrankenpflegerinnen = weitereKrankenpflegerinnen;
    }

    public LocalDate getBetreuungsbeginn() {
        return betreuungsbeginn;
    }

    public void setBetreuungsbeginn(LocalDate betreuungsbeginn) {
        this.betreuungsbeginn = betreuungsbeginn;
    }

    public LocalDate getBetreuungsende() {
        return betreuungsende;
    }

    public void setBetreuungsende(LocalDate betreuungsende) {
        this.betreuungsende = betreuungsende;
    }

    public String getKmProHb() {
        return kmProHb;
    }

    public void setKmProHb(String kmProHb) {
        this.kmProHb = kmProHb;
    }
}
