package at.moki.domainmodel.klientin;

import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.domainmodel.finanzierung.Finanzierung;

import java.util.List;

/**
 * @author Green Arrow
 * @date 15.08.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class FK {

    private KlientIn klientIn;
    private KlientIn zwilling;
    private KlientIn drilling;
    private List<Betreuungsstunden> betreuungsstundenKlientIn;
    private List<Betreuungsstunden> betreuungsstundenZwilling;
    private List<Betreuungsstunden> betreuungsstundenDrilling;
    private List<Finanzierung> finanzierungList;

    public KlientIn getKlientIn() {
        return klientIn;
    }

    public void setKlientIn(KlientIn klientIn) {
        this.klientIn = klientIn;
    }

    public KlientIn getZwilling() {
        return zwilling;
    }

    public void setZwilling(KlientIn zwilling) {
        this.zwilling = zwilling;
    }

    public KlientIn getDrilling() {
        return drilling;
    }

    public void setDrilling(KlientIn drilling) {
        this.drilling = drilling;
    }

    public List<Betreuungsstunden> getBetreuungsstundenKlientIn() {
        return betreuungsstundenKlientIn;
    }

    public void setBetreuungsstundenKlientIn(List<Betreuungsstunden> betreuungsstundenKlientIn) {
        this.betreuungsstundenKlientIn = betreuungsstundenKlientIn;
    }

    public List<Betreuungsstunden> getBetreuungsstundenZwilling() {
        return betreuungsstundenZwilling;
    }

    public void setBetreuungsstundenZwilling(List<Betreuungsstunden> betreuungsstundenZwilling) {
        this.betreuungsstundenZwilling = betreuungsstundenZwilling;
    }

    public List<Betreuungsstunden> getBetreuungsstundenDrilling() {
        return betreuungsstundenDrilling;
    }

    public void setBetreuungsstundenDrilling(List<Betreuungsstunden> betreuungsstundenDrilling) {
        this.betreuungsstundenDrilling = betreuungsstundenDrilling;
    }

    public List<Finanzierung> getFinanzierungList() {
        return finanzierungList;
    }

    public void setFinanzierungList(List<Finanzierung> finanzierungList) {
        this.finanzierungList = finanzierungList;
    }
}
