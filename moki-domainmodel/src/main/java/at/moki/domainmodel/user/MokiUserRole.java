package at.moki.domainmodel.user;

/**
 * Created by Green Arrow on 13.01.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public enum MokiUserRole {

    ADMINISTRATOR("ADMINISTRATOR"),
    KRANKENPFLEGERIN("KRANKENPFLEGERIN"),
    VERWALTUNG("VERWALTUNG");

    private final String rolename;

    MokiUserRole(String rolename) {
        this.rolename = rolename;
    }

    public String getRolename() {
        return rolename;
    }
}
