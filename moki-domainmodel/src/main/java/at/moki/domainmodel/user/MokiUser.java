package at.moki.domainmodel.user;

/**
 * Created by Green Arrow on 22.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class MokiUser {

    private String id;
    private String username;
    private String password;
    private boolean gesperrt;
    private MokiUserRole mokiUserRole;
    private String krankenpflegerinId;
    private String userrechteId;
    private Integer sperrungCounter;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isGesperrt() {
        return gesperrt;
    }

    public void setGesperrt(boolean gesperrt) {
        this.gesperrt = gesperrt;
    }

    public MokiUserRole getMokiUserRole() {
        return mokiUserRole;
    }

    public void setMokiUserRole(MokiUserRole mokiUserRole) {
        this.mokiUserRole = mokiUserRole;
    }

    public String getKrankenpflegerinId() {
        return krankenpflegerinId;
    }

    public void setKrankenpflegerinId(String krankenpflegerinId) {
        this.krankenpflegerinId = krankenpflegerinId;
    }

    public String getUserrechteId() {
        return userrechteId;
    }

    public void setUserrechteId(String userrechteId) {
        this.userrechteId = userrechteId;
    }

    public Integer getSperrungCounter() {
        return sperrungCounter;
    }

    public void setSperrungCounter(Integer sperrungCounter) {
        this.sperrungCounter = sperrungCounter;
    }
}
