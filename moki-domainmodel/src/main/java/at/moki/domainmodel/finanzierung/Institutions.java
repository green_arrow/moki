package at.moki.domainmodel.finanzierung;

/**
 * Created by Green Arrow on 09.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public enum Institutions {

    LAND("Land NÖ"),
    MUKI("Muki"),
    KINDER_JUGENDHILFE("Kinder- und Jugendhilfe"),
    VERSICHERUNG("Versicherung"),
    HOSPIZ("Hospiz/MPT"),
    SPENDEN("Spenden"),
    SOZIALVERSICHERUNG("Sozialversicherung"),
    PRIVAT("privat"),
    KIB("KiB"),
    ANDERE("andere");

    private final String institution;

    Institutions(String institution) {
        this.institution = institution;
    }

    public String getInstitutionFullName() {
        return institution;
    }

    public static boolean exists(String rolename) {
        for (Institutions role : values()) {
            if (role.institution.equals(rolename)) {
                return true;
            }
        }
        return false;
    }
}
