package at.moki.domainmodel.excel.dateien;

/**
 * @author Green Arrow
 * @date 17.08.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class ExcelDatei {

    private String id;
    private String dateiname;
    private byte[] datei;
    private String taetigkeitsfeldId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDateiname() {
        return dateiname;
    }

    public void setDateiname(String dateiname) {
        this.dateiname = dateiname;
    }

    public byte[] getDatei() {
        return datei;
    }

    public void setDatei(byte[] datei) {
        this.datei = datei;
    }

    public String getTaetigkeitsfeldId() {
        return taetigkeitsfeldId;
    }

    public void setTaetigkeitsfeldId(String taetigkeitsfeldId) {
        this.taetigkeitsfeldId = taetigkeitsfeldId;
    }
}
