package at.moki.util;

/**
 * Created by Green Arrow on 31.08.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public final class MokiFileUtil {

    private static final String FK_FILE = "Klientendatei und HN FK 2017.xlsx";
    private static final String FRZ_FILE = "Klientendatei und HN FREIZEIT 2017.xlsx";
    private static final String ICH_FILE = "Klientendatei und HN ICH 2017.xlsx";
    private static final String MPT_FILE = "Klientendatei und HN LISA-MARIE 2017.xlsx";
    private static final String KJH_FILE = "Klientendatei und HN MARIENKÄFER 2018.xlsx";
    private static final String TS_FILE = "Klientendatei und HN TAPFERES SCHNEIDERLEIN 2017.xlsx";

    private MokiFileUtil() {
        // util class
    }

    public static String getTaetigkeitsfeldFileName(String klientInTaetigkeitsfeld) {
        switch (klientInTaetigkeitsfeld) {
            case "FRZ":
                return FRZ_FILE;
            case "MPT":
                return MPT_FILE;
            case "ICH":
                return ICH_FILE;
            case "TS":
                return TS_FILE;
            case "FK":
                return FK_FILE;
            case "KJH":
                return KJH_FILE;
            default:
                throw new UnsupportedOperationException("Kein Taetigkeitsfeld vorhanden");
        }
    }
}
