package at.moki.util;

import java.time.Year;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Green Arrow on 11.09.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public final class MokiDateUtil {

    private MokiDateUtil() {
        // util class
    }

    public static Integer getYearNow() {
        return generateFutureDates().get(0);
    }

    public static Integer getPlusOneYear() {
        return generateFutureDates().get(1);
    }

    public static Integer getplusTwoYears() {
        return generateFutureDates().get(2);
    }

    private static List<Integer> generateFutureDates() {
        Year now = Year.now();
        return IntStream.of(now.getValue(), now.plusYears(1).getValue(), now.plusYears(2).getValue())
                .boxed()
                .collect(Collectors.toList());
    }
}
