package at.moki.util;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by Green Arrow on 23.01.2019.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public final class MokiFilterUtil {

    private MokiFilterUtil() {
        // private constructor
    }

    public static boolean containsIgnoreCaseMatch(String value, String filter) {
        return filter == null || StringUtils.isEmpty(filter) || StringUtils.containsIgnoreCase(value, filter);
    }
}
