package at.moki.security;

import at.moki.domainmodel.user.MokiUserRole;
import at.moki.exception.MokiSecurityException;
import at.moki.logger.MokiLogger;
import at.moki.session.SessionModel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.io.Serializable;
import java.lang.reflect.Method;

/**
 * Created by Green Arrow on 04.05.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Interceptor
@MokiRolesAllowed
public class MokiRolesAllowedInterceptor implements Serializable {

    private static final long serialVersionUID = 5651245389015580133L;

    @Inject
    @MokiLogger
    private Logger logger;

    @Inject
    private SessionModel sessionModel;

    @AroundInvoke
    public Object checkRoles(InvocationContext invocationContext) throws Exception {

        Method method = invocationContext.getMethod();
        MokiRolesAllowed privilegeAnnotation = method.getAnnotation(MokiRolesAllowed.class);

        MokiUserRole[] mokiUserRoleList = privilegeAnnotation.userRoles();

        for (MokiUserRole mokiUserRoleAvailable : mokiUserRoleList) {
            MokiUserRole mokiUserRoleSessionModel = this.sessionModel.getMokiUser().getMokiUserRole();
            if (mokiUserRoleSessionModel != null && StringUtils.equals(mokiUserRoleAvailable.getRolename(), mokiUserRoleSessionModel.getRolename())) {
                return invocationContext.proceed();
            }
        }

        String message = String.format("User %s hat keine Rechte um die Methode %s aufzurufen",
                this.sessionModel.getMokiUser().getUsername(),
                method.getDeclaringClass() + ":" + method.getName());

        this.logger.error(message);
        throw new MokiSecurityException(message);
    }
}
