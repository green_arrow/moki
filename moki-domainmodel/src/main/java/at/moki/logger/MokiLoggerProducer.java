package at.moki.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

/**
 * Created by Green Arrow on 09.01.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class MokiLoggerProducer {

    private static Logger logger = LoggerFactory.getLogger(MokiLoggerProducer.class);

    @Produces
    @MokiLogger
    public Logger createLogger(InjectionPoint injectionPoint) {
        logger.debug("erstelle logger in producer");
        return LoggerFactory.getLogger(injectionPoint.getMember().getDeclaringClass());
    }

    private void destroyLogger(@Disposes @MokiLogger Logger logger) {
        logger.debug("logger wird zerstoert");
    }
}
