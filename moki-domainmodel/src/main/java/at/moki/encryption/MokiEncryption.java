package at.moki.encryption;

import org.apache.commons.codec.digest.DigestUtils;

import javax.inject.Named;

/**
 * Created by Green Arrow on 10.02.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
public class MokiEncryption {

    /**
     * generate a hash value for password three times
     *
     * @param password the hardcoded password
     * @return a three time hash
     */
    public String generateHashPassword(String password, String salt) {
        String hashPassword = DigestUtils.md5Hex(password + salt); // first time hash
        hashPassword = DigestUtils.md5Hex(hashPassword + salt); // second time hash
        hashPassword = DigestUtils.md5Hex(hashPassword + salt); // third time hash

        return hashPassword + salt;
    }
}
