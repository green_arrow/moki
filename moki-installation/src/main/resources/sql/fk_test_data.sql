
#Klientin:

insert into mokidb.klientin values('101e8a1d-1a3c-4dc4-9d47-20e647e530fc', '111111', '2', 'FK', '2', NULL, NULL, '2', '2', '1', '2', 'weiblich', '2', '2', '2', '2018-08-01', '2', '2', '2', '2018-08-02', 'false', '1', '2018-08-01', '1', 'false', '1', '2018-08-01', '1', 'false', '1', '2018-08-01', '1', '1', '1', '1', '1', 'true', '1', '1', '1', '1', '1', '1', '1', '2018-08-01', '2018-08-01', '1');
insert into mokidb.klientin values('57e7c097-4243-483e-8801-09561807409f', '00000', '1', 'FK', '1', 'Weitere Kontaktdaten\n', NULL, '1', '1', '1', '1', 'männlich', '1', '1', '1', '2018-08-01', '1', '1', '1', '2018-08-01', 'false', '1', '2018-08-01', '1', 'false', '1', '2018-08-01', '1', 'false', '1', '2018-08-01', '1', '1', '1', '1', '1', 'true', '1', '1', '1', '1', '1', '1', '1', '2018-08-01', '2018-08-01', '1');
insert into mokidb.klientin values('76318922-9ab8-4cf2-9ccf-8f30401f9b87', '33333', '3', 'FK', '3', NULL, NULL, '3', '3', '1', '3', 'männlich', '3', '3', '3', '2018-08-01', '3', '3', '3', '2018-08-03', 'false', '1', '2018-08-01', '1', 'false', '1', '2018-08-01', '1', 'false', '1', '2018-08-01', '1', '1', '1', '1', '1', 'true', '1', '1', '1', '1', '1', '1', '1', '2018-08-01', '2018-08-01', '1');

#Organisationsprotokoll

insert into mokidb.organisationsprotokoll values('6eb2d9c9-df9a-4429-a9c2-d4cd85a22cd0', 'neuer eintrag', 'fadi', '2018-08-27 18:29:10', '57e7c097-4243-483e-8801-09561807409f');

#Verwandte:

insert into mokidb.verwandte values('538d9370-383e-4132-8072-871912bb5dbc', '57e7c097-4243-483e-8801-09561807409f', '101e8a1d-1a3c-4dc4-9d47-20e647e530fc', '76318922-9ab8-4cf2-9ccf-8f30401f9b87', NULL);

#Betreuungsstunden:

insert into mokidb.betreuungsstunden values('0595f39f-86f4-42eb-bdd5-0be37860650c', '2018', '3', '76318922-9ab8-4cf2-9ccf-8f30401f9b87');
insert into mokidb.betreuungsstunden values('1d06fac3-6b43-4a36-831f-33c595b4f03e', '2017', '1', '57e7c097-4243-483e-8801-09561807409f');
insert into mokidb.betreuungsstunden values('2377d62b-f63e-426b-9a50-116f263b2560', '2018', '2', '101e8a1d-1a3c-4dc4-9d47-20e647e530fc');
insert into mokidb.betreuungsstunden values('a29931f0-6c36-4ec3-9fcc-0997827df30c', '2016', '3', '76318922-9ab8-4cf2-9ccf-8f30401f9b87');
insert into mokidb.betreuungsstunden values('cce1056f-b4dc-4f11-bd03-5795f8250cc6', '2017', '3', '76318922-9ab8-4cf2-9ccf-8f30401f9b87');
insert into mokidb.betreuungsstunden values('d299de9e-be1a-4a16-a2f3-20b0430f6ce4', '2016', '2', '101e8a1d-1a3c-4dc4-9d47-20e647e530fc');
insert into mokidb.betreuungsstunden values('d69bf56f-d80c-438a-993e-a90b5b6a03b8', '2017', '2', '101e8a1d-1a3c-4dc4-9d47-20e647e530fc');
insert into mokidb.betreuungsstunden values('ef565609-2f72-46c1-93ee-7ed99e9f65c0', '2016', '1', '57e7c097-4243-483e-8801-09561807409f');
insert into mokidb.betreuungsstunden values('f15672da-522b-4d2f-9e95-ee281b91f416', '2018', '1', '57e7c097-4243-483e-8801-09561807409f');

#Finanzierung:

insert into mokidb.finanzierung values('08f58569-94d8-47a0-aba4-f63206387311', 'andere', 'false', 'false', '57e7c097-4243-483e-8801-09561807409f');
insert into mokidb.finanzierung values('2082a4b8-0e8b-4bc1-bf87-f173cfd5f0d1', 'Versicherung', 'false', 'false', '57e7c097-4243-483e-8801-09561807409f');
insert into mokidb.finanzierung values('2492bd86-23ba-47d3-b20f-57fb9667cf3e', 'Spenden', 'false', 'false', '57e7c097-4243-483e-8801-09561807409f');
insert into mokidb.finanzierung values('44cec12f-b8e5-4813-a615-4a7d8dfe942c', 'Sozialversicherung', 'false', 'false', '57e7c097-4243-483e-8801-09561807409f');
insert into mokidb.finanzierung values('51cc24ca-0a97-40e1-8b67-dc40349cd8c1', 'Land NÖ', 'false', 'false', '57e7c097-4243-483e-8801-09561807409f');
insert into mokidb.finanzierung values('969c40e4-0cd7-468a-b4d3-ec7440ecfb4b', 'Muki', 'false', 'false', '57e7c097-4243-483e-8801-09561807409f');
insert into mokidb.finanzierung values('b479fe70-ba85-4098-b94d-26069d9b63bf', 'KiB', 'false', 'false', '57e7c097-4243-483e-8801-09561807409f');
insert into mokidb.finanzierung values('c6f87742-f1ad-4036-a965-1dad96b2c3dc', 'Hospiz/MPT', 'false', 'true', '57e7c097-4243-483e-8801-09561807409f');
insert into mokidb.finanzierung values('d1de92d6-c253-4f4c-bc6d-d550de724541', 'privat', 'false', 'false', '57e7c097-4243-483e-8801-09561807409f');
insert into mokidb.finanzierung values('d27a109e-ba5d-45d0-8872-8724a23184bb', 'Kinder- und Jugendhilfe', 'false', 'true', '57e7c097-4243-483e-8801-09561807409f');
