package at.moki.dbrepo.organisationsprotokoll;

import at.moki.dbrepo.MockBase;
import at.moki.domainmodel.organisationsprotokoll.Organisationsprotokoll;

/**
 * Created by Green Arrow on 18.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class OrganisationsprotokollMock extends MockBase {

    private String id;
    private String text;
    private String handschriftDgkp;
    private String aenderungszeitpunkt;
    private String klientInId;

    Organisationsprotokoll mapToDomainmodel() {
        Organisationsprotokoll organisationsprotokoll = new Organisationsprotokoll();

        organisationsprotokoll.setId(super.denull(getId()));
        organisationsprotokoll.setText(super.denull(getText()));
        organisationsprotokoll.setHandschriftDgkp(super.denull(getHandschriftDgkp()));
        organisationsprotokoll.setAenderungszeitpunkt(toLocalDateTime(getAenderungszeitpunkt()));
        organisationsprotokoll.setKlientInId(super.denull(getKlientInId()));

        return organisationsprotokoll;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    private String getHandschriftDgkp() {
        return handschriftDgkp;
    }

    public void setHandschriftDgkp(String handschriftDgkp) {
        this.handschriftDgkp = handschriftDgkp;
    }

    private String getAenderungszeitpunkt() {
        return aenderungszeitpunkt;
    }

    public void setAenderungszeitpunkt(String aenderungszeitpunkt) {
        this.aenderungszeitpunkt = aenderungszeitpunkt;
    }

    private String getKlientInId() {
        return klientInId;
    }

    public void setKlientInId(String klientInId) {
        this.klientInId = klientInId;
    }
}
