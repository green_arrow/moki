package at.moki.dbrepo.krankenpflegerin;

import at.moki.dbrepo.MockBase;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;

/**
 * Created by Green Arrow on 25.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class KrankenpflegerinMock extends MockBase {

    private String id;
    private String vorname;
    private String nachname;
    private String bezeichnung;
    private String strasse;
    private String postleitzahl;
    private String telefonnummer;
    private String bankverbindung;
    private String iban;
    private String bic;
    private String email;

    Krankenpflegerin mapToDomainmodel() {
        Krankenpflegerin krankenpflegerin = new Krankenpflegerin();
        
        krankenpflegerin.setId(super.denull(getId()));
        krankenpflegerin.setVorname(super.denull(getVorname()));
        krankenpflegerin.setNachname(super.denull(getNachname()));
        krankenpflegerin.setBezeichnung(super.denull(getBezeichnung()));
        krankenpflegerin.setStrasse(super.denull(getStrasse()));
        krankenpflegerin.setPostleitzahl(super.denull(getPostleitzahl()));
        krankenpflegerin.setTelefonnummer(super.denull(getTelefonnummer()));
        krankenpflegerin.setBankverbindung(super.denull(getBankverbindung()));
        krankenpflegerin.setIban(super.denull(getIban()));
        krankenpflegerin.setBic(super.denull(getBic()));
        krankenpflegerin.setEmail(super.denull(getEmail()));

        return krankenpflegerin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        id = id;
    }

    private String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        vorname = vorname;
    }

    private String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        nachname = nachname;
    }

    private String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        bezeichnung = bezeichnung;
    }

    private String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        strasse = strasse;
    }

    private String getPostleitzahl() {
        return postleitzahl;
    }

    public void setPostleitzahl(String postleitzahl) {
        postleitzahl = postleitzahl;
    }

    private String getTelefonnummer() {
        return telefonnummer;
    }

    public void setTelefonnummer(String telefonnummer) {
        telefonnummer = telefonnummer;
    }

    private String getBankverbindung() {
        return bankverbindung;
    }

    public void setBankverbindung(String bankverbindung) {
        bankverbindung = bankverbindung;
    }

    private String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        iban = iban;
    }

    private String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        bic = bic;
    }

    private String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        email = email;
    }
}
