package at.moki.dbrepo.entitymanager.producer;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by Green Arrow on 31.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class LocalMokiEntityManagerProducer {

    private static final String PERSISTENCE_UNIT = "mokiDbIntegration";

    private EntityManagerFactory entityManagerFactory;

    /**
     * Producer-Methode für den {@link EntityManager}.
     *
     * @return den aktuellen {@link EntityManager}
     */
    @Produces
    @MokiEntityManager
    @ApplicationScoped
    public EntityManager produceEntityManager() {
        if (entityManagerFactory == null) {
            entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
        }
        return entityManagerFactory.createEntityManager();
    }

    public void disposeEntityManager(@Disposes @MokiEntityManager EntityManager em) {
        if (em.isOpen()) {
            em.close();
        }

        if (entityManagerFactory != null && entityManagerFactory.isOpen()) {
            entityManagerFactory.close();
        }
    }
}
