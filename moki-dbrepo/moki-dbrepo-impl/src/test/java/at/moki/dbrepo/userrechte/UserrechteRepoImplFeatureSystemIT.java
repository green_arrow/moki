package at.moki.dbrepo.userrechte;

import at.cucumber.dbunit.bdd.glue.CucumberDbunitGlueDe;
import at.moki.dbrepo.AbsIntegrationTest;
import at.moki.dbrepo.entitymanager.producer.LocalMokiEntityManagerProducer;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.domainmodel.userrechte.Userrechte;
import at.moki.exception.MokiSecurityException;
import at.moki.logger.MokiLoggerProducer;
import at.moki.security.MokiRolesAllowedInterceptor;
import at.moki.session.SessionModel;
import cucumber.api.java.de.Angenommen;
import cucumber.api.java.de.Dann;
import cucumber.api.java.de.Wenn;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import cucumber.runtime.arquillian.api.Glues;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.unitils.reflectionassert.ReflectionAssert;

import javax.inject.Inject;
import java.io.File;
import java.util.List;

/**
 * Created by Green Arrow on 17.01.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(CukeSpace.class)
@Features({"src/test/resources/features/userrechte.feature"})
@Glues({CucumberDbunitGlueDe.class, UserrechteRepoImplFeatureSystemIT.class})
public class UserrechteRepoImplFeatureSystemIT extends AbsIntegrationTest {

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClass(UserrechteRepoImpl.class)
                .addClass(MokiLoggerProducer.class)
                .addClass(LocalMokiEntityManagerProducer.class)
                .addClass(SessionModel.class)
                .addClass(MokiRolesAllowedInterceptor.class)
                .addAsManifestResource(new FileAsset(new File("src/main/resources/META-INF/beans.xml")), "beans.xml");
        System.out.println(jar.toString(true));
        return jar;
    }

    @Inject
    private UserrechteRepo repo;

    private Userrechte userrechte;

    private Exception exception;
    private List<Userrechte> userrechteList;

    @Wenn("^nach allen Userrechten gesucht wird$")
    public void wennNachAllenUserrechtenGesuchtWird() {
        this.userrechteList = this.repo.findAll();
    }

    @Dann("^werden folgende Userrechte geladen$")
    public void werdenFolgendeUserrechteGeladen(List<Userrechte> list) {
        ReflectionAssert.assertReflectionEquals(list, this.userrechteList);
    }

    @Wenn("^nach der Userrechte Id (.*) gesucht wird$")
    public void nachDerUserrechteIdGesuchtWird(String userrechteId) {
        this.userrechte = this.repo.findById(userrechteId);
    }

    @Dann("^wird folgender Eintrag gefunden$")
    public void wirdFolgenderEintragGefunden(List<Userrechte> list) {
        ReflectionAssert.assertReflectionEquals(list.get(0), this.userrechte);
    }

    @Angenommen("^nach der Userrechte Bezeichnung (.*) gesucht wird$")
    public void nachGesuchtWird(String bezeichnung) {
        this.userrechte = this.repo.findByBezeichnung(MokiUserRole.valueOf(bezeichnung));
    }

    @Dann("^wird ein Eintrag mit der ID (.*) und der Bezeichnung (.*) gefunden$")
    public void wirdEinEintragMitDerIDUndDerBezeichnungGefunden(String id, String bezeichnung) {
        Assert.assertEquals(id, this.userrechte.getId());
        Assert.assertEquals(bezeichnung, this.userrechte.getMokiUserRole().getRolename());
    }

    @Angenommen("^der User ist eine Krankenpflegerin$")
    public void derUserIstEineKrankenpflegerin() {
        super.sessionModel.setMokiUser(super.genereateUserKrankenpflegerin());
    }

    @Wenn("^die Suche nach allen Eintraegen ausgefuehrt wird$")
    public void dieSucheNachAllenEintraegenAusgefuehrtWird() {
        try {
            this.repo.findAll();
        } catch (Exception e) {
            this.exception = e;
        }
    }

    @Wenn("^die Suche nach der Bezeichnung ausgefuehrt wird$")
    public void dieSucheNachDerBezeichnungAusgefuehrtWird() {
        try {
            this.repo.findByBezeichnung(null); //Should be unnecessary
        } catch (Exception e) {
            this.exception = e;
        }
    }

    @Dann("^wird eine MokiSecurityException geworfen$")
    public void wirdEineMokiSecurityExceptionGeworfen() {
        Assert.assertTrue(this.exception instanceof MokiSecurityException);
    }
}
