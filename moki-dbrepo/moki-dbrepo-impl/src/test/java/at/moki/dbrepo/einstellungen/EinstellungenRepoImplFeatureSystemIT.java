package at.moki.dbrepo.einstellungen;

import at.cucumber.dbunit.bdd.glue.CucumberDbunitGlueDe;
import at.moki.dbrepo.AbsIntegrationTest;
import at.moki.dbrepo.entitymanager.producer.LocalMokiEntityManagerProducer;
import at.moki.dbrepo.entitymanager.producer.MokiEntityManager;
import at.moki.einstellungen.Einstellungen;
import at.moki.logger.MokiLoggerProducer;
import at.moki.security.MokiRolesAllowedInterceptor;
import at.moki.session.SessionModel;
import cucumber.api.java.de.Dann;
import cucumber.api.java.de.Wenn;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import cucumber.runtime.arquillian.api.Glues;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.runner.RunWith;
import org.unitils.reflectionassert.ReflectionAssert;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.io.File;
import java.util.List;

/**
 * @author Green Arrow
 * @date 25.08.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(CukeSpace.class)
@Features({"src/test/resources/features/einstellungen.feature"})
@Glues({CucumberDbunitGlueDe.class, EinstellungenRepoImplFeatureSystemIT.class})
public class EinstellungenRepoImplFeatureSystemIT extends AbsIntegrationTest {

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClass(EinstellungenRepoImpl.class)
                .addClass(MokiLoggerProducer.class)
                .addClass(LocalMokiEntityManagerProducer.class)
                .addClass(SessionModel.class)
                .addClass(MokiRolesAllowedInterceptor.class)
                .addAsManifestResource(new FileAsset(new File("src/main/resources/META-INF/beans.xml")), "beans.xml");
        System.out.println(jar.toString(true));
        return jar;
    }

    @Inject
    private EinstellungenRepo repo;

    private Einstellungen einstellungen;

    @Wenn("die Suche mit der Fachlichkeit TAETIGKEITSFELDER durchgefuehrt wird$")
    public void testFindByTaetigkeitsfeld() {
        this.einstellungen = this.repo.findByTaetigkeitsfeld();
    }

    @Dann("^werden folgende Einstellungen mit dem Inhalt gefunden$")
    public void werdenFolgendeEinstellungenMitDemInhaltGefunden(List<Einstellungen> einstellungenExpectedList) {
        ReflectionAssert.assertReflectionEquals(einstellungenExpectedList.get(0), this.einstellungen);
    }

    @Wenn("^die Einstellungen mit folgenden Daten gespeichert werden$")
    public void dieEinstellungenMitFolgendenDatenGespeichertWerden(List<Einstellungen> saved) {
        super.entityManager.getTransaction().begin();
        this.repo.merge(saved.get(0));
        super.entityManager.getTransaction().commit();
    }
}
