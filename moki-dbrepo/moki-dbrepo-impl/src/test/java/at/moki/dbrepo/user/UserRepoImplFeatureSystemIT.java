package at.moki.dbrepo.user;

import at.cucumber.dbunit.bdd.glue.CucumberDbunitGlueDe;
import at.moki.dbrepo.AbsIntegrationTest;
import at.moki.dbrepo.entitymanager.producer.LocalMokiEntityManagerProducer;
import at.moki.domainmodel.user.MokiUser;
import at.moki.exception.MokiSecurityException;
import at.moki.logger.MokiLoggerProducer;
import at.moki.security.MokiRolesAllowedInterceptor;
import at.moki.session.SessionModel;
import cucumber.api.java.de.Angenommen;
import cucumber.api.java.de.Dann;
import cucumber.api.java.de.Wenn;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import cucumber.runtime.arquillian.api.Glues;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.unitils.reflectionassert.ReflectionAssert;

import javax.inject.Inject;
import java.io.File;
import java.util.List;

/**
 * Created by Green Arrow on 20.01.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(CukeSpace.class)
@Features({"src/test/resources/features/user.feature"})
@Glues({CucumberDbunitGlueDe.class, UserRepoImplFeatureSystemIT.class})
public class UserRepoImplFeatureSystemIT extends AbsIntegrationTest {

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClass(UserRepoImpl.class)
                .addClass(MokiLoggerProducer.class)
                .addClass(LocalMokiEntityManagerProducer.class)
                .addClass(SessionModel.class)
                .addClass(MokiRolesAllowedInterceptor.class)
                .addAsManifestResource(new FileAsset(new File("src/main/resources/META-INF/beans.xml")), "beans.xml");
        System.out.println(jar.toString(true));
        return jar;
    }

    @Inject
    private UserRepo repo;

    private Exception exception;
    private List<MokiUser> mokiUserList;

    @Wenn("^nach allen Usern gesucht wird$")
    public void nachAllenUsernGesuchtWird() {
        this.mokiUserList = this.repo.findAll();
    }

    @Dann("^werden folgende User geladen$")
    public void werdenFolgendeUserGeladen(List<MokiUser> list) {
        ReflectionAssert.assertReflectionEquals(list, this.mokiUserList);
    }

    @Wenn("^nach der Userin mit dem Usernamen (.*)gesucht wird$")
    public void nachDerUserinMitDerIdGesuchtWird(String username) {
        this.mokiUserList = this.repo.findByUsername(username);
    }

    @Dann("^wird folgende Userin gefunden$")
    public void wirdFolgendeUserinGefunden(List<MokiUser> list) {
        ReflectionAssert.assertReflectionEquals(list, this.mokiUserList);
    }

    @Angenommen("^der User mit folgenden Daten gespeichert wird$")
    public void derUserMitFolgendenDatenGespeichertWirt(List<MokiUser> mokiUserList) {
        super.entityManager.getTransaction().begin();

        MokiUser mokiUser = mokiUserList.get(0);
        mokiUser.setKrankenpflegerinId(null);

        this.repo.merge(mokiUser);
        super.entityManager.getTransaction().commit();
    }

    @Wenn("^der User mit der ID (.*) geloescht wird$")
    public void derUserMitDerIDGeloeschtWird(String id) {
        super.entityManager.getTransaction().begin();
        this.repo.remove(id);
        super.entityManager.getTransaction().commit();
    }

    @Angenommen("^der User ist eine Krankenpflegerin$")
    public void derUserIstEineKrankenpflegerin() {
        super.sessionModel.setMokiUser(super.genereateUserKrankenpflegerin());
    }

    @Wenn("^die Suche nach allen Eintraegen ausgefuehrt wird$")
    public void dieSucheNachAllenEintraegenAusgefuehrtWird() {
        try {
            this.repo.findAll();
        } catch (Exception e) {
            this.exception = e;
        }
    }

    @Wenn("^eine KlientIn geloescht wird$")
    public void eineKlientInGeloeschtWird() {
        try {
            this.repo.remove(null); //Should be unnecessary
        } catch (Exception e) {
            this.exception = e;
        }
    }

    @Dann("^wird eine MokiSecurityException geworfen$")
    public void wirdEineMokiSecurityExceptionGeworfen() {
        Assert.assertTrue(this.exception instanceof MokiSecurityException);
    }
}
