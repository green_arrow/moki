package at.moki.dbrepo.krankenpflegerin;

import at.cucumber.dbunit.bdd.glue.CucumberDbunitGlueDe;
import at.moki.dbrepo.AbsIntegrationTest;
import at.moki.dbrepo.entitymanager.producer.LocalMokiEntityManagerProducer;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;
import at.moki.exception.MokiSecurityException;
import at.moki.logger.MokiLoggerProducer;
import at.moki.security.MokiRolesAllowedInterceptor;
import at.moki.session.SessionModel;
import cucumber.api.java.de.Angenommen;
import cucumber.api.java.de.Dann;
import cucumber.api.java.de.Wenn;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import cucumber.runtime.arquillian.api.Glues;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.unitils.reflectionassert.ReflectionAssert;

import javax.inject.Inject;
import java.io.File;
import java.util.List;

/**
 * Created by Green Arrow on 17.01.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(CukeSpace.class)
@Features({"src/test/resources/features/krankenpflegerin.feature"})
@Glues({CucumberDbunitGlueDe.class, KrankenpflegerinRepoImplFeatureSystemIT.class})
public class KrankenpflegerinRepoImplFeatureSystemIT extends AbsIntegrationTest {

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClass(KrankenpflegerinRepoImpl.class)
                .addClass(MokiLoggerProducer.class)
                .addClass(LocalMokiEntityManagerProducer.class)
                .addClass(SessionModel.class)
                .addClass(MokiRolesAllowedInterceptor.class)
                .addAsManifestResource(new FileAsset(new File("src/main/resources/META-INF/beans.xml")), "beans.xml");
        System.out.println(jar.toString(true));
        return jar;
    }

    @Inject
    private KrankenpflegerinRepo repo;

    private Krankenpflegerin krankenpflegerin;
    private Exception exception;
    private List<Krankenpflegerin> krankenpflegerinList;

    @Wenn("^nach allen Krankenpflegerinnen gesucht wird$")
    public void nachAllenKrankenpflegerinnenGesuchtWird() {
        this.krankenpflegerinList = this.repo.findAll();
    }

    @Dann("^werden folgende Eintraege gefunden$")
    public void werdenFolgendeEintraegeGefunden(List<Krankenpflegerin> list) {
        ReflectionAssert.assertReflectionEquals(list, this.krankenpflegerinList);
    }

    @Wenn("^nach der Krankenpflegerin mit der ID (.*) gesucht wird$")
    public void nachDerKrankenpflegerinMitDerIDGesuchtWird(String id) {
        this.krankenpflegerin = this.repo.findByKlientInId(id);
    }

    @Dann("^wird folgender Eintrag gefunden$")
    public void wirdFolgenderEintragGefunden(List<KrankenpflegerinMock> krankenpflegerinMock) {
        ReflectionAssert.assertReflectionEquals(krankenpflegerinMock.get(0).mapToDomainmodel(), this.krankenpflegerin);
    }

    @Wenn("^die Krankenpflegerin mit folgenden Daten gespeichert wird$")
    public void dieKrankenpflegerinMitFolgendenDatenGespeichertWird(List<KrankenpflegerinMock> krankenpflegerinMock) {
        super.entityManager.getTransaction().begin();
        this.repo.merge(krankenpflegerinMock.get(0).mapToDomainmodel());
        super.entityManager.getTransaction().commit();
    }

    @Wenn("^die Krankenpflegerin mit der ID (.*) geloescht wird$")
    public void dieKrankenpflegerinMitDerIdGeloeschtWird(String id) {
        super.entityManager.getTransaction().begin();
        this.repo.remove(id);
        super.entityManager.getTransaction().commit();
    }

    @Angenommen("^der User ist eine Verwaltung$")
    public void derUserIstEineVerwaltung() {
        super.sessionModel.setMokiUser(super.genereateUserVerwaltung());
    }

    @Angenommen("^der User ist eine Krankenpflegerin$")
    public void derUserIstEineKrankenpflegerin() {
        super.sessionModel.setMokiUser(super.genereateUserKrankenpflegerin());
    }

    @Wenn("^die Suche nach allen Eintraegen ausgefuehrt wird$")
    public void dieSucheNachAllenEintraegenAusgefuehrtWird() {
        try {
            this.repo.findAll();
        } catch (Exception e) {
            this.exception = e;
        }
    }

    @Wenn("^eine Krankenpflegerin angelegt wird$")
    public void eineKrankenpflegerinAngelegtWird() {
        try {
            this.repo.merge(new Krankenpflegerin()); //Should be unnecessary
        } catch (Exception e) {
            this.exception = e;
        }
    }

    @Wenn("^eine Krankenpflegerin geloescht wird$")
    public void eineKrankenpflegerinGeloeschtWird() {
        try {
            this.repo.remove(null); //Should be unnecessary
        } catch (Exception e) {
            this.exception = e;
        }
    }

    @Dann("^wird eine MokiSecurityException geworfen$")
    public void wirdEineMokiSecurityExceptionGeworfen() {
        Assert.assertTrue(this.exception instanceof MokiSecurityException);
    }
}
