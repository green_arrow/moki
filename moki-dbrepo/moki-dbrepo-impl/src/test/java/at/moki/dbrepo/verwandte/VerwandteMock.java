package at.moki.dbrepo.verwandte;

import at.moki.dbrepo.MockBase;
import at.moki.domainmodel.verwandte.Verwandte;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Green Arrow on 12.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class VerwandteMock extends MockBase {

    private String id;
    private String klientInId;
    private String verwandte1Id;
    private String verwandte2Id;
    private String verwandte3Id;

    public Verwandte mapToDomainmodel() {
        Verwandte verwandte = new Verwandte();

        verwandte.setId(super.denull(getId()));
        verwandte.setKlientInId(super.denull(getKlientInId()));

        List<String> geschwisterList = new ArrayList<>();
        geschwisterList.add(super.denull(getVerwandte1Id()));
        geschwisterList.add(super.denull(getVerwandte2Id()));
        geschwisterList.add(super.denull(getVerwandte3Id()));

        verwandte.setVerwandteList(geschwisterList);

        return verwandte;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String getKlientInId() {
        return klientInId;
    }

    public void setKlientInId(String klientInId) {
        this.klientInId = klientInId;
    }

    private String getVerwandte1Id() {
        return verwandte1Id;
    }

    public void setVerwandte1Id(String verwandte1Id) {
        this.verwandte1Id = verwandte1Id;
    }

    private String getVerwandte2Id() {
        return verwandte2Id;
    }

    public void setVerwandte2Id(String verwandte2Id) {
        this.verwandte2Id = verwandte2Id;
    }

    private String getVerwandte3Id() {
        return verwandte3Id;
    }

    public void setVerwandte3Id(String verwandte3Id) {
        this.verwandte3Id = verwandte3Id;
    }
}
