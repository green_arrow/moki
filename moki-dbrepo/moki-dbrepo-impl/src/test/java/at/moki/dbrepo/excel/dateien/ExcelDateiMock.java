package at.moki.dbrepo.excel.dateien;

import at.moki.domainmodel.excel.dateien.ExcelDatei;

import java.nio.charset.StandardCharsets;

/**
 * @author Green Arrow
 * @date 31.08.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class ExcelDateiMock {

    private String id;
    private String dateiname;
    private String datei;
    private String taetigkeitsfeldId;

    public ExcelDatei mapToDomainmodel() {
        ExcelDatei to = new ExcelDatei();

        to.setId(getId());
        to.setDateiname(getDateiname());
        to.setDatei(getDatei().getBytes(StandardCharsets.UTF_8));
        to.setTaetigkeitsfeldId(getTaetigkeitsfeldId());

        return to;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDateiname() {
        return dateiname;
    }

    public void setDateiname(String dateiname) {
        this.dateiname = dateiname;
    }

    public String getDatei() {
        return datei;
    }

    public void setDatei(String datei) {
        this.datei = datei;
    }

    public String getTaetigkeitsfeldId() {
        return taetigkeitsfeldId;
    }

    public void setTaetigkeitsfeldId(String taetigkeitsfeldId) {
        this.taetigkeitsfeldId = taetigkeitsfeldId;
    }
}
