package at.moki.dbrepo;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Green Arrow on 17.02.2019.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class MockBase {

    private static final DateTimeFormatter DEFAULT_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd kk:mm:ss");

    protected String denull(String value) {
        return StringUtils.equals("[null]", value) ? null : value;
    }

    protected LocalDate toLocalDate(String datum) {
        return datum == null ? null : LocalDate.parse(datum, DEFAULT_DATE_FORMATTER);
    }

    protected LocalDateTime toLocalDateTime(String aenderungszeitpunkt) {
        return aenderungszeitpunkt == null ? null : LocalDateTime.parse(aenderungszeitpunkt, DATE_TIME_FORMATTER);
    }
}
