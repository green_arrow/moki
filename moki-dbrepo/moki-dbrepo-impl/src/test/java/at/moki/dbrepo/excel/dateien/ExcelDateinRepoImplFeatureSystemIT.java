package at.moki.dbrepo.excel.dateien;

import at.cucumber.dbunit.bdd.glue.CucumberDbunitGlueDe;
import at.moki.dbrepo.AbsIntegrationTest;
import at.moki.dbrepo.entitymanager.producer.LocalMokiEntityManagerProducer;
import at.moki.domainmodel.excel.dateien.ExcelDatei;
import at.moki.logger.MokiLoggerProducer;
import at.moki.security.MokiRolesAllowedInterceptor;
import at.moki.session.SessionModel;
import cucumber.api.java.de.Dann;
import cucumber.api.java.de.Wenn;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import cucumber.runtime.arquillian.api.Glues;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.runner.RunWith;
import org.unitils.reflectionassert.ReflectionAssert;

import javax.inject.Inject;
import java.io.File;
import java.util.List;

/**
 * @author Green Arrow
 * @date 28.08.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(CukeSpace.class)
@Features({"src/test/resources/features/exceldateien.feature"})
@Glues({CucumberDbunitGlueDe.class, ExcelDateinRepoImplFeatureSystemIT.class})
public class ExcelDateinRepoImplFeatureSystemIT extends AbsIntegrationTest {

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClass(ExcelDateinRepoImpl.class)
                .addClass(MokiLoggerProducer.class)
                .addClass(LocalMokiEntityManagerProducer.class)
                .addClass(SessionModel.class)
                .addClass(MokiRolesAllowedInterceptor.class)
                .addAsManifestResource(new FileAsset(new File("src/main/resources/META-INF/beans.xml")), "beans.xml");
        System.out.println(jar.toString(true));
        return jar;
    }

    @Inject
    private ExcelDateinRepo repo;

    ExcelDatei excelDatei;

    @Wenn("^die Suche mit der taetigkeitsfeldId (.*) durchgefuehrt wird$")
    public void dieSucheMitDerTaetigkeitsdeldIdDurchgefuehrtWird(String taetigkeitsfeldId) {
        this.excelDatei = this.repo.findByTaetigkeitsfeldId(taetigkeitsfeldId);
    }

    @Dann("^werden folgende Excel Dateien mit dem Inhalt gefunden$")
    public void werdenFolgendeExcelDateienMitDemInhaltGefunden(List<ExcelDateiMock> expectedList) {
        ReflectionAssert.assertReflectionEquals(expectedList.get(0).mapToDomainmodel(), this.excelDatei);
    }

    @Wenn("^eine Excel Datei mit folgendem Daten gespeichert wird$")
    public void eineExcelDateiMitFolgendemDatenGespeichertWird(List<ExcelDateiMock> list) {
        super.entityManager.getTransaction().begin();
        this.repo.merge(list.get(0).mapToDomainmodel());
        super.entityManager.getTransaction().commit();
    }
}
