package at.moki.dbrepo;

import at.moki.dbrepo.entitymanager.producer.MokiEntityManager;
import at.moki.domainmodel.user.MokiUser;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.session.SessionModel;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 * Created by Green Arrow on 20.01.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public abstract class AbsIntegrationTest {

    @Inject
    @MokiEntityManager
    protected EntityManager entityManager;

    @Inject
    protected SessionModel sessionModel;

    @Before
    public void setup() {
        this.sessionModel.setMokiUser(genereateUserAdministrator());
        this.entityManager.clear();
    }

    @After
    public void termiante() {
        if (this.entityManager != null && this.entityManager.getTransaction().isActive()) {
            this.entityManager.getTransaction().rollback();
        }
    }

    private MokiUser genereateUserAdministrator() {
        MokiUser mokiUser = new MokiUser();
        mokiUser.setUsername("Edith");
        mokiUser.setMokiUserRole(MokiUserRole.ADMINISTRATOR);
        return mokiUser;
    }

    protected MokiUser genereateUserKrankenpflegerin() {
        MokiUser mokiUser = new MokiUser();
        mokiUser.setUsername("Astrid");
        mokiUser.setMokiUserRole(MokiUserRole.KRANKENPFLEGERIN);
        return mokiUser;
    }

    protected MokiUser genereateUserVerwaltung() {
        MokiUser mokiUser = new MokiUser();
        mokiUser.setUsername("Anja");
        mokiUser.setMokiUserRole(MokiUserRole.VERWALTUNG);
        return mokiUser;
    }
}
