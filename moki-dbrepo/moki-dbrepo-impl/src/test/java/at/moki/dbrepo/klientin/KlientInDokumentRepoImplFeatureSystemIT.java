package at.moki.dbrepo.klientin;

import at.cucumber.dbunit.bdd.glue.CucumberDbunitGlueDe;
import at.moki.dbrepo.AbsIntegrationTest;
import at.moki.dbrepo.entitymanager.producer.LocalMokiEntityManagerProducer;
import at.moki.logger.MokiLoggerProducer;
import at.moki.security.MokiRolesAllowedInterceptor;
import at.moki.session.SessionModel;
import cucumber.api.java.de.Dann;
import cucumber.api.java.de.Wenn;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import cucumber.runtime.arquillian.api.Glues;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * Created by Green Arrow on 27.01.2019.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(CukeSpace.class)
@Features({"src/test/resources/features/klientInDokument.feature"})
@Glues({CucumberDbunitGlueDe.class, KlientInDokumentRepoImplFeatureSystemIT.class})
public class KlientInDokumentRepoImplFeatureSystemIT extends AbsIntegrationTest {

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClass(KlientInDokumentRepoImpl.class)
                .addClass(MokiLoggerProducer.class)
                .addClass(LocalMokiEntityManagerProducer.class)
                .addClass(SessionModel.class)
                .addClass(MokiRolesAllowedInterceptor.class)
                .addAsManifestResource(new FileAsset(new File("src/main/resources/META-INF/beans.xml")), "beans.xml");
        System.out.println(jar.toString(true));
        return jar;
    }

    @Inject
    private KlientInDokumentRepo repo;

    private byte[] dokumentInhalt;

    @Wenn("^die Suche mit der ID (.*) durchgefuehrt wird$")
    public void dieSucheMitDerIdDurchgefuehrtWird(String id) {
        this.dokumentInhalt = this.repo.findById(id);
    }

    @Dann("^wird folgendes Dokument mit dem Inhalt (.*) gefunden$")
    public void wirdFolgendesDokumentMitDemInhaltEdithInhaltGefunden(String inhalt) {
        Assert.assertEquals(true, Arrays.equals(inhalt.getBytes(StandardCharsets.UTF_8), this.dokumentInhalt));
    }

    @Wenn("ein Blob mit der Id (.*) und der KlientInZusaetzlicheDokumenteId (.*) und mit dem Blob Wert (.*) angelegt wird$")
    public void einBlobmitDerKlientInIdMitDemWertAngelegtWird(String id, String klientInZusaetzlicheDokumenteId, String blob) {
        super.entityManager.getTransaction().begin();
        this.repo.merge(id, blob.getBytes(StandardCharsets.UTF_8), klientInZusaetzlicheDokumenteId);
        super.entityManager.getTransaction().commit();
    }
}
