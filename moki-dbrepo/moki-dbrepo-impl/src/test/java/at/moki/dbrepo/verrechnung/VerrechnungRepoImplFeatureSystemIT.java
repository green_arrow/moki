package at.moki.dbrepo.verrechnung;

import at.cucumber.dbunit.bdd.glue.CucumberDbunitGlueDe;
import at.moki.dbrepo.AbsIntegrationTest;
import at.moki.dbrepo.entitymanager.producer.LocalMokiEntityManagerProducer;
import at.moki.domainmodel.verrechnung.Verrechnung;
import at.moki.logger.MokiLoggerProducer;
import at.moki.security.MokiRolesAllowedInterceptor;
import at.moki.session.SessionModel;
import cucumber.api.java.de.Angenommen;
import cucumber.api.java.de.Dann;
import cucumber.api.java.de.Wenn;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import cucumber.runtime.arquillian.api.Glues;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.unitils.reflectionassert.ReflectionAssert;

import javax.inject.Inject;
import java.io.File;
import java.util.List;

/**
 * @author Green Arrow
 * @date 21.07.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(CukeSpace.class)
@Features({"src/test/resources/features/verrechnung.feature"})
@Glues({CucumberDbunitGlueDe.class, VerrechnungRepoImplFeatureSystemIT.class})
public class VerrechnungRepoImplFeatureSystemIT extends AbsIntegrationTest {

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClass(VerrechnungRepoImpl.class)
                .addClass(MokiLoggerProducer.class)
                .addClass(LocalMokiEntityManagerProducer.class)
                .addClass(SessionModel.class)
                .addClass(MokiRolesAllowedInterceptor.class)
                .addAsManifestResource(new FileAsset(new File("src/main/resources/META-INF/beans.xml")), "beans.xml");
        System.out.println(jar.toString(true));
        return jar;
    }

    @Inject
    private VerrechnungRepo repo;

    private Verrechnung verrechnung;
    private List<Verrechnung> verrechnungList;

    @Wenn("^die Suche nach allen Eintraegen ausgefuehrt wird$")
    public void dieSucheNachAllenEintraegenAusgefuehrtWird() {
        this.verrechnungList = this.repo.findAll();
    }

    @Dann("werden folegende Eintraege gefunden")
    public void werdenFolegendeEintraegeGefunden(List<Verrechnung> list) {
        ReflectionAssert.assertReflectionEquals(list, this.verrechnungList);
    }

    @Angenommen("^es wird nach der Verrechnung mit der Id (.*) gesucht$")
    public void esWirdNachDerVerrechnungMitDerIdAadDbebBABDBBDGesucht(String verrechnungId) {
        this.verrechnung = this.repo.findByKlienInId(verrechnungId);
    }

    @Dann("^wird ein Eintrag mit der ID (.*) und der Bezeichnung (.*) gefunden$")
    public void wirdEinEintragMitDerIDUndDerBezeichnungGefunden(String id, String bezeichnung) {
        Assert.assertEquals(id, this.verrechnung.getId());
        Assert.assertEquals(bezeichnung, this.verrechnung.getBezeichnung());
    }

    @Wenn("^eine Verrechnug mit folgenden Daten gespeichert wird$")
    public void eineVerrechnugMitFolgendenDatenGespeichertWird(List<Verrechnung> verrechnungen) {
        super.entityManager.getTransaction().begin();
        this.repo.merge(verrechnungen.get(0));
        super.entityManager.getTransaction().commit();
    }
}
