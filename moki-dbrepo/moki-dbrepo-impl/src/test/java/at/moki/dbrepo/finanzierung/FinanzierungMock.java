package at.moki.dbrepo.finanzierung;

import at.moki.dbrepo.MockBase;
import at.moki.domainmodel.finanzierung.Finanzierung;
import at.moki.domainmodel.finanzierung.Institutions;

/**
 * Created by Green Arrow on 25.02.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General  License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General  License for more details.
 * You should have received a copy of the GNU General  License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
class FinanzierungMock extends MockBase {

    private String id;
    private String institution;
    private boolean kostenUebernahme;
    private boolean kostenSelbstbehalt;
    private String klientInId;

    Finanzierung mapToDomainmodel() {
        Finanzierung finanzierung = new Finanzierung();

        finanzierung.setId(super.denull(getId()));
        finanzierung.setInstitution(Institutions.valueOf(super.denull(getInstitution())));
        finanzierung.setKostenUebernahme(isKostenUebernahme());
        finanzierung.setKostenSelbstbehalt(isKostenSelbstbehalt());
        finanzierung.setKlientInId(super.denull(getKlientInId()));

        return finanzierung;
    }

    String getId() {
        return id;
    }

    void setId(String id) {
        this.id = id;
    }

    private String getInstitution() {
        return institution;
    }

    void setInstitution(String institution) {
        this.institution = institution;
    }

    private boolean isKostenUebernahme() {
        return kostenUebernahme;
    }

    void setKostenUebernahme(boolean kostenUebernahme) {
        this.kostenUebernahme = kostenUebernahme;
    }

    private boolean isKostenSelbstbehalt() {
        return kostenSelbstbehalt;
    }

    void setKostenSelbstbehalt(boolean kostenSelbstbehalt) {
        this.kostenSelbstbehalt = kostenSelbstbehalt;
    }

    private String getKlientInId() {
        return klientInId;
    }

    void setKlientInId(String klientInId) {
        this.klientInId = klientInId;
    }
}
