package at.moki.dbrepo.verwandte;

import at.cucumber.dbunit.bdd.glue.CucumberDbunitGlueDe;
import at.moki.dbrepo.AbsIntegrationTest;
import at.moki.dbrepo.entitymanager.producer.LocalMokiEntityManagerProducer;
import at.moki.domainmodel.verwandte.Verwandte;
import at.moki.exception.MokiSecurityException;
import at.moki.logger.MokiLoggerProducer;
import at.moki.security.MokiRolesAllowedInterceptor;
import at.moki.session.SessionModel;
import cucumber.api.java.de.Angenommen;
import cucumber.api.java.de.Dann;
import cucumber.api.java.de.Wenn;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import cucumber.runtime.arquillian.api.Glues;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.unitils.reflectionassert.ReflectionAssert;

import javax.inject.Inject;
import java.io.File;
import java.util.List;

/**
 * Created by Green Arrow on 12.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(CukeSpace.class)
@Features({"src/test/resources/features/verwandte.feature"})
@Glues({CucumberDbunitGlueDe.class, VerwandteRepoImplFeatureSystemIT.class})
public class VerwandteRepoImplFeatureSystemIT extends AbsIntegrationTest {

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClass(VerwandteRepoImpl.class)
                .addClass(MokiLoggerProducer.class)
                .addClass(LocalMokiEntityManagerProducer.class)
                .addClass(SessionModel.class)
                .addClass(MokiRolesAllowedInterceptor.class)
                .addAsManifestResource(new FileAsset(new File("src/main/resources/META-INF/beans.xml")), "beans.xml");
        System.out.println(jar.toString(true));
        return jar;
    }

    @Inject
    private VerwandteRepo repo;

    private Verwandte verwandte;
    private Exception exception;

    @Wenn("^die Suche mit der ID (.*) durchgefuehrt wird$")
    public void dieSucheMitDerIdDurchgefuehrtWird(String id) {
        this.verwandte = this.repo.findByKlientInId(id);
    }

    @Dann("^wird folgender Eintrag gefunden$")
    public void wirdFolgenderEintragGefunden(List<VerwandteMock> mocks) {
        ReflectionAssert.assertReflectionEquals(mocks.get(0).mapToDomainmodel(), this.verwandte);
    }

    @Wenn("^die Verwandte mit folgenden Daten gespeichert wird$")
    public void dieVerwandteMitFolgendenDatenGespeichertWird(List<VerwandteMock> mocks) {
        super.entityManager.getTransaction().begin();
        this.repo.merge(mocks.get(0).mapToDomainmodel());
        super.entityManager.getTransaction().commit();
    }

    @Wenn("^die Verwandte mit der ID (.*) geloescht wird$")
    public void dieVerwandteMitDerIDGeloeschtWird(String id) {
        super.entityManager.getTransaction().begin();
        this.repo.removeByKlientInId(id);
        super.entityManager.getTransaction().commit();
    }

    @Angenommen("^der User ist eine Krankenpflegerin$")
    public void derUserIstEineKrankenpflegerin() {
        super.sessionModel.setMokiUser(super.genereateUserKrankenpflegerin());
    }

    @Wenn("^eine Verwandte geloescht wird$")
    public void eineVerwandteGeloeschtWird() {
        try {
            this.repo.removeByKlientInId(null); //Should be unnecessary
        } catch (Exception e) {
            this.exception = e;
        }
    }

    @Dann("^wird eine MokiSecurityException geworfen$")
    public void wirdEineMokiSecurityExceptionGeworfen() {
        Assert.assertTrue(this.exception instanceof MokiSecurityException);
    }
}
