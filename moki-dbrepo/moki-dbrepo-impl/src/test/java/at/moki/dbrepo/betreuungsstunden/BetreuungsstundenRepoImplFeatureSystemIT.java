package at.moki.dbrepo.betreuungsstunden;

import at.cucumber.dbunit.bdd.glue.CucumberDbunitGlueDe;
import at.moki.dbrepo.AbsIntegrationTest;
import at.moki.dbrepo.entitymanager.producer.LocalMokiEntityManagerProducer;
import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.exception.MokiSecurityException;
import at.moki.logger.MokiLoggerProducer;
import at.moki.security.MokiRolesAllowedInterceptor;
import at.moki.session.SessionModel;
import cucumber.api.java.de.Angenommen;
import cucumber.api.java.de.Dann;
import cucumber.api.java.de.Wenn;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import cucumber.runtime.arquillian.api.Glues;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.unitils.reflectionassert.ReflectionAssert;

import javax.inject.Inject;
import java.io.File;
import java.util.List;

/**
 * Created by Green Arrow on 25.02.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(CukeSpace.class)
@Features({"src/test/resources/features/betreuungsstunden.feature"})
@Glues({CucumberDbunitGlueDe.class, BetreuungsstundenRepoImplFeatureSystemIT.class})
public class BetreuungsstundenRepoImplFeatureSystemIT extends AbsIntegrationTest {

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClass(BetreuungsstundenRepoImpl.class)
                .addClass(MokiLoggerProducer.class)
                .addClass(LocalMokiEntityManagerProducer.class)
                .addClass(SessionModel.class)
                .addClass(MokiRolesAllowedInterceptor.class)
                .addAsManifestResource(new FileAsset(new File("src/main/resources/META-INF/beans.xml")), "beans.xml");
        System.out.println(jar.toString(true));
        return jar;
    }

    @Inject
    private BetreuungsstundenRepo repo;

    private Exception exception;
    private List<Betreuungsstunden> betreuungsstundenList;

    @Wenn("^die Betreuungsstunden mit folgenden Daten gespeichert werden$")
    public void dieBetreuungsstundenMitFolgendenDatenGespeichertWerden(List<Betreuungsstunden> betreuungsstundenList) {
        super.entityManager.getTransaction().begin();
        this.repo.merge(betreuungsstundenList.get(0));
        super.entityManager.getTransaction().commit();
    }

    @Wenn("^die Betreuungsstunden mit der KLIENTIN_ID (.*) geloescht werden$")
    public void dieBetreuungsstundenMitDerKlientInIdGeloeschtWerden(String id) {
        super.entityManager.getTransaction().begin();
        this.repo.remove(id);
        super.entityManager.getTransaction().commit();
    }

    @Wenn("^die Suche mit der klientInId (.*) durchgefuehrt wird$")
    public void dieSucheMitDerKlientInIdBEBDCFFCEefDurchgefuehrtWird(String klientInId) {
        this.betreuungsstundenList = this.repo.findByKlientInId(klientInId);
    }

    @Dann("^werden folgende Betreuungsstunden gefunden$")
    public void werdenFolgendeBetreuungsstundenGefunden(List<Betreuungsstunden> list) {
        ReflectionAssert.assertReflectionEquals(list, this.betreuungsstundenList);
    }

    @Angenommen("^der User ist eine Krankenpflegerin$")
    public void derUserIstEineKrankenpflegerin() {
        super.sessionModel.setMokiUser(super.genereateUserKrankenpflegerin());
    }

    @Angenommen("^der User ist eine Verwaltung$")
    public void derUserIstEineVerwaltung() {
        super.sessionModel.setMokiUser(super.genereateUserVerwaltung());
    }

    @Wenn("^Betreuungsstunden gesucht werden$")
    public void betreuungsstundenGesuchtWerden() {
        try {
            this.repo.findByKlientInId(null); //Should be unnecessary
        } catch (Exception e) {
            this.exception = e;
        }
    }

    @Wenn("^Betreuungsstunden geloescht werden$")
    public void betreuungsstundenGeloeschtWerden() {
        try {
            this.repo.remove(null); //Should be unnecessary
        } catch (Exception e) {
            this.exception = e;
        }
    }

    @Dann("^wird eine MokiSecurityException geworfen$")
    public void wirdEineMokiSecurityExceptionGeworfen() {
        Assert.assertTrue(this.exception instanceof MokiSecurityException);
    }
}
