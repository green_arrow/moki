package at.moki.dbrepo.klientin;

import at.moki.dbrepo.MockBase;
import at.moki.domainmodel.klientin.KlientInZusaetzlicheDokumente;

/**
 * Created by Green Arrow on 27.01.2019.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class KlientInZusaetzlicheDokumenteMock extends MockBase {

    private String id;
    private String dateiname;
    private String erstellungszeitpunkt;
    private String klientInId;

    public KlientInZusaetzlicheDokumente mapToDomainmodel() {
        KlientInZusaetzlicheDokumente klientInZusaetzlicheDokumente = new KlientInZusaetzlicheDokumente();

        klientInZusaetzlicheDokumente.setId(super.denull(getId()));
        klientInZusaetzlicheDokumente.setDateiname(super.denull(getDateiname()));
        klientInZusaetzlicheDokumente.setErstellungszeitpunkt(super.toLocalDateTime(getErstellungszeitpunkt()));
        klientInZusaetzlicheDokumente.setKlientInId(super.denull(getKlientInId()));

        return klientInZusaetzlicheDokumente;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String getDateiname() {
        return dateiname;
    }

    public void setDateiname(String dateiname) {
        this.dateiname = dateiname;
    }

    private String getErstellungszeitpunkt() {
        return erstellungszeitpunkt;
    }

    public void setErstellungszeitpunkt(String erstellungszeitpunkt) {
        this.erstellungszeitpunkt = erstellungszeitpunkt;
    }

    private String getKlientInId() {
        return klientInId;
    }

    public void setKlientInId(String klientInId) {
        this.klientInId = klientInId;
    }
}
