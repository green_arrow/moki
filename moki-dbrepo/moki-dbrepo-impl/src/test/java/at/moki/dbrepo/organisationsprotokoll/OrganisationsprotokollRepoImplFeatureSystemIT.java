package at.moki.dbrepo.organisationsprotokoll;

import at.cucumber.dbunit.bdd.glue.CucumberDbunitGlueDe;
import at.moki.dbrepo.AbsIntegrationTest;
import at.moki.dbrepo.entitymanager.producer.LocalMokiEntityManagerProducer;
import at.moki.domainmodel.organisationsprotokoll.Organisationsprotokoll;
import at.moki.exception.MokiSecurityException;
import at.moki.logger.MokiLoggerProducer;
import at.moki.security.MokiRolesAllowedInterceptor;
import at.moki.session.SessionModel;
import cucumber.api.java.de.Angenommen;
import cucumber.api.java.de.Dann;
import cucumber.api.java.de.Wenn;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import cucumber.runtime.arquillian.api.Glues;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.unitils.reflectionassert.ReflectionAssert;

import javax.inject.Inject;
import java.io.File;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Green Arrow on 18.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(CukeSpace.class)
@Features({"src/test/resources/features/organisationsprotokoll.feature"})
@Glues({CucumberDbunitGlueDe.class, OrganisationsprotokollRepoImplFeatureSystemIT.class})
public class OrganisationsprotokollRepoImplFeatureSystemIT extends AbsIntegrationTest {

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClass(OrganisationsprotokollRepoImpl.class)
                .addClass(MokiLoggerProducer.class)
                .addClass(LocalMokiEntityManagerProducer.class)
                .addClass(SessionModel.class)
                .addClass(MokiRolesAllowedInterceptor.class)
                .addAsManifestResource(new FileAsset(new File("src/main/resources/META-INF/beans.xml")), "beans.xml");
        System.out.println(jar.toString(true));
        return jar;
    }

    @Inject
    private OrganisationsprotokollRepo repo;

    private List<Organisationsprotokoll> organisationsprotokollList;
    private Exception exception;

    @Wenn("^die Suche mit der ID (.*) geloescht wird$")
    public void dieSucheMitDerIdGeloeschtWird(String id) {
        this.organisationsprotokollList = this.repo.findByKlientInId(id);
    }

    @Dann("^werden folgende Eintraege in dieser Reihenfolge gefunden$")
    public void werdenFolgendeEintraegeInDieserReihenfolgeGefunden(List<OrganisationsprotokollMock> mocks) {

        // sort both lists to allow comparing

        List<Organisationsprotokoll> organisationsprotokollList = mocks.stream()
                .sorted(Comparator.comparing(OrganisationsprotokollMock::getId))
                .map(OrganisationsprotokollMock::mapToDomainmodel)
                .collect(Collectors.toList());

        Assert.assertEquals(2, organisationsprotokollList.size());

        this.organisationsprotokollList = this.organisationsprotokollList.stream()
                .sorted(Comparator.comparing(Organisationsprotokoll::getId))
                .collect(Collectors.toList());

        ReflectionAssert.assertReflectionEquals(organisationsprotokollList, this.organisationsprotokollList);
    }

    @Wenn("^das Organisationsprotokoll mit folgenden Daten gespeichert wird$")
    public void dasOrganisationsprotokollMitFolgendenDatenGespeichertWird(List<OrganisationsprotokollMock> mocks) {
        super.entityManager.getTransaction().begin();
        this.repo.merge(mocks.get(0).mapToDomainmodel());
        super.entityManager.getTransaction().commit();
    }

    @Wenn("^die Organisationsprotokollee mit der KlientinId (.*) geloescht werden$")
    public void dieOrganisationsprotokolleeMitDerKlientinIdGeloeschtWird(String id) {
        super.entityManager.getTransaction().begin();
        this.repo.removeByKlientInId(id);
        super.entityManager.getTransaction().commit();
    }

    @Angenommen("^der User ist eine Krankenpflegerin$")
    public void derUserIstEineKrankenpflegerin() {
        super.sessionModel.setMokiUser(super.genereateUserKrankenpflegerin());
    }

    @Wenn("^die Suche nach allen Eintraegen der KlientInId ausgefuehrt wird$")
    public void dieSucheNachAllenEintraegenDerKlientInIdAusgefuehrtWird() {
        try {
            this.repo.findByKlientInId(null); //Should be unnecessary
        } catch (Exception e) {
            this.exception = e;
        }
    }

    @Wenn("^ein Organisationsprotokoll angelegt wird$")
    public void einRganisationsprotokollAngelegtWird() {
        try {
            this.repo.merge(null); //Should be unnecessary
        } catch (Exception e) {
            this.exception = e;
        }
    }

    @Wenn("^ein Organisationsprotokoll geloescht wird$")
    public void einOrganisationsprotokollGeloeschtWird() {
        try {
            this.repo.removeByKlientInId(null); //Should be unnecessary
        } catch (Exception e) {
            this.exception = e;
        }
    }

    @Dann("^wird eine MokiSecurityException geworfen$")
    public void wirdEineMokiSecurityExceptionGeworfen() {
        Assert.assertTrue(this.exception instanceof MokiSecurityException);
    }
}
