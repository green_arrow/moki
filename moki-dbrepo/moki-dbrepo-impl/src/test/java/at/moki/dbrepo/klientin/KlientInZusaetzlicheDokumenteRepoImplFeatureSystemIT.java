package at.moki.dbrepo.klientin;

import at.cucumber.dbunit.bdd.glue.CucumberDbunitGlueDe;
import at.moki.dbrepo.AbsIntegrationTest;
import at.moki.dbrepo.entitymanager.producer.LocalMokiEntityManagerProducer;
import at.moki.domainmodel.klientin.KlientInZusaetzlicheDokumente;
import at.moki.logger.MokiLoggerProducer;
import at.moki.security.MokiRolesAllowedInterceptor;
import at.moki.session.SessionModel;
import cucumber.api.java.de.Dann;
import cucumber.api.java.de.Wenn;
import cucumber.runtime.arquillian.CukeSpace;
import cucumber.runtime.arquillian.api.Features;
import cucumber.runtime.arquillian.api.Glues;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.runner.RunWith;
import org.unitils.reflectionassert.ReflectionAssert;

import javax.inject.Inject;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Green Arrow on 27.01.2019.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(CukeSpace.class)
@Features({"src/test/resources/features/klientInZusaetzlicheDokumente.feature"})
@Glues({CucumberDbunitGlueDe.class, KlientInZusaetzlicheDokumenteRepoImplFeatureSystemIT.class})
public class KlientInZusaetzlicheDokumenteRepoImplFeatureSystemIT extends AbsIntegrationTest {

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClass(KlientInZusaetzlicheDokumenteRepoImpl.class)
                .addClass(MokiLoggerProducer.class)
                .addClass(LocalMokiEntityManagerProducer.class)
                .addClass(SessionModel.class)
                .addClass(MokiRolesAllowedInterceptor.class)
                .addAsManifestResource(new FileAsset(new File("src/main/resources/META-INF/beans.xml")), "beans.xml");
        System.out.println(jar.toString(true));
        return jar;
    }

    @Inject
    private KlientInZusaetzlicheDokumenteRepo repo;

    private List<KlientInZusaetzlicheDokumente> klientInZusaetzlicheDokumenteList;
    private byte[] dokumentInhalt;

    @Wenn("^die Suche mit der KlientIn ID (.*) durchgefuehrt wird$")
    public void dieSucheMitDerKlientInIdDurchgefuehrtWird(String id) {
        this.klientInZusaetzlicheDokumenteList = this.repo.findAllByKlientInId(id);
    }

    @Dann("^werden folgende Eintraege in dieser Reihenfolge gefunden$")
    public void werdenFolgendeEintraegeInDieserReihenfolgeGefunden(List<KlientInZusaetzlicheDokumenteMock> mocks) {
        List<KlientInZusaetzlicheDokumente> klientInZusaetzlicheDokumenteExpected = mocks.stream()
                .map(KlientInZusaetzlicheDokumenteMock::mapToDomainmodel)
                .collect(Collectors.toList());

        ReflectionAssert.assertReflectionEquals(klientInZusaetzlicheDokumenteExpected, this.klientInZusaetzlicheDokumenteList);
    }

    @Wenn("^ein Dokument mit folgenden Daten gespeichert wird$")
    public void einDokumentMitFolgendenDatenGespeichertWird(List<KlientInZusaetzlicheDokumenteMock> mocks) {
        super.entityManager.getTransaction().begin();
        this.repo.merge(mocks.get(0).mapToDomainmodel());
        super.entityManager.getTransaction().commit();
    }

    @Wenn("^die Dokumente mit der ID (.*) geloescht werden$")
    public void dieKlientinMitDerIDGeloeschtWird(String id) {
        super.entityManager.getTransaction().begin();
        this.repo.removeById(id);
        super.entityManager.getTransaction().commit();
    }

    @Wenn("^die Dokumente mit der KlientIn ID (.*) geloescht werden$")
    public void dieKlientinMitDerKlientInIDGeloeschtWird(String klientInId) {
        super.entityManager.getTransaction().begin();
        this.repo.removeAllByKlientInId(klientInId);
        super.entityManager.getTransaction().commit();
    }
}
