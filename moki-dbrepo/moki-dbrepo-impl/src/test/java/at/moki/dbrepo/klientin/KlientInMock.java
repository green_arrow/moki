package at.moki.dbrepo.klientin;

import at.moki.dbrepo.MockBase;
import at.moki.domainmodel.klientin.Geschlecht;
import at.moki.domainmodel.klientin.KlientIn;

/**
 * Created by Green Arrow on 10.02.2019.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class KlientInMock extends MockBase {

    private String id;
    private String klientenNummer;
    private String vorname;
    private String nachname;
    private String geschlecht;
    private String geburtsdatum;
    private String sterbedatum;
    private String taetigkeitsfeldId;
    private String taetigkeitsfeld;
    private String taetigkeitsfeldNummer;
    private String weitereKontaktdaten;
    private String verrechnungId;
    private String verrechnungNummer;
    private String spende;
    private String werkvertragsNummer;
    private String impacctGruppe;
    private String pflegestufe;
    private String sozialversicherungsnummer;
    private String sozialversicherungstraeger;
    private String diagnose;
    private String diagnoseschluessel;
    private boolean mutterVertreterin;
    private String mutterName;
    private String mutterGeburtsdatum;
    private String mutterTelefonnummer;
    private boolean vaterVertreter;
    private String vaterName;
    private String vaterGeburtsdatum;
    private String vaterTelefonnummer;
    private boolean sonstigeVertreter;
    private String sonstigeName;
    private String sonstigeGeburtsdatum;
    private String sonstigeTelefonnummer;
    private String strasse;
    private String mutterEmail;
    private String ort;
    private String vaterEmail;
    private boolean hauptwohnsitz;
    private String zuweisendeStelleKH;
    private String zuweisendeStelleKJH;
    private String zuweisendeStelleSonstige;
    private String kinderfacharztName;
    private String kinderfacharztTelefonnummer;
    private String betreuendeKrankenpflegerin;
    private String weitereKrankenpflegerinnen;
    private String betreuungsbeginn;
    private String betreuungsende;
    private String kmProHb;

    public KlientIn mapToDomainmodel() {
        KlientIn klientIn = new KlientIn();

        klientIn.setId(super.denull(super.denull(getId())));
        klientIn.setKlientenNummer(super.denull(getKlientenNummer()));
        klientIn.setVorname(super.denull(getVorname()));
        klientIn.setNachname(super.denull(getNachname()));
        klientIn.setGeschlecht(Geschlecht.valueOf(super.denull(getGeschlecht())));
        klientIn.setGeburtsdatum(super.toLocalDate(super.denull(getGeburtsdatum())));
        klientIn.setSterbedatum(super.toLocalDate(super.denull(getSterbedatum())));
        klientIn.setTaetigkeitsfeldId(super.denull(getTaetigkeitsfeldId()));
        klientIn.setTaetigkeitsfeld(super.denull(getTaetigkeitsfeld()));
        klientIn.setTaetigkeitsfeldNummer(super.denull(getTaetigkeitsfeldNummer()));
        klientIn.setWeitereKontaktdaten(super.denull(getWeitereKontaktdaten()));
        klientIn.setVerrechnungId(super.denull(getVerrechnungId()));
        klientIn.setVerrechnungNummer(super.denull(getVerrechnungNummer()));
        klientIn.setSpende(super.denull(getSpende()));
        klientIn.setWerkvertragsNummer(super.denull(getWerkvertragsNummer()));
        klientIn.setImpacctGruppe(super.denull(getImpacctGruppe()));
        klientIn.setPflegestufe(super.denull(getPflegestufe()));
        klientIn.setSozialversicherungsnummer(super.denull(getSozialversicherungsnummer()));
        klientIn.setSozialversicherungstraeger(super.denull(getSozialversicherungstraeger()));
        klientIn.setDiagnose(super.denull(getDiagnose()));
        klientIn.setDiagnoseschluessel(super.denull(getDiagnoseschluessel()));
        klientIn.setMutterVertreterin(isMutterVertreterin());
        klientIn.setMutterName(super.denull(getMutterName()));
        klientIn.setMutterGeburtsdatum(super.toLocalDate(super.denull(getMutterGeburtsdatum())));
        klientIn.setMutterTelefonnummer(super.denull(getMutterTelefonnummer()));
        klientIn.setVaterVertreter(isVaterVertreter());
        klientIn.setVaterName(super.denull(getVaterName()));
        klientIn.setVaterGeburtsdatum(super.toLocalDate(super.denull(getVaterGeburtsdatum())));
        klientIn.setVaterTelefonnummer(super.denull(getVaterTelefonnummer()));
        klientIn.setSonstigeVertreter(isSonstigeVertreter());
        klientIn.setSonstigeName(super.denull(getSonstigeName()));
        klientIn.setSonstigeGeburtsdatum(super.toLocalDate(super.denull(getSonstigeGeburtsdatum())));
        klientIn.setSonstigeTelefonnummer(super.denull(getSonstigeTelefonnummer()));
        klientIn.setStrasse(super.denull(getStrasse()));
        klientIn.setMutterEmail(super.denull(getMutterEmail()));
        klientIn.setOrt(super.denull(getOrt()));
        klientIn.setVaterEmail(super.denull(getVaterEmail()));
        klientIn.setHauptwohnsitz(isHauptwohnsitz());
        klientIn.setZuweisendeStelleKH(super.denull(getZuweisendeStelleKH()));
        klientIn.setZuweisendeStelleKJH(super.denull(getZuweisendeStelleKJH()));
        klientIn.setZuweisendeStelleSonstige(super.denull(getZuweisendeStelleSonstige()));
        klientIn.setKinderfacharztName(super.denull(getKinderfacharztName()));
        klientIn.setKinderfacharztTelefonnummer(super.denull(getKinderfacharztTelefonnummer()));
        klientIn.setBetreuendeKrankenpflegerin(super.denull(getBetreuendeKrankenpflegerin()));
        klientIn.setWeitereKrankenpflegerinnen(super.denull(getWeitereKrankenpflegerinnen()));
        klientIn.setBetreuungsbeginn(super.toLocalDate(super.denull(getBetreuungsbeginn())));
        klientIn.setBetreuungsende(super.toLocalDate(super.denull(getBetreuungsende())));
        klientIn.setKmProHb(super.denull(getKmProHb()));

        return klientIn;
    }

    public String getId() {
        return id;
    }

    public String getKlientenNummer() {
        return klientenNummer;
    }

    public String getVorname() {
        return vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public String getGeschlecht() {
        return geschlecht;
    }

    public String getGeburtsdatum() {
        return geburtsdatum;
    }

    public String getSterbedatum() {
        return sterbedatum;
    }

    public String getTaetigkeitsfeldId() {
        return taetigkeitsfeldId;
    }

    public String getTaetigkeitsfeld() {
        return taetigkeitsfeld;
    }

    public String getTaetigkeitsfeldNummer() {
        return taetigkeitsfeldNummer;
    }

    public String getWeitereKontaktdaten() {
        return weitereKontaktdaten;
    }

    public String getVerrechnungId() {
        return verrechnungId;
    }

    public String getVerrechnungNummer() {
        return verrechnungNummer;
    }

    public String getSpende() {
        return spende;
    }

    public String getWerkvertragsNummer() {
        return werkvertragsNummer;
    }

    public String getImpacctGruppe() {
        return impacctGruppe;
    }

    public String getPflegestufe() {
        return pflegestufe;
    }

    public String getSozialversicherungsnummer() {
        return sozialversicherungsnummer;
    }

    public String getSozialversicherungstraeger() {
        return sozialversicherungstraeger;
    }

    public String getDiagnose() {
        return diagnose;
    }

    public String getDiagnoseschluessel() {
        return diagnoseschluessel;
    }

    public boolean isMutterVertreterin() {
        return mutterVertreterin;
    }

    public String getMutterName() {
        return mutterName;
    }

    public String getMutterGeburtsdatum() {
        return mutterGeburtsdatum;
    }

    public String getMutterTelefonnummer() {
        return mutterTelefonnummer;
    }

    public boolean isVaterVertreter() {
        return vaterVertreter;
    }

    public String getVaterName() {
        return vaterName;
    }

    public String getVaterGeburtsdatum() {
        return vaterGeburtsdatum;
    }

    public String getVaterTelefonnummer() {
        return vaterTelefonnummer;
    }

    public boolean isSonstigeVertreter() {
        return sonstigeVertreter;
    }

    public String getSonstigeName() {
        return sonstigeName;
    }

    public String getSonstigeGeburtsdatum() {
        return sonstigeGeburtsdatum;
    }

    public String getSonstigeTelefonnummer() {
        return sonstigeTelefonnummer;
    }

    public String getStrasse() {
        return strasse;
    }

    public String getMutterEmail() {
        return mutterEmail;
    }

    public String getOrt() {
        return ort;
    }

    public String getVaterEmail() {
        return vaterEmail;
    }

    public boolean isHauptwohnsitz() {
        return hauptwohnsitz;
    }

    public String getZuweisendeStelleKH() {
        return zuweisendeStelleKH;
    }

    public String getZuweisendeStelleKJH() {
        return zuweisendeStelleKJH;
    }

    public String getZuweisendeStelleSonstige() {
        return zuweisendeStelleSonstige;
    }

    public String getKinderfacharztName() {
        return kinderfacharztName;
    }

    public String getKinderfacharztTelefonnummer() {
        return kinderfacharztTelefonnummer;
    }

    public String getBetreuendeKrankenpflegerin() {
        return betreuendeKrankenpflegerin;
    }

    public String getWeitereKrankenpflegerinnen() {
        return weitereKrankenpflegerinnen;
    }

    public String getBetreuungsbeginn() {
        return betreuungsbeginn;
    }

    public String getBetreuungsende() {
        return betreuungsende;
    }

    public String getKmProHb() {
        return kmProHb;
    }
}
