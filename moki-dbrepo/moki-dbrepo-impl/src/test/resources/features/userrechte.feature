# language: de

Funktionalität: dieses feature file ist ein test fuer dbunit

  Grundlage:
    Angenommen der Treiber ist org.mariadb.jdbc.Driver
    Und die URL ist jdbc:mysql://localhost:3306/mokidb_integration
    Und der Username ist moki
    Und das Passwort ist 288f3d45-e741-4a4c-ace4-13841f1ea50c
    Und die Datenbank befindet sich in folgendem Zustand src/test/resources/cleanAll.xml
    Und in der Tabelle USERRECHTE sind folgende Daten vorhanden
      | ID                                   | BEZEICHNUNG      |
      | 175bf557-da80-49f3-b9f2-787aa57af9e1 | ADMINISTRATOR    |
      | 175bf557-da80-49f3-b9f2-787aa57af9e2 | KRANKENPFLEGERIN |
      | 175bf557-da80-49f3-b9f2-787aa57af9e3 | VERWALTUNG       |

  Szenario: Es soll ueberprueft werden ob alle Userrechte geladen werden koennen
    Wenn nach allen Userrechten gesucht wird
    Dann werden folgende Userrechte geladen
      | id                                   | mokiUserRole     |
      | 175bf557-da80-49f3-b9f2-787aa57af9e1 | ADMINISTRATOR    |
      | 175bf557-da80-49f3-b9f2-787aa57af9e2 | KRANKENPFLEGERIN |
      | 175bf557-da80-49f3-b9f2-787aa57af9e3 | VERWALTUNG       |

  Szenario: Es soll ueberprueft werden ob die Suche nach einer bestimmten Userrechte Id funktioniert
    Wenn nach der Userrechte Id 175bf557-da80-49f3-b9f2-787aa57af9e1 gesucht wird
    Dann wird folgender Eintrag gefunden
      | id                                   | mokiUserRole  |
      | 175bf557-da80-49f3-b9f2-787aa57af9e1 | ADMINISTRATOR |

  Szenario: Es soll ueberprueft werden ob die Suche nach einer bestimmten Bezeichnung funktioniert
    Wenn nach der Userrechte Bezeichnung ADMINISTRATOR gesucht wird
    Dann wird ein Eintrag mit der ID 175bf557-da80-49f3-b9f2-787aa57af9e1 und der Bezeichnung ADMINISTRATOR gefunden

    Wenn nach der Userrechte Bezeichnung KRANKENPFLEGERIN gesucht wird
    Dann wird ein Eintrag mit der ID 175bf557-da80-49f3-b9f2-787aa57af9e2 und der Bezeichnung KRANKENPFLEGERIN gefunden

    Wenn nach der Userrechte Bezeichnung VERWALTUNG gesucht wird
    Dann wird ein Eintrag mit der ID 175bf557-da80-49f3-b9f2-787aa57af9e3 und der Bezeichnung VERWALTUNG gefunden


  Szenario: Eine Krankenpflegerin darf nicht nach allen Eintraegen der Userrechte suchen
    Angenommen der User ist eine Krankenpflegerin
    Wenn die Suche nach allen Eintraegen ausgefuehrt wird
    Dann wird eine MokiSecurityException geworfen

  Szenario: Eine Krankenpflegerin darf nicht nach der Bezeichnung der Userrechte suchen
    Angenommen der User ist eine Krankenpflegerin
    Wenn die Suche nach der Bezeichnung ausgefuehrt wird
    Dann wird eine MokiSecurityException geworfen
