# language: de

Funktionalität: dieses feature file ist ein test fuer dbunit

  Grundlage:
    Angenommen der Treiber ist org.mariadb.jdbc.Driver
    Und die URL ist jdbc:mysql://localhost:3306/mokidb_integration
    Und der Username ist moki
    Und das Passwort ist 288f3d45-e741-4a4c-ace4-13841f1ea50c
    Und die Datenbank befindet sich in folgendem Zustand src/test/resources/cleanAll.xml
    Und in der Tabelle VERRECHNUNG sind folgende Daten vorhanden
      | ID                                   | BEZEICHNUNG |
      | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | EPP-NR      |
    Und in der Tabelle TAETIGKEITSFELD sind folgende Daten vorhanden
      | ID                                   | BEZEICHNUNG |
      | a23ba0da-c5af-4f59-86d9-3b432de48b7e | TS          |
    Und in der Tabelle KLIENTIN sind folgende Daten vorhanden
      | ID                                   | KLIENTENNUMMER | VORNAME | NACHNAME | GESCHLECHT | GEBURTSDATUM | TAETIGKEITSFELD_ID                   | VERRECHNUNG_ID                       | VERRECHNUNG_NUMMER | SOZIALVERSICHERUNGSNUMMER | SOZIALVERSICHERUNGSTRAEGER | MUTTER_VERTRETERIN | VATER_VERTRETER | SONSTIGE_VERTRETER | HAUPTWOHNSITZ |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 | 123456         | Anja    | K.       | W          | 1992-07-31   | a23ba0da-c5af-4f59-86d9-3b432de48b7e | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | 484                | 4210010192                | NOEGKK                     | N                  | N               | N                  | N             |

  Szenario: Es soll ueberprueft werden ob alle Finanzierungen gelesen werden koennen
    Angenommen in der Tabelle FINANZIERUNG sind folgende Daten vorhanden
      | ID                                   | INSTITUTION        | KOSTEN | SELBSTBEHALT | KLIENTIN_ID                          |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe36 | Land NÖ            | J      | N            | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe37 | Muki               | N      | J            | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe38 | Sozialversicherung | J      | N            | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe39 | KiB                | N      | J            | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |
    Wenn die Suche nach allen Eintraegen ausgefuehrt wird
    Dann werden folgende Eintraege gefunden
      | id                                   | institution        | kostenUebernahme | kostenSelbstbehalt | klientInId                           |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe36 | LAND               | true             | false              | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe37 | MUKI               | false            | true               | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe38 | SOZIALVERSICHERUNG | true             | false              | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe39 | KIB                | false            | true               | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |

  Szenario: Es soll ueberprueft werden ob eine Finanzierung gelesen werden kann
    Angenommen in der Tabelle FINANZIERUNG sind folgende Daten vorhanden
      | ID                                   | INSTITUTION        | KOSTEN | SELBSTBEHALT | KLIENTIN_ID                          |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe36 | Land NÖ            | J      | N            | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe37 | Muki               | N      | J            | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe38 | Sozialversicherung | J      | N            | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe39 | KiB                | N      | J            | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |

    Wenn die Suche mit der KlientIn ID b9dbffef-63f2-4afb-baec-bc6e9ad64d38 durchgefuehrt wird
    Dann werden folgende Eintraege gefunden
      | id                                   | institution        | kostenUebernahme | kostenSelbstbehalt | klientInId                           |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe36 | LAND               | true             | false              | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe37 | MUKI               | false            | true               | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe38 | SOZIALVERSICHERUNG | true             | false              | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe39 | KIB                | false            | true               | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |


  Szenario: Es soll ueberprueft werden ob eine Finanzierung angelegt werden kann
    Wenn die Finanzierung mit folgenden Daten gespeichert wird
      | id                                   | institution | kostenUebernahme | kostenSelbstbehalt | klientInId                           |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe36 | LAND        | true             | false              | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |
    Dann ist die Tabelle FINANZIERUNG in folgendem Zustand
      | ID                                   | INSTITUTION | KOSTEN | SELBSTBEHALT | KLIENTIN_ID                          |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe36 | Land NÖ     | J      | N            | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |


  Szenario: Es soll ueberprueft werden ob eine Finanzierung bearbeitet werden kann
    Angenommen in der Tabelle FINANZIERUNG sind folgende Daten vorhanden
      | ID                                   | INSTITUTION | KOSTEN | SELBSTBEHALT | KLIENTIN_ID                          |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe36 | Land NÖ     | J      | N            | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |
    Wenn die Finanzierung mit folgenden Daten gespeichert wird
      | id                                   | institution | kostenUebernahme | kostenSelbstbehalt | klientInId                           |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe36 | MUKI        | true             | false              | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |
    Dann ist die Tabelle FINANZIERUNG in folgendem Zustand
      | ID                                   | INSTITUTION | KOSTEN | SELBSTBEHALT | KLIENTIN_ID                          |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe36 | Muki        | J      | N            | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |


  Szenario: Es soll ueberprueft werden ob eine Finanzierung geloescht werden kann
    Angenommen in der Tabelle FINANZIERUNG sind folgende Daten vorhanden
      | ID                                   | INSTITUTION | KOSTEN | SELBSTBEHALT | KLIENTIN_ID                          |
      | 479f5623-83d5-4c7c-a6ed-515034c6fe36 | Land NÖ     | J      | N            | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 |
    Wenn die Finanzierung mit der KlientIn ID b9dbffef-63f2-4afb-baec-bc6e9ad64d38 geloescht wird
    Dann ist die Tabelle FINANZIERUNG in folgendem Zustand
      | ID | INSTITUTION | KOSTEN | SELBSTBEHALT | KLIENTIN_ID |


  Szenario: Eine Krankenpflegerin darf keine Finanzierung löschen
    Angenommen der User ist eine Krankenpflegerin
    Wenn eine Finanzierung geloescht wird
    Dann wird eine MokiSecurityException geworfen