# language: de

Funktionalität: dieses feature file ist ein test fuer dbunit

  Grundlage:
    Angenommen der Treiber ist org.mariadb.jdbc.Driver
    Und die URL ist jdbc:mysql://localhost:3306/mokidb_integration
    Und der Username ist moki
    Und das Passwort ist 288f3d45-e741-4a4c-ace4-13841f1ea50c
    Und die Datenbank befindet sich in folgendem Zustand src/test/resources/cleanAll.xml
    Und in der Tabelle VERRECHNUNG sind folgende Daten vorhanden
      | ID                                   | BEZEICHNUNG |
      | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | EPP-NR      |
    Und in der Tabelle TAETIGKEITSFELD sind folgende Daten vorhanden
      | ID                                   | BEZEICHNUNG |
      | a23ba0da-c5af-4f59-86d9-3b432de48b7e | TS          |
    Und in der Tabelle KLIENTIN sind folgende Daten vorhanden
      | ID                                   | KLIENTENNUMMER | VORNAME | NACHNAME | GESCHLECHT | GEBURTSDATUM | TAETIGKEITSFELD_ID                   | VERRECHNUNG_ID                       | VERRECHNUNG_NUMMER | SOZIALVERSICHERUNGSNUMMER | SOZIALVERSICHERUNGSTRAEGER | MUTTER_VERTRETERIN | VATER_VERTRETER | SONSTIGE_VERTRETER | HAUPTWOHNSITZ |
      | dab2d986-489f-467b-aad1-5af63311d25d | 123456         | Anja    | K.       | W          | 1992-07-31   | a23ba0da-c5af-4f59-86d9-3b432de48b7e | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | 484                | 4210010192                | NOEGKK                     | N                  | N               | N                  | N             |


  Szenario: Es soll ueberprueft werden ob alle Betreuungsstunden gelsen werden können
    Angenommen in der Tabelle BETREUUNGSSTUNDEN sind folgende Daten vorhanden
      | ID                                   | JAHR | STUNDEN | KLIENTIN_ID                          |
      | 175bf557-da80-49f3-b9f2-787aaaaaf9e0 | 2017 | 6000    | dab2d986-489f-467b-aad1-5af63311d25d |
      | ca266202-e7b5-49d4-bc49-99eb722b53b5 | 2019 | 6000    | dab2d986-489f-467b-aad1-5af63311d25d |
      | 8e4439f5-9839-48c2-bacb-f5bdf2cd4e2d | 2020 | 6000    | dab2d986-489f-467b-aad1-5af63311d25d |
    Wenn die Suche mit der klientInId dab2d986-489f-467b-aad1-5af63311d25d durchgefuehrt wird
    Dann werden folgende Betreuungsstunden gefunden
      | id                                   | jahr | stunden | klientInId                           |
      | 175bf557-da80-49f3-b9f2-787aaaaaf9e0 | 2017 | 6000    | dab2d986-489f-467b-aad1-5af63311d25d |
      | ca266202-e7b5-49d4-bc49-99eb722b53b5 | 2019 | 6000    | dab2d986-489f-467b-aad1-5af63311d25d |
      | 8e4439f5-9839-48c2-bacb-f5bdf2cd4e2d | 2020 | 6000    | dab2d986-489f-467b-aad1-5af63311d25d |


  Szenario: Es soll ueberprueft werden ob Betreuungsstunden angelegt werden können
    Wenn die Betreuungsstunden mit folgenden Daten gespeichert werden
      | id                                   | jahr | stunden | klientInId                           |
      | 175bf557-da80-49f3-b9f2-787aaaaaf9e0 | 2018 | 6000    | dab2d986-489f-467b-aad1-5af63311d25d |
    Dann ist die Tabelle BETREUUNGSSTUNDEN in folgendem Zustand
      | ID                                   | JAHR | STUNDEN | KLIENTIN_ID                          |
      | 175bf557-da80-49f3-b9f2-787aaaaaf9e0 | 2018 | 6000    | dab2d986-489f-467b-aad1-5af63311d25d |


  Szenario: Es soll ueberprueft werden ob Betreuungsstunden bearbeitet werden koennen
    Angenommen in der Tabelle BETREUUNGSSTUNDEN sind folgende Daten vorhanden
      | ID                                   | JAHR | STUNDEN | KLIENTIN_ID                          |
      | 175bf557-da80-49f3-b9f2-787aaaaaf9e0 | 2018 | 6000    | dab2d986-489f-467b-aad1-5af63311d25d |
    Wenn die Betreuungsstunden mit folgenden Daten gespeichert werden
      | id                                   | jahr | stunden | klientInId                           |
      | 175bf557-da80-49f3-b9f2-787aaaaaf9e0 | 2019 | 7000    | dab2d986-489f-467b-aad1-5af63311d25d |
    Dann ist die Tabelle BETREUUNGSSTUNDEN in folgendem Zustand
      | ID                                   | JAHR | STUNDEN | KLIENTIN_ID                          |
      | 175bf557-da80-49f3-b9f2-787aaaaaf9e0 | 2019 | 7000    | dab2d986-489f-467b-aad1-5af63311d25d |


  Szenario: Es soll ueberprueft werden ob Betreuungsstunden zu einer KlientIn geloescht werden koennen
    Angenommen in der Tabelle BETREUUNGSSTUNDEN sind folgende Daten vorhanden
      | ID                                   | JAHR | STUNDEN | KLIENTIN_ID                          |
      | 175bf557-da80-49f3-b9f2-787aaaaaf9e0 | 2018 | 6000    | dab2d986-489f-467b-aad1-5af63311d25d |
    Wenn die Betreuungsstunden mit der KLIENTIN_ID dab2d986-489f-467b-aad1-5af63311d25d geloescht werden
    Dann ist die Tabelle BETREUUNGSSTUNDEN in folgendem Zustand
      | ID | JAHR | STUNDEN | KLIENTIN_ID |


  Szenario: Eine Krankenpflegerin darf keine Betreuungsstunden loeschen
    Angenommen der User ist eine Krankenpflegerin
    Wenn Betreuungsstunden geloescht werden
    Dann wird eine MokiSecurityException geworfen