# language: de

Funktionalität: dieses feature file ist ein test fuer dbunit

  Grundlage:
    Angenommen der Treiber ist org.mariadb.jdbc.Driver
    Und die URL ist jdbc:mysql://localhost:3306/mokidb_integration
    Und der Username ist moki
    Und das Passwort ist 288f3d45-e741-4a4c-ace4-13841f1ea50c
    Und die Datenbank befindet sich in folgendem Zustand src/test/resources/cleanAll.xml
    Und in der Tabelle USERRECHTE sind folgende Daten vorhanden
      | ID                                   | BEZEICHNUNG      |
      | 175bf557-da80-49f3-b9f2-787aa57af9e1 | ADMINISTRATOR    |
      | 175bf557-da80-49f3-b9f2-787aa57af9e2 | KRANKENPFLEGERIN |
      | 175bf557-da80-49f3-b9f2-787aa57af9e3 | VERWALTUNG       |

  Szenario: Es soll ueberprueft werden ob alle User geladen werden koennen
    Angenommen in der Tabelle USER sind folgende Daten vorhanden
      | ID                                   | USERNAME | PASSWORD | GESPERRT | SPERRUNG_COUNTER | USERRECHTE_ID                        | KRANKENPFLEGERIN_ID |
      | efb35004-ae47-4f0c-ac44-d597996f621c | Anja     | s3cret   | N        | 0                | 175bf557-da80-49f3-b9f2-787aa57af9e2 | [null]              |
      | 379cab2d-46ca-4345-9346-fb4f7f250a90 | Edith    | 1234     | N        | 0                | 175bf557-da80-49f3-b9f2-787aa57af9e2 | [null]              |
    Wenn nach allen Usern gesucht wird
    Dann werden folgende User geladen
      | id                                   | username | password | gesperrt | sperrungCounter | userrechteId                         |
      | efb35004-ae47-4f0c-ac44-d597996f621c | Anja     | s3cret   | false    | 0               | 175bf557-da80-49f3-b9f2-787aa57af9e2 |
      | 379cab2d-46ca-4345-9346-fb4f7f250a90 | Edith    | 1234     | false    | 0               | 175bf557-da80-49f3-b9f2-787aa57af9e2 |

  Szenario: Es soll ueberprueft werden ob ein bestimmter User geladen werden kann
    Angenommen in der Tabelle USER sind folgende Daten vorhanden
      | ID                                   | USERNAME | PASSWORD | GESPERRT | SPERRUNG_COUNTER | USERRECHTE_ID                        | KRANKENPFLEGERIN_ID |
      | efb35004-ae47-4f0c-ac44-d597996f621c | Anja     | s3cret   | N        | 0                | 175bf557-da80-49f3-b9f2-787aa57af9e2 | [null]              |
      | e1983053-fc97-427a-8511-9d1b5ab562e3 | Anja     | s3cret   | J        | 3                | 175bf557-da80-49f3-b9f2-787aa57af9e2 | [null]              |
      | 379cab2d-46ca-4345-9346-fb4f7f250a90 | Edith    | 1234     | N        | 0                | 175bf557-da80-49f3-b9f2-787aa57af9e2 | [null]              |
    Wenn nach der Userin mit dem Usernamen Anja gesucht wird
    Dann wird folgende Userin gefunden
      | id                                   | username | password | gesperrt | sperrungCounter | userrechteId                         |
      | e1983053-fc97-427a-8511-9d1b5ab562e3 | Anja     | s3cret   | true     | 3               | 175bf557-da80-49f3-b9f2-787aa57af9e2 |
      | efb35004-ae47-4f0c-ac44-d597996f621c | Anja     | s3cret   | false    | 0               | 175bf557-da80-49f3-b9f2-787aa57af9e2 |


  Szenario: Es soll ueberprueft werden ob ein neuer User angelegt werden kann
    Wenn der User mit folgenden Daten gespeichert wird
      | id                                   | username | password | gesperrt | mokiUserRole     | sperrungCounter | userrechteId                         | krankenpflegerinId |
      | 175bf557-da80-49f3-b9f2-787aa58af9f2 | Anja     | s3cret   | false    | KRANKENPFLEGERIN | 0               | 175bf557-da80-49f3-b9f2-787aa57af9e2 | [null]             |
    Dann ist die Tabelle User in folgendem Zustand
      | ID                                   | USERNAME | PASSWORD | GESPERRT | SPERRUNG_COUNTER | USERRECHTE_ID                        | KRANKENPFLEGERIN_ID |
      | 175bf557-da80-49f3-b9f2-787aa58af9f2 | Anja     | s3cret   | N        | 0                | 175bf557-da80-49f3-b9f2-787aa57af9e2 | [null]              |

  Szenario: Es soll ueberprueft werden ob ein User bearbeitet werden kann
    Angenommen in der Tabelle USER sind folgende Daten vorhanden
      | ID                                   | USERNAME | PASSWORD | GESPERRT | SPERRUNG_COUNTER | USERRECHTE_ID                        | KRANKENPFLEGERIN_ID |
      | 175bf557-da80-49f3-b9f2-787aa58af9f2 | Anja     | s3cret   | N        | [null]           | 175bf557-da80-49f3-b9f2-787aa57af9e2 | [null]              |
    Wenn der User mit folgenden Daten gespeichert wird
      | id                                   | username | password | gesperrt | mokiUserRole     | sperrungCounter | userrechteId                         | krankenpflegerinId |
      | 175bf557-da80-49f3-b9f2-787aa58af9f2 | Edith    | 1234     | false    | KRANKENPFLEGERIN | 0               | 175bf557-da80-49f3-b9f2-787aa57af9e2 | [null]             |
    Dann ist die Tabelle User in folgendem Zustand
      | ID                                   | USERNAME | PASSWORD | GESPERRT | SPERRUNG_COUNTER | USERRECHTE_ID                        | KRANKENPFLEGERIN_ID |
      | 175bf557-da80-49f3-b9f2-787aa58af9f2 | Edith    | 1234     | N        | 0                | 175bf557-da80-49f3-b9f2-787aa57af9e2 | [null]              |

  Szenario: Es soll ueberprueft werden ob ein User geloescht werden kann
    Angenommen in der Tabelle USER sind folgende Daten vorhanden
      | ID                                   | USERNAME | PASSWORD | GESPERRT | SPERRUNG_COUNTER | USERRECHTE_ID                        | KRANKENPFLEGERIN_ID |
      | 175bf557-da80-49f3-b9f2-787aa58af9f2 | Anja     | s3cret   | N        | [null]           | 175bf557-da80-49f3-b9f2-787aa57af9e2 | [null]              |
    Wenn der User mit der ID 175bf557-da80-49f3-b9f2-787aa58af9f2 geloescht wird
    Dann ist die Tabelle User in folgendem Zustand
      | ID | USERNAME | PASSWORD | GESPERRT | SPERRUNG_COUNTER | USERRECHTE_ID | KRANKENPFLEGERIN_ID |

  Szenario: Eine Krankenpflegerin darf nicht andere Userinnen loeschen
    Angenommen der User ist eine Krankenpflegerin
    Wenn eine KlientIn geloescht wird
    Dann wird eine MokiSecurityException geworfen
