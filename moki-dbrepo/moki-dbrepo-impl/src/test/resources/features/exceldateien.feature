# language: de

Funktionalität: dieses feature file ist ein test fuer dbunit

  Grundlage:
    Angenommen der Treiber ist org.mariadb.jdbc.Driver
    Und die URL ist jdbc:mysql://localhost:3306/mokidb_integration
    Und der Username ist moki
    Und das Passwort ist 288f3d45-e741-4a4c-ace4-13841f1ea50c
    Und die Datenbank befindet sich in folgendem Zustand src/test/resources/cleanAll.xml
    Und in der Tabelle TAETIGKEITSFELD sind folgende Daten vorhanden
      | ID                                   | BEZEICHNUNG |
      | b5e16b22-96d4-4760-96c5-92f2f30c2eef | FK          |
      | 0cd2be6e-9f34-448d-be6a-87a127c99e16 | FRZ         |
      | 1e0184ac-09a0-400a-aaef-cce7c10d3ab5 | ICH         |
      | 061b70f5-aba7-4d13-92f6-c8f6a4a9db93 | KJH         |
      | cf88ff8b-9f6b-42e8-acd5-2c4f47a38c59 | LZP         |
      | 7e69c3f3-2e26-4488-8ad0-4785f715a5f6 | MPT         |
      | cb4e0fe3-1e66-4d6e-aa88-0075f31f5bc9 | TS          |


  Szenario: Es soll ueberprueft werden ob eine Excel Datei mit einer bestimmten Taetigkeitsfeld ID gelesen werden kann
    Angenommen in der Tabelle EXCEL_DATEIEN sind folgende Daten vorhanden
      | ID                                   | DATEINAME                         | DATEI                      | TAETIGKEITSFELD_ID                   |
      | 724414fe-cb8f-4aa5-8b06-f99f2eec5336 | Klientendatei und HN FK 2017.xlsx | [TEXT]Excel FK Inhalt.xlsx | b5e16b22-96d4-4760-96c5-92f2f30c2eef |

    Wenn die Suche mit der taetigkeitsfeldId b5e16b22-96d4-4760-96c5-92f2f30c2eef durchgefuehrt wird
    Dann werden folgende Excel Dateien mit dem Inhalt gefunden
      | id                                   | dateiname                         | datei                | taetigkeitsfeldId                    |
      | 724414fe-cb8f-4aa5-8b06-f99f2eec5336 | Klientendatei und HN FK 2017.xlsx | Excel FK Inhalt.xlsx | b5e16b22-96d4-4760-96c5-92f2f30c2eef |


  Szenario: Es soll ueberprueft werden ob Einstellungen gespeichert werden koennen
    Wenn eine Excel Datei mit folgendem Daten gespeichert wird
      | id                                   | dateiname                               | datei                 | taetigkeitsfeldId                    |
      | 724414fe-cb8f-4aa5-8b06-f99f2eec5336 | Klientendatei und HN FREIZEIT 2017.xlsx | Excel FRZ Inhalt.xlsx | 0cd2be6e-9f34-448d-be6a-87a127c99e16 |
    Dann ist die Tabelle EXCEL_DATEIEN in folgendem Zustand
      | ID                                   | DATEINAME                               | DATEI                       | TAETIGKEITSFELD_ID                   |
      | 724414fe-cb8f-4aa5-8b06-f99f2eec5336 | Klientendatei und HN FREIZEIT 2017.xlsx | [TEXT]Excel FRZ Inhalt.xlsx | 0cd2be6e-9f34-448d-be6a-87a127c99e16 |
