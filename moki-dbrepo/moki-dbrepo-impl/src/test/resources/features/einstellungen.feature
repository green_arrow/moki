# language: de

Funktionalität: dieses feature file ist ein test fuer dbunit

  Grundlage:
    Angenommen der Treiber ist org.mariadb.jdbc.Driver
    Und die URL ist jdbc:mysql://localhost:3306/mokidb_integration
    Und der Username ist moki
    Und das Passwort ist 288f3d45-e741-4a4c-ace4-13841f1ea50c
    Und die Datenbank befindet sich in folgendem Zustand src/test/resources/cleanAll.xml

  Szenario: Es soll ueberprueft werden ob Einstellungen fuer Taetigkeitsfelder gelesen werden kann
    Angenommen in der Tabelle EINSTELLUNGEN sind folgende Daten vorhanden
      | ID                                   | FACHLICHKEIT      | INHALT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
      | efd85359-4744-4a77-958f-4566b932277a | TAETIGKEITSFELDER | [TEXT][{"taetigkietsfeld":"FK","orgprot":"Org. Prot.","stammblatt":"FK Stammblatt","blattList":["Klientenblatt FK und EPP","Klientenblatt Zwilling","Klientenblatt Drilling"]},{"taetigkietsfeld":"FRZ","orgprot":"Org. Prot.","stammblatt":"FRZ Stammblatt","blattList":["Klientenblatt FRZ"]},{"taetigkietsfeld":"ICH","orgprot":"Org. Prot.","stammblatt":"Ich Stammblatt","blattList":["Klientenblatt ICH"]},{"taetigkietsfeld":"KJH","orgprot":"Org. Prot.","stammblatt":"Stammblatt MK","blattList":["Klientenblatt 1 MK","Klientenblatt 2 MK","Klientenblatt 3 MK","Klientenblatt 4 MK"]},{"taetigkietsfeld":"LZP","orgprot":"2","stammblatt":"3","blattList":["1","4"]},{"taetigkietsfeld":"MPT","orgprot":"Org. Prot.","stammblatt":"KI-JU-PALL Stammblatt","blattList":["Klientenblatt KI-JU-PALL"]},{"taetigkietsfeld":"TS","orgprot":"Org. Prot.","stammblatt":"TS Stammblatt","blattList":["Klientenblatt TS"]}] |
    Wenn die Suche mit der Fachlichkeit TAETIGKEITSFELDER durchgefuehrt wird
    Dann werden folgende Einstellungen mit dem Inhalt gefunden
      | id                                   | fachlichkeit      | inhalt                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
      | efd85359-4744-4a77-958f-4566b932277a | TAETIGKEITSFELDER | [{"taetigkietsfeld":"FK","orgprot":"Org. Prot.","stammblatt":"FK Stammblatt","blattList":["Klientenblatt FK und EPP","Klientenblatt Zwilling","Klientenblatt Drilling"]},{"taetigkietsfeld":"FRZ","orgprot":"Org. Prot.","stammblatt":"FRZ Stammblatt","blattList":["Klientenblatt FRZ"]},{"taetigkietsfeld":"ICH","orgprot":"Org. Prot.","stammblatt":"Ich Stammblatt","blattList":["Klientenblatt ICH"]},{"taetigkietsfeld":"KJH","orgprot":"Org. Prot.","stammblatt":"Stammblatt MK","blattList":["Klientenblatt 1 MK","Klientenblatt 2 MK","Klientenblatt 3 MK","Klientenblatt 4 MK"]},{"taetigkietsfeld":"LZP","orgprot":"2","stammblatt":"3","blattList":["1","4"]},{"taetigkietsfeld":"MPT","orgprot":"Org. Prot.","stammblatt":"KI-JU-PALL Stammblatt","blattList":["Klientenblatt KI-JU-PALL"]},{"taetigkietsfeld":"TS","orgprot":"Org. Prot.","stammblatt":"TS Stammblatt","blattList":["Klientenblatt TS"]}] |

  Szenario: Es soll ueberprueft werden ob Einstellungen gespeichert werden koennen
    Wenn die Einstellungen mit folgenden Daten gespeichert werden
      | id                                   | fachlichkeit      | inhalt      |
      | c7dc6488-f6b7-4d6c-9b50-968a27e9961d | TAETIGKEITSFELDER | Excel Datei |
    Dann ist die Tabelle EINSTELLUNGEN in folgendem Zustand
      | ID                                   | FACHLICHKEIT      | INHALT            |
      | c7dc6488-f6b7-4d6c-9b50-968a27e9961d | TAETIGKEITSFELDER | [TEXT]Excel Datei |