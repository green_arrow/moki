# language: de

Funktionalität: dieses feature file ist ein test fuer dbunit

  Grundlage:
    Angenommen der Treiber ist org.mariadb.jdbc.Driver
    Und die URL ist jdbc:mysql://localhost:3306/mokidb_integration
    Und der Username ist moki
    Und das Passwort ist 288f3d45-e741-4a4c-ace4-13841f1ea50c
    Und die Datenbank befindet sich in folgendem Zustand src/test/resources/cleanAll.xml
    Und in der Tabelle VERRECHNUNG sind folgende Daten vorhanden
      | ID                                   | BEZEICHNUNG |
      | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | EPP-NR      |
      | 2802a720-80f4-4021-b71d-71f37d01a854 | EPP-NR      |
    Und in der Tabelle TAETIGKEITSFELD sind folgende Daten vorhanden
      | ID                                   | BEZEICHNUNG |
      | a23ba0da-c5af-4f59-86d9-3b432de48b7e | TS          |
      | 562efe42-4062-4f9f-91ec-984eebbf5a58 | FRZ         |
    Und in der Tabelle KLIENTIN sind folgende Daten vorhanden
      | ID                                   | KLIENTENNUMMER | VORNAME | NACHNAME | GESCHLECHT | GEBURTSDATUM | TAETIGKEITSFELD_ID                   | VERRECHNUNG_ID                       | VERRECHNUNG_NUMMER | SOZIALVERSICHERUNGSNUMMER | SOZIALVERSICHERUNGSTRAEGER | MUTTER_VERTRETERIN | VATER_VERTRETER | SONSTIGE_VERTRETER | HAUPTWOHNSITZ |
      | ed8284c2-9369-499d-8d4c-d68928fbc958 | 123456         | Anja    | K.       | W          | 1992-07-31   | a23ba0da-c5af-4f59-86d9-3b432de48b7e | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | 484                | 4210010192                | NOEGKK                     | N                  | N               | N                  | N             |
      | b19bf1bd-f117-43dc-9197-27358035ad11 | 123457         | Edith   | B.       | W          | 1992-07-31   | 562efe42-4062-4f9f-91ec-984eebbf5a58 | 2802a720-80f4-4021-b71d-71f37d01a854 | 578                | 4210010192                | NOEGKK                     | N                  | N               | N                  | N             |


  Szenario: Es soll ueberprueft werden ob alle Dokumente zu einer KlientIn gelesen werden können
    Angenommen in der Tabelle KLIENTIN_ZUSAETZLICHE_DOKUMENTE sind folgende Daten vorhanden
      | ID                                   | DATEINAME           | ERSTELLUNGSZEITPUNKT | KLIENTIN_ID                          |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d36 | Anja K.pdf          | 1992-07-31 12:00:15  | ed8284c2-9369-499d-8d4c-d68928fbc958 |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d37 | Edith B Datei 1.pdf | 1992-07-31 14:00:15  | b19bf1bd-f117-43dc-9197-27358035ad11 |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 | Edith B Datei 2.pdf | 1992-07-31 16:00:15  | b19bf1bd-f117-43dc-9197-27358035ad11 |
    Wenn die Suche mit der KlientIn ID b19bf1bd-f117-43dc-9197-27358035ad11 durchgefuehrt wird
    Dann werden folgende Eintraege in dieser Reihenfolge gefunden
      | id                                   | dateiname           | erstellungszeitpunkt | klientInId                           |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 | Edith B Datei 2.pdf | 1992-07-31 16:00:15  | b19bf1bd-f117-43dc-9197-27358035ad11 |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d37 | Edith B Datei 1.pdf | 1992-07-31 14:00:15  | b19bf1bd-f117-43dc-9197-27358035ad11 |


  Szenario: Es soll ueberprueft werden ob ein Dokument zur KlientIn angelegt werden kann
    Wenn ein Dokument mit folgenden Daten gespeichert wird
      | id                                   | dateiname   | erstellungszeitpunkt | klientInId                           |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d37 | Edith B.pdf | 1992-07-31 14:00:15  | b19bf1bd-f117-43dc-9197-27358035ad11 |
    Dann ist die Tabelle KLIENTIN_ZUSAETZLICHE_DOKUMENTE in folgendem Zustand
      | ID                                   | DATEINAME   | ERSTELLUNGSZEITPUNKT | KLIENTIN_ID                          |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d37 | Edith B.pdf | 1992-07-31 14:00:15  | b19bf1bd-f117-43dc-9197-27358035ad11 |


  Szenario: Es soll ueberprueft werden ob ein Dokument zur KlientIn bearbeitet werden kann
    Angenommen in der Tabelle KLIENTIN_ZUSAETZLICHE_DOKUMENTE sind folgende Daten vorhanden
      | ID                                   | DATEINAME   | ERSTELLUNGSZEITPUNKT | KLIENTIN_ID                          |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d37 | Edith B.pdf | 1992-07-31 14:00:15  | b19bf1bd-f117-43dc-9197-27358035ad11 |
    Wenn ein Dokument mit folgenden Daten gespeichert wird
      | id                                   | dateiname   | erstellungszeitpunkt | klientInId                           |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d37 | Edith B.pdf | 1992-07-31 14:00:15  | b19bf1bd-f117-43dc-9197-27358035ad11 |
    Dann ist die Tabelle KLIENTIN_ZUSAETZLICHE_DOKUMENTE in folgendem Zustand
      | ID                                   | DATEINAME   | ERSTELLUNGSZEITPUNKT | KLIENTIN_ID                          |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d37 | Edith B.pdf | 1992-07-31 14:00:15  | b19bf1bd-f117-43dc-9197-27358035ad11 |

  Szenario: Es soll ueberprueft werden ob ein Dokument zur KlientIn geloescht werden kann
    Angenommen in der Tabelle KLIENTIN_ZUSAETZLICHE_DOKUMENTE sind folgende Daten vorhanden
      | ID                                   | DATEINAME           | ERSTELLUNGSZEITPUNKT | KLIENTIN_ID                          |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d36 | Anja K.pdf          | 1992-07-31 12:00:15  | ed8284c2-9369-499d-8d4c-d68928fbc958 |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d37 | Edith B Datei 1.pdf | 1992-07-31 14:00:15  | b19bf1bd-f117-43dc-9197-27358035ad11 |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 | Edith B Datei 2.pdf | 1992-07-31 16:00:15  | b19bf1bd-f117-43dc-9197-27358035ad11 |
    Wenn die Dokumente mit der ID b9dbffef-63f2-4afb-baec-bc6e9ad64d38 geloescht werden
    Dann ist die Tabelle KLIENTIN_ZUSAETZLICHE_DOKUMENTE in folgendem Zustand
      | ID                                   | DATEINAME           | ERSTELLUNGSZEITPUNKT | KLIENTIN_ID                          |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d36 | Anja K.pdf          | 1992-07-31 12:00:15  | ed8284c2-9369-499d-8d4c-d68928fbc958 |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d37 | Edith B Datei 1.pdf | 1992-07-31 14:00:15  | b19bf1bd-f117-43dc-9197-27358035ad11 |


  Szenario: Es soll ueberprueft werden ob alle Dokumente zu einer KlientIn geloescht werden koennen
    Angenommen in der Tabelle KLIENTIN_ZUSAETZLICHE_DOKUMENTE sind folgende Daten vorhanden
      | ID                                   | DATEINAME           | ERSTELLUNGSZEITPUNKT | KLIENTIN_ID                          |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d36 | Anja K.pdf          | 1992-07-31 12:00:15  | ed8284c2-9369-499d-8d4c-d68928fbc958 |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d37 | Edith B Datei 1.pdf | 1992-07-31 14:00:15  | b19bf1bd-f117-43dc-9197-27358035ad11 |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 | Edith B Datei 2.pdf | 1992-07-31 16:00:15  | b19bf1bd-f117-43dc-9197-27358035ad11 |
    Wenn die Dokumente mit der KlientIn ID b19bf1bd-f117-43dc-9197-27358035ad11 geloescht werden
    Dann ist die Tabelle KLIENTIN_ZUSAETZLICHE_DOKUMENTE in folgendem Zustand
      | ID                                   | DATEINAME  | ERSTELLUNGSZEITPUNKT | KLIENTIN_ID                          |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d36 | Anja K.pdf | 1992-07-31 12:00:15  | ed8284c2-9369-499d-8d4c-d68928fbc958 |
