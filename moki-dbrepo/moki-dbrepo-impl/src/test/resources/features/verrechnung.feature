# language: de

Funktionalität: dieses feature file ist ein test fuer dbunit

  Grundlage:
    Angenommen der Treiber ist org.mariadb.jdbc.Driver
    Und die URL ist jdbc:mysql://localhost:3306/mokidb_integration
    Und der Username ist moki
    Und das Passwort ist 288f3d45-e741-4a4c-ace4-13841f1ea50c
    Und die Datenbank befindet sich in folgendem Zustand src/test/resources/cleanAll.xml
    Und in der Tabelle VERRECHNUNG sind folgende Daten vorhanden
      | ID                                   | BEZEICHNUNG |
      | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | EPP-NR      |
      | 2802a720-80f4-4021-b71d-71f37d01a854 | EPP-NR      |

  Szenario: Es soll ueberprueft werden ob alle Verrechnung Eintraege gefunden werden
    Wenn die Suche nach allen Eintraegen ausgefuehrt wird
    Dann werden folegende Eintraege gefunden
      | id                                   | bezeichnung |
      | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | EPP-NR      |
      | 2802a720-80f4-4021-b71d-71f37d01a854 | EPP-NR      |

  Szenario: Es soll ueberprueft werden ob ein bestimmter Eintrag ueber die ID gelesen werden kann
    Angenommen es wird nach der Verrechnung mit der Id 15055aad-dbeb-47b9-a104-b8d2679b5b9d gesucht
    Dann wird ein Eintrag mit der ID 15055aad-dbeb-47b9-a104-b8d2679b5b9d und der Bezeichnung EPP-NR gefunden

  Szenario: Es soll ueberprueft werden ob eine Verrechnug angelegt werden kann
    Wenn eine Verrechnug mit folgenden Daten gespeichert wird
      | id                                   | bezeichnung     |
      | ecfd2efe-c6d5-42eb-8c42-f20bf77e66de | Kindergarten-NR |
    Dann ist die Tabelle VERRECHNUNG in folgendem Zustand
      | ID                                   | BEZEICHNUNG     |
      | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | EPP-NR          |
      | 2802a720-80f4-4021-b71d-71f37d01a854 | EPP-NR          |
      | ecfd2efe-c6d5-42eb-8c42-f20bf77e66de | Kindergarten-NR |
