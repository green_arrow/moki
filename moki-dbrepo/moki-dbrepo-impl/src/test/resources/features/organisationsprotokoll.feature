# language: de

Funktionalität: dieses feature file ist ein test fuer dbunit

  Grundlage:
    Angenommen der Treiber ist org.mariadb.jdbc.Driver
    Und die URL ist jdbc:mysql://localhost:3306/mokidb_integration
    Und der Username ist moki
    Und das Passwort ist 288f3d45-e741-4a4c-ace4-13841f1ea50c
    Und die Datenbank befindet sich in folgendem Zustand src/test/resources/cleanAll.xml
    Und in der Tabelle VERRECHNUNG sind folgende Daten vorhanden
      | ID                                   | BEZEICHNUNG |
      | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | EPP-NR      |
      | 2802a720-80f4-4021-b71d-71f37d01a854 | EPP-NR      |
    Und in der Tabelle TAETIGKEITSFELD sind folgende Daten vorhanden
      | ID                                   | BEZEICHNUNG |
      | a23ba0da-c5af-4f59-86d9-3b432de48b7e | TS          |
    Und in der Tabelle KLIENTIN sind folgende Daten vorhanden
      | ID                                   | KLIENTENNUMMER | VORNAME | NACHNAME | GESCHLECHT | GEBURTSDATUM | TAETIGKEITSFELD_ID                   | VERRECHNUNG_ID                       | VERRECHNUNG_NUMMER | SOZIALVERSICHERUNGSNUMMER | SOZIALVERSICHERUNGSTRAEGER | MUTTER_VERTRETERIN | VATER_VERTRETER | SONSTIGE_VERTRETER | HAUPTWOHNSITZ |
      | ed8284c2-9369-499d-8d4c-d68928fbc958 | 123456         | Anja    | K.       | W          | 1992-07-31   | a23ba0da-c5af-4f59-86d9-3b432de48b7e | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | 484                | 4210010192                | NOEGKK                     | N                  | N               | N                  | N             |
      | b19bf1bd-f117-43dc-9197-27358035ad11 | 123457         | Edith   | B.       | W          | 1992-07-31   | a23ba0da-c5af-4f59-86d9-3b432de48b7e | 2802a720-80f4-4021-b71d-71f37d01a854 | 578                | 4210010192                | NOEGKK                     | N                  | N               | N                  | N             |


  Szenario: Es soll ueberprueft werden ob alle Organisationsprotokolle zu einer KlientIn gelesen werden können
    Angenommen in der Tabelle ORGANISATIONSPROTOKOLL sind folgende Daten vorhanden
      | ID                                   | TEXT  | HANDSCHRIFT_DGKP | AENDERUNGSZEITPUNKT | KLIENTIN_ID                          |
      | 6b1e69b0-2c74-41a1-96a8-574a06e95b99 | text1 | anja             | 1992-08-31 12:00:15 | ed8284c2-9369-499d-8d4c-d68928fbc958 |
      | a8cefe8f-6082-444f-8a5b-2a1924488094 | text2 | anja             | 1992-09-30 12:00:15 | ed8284c2-9369-499d-8d4c-d68928fbc958 |
      | f9a84cc4-799b-4f30-8bb5-f388f5185e31 | text1 | edith            | 1992-10-31 12:00:15 | b19bf1bd-f117-43dc-9197-27358035ad11 |
      | 07f851f4-b899-4324-9450-d3f2aefc5ce0 | text2 | edith            | 1992-11-30 12:00:15 | b19bf1bd-f117-43dc-9197-27358035ad11 |
    Wenn die Suche mit der ID b19bf1bd-f117-43dc-9197-27358035ad11 geloescht wird
    Dann werden folgende Eintraege in dieser Reihenfolge gefunden
      | id                                   | text  | handschriftDgkp | aenderungszeitpunkt | klientInId                           |
      | 07f851f4-b899-4324-9450-d3f2aefc5ce0 | text2 | edith           | 1992-11-30 12:00:15 | b19bf1bd-f117-43dc-9197-27358035ad11 |
      | f9a84cc4-799b-4f30-8bb5-f388f5185e31 | text1 | edith           | 1992-10-31 12:00:15 | b19bf1bd-f117-43dc-9197-27358035ad11 |


  Szenario: Es soll ueberprueft werden ob ein Organisationsprotokoll angelegt werden kann
    Wenn das Organisationsprotokoll mit folgenden Daten gespeichert wird
      | id                                   | text      | handschriftDgkp | aenderungszeitpunkt | klientInId                           |
      | 52a2bb63-3ef3-49da-9443-a0816a0b560a | speichern | anja            | 1992-07-31 12:00:15 | ed8284c2-9369-499d-8d4c-d68928fbc958 |
    Dann ist die Tabelle ORGANISATIONSPROTOKOLL in folgendem Zustand
      | ID                                   | TEXT      | HANDSCHRIFT_DGKP | AENDERUNGSZEITPUNKT | KLIENTIN_ID                          |
      | 52a2bb63-3ef3-49da-9443-a0816a0b560a | speichern | anja             | 1992-07-31 12:00:15 | ed8284c2-9369-499d-8d4c-d68928fbc958 |


  Szenario: Es soll ueberprueft werden ob alle Organisationsprotokolle zu einer KlientIn geloescht werden koennen
    Angenommen in der Tabelle ORGANISATIONSPROTOKOLL sind folgende Daten vorhanden
      | ID                                   | TEXT  | HANDSCHRIFT_DGKP | AENDERUNGSZEITPUNKT | KLIENTIN_ID                          |
      | 6b1e69b0-2c74-41a1-96a8-574a06e95b99 | text1 | anja             | 1992-08-31 12:00:15 | ed8284c2-9369-499d-8d4c-d68928fbc958 |
      | a8cefe8f-6082-444f-8a5b-2a1924488094 | text2 | anja             | 1992-09-30 12:00:15 | ed8284c2-9369-499d-8d4c-d68928fbc958 |
      | f9a84cc4-799b-4f30-8bb5-f388f5185e31 | text1 | edith            | 1992-10-31 12:00:15 | b19bf1bd-f117-43dc-9197-27358035ad11 |
      | 07f851f4-b899-4324-9450-d3f2aefc5ce0 | text2 | edith            | 1992-11-30 12:00:15 | b19bf1bd-f117-43dc-9197-27358035ad11 |
    Wenn die Organisationsprotokollee mit der KlientinId ed8284c2-9369-499d-8d4c-d68928fbc958 geloescht werden
    Dann ist die Tabelle ORGANISATIONSPROTOKOLL in folgendem Zustand
      | ID                                   | TEXT  | HANDSCHRIFT_DGKP | AENDERUNGSZEITPUNKT | KLIENTIN_ID                          |
      | 07f851f4-b899-4324-9450-d3f2aefc5ce0 | text2 | edith            | 1992-11-30 12:00:15 | b19bf1bd-f117-43dc-9197-27358035ad11 |
      | f9a84cc4-799b-4f30-8bb5-f388f5185e31 | text1 | edith            | 1992-10-31 12:00:15 | b19bf1bd-f117-43dc-9197-27358035ad11 |


  Szenario: Eine Krankenpflegerin darf keine Organisationsprotokolle loeschen
    Angenommen der User ist eine Krankenpflegerin
    Wenn ein Organisationsprotokoll geloescht wird
    Dann wird eine MokiSecurityException geworfen