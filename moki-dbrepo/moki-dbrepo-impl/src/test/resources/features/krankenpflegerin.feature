# language: de

Funktionalität: dieses feature file ist ein test fuer dbunit

  Grundlage:
    Angenommen der Treiber ist org.mariadb.jdbc.Driver
    Und die URL ist jdbc:mysql://localhost:3306/mokidb_integration
    Und der Username ist moki
    Und das Passwort ist 288f3d45-e741-4a4c-ace4-13841f1ea50c
    Und die Datenbank befindet sich in folgendem Zustand src/test/resources/cleanAll.xml

  Szenario: Es soll ueberprueft werden ob alle Krankenpflegerinnen gesucht werden koennen
    Angenommen in der Tabelle KRANKENPFLEGERIN sind folgende Daten vorhanden
      | ID                                   | VORNAME | NACHNAME |
      | 365cc72d-efcd-45c0-815d-6be22d2835a5 | Anja    | K.       |
      | a5298fba-53fd-4f6c-8157-064e79f8c8a8 | Edith   | B.       |
    Wenn nach allen Krankenpflegerinnen gesucht wird
    Dann werden folgende Eintraege gefunden
      | id                                   | vorname | nachname |
      | 365cc72d-efcd-45c0-815d-6be22d2835a5 | Anja    | K.       |
      | a5298fba-53fd-4f6c-8157-064e79f8c8a8 | Edith   | B.       |


  Szenario: Es soll ueberprueft werden ob eine Krankenpflegerin gesucht werden kann
    Angenommen in der Tabelle KRANKENPFLEGERIN sind folgende Daten vorhanden
      | ID                                   | VORNAME | NACHNAME |
      | 175bf557-da80-49f3-b9f2-787aa57af9e0 | Anja    | K.       |
    Wenn nach der Krankenpflegerin mit der ID 175bf557-da80-49f3-b9f2-787aa57af9e0 gesucht wird
    Dann wird folgender Eintrag gefunden
      | id                                   | vorname | nachname |
      | 175bf557-da80-49f3-b9f2-787aa57af9e0 | Anja    | K.       |

  Szenario: Es soll ueberprueft werden ob eine Krankenpflegerin angelegt werden kann
    Wenn die Krankenpflegerin mit folgenden Daten gespeichert wird
      | id                                   | vorname | nachname |
      | 175bf557-da80-49f3-b9f2-787aa57af9e0 | Anja    | K.       |
    Dann ist die Tabelle KRANKENPFLEGERIN in folgendem Zustand
      | ID                                   | VORNAME | NACHNAME |
      | 175bf557-da80-49f3-b9f2-787aa57af9e0 | Anja    | K.       |

  Szenario: Es soll ueberprueft werden ob eine Krankenpflegerin bearbeitet werden kann
    Angenommen in der Tabelle KRANKENPFLEGERIN sind folgende Daten vorhanden
      | ID                                   | VORNAME | NACHNAME |
      | 175bf557-da80-49f3-b9f2-787aa57af9e0 | Anja    | K.       |
    Wenn die Krankenpflegerin mit folgenden Daten gespeichert wird
      | id                                   | vorname | nachname |
      | 175bf557-da80-49f3-b9f2-787aa57af9e0 | Edith   | B.       |
    Dann ist die Tabelle KRANKENPFLEGERIN in folgendem Zustand
      | ID                                   | VORNAME | NACHNAME |
      | 175bf557-da80-49f3-b9f2-787aa57af9e0 | Edith   | B.       |

  Szenario: Es soll ueberprueft werden ob eine Krankenpflegerin geloescht werden kann
    Angenommen in der Tabelle KRANKENPFLEGERIN sind folgende Daten vorhanden
      | ID                                   | VORNAME | NACHNAME |
      | 175bf557-da80-49f3-b9f2-787aa57af9e0 | Anja    | K.       |
    Wenn die Krankenpflegerin mit der ID 175bf557-da80-49f3-b9f2-787aa57af9e0 geloescht wird
    Dann ist die Tabelle KRANKENPFLEGERIN in folgendem Zustand
      | ID | VORNAME | NACHNAME |

  Szenario: Eine Verwaltung darf keine Krankenpflegerin suchen
    Angenommen der User ist eine Verwaltung
    Wenn die Suche nach allen Eintraegen ausgefuehrt wird
    Dann wird eine MokiSecurityException geworfen

  Szenario: Eine Verwaltung darf keine Krankenpflegerin anlegen
    Angenommen der User ist eine Verwaltung
    Wenn eine Krankenpflegerin angelegt wird
    Dann wird eine MokiSecurityException geworfen

  Szenario: Eine Verwaltung darf keine Krankenpflegerin loeschen
    Angenommen der User ist eine Verwaltung
    Wenn eine Krankenpflegerin geloescht wird
    Dann wird eine MokiSecurityException geworfen

  Szenario: Eine Krankenpflegerin darf keine andere Krankenpflegerin loeschen
    Angenommen der User ist eine Krankenpflegerin
    Wenn eine Krankenpflegerin geloescht wird
    Dann wird eine MokiSecurityException geworfen