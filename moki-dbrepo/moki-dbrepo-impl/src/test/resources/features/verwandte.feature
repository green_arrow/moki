# language: de

Funktionalität: dieses feature file ist ein test fuer dbunit

  Grundlage:
    Angenommen der Treiber ist org.mariadb.jdbc.Driver
    Und die URL ist jdbc:mysql://localhost:3306/mokidb_integration
    Und der Username ist moki
    Und das Passwort ist 288f3d45-e741-4a4c-ace4-13841f1ea50c
    Und die Datenbank befindet sich in folgendem Zustand src/test/resources/cleanAll.xml
    Und in der Tabelle VERRECHNUNG sind folgende Daten vorhanden
      | ID                                   | BEZEICHNUNG |
      | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | EPP-NR      |
      | 2802a720-80f4-4021-b71d-71f37d01a854 | EPP-NR      |
      | 7eca0a43-9a35-4e61-be37-9a791429b008 | EPP-NR      |
      | 926c6d75-f854-4cd7-bd72-fa250782ea0b | EPP_NR      |
    Und in der Tabelle TAETIGKEITSFELD sind folgende Daten vorhanden
      | ID                                   | BEZEICHNUNG |
      | a23ba0da-c5af-4f59-86d9-3b432de48b7e | TS          |
    Und in der Tabelle KLIENTIN sind folgende Daten vorhanden
      | ID                                   | KLIENTENNUMMER | VORNAME | NACHNAME | GESCHLECHT | GEBURTSDATUM | TAETIGKEITSFELD_ID                   | VERRECHNUNG_ID                       | VERRECHNUNG_NUMMER | SOZIALVERSICHERUNGSNUMMER | SOZIALVERSICHERUNGSTRAEGER | MUTTER_VERTRETERIN | VATER_VERTRETER | SONSTIGE_VERTRETER | HAUPTWOHNSITZ |
      | 175bf557-da80-49f3-b9f2-787aa57af9e1 | 123456         | Anja    | K.       | W          | 1992-07-31   | a23ba0da-c5af-4f59-86d9-3b432de48b7e | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | 484                | 4210010192                | NOEGKK                     | N                  | N               | N                  | N             |
      | 175bf557-da80-49f3-b9f2-787aa57af9e2 | 123457         | Edith   | B.       | W          | 1992-07-31   | a23ba0da-c5af-4f59-86d9-3b432de48b7e | 2802a720-80f4-4021-b71d-71f37d01a854 | 578                | 4210010192                | NOEGKK                     | N                  | N               | N                  | N             |
      | 175bf557-da80-49f3-b9f2-787aa57af9f1 | 123458         | Grace   | C.M.     | W          | 1992-07-31   | a23ba0da-c5af-4f59-86d9-3b432de48b7e | 7eca0a43-9a35-4e61-be37-9a791429b008 | 187                | 4210010192                | NOEGKK                     | N                  | N               | N                  | N             |
      | 175bf557-da80-49f3-b9f2-787aa57af9F2 | 123459         | Liya    | S.       | W          | 1992-07-31   | a23ba0da-c5af-4f59-86d9-3b432de48b7e | 926c6d75-f854-4cd7-bd72-fa250782ea0b | 896                | 4210010192                | NOEGKK                     | N                  | N               | N                  | N             |


  Szenario: Es soll ueberprueft werden ob Verwandte zu einer KlientIn gelesen werden können
    Angenommen in der Tabelle VERWANDTE sind folgende Daten vorhanden
      | ID                                   | KLIENTIN_ID                          | VERWANDTE_1_ID                       | VERWANDTE_2_ID                       | VERWANDTE_3_ID                       |
      | 175bf557-da80-49f3-b9f2-787aa57af9e0 | 175bf557-da80-49f3-b9f2-787aa57af9e1 | 175bf557-da80-49f3-b9f2-787aa57af9e2 | 175bf557-da80-49f3-b9f2-787aa57af9f1 | 175bf557-da80-49f3-b9f2-787aa57af9F2 |
    Wenn die Suche mit der ID 175bf557-da80-49f3-b9f2-787aa57af9e1 durchgefuehrt wird
    Dann wird folgender Eintrag gefunden
      | id                                   | klientInId                           | verwandte1Id                         | verwandte2Id                         | verwandte3Id                         |
      | 175bf557-da80-49f3-b9f2-787aa57af9e0 | 175bf557-da80-49f3-b9f2-787aa57af9e1 | 175bf557-da80-49f3-b9f2-787aa57af9e2 | 175bf557-da80-49f3-b9f2-787aa57af9f1 | 175bf557-da80-49f3-b9f2-787aa57af9F2 |


  Szenario: Es soll ueberprueft werden ob Verwandte zu einer KlientIn angelegt werden können
    Wenn die Verwandte mit folgenden Daten gespeichert wird
      | id                                   | klientInId                           | verwandte1Id                         | verwandte2Id                         | verwandte3Id                         |
      | 175bf557-da80-49f3-b9f2-787aa57af9e0 | 175bf557-da80-49f3-b9f2-787aa57af9e1 | 175bf557-da80-49f3-b9f2-787aa57af9e2 | 175bf557-da80-49f3-b9f2-787aa57af9f1 | 175bf557-da80-49f3-b9f2-787aa57af9F2 |
    Dann ist die Tabelle VERWANDTE in folgendem Zustand
      | ID                                   | KLIENTIN_ID                          | VERWANDTE_1_ID                       | VERWANDTE_2_ID                       | VERWANDTE_3_ID                       |
      | 175bf557-da80-49f3-b9f2-787aa57af9e0 | 175bf557-da80-49f3-b9f2-787aa57af9e1 | 175bf557-da80-49f3-b9f2-787aa57af9e2 | 175bf557-da80-49f3-b9f2-787aa57af9f1 | 175bf557-da80-49f3-b9f2-787aa57af9F2 |


  Szenario: Es soll ueberprueft werden ob Verwandte zu einer KlientIn angelegt werden können
    Wenn die Verwandte mit folgenden Daten gespeichert wird
      | id                                   | klientInId                           | verwandte1Id                         | verwandte2Id                         | verwandte3Id |
      | 175bf557-da80-49f3-b9f2-787aa57af9e0 | 175bf557-da80-49f3-b9f2-787aa57af9e1 | 175bf557-da80-49f3-b9f2-787aa57af9e2 | 175bf557-da80-49f3-b9f2-787aa57af9f1 | [null]       |
    Dann ist die Tabelle VERWANDTE in folgendem Zustand
      | ID                                   | KLIENTIN_ID                          | VERWANDTE_1_ID                       | VERWANDTE_2_ID                       | VERWANDTE_3_ID |
      | 175bf557-da80-49f3-b9f2-787aa57af9e0 | 175bf557-da80-49f3-b9f2-787aa57af9e1 | 175bf557-da80-49f3-b9f2-787aa57af9e2 | 175bf557-da80-49f3-b9f2-787aa57af9f1 | [null]         |


  Szenario: Es soll ueberprueft werden ob eine KlientIn geloescht werden kann
    Angenommen in der Tabelle VERWANDTE sind folgende Daten vorhanden
      | ID                                   | KLIENTIN_ID                          | VERWANDTE_1_ID                       | VERWANDTE_2_ID                       | VERWANDTE_3_ID                       |
      | 175bf557-da80-49f3-b9f2-787aa57af9e0 | 175bf557-da80-49f3-b9f2-787aa57af9e1 | 175bf557-da80-49f3-b9f2-787aa57af9e2 | 175bf557-da80-49f3-b9f2-787aa57af9f1 | 175bf557-da80-49f3-b9f2-787aa57af9F2 |
    Wenn die Verwandte mit der ID 175bf557-da80-49f3-b9f2-787aa57af9e1 geloescht wird
    Dann ist die Tabelle VERWANDTE in folgendem Zustand
      | ID | KLIENTIN_ID | VERWANDTE_1_ID | VERWANDTE_2_ID | VERWANDTE_3_ID |


  Szenario: Eine Krankenpflegerin darf keine Klienten löschen
    Angenommen der User ist eine Krankenpflegerin
    Wenn eine Verwandte geloescht wird
    Dann wird eine MokiSecurityException geworfen