# language: de

Funktionalität: dieses feature file ist ein test fuer dbunit

  Grundlage:
    Angenommen der Treiber ist org.mariadb.jdbc.Driver
    Und die URL ist jdbc:mysql://localhost:3306/mokidb_integration
    Und der Username ist moki
    Und das Passwort ist 288f3d45-e741-4a4c-ace4-13841f1ea50c
    Und die Datenbank befindet sich in folgendem Zustand src/test/resources/cleanAll.xml
    Und in der Tabelle VERRECHNUNG sind folgende Daten vorhanden
      | ID                                   | BEZEICHNUNG |
      | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | EPP-NR      |
    Und in der Tabelle TAETIGKEITSFELD sind folgende Daten vorhanden
      | ID                                   | BEZEICHNUNG |
      | a23ba0da-c5af-4f59-86d9-3b432de48b7e | TS          |
      | 007a64df-fbda-44e7-9396-bd7dac89a44a | FRZ         |

  Szenario: Es soll ueberprueft werden ob alle KlientInnnen gelesen werden können
    Angenommen in der Tabelle KLIENTIN sind folgende Daten vorhanden
      | ID                                   | KLIENTENNUMMER | VORNAME | NACHNAME | GESCHLECHT | GEBURTSDATUM | TAETIGKEITSFELD_ID                   | VERRECHNUNG_ID                       | VERRECHNUNG_NUMMER | SOZIALVERSICHERUNGSNUMMER | SOZIALVERSICHERUNGSTRAEGER | MUTTER_VERTRETERIN | VATER_VERTRETER | SONSTIGE_VERTRETER | HAUPTWOHNSITZ |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d35 | 123456         | Anja    | K.       | W          | 1992-07-31   | 007a64df-fbda-44e7-9396-bd7dac89a44a | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | 484                | 4210010192                | NOEGKK                     | N                  | N               | N                  | N             |
    Wenn die Suche ausgefuehrt wird
    Dann werden folegende Eintraege gefunden
      | id                                   | klientenNummer | vorname | nachname | geschlecht | geburtsdatum | taetigkeitsfeldId                    | taetigkeitsfeld | verrechnungId                        | verrechnungNummer | sozialversicherungsnummer | sozialversicherungstraeger |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d35 | 123456         | Anja    | K.       | WEIBLICH   | 1992-07-31   | 007a64df-fbda-44e7-9396-bd7dac89a44a | FRZ             | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | 484               | 4210010192                | NOEGKK                     |


  Szenario: Es soll ueberprueft werden ob eine KlientIn angelegt werden kann
    Angenommen in der Tabelle KLIENTIN sind folgende Daten vorhanden
      | ID | KLIENTENNUMMER | VORNAME | NACHNAME | GESCHLECHT | GEBURTSDATUM | VERRECHNUNG_ID | VERRECHNUNG_NUMMER | SOZIALVERSICHERUNGSNUMMER | SOZIALVERSICHERUNGSTRAEGER | MUTTER_VERTRETERIN | VATER_VERTRETER | SONSTIGE_VERTRETER | HAUPTWOHNSITZ |
    Wenn die Klientin mit folgenden Daten gespeichert wird
      | id                                   | klientenNummer | vorname | nachname | geschlecht | geburtsdatum | taetigkeitsfeldId                    | taetigkeitsfeld | verrechnungId                        | verrechnungNummer | sozialversicherungsnummer | sozialversicherungstraeger |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d36 | 78910          | Anja    | K.       | WEIBLICH   | 1992-07-31   | 007a64df-fbda-44e7-9396-bd7dac89a44a | FRZ             | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | 484               | 4210010192                | NOEGKK                     |
    Dann ist die Tabelle KLIENTIN in folgendem Zustand
      | ID                                   | KLIENTENNUMMER | VORNAME | NACHNAME | GESCHLECHT | GEBURTSDATUM | TAETIGKEITSFELD_ID                   | VERRECHNUNG_ID                       | VERRECHNUNG_NUMMER | SOZIALVERSICHERUNGSNUMMER | SOZIALVERSICHERUNGSTRAEGER | MUTTER_VERTRETERIN | VATER_VERTRETER | SONSTIGE_VERTRETER | HAUPTWOHNSITZ |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d36 | 78910          | Anja    | K.       | W          | 1992-07-31   | 007a64df-fbda-44e7-9396-bd7dac89a44a | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | 484                | 4210010192                | NOEGKK                     | N                  | N               | N                  | N             |


  Szenario: Es soll ueberprueft werden ob eine KlientIn bearbeitet werden kann
    Angenommen in der Tabelle KLIENTIN sind folgende Daten vorhanden
      | ID                                   | KLIENTENNUMMER | VORNAME | NACHNAME | GESCHLECHT | GEBURTSDATUM | TAETIGKEITSFELD_ID                   | VERRECHNUNG_ID                       | VERRECHNUNG_NUMMER | SOZIALVERSICHERUNGSNUMMER | SOZIALVERSICHERUNGSTRAEGER | MUTTER_VERTRETERIN | VATER_VERTRETER | SONSTIGE_VERTRETER | HAUPTWOHNSITZ |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d36 | 78910          | Anja    | K.       | W          | 1992-07-31   | 007a64df-fbda-44e7-9396-bd7dac89a44a | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | 484                | 4210010192                | NOEGKK                     | N                  | N               | N                  | N             |
    Wenn die Klientin mit folgenden Daten gespeichert wird
      | id                                   | klientenNummer | vorname | nachname | geschlecht | geburtsdatum | taetigkeitsfeld | taetigkeitsfeldId                    | verrechnungId                        | verrechnungNummer | sozialversicherungsnummer | sozialversicherungstraeger | mutterVertreterin | vaterVertreter | sonstigeVertreter |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d36 | 78910          | Anja    | K.       | WEIBLICH   | 1992-07-31   | TS              | 007a64df-fbda-44e7-9396-bd7dac89a44a | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | 484               | 4210010192                | NOEGKK                     | true              | true           | true              |
    Dann ist die Tabelle KLIENTIN in folgendem Zustand
      | ID                                   | KLIENTENNUMMER | VORNAME | NACHNAME | GESCHLECHT | GEBURTSDATUM | TAETIGKEITSFELD_ID                   | VERRECHNUNG_ID                       | VERRECHNUNG_NUMMER | SOZIALVERSICHERUNGSNUMMER | SOZIALVERSICHERUNGSTRAEGER | MUTTER_VERTRETERIN | VATER_VERTRETER | SONSTIGE_VERTRETER | HAUPTWOHNSITZ |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d36 | 78910          | Anja    | K.       | W          | 1992-07-31   | a23ba0da-c5af-4f59-86d9-3b432de48b7e | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | 484                | 4210010192                | NOEGKK                     | J                  | J               | J                  | N             |


  Szenario: Es soll ueberprueft werden ob eine KlientIn geloescht werden kann
    Angenommen in der Tabelle KLIENTIN sind folgende Daten vorhanden
      | ID                                   | KLIENTENNUMMER | VORNAME | NACHNAME | GESCHLECHT | GEBURTSDATUM | TAETIGKEITSFELD_ID                   | VERRECHNUNG_ID                       | VERRECHNUNG_NUMMER | SOZIALVERSICHERUNGSNUMMER | SOZIALVERSICHERUNGSTRAEGER | MUTTER_VERTRETERIN | VATER_VERTRETER | SONSTIGE_VERTRETER | HAUPTWOHNSITZ |
      | b9dbffef-63f2-4afb-baec-bc6e9ad64d38 | 78910          | Anja    | K.       | W          | 1992-07-31   | 007a64df-fbda-44e7-9396-bd7dac89a44a | 15055aad-dbeb-47b9-a104-b8d2679b5b9d | 484                | 4210010192                | NOEGKK                     | N                  | N               | N                  | N             |
    Wenn die Klientin mit der ID b9dbffef-63f2-4afb-baec-bc6e9ad64d38 geloescht wird
    Dann ist die Tabelle KLIENTIN in folgendem Zustand
      | ID | KLIENTENNUMMER | VORNAME | NACHNAME | GESCHLECHT | GEBURTSDATUM | VERRECHNUNG_ID | VERRECHNUNG_NUMMER | SOZIALVERSICHERUNGSNUMMER | SOZIALVERSICHERUNGSTRAEGER | MUTTER_VERTRETERIN | VATER_VERTRETER | SONSTIGE_VERTRETER | HAUPTWOHNSITZ |


  Szenario: Eine Krankenpflegerin darf keine Klienten löschen
    Angenommen der User ist eine Krankenpflegerin
    Wenn eine KlientIn geloescht wird
    Dann wird eine MokiSecurityException geworfen