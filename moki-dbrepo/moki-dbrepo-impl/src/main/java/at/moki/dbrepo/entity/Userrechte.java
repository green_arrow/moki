package at.moki.dbrepo.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Green Arrow on 31.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Entity
@Table(name = "USERRECHTE")
@NamedQueries({
        @NamedQuery(name = Userrechte.QUERY_FIND_ALL, query = "SELECT p FROM Userrechte p"),
        @NamedQuery(name = Userrechte.QUERY_FIND_BY_ID, query = "SELECT p FROM Userrechte p WHERE p.id = :id"),
        @NamedQuery(name = Userrechte.QUERY_FIND_BY_BEZEICHNUNG, query = "SELECT p FROM Userrechte p WHERE p.bezeichnung = :bezeichnung")
})
public class Userrechte implements Serializable {

    private static final long serialVersionUID = -5769595651877800269L;

    public static final String QUERY_FIND_ALL = "Userrechte.findAll";

    public static final String QUERY_FIND_BY_ID = "Userrechte.findById";

    public static final String QUERY_FIND_BY_BEZEICHNUNG = "Userrechte.findByBezeichnung";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false, length = 36)
    private String id;

    @Column(name = "BEZEICHNUNG", length = 45)
    private String bezeichnung;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }
}