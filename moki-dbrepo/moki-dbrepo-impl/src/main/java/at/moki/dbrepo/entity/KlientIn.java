package at.moki.dbrepo.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * Created by Green Arrow on 31.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Entity
@Table(name = "KLIENTIN")
@NamedQueries({
        @NamedQuery(name = KlientIn.QUERY_FIND_ALL, query = "SELECT k FROM KlientIn k"),
        @NamedQuery(name = KlientIn.QUERY_FIND_BY_KLIENTINID, query = "SELECT k FROM KlientIn k WHERE k.id = :id"),
        @NamedQuery(name = KlientIn.QUERY_REMOVE, query = "DELETE from KlientIn where id = :id")
})
public class KlientIn implements Serializable {

    private static final long serialVersionUID = -2580930606326302310L;

    public static final String QUERY_FIND_ALL = "KlientIn.findAll";

    public static final String QUERY_FIND_BY_KLIENTINID = "KlientIn.findByKlientInId";

    public static final String QUERY_REMOVE = "KlientIn.remove";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false, length = 36)
    private String id;

    @Column(name = "KLIENTENNUMMER", length = 45)
    private String klientenNummer;

    @Column(name = "VORNAME", length = 45)
    private String vorname;

    @Column(name = "NACHNAME", length = 45)
    private String nachname;

    @Column(name = "GESCHLECHT", length = 1)
    private String geschlecht;

    @Column(name = "GEBURTSDATUM")
    private LocalDate geburtsdatum;

    @Column(name = "STERBEDATUM")
    private LocalDate sterbedatum;

    @Column(name = "TAETIGKEITSFELD_ID", length = 36)
    private String taetigkeitsfeldId;

    @Column(name = "TAETIGKEITSFELD_NUMMER", length = 45)
    private String taetigkeitsfeldNummer;

    @Column(name = "WEITERE_KONTAKTDATEN", length = 350)
    private String weitereKontaktdaten;

    @Column(name = "VERRECHNUNG_ID", length = 36)
    private String verrechnungId;

    @Column(name = "VERRECHNUNG_NUMMER", length = 15)
    private String verrechnungNummer;

    @Column(name = "SPENDE", length = 45)
    private String spende;

    @Column(name = "WERKVERTRAGSNUMMER", length = 45)
    private String werkvertragsNummer;

    @Column(name = "IMPACCT_GRUPPE", length = 45)
    private String impacctGruppe;

    @Column(name = "PFLEGESTUFE", length = 1)
    private String pflegestufe;

    @Column(name = "SOZIALVERSICHERUNGSNUMMER", length = 10)
    private String sozialversicherungsnummer;

    @Column(name = "SOZIALVERSICHERUNGSTRAEGER", length = 45)
    private String sozialversicherungstraeger;

    @Column(name = "DIAGNOSE", length = 500)
    private String diagnose;

    @Column(name = "DIAGNOSESCHLUESSEL", length = 45)
    private String diagnoseschluessel;

    @Column(name = "MUTTER_VERTRETERIN", length = 1)
    private String mutterVertreterin;

    @Column(name = "MUTTER_NAME", length = 45)
    private String mutterName;

    @Column(name = "MUTTER_GEBURTSDATUM")
    private LocalDate mutterGeburtsdatum;

    @Column(name = "MUTTER_TELEFONNUMMER", length = 20)
    private String mutterTelefonnummer;

    @Column(name = "VATER_VERTRETER", length = 1)
    private String vaterVertreter;

    @Column(name = "VATER_NAME", length = 45)
    private String vaterName;

    @Column(name = "VATER_GEBURTSDATUM")
    private LocalDate vaterGeburtsdatum;

    @Column(name = "VATER_TELEFONNUMMER", length = 20)
    private String vaterTelefonnummer;

    @Column(name = "SONSTIGE_VERTRETER", length = 1)
    private String sonstigeVertreter;

    @Column(name = "SONSTIGE_NAME", length = 45)
    private String sonstigeName;

    @Column(name = "SONSTIGE_GEBURTSDATUM")
    private LocalDate sonstigeGeburtsdatum;

    @Column(name = "SONSTIGE_TELEFONNUMMER", length = 20)
    private String sonstigeTelefonnummer;

    @Column(name = "STRASSE", length = 45)
    private String strasse;

    @Column(name = "MUTTER_EMAIL", length = 45)
    private String mutterEmail;

    @Column(name = "ORT", length = 45)
    private String ort;

    @Column(name = "VATER_EMAIL", length = 45)
    private String vaterEmail;

    @Column(name = "HAUPTWOHNSITZ", length = 1)
    private String hauptwohnsitz;

    @Column(name = "ZUWEISENDE_STELLE_KH", length = 45)
    private String zuweisendeStelleKH;

    @Column(name = "ZUWEISENDE_STELLE_KJH", length = 45)
    private String zuweisendeStelleKJH;

    @Column(name = "ZUWEISENDE_STELLE_SONSTIGE", length = 45)
    private String zuweisendeStelleSonstige;

    @Column(name = "KINDERFACHARZT_NAME", length = 45)
    private String kinderfacharztName;

    @Column(name = "KINDERFACHARZT_TELEFONNUMMER", length = 45)
    private String kinderfacharztTelefonnummer;

    @Column(name = "BETREUENDE_KRANKENPFLEGERIN", length = 45)
    private String betreuendeKrankenpflegerin;

    @Column(name = "WEITERE_KRANKENPFLEGERINNEN", length = 45)
    private String weitereKrankenpflegerinnen;

    @Column(name = "BETREUUNGSBEGINN")
    private LocalDate betreuungsbeginn;

    @Column(name = "BETREUUNGSENDE")
    private LocalDate betreuungsende;

    @Column(name = "KM_PRO_HB", length = 45)
    private String kmProHb;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKlientenNummer() {
        return klientenNummer;
    }

    public void setKlientenNummer(String klientenNummer) {
        this.klientenNummer = klientenNummer;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getGeschlecht() {
        return geschlecht;
    }

    public void setGeschlecht(String geschlecht) {
        this.geschlecht = geschlecht;
    }

    public LocalDate getGeburtsdatum() {
        return geburtsdatum;
    }

    public void setGeburtsdatum(LocalDate geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    public LocalDate getSterbedatum() {
        return sterbedatum;
    }

    public void setSterbedatum(LocalDate sterbedatum) {
        this.sterbedatum = sterbedatum;
    }

    public String getTaetigkeitsfeldId() {
        return taetigkeitsfeldId;
    }

    public void setTaetigkeitsfeldId(String taetigkeitsfeldId) {
        this.taetigkeitsfeldId = taetigkeitsfeldId;
    }

    public String getTaetigkeitsfeldNummer() {
        return taetigkeitsfeldNummer;
    }

    public void setTaetigkeitsfeldNummer(String taetigkeitsfeldNummer) {
        this.taetigkeitsfeldNummer = taetigkeitsfeldNummer;
    }

    public String getWeitereKontaktdaten() {
        return weitereKontaktdaten;
    }

    public void setWeitereKontaktdaten(String weitereKontaktdaten) {
        this.weitereKontaktdaten = weitereKontaktdaten;
    }

    public String getVerrechnungId() {
        return verrechnungId;
    }

    public void setVerrechnungId(String verrechnungId) {
        this.verrechnungId = verrechnungId;
    }

    public String getVerrechnungNummer() {
        return verrechnungNummer;
    }

    public void setVerrechnungNummer(String verrechnungNummer) {
        this.verrechnungNummer = verrechnungNummer;
    }

    public String getSpende() {
        return spende;
    }

    public void setSpende(String spende) {
        this.spende = spende;
    }

    public String getWerkvertragsNummer() {
        return werkvertragsNummer;
    }

    public void setWerkvertragsNummer(String werkvertragsNummer) {
        this.werkvertragsNummer = werkvertragsNummer;
    }

    public String getImpacctGruppe() {
        return impacctGruppe;
    }

    public void setImpacctGruppe(String impacctGruppe) {
        this.impacctGruppe = impacctGruppe;
    }

    public String getPflegestufe() {
        return pflegestufe;
    }

    public void setPflegestufe(String pflegestufe) {
        this.pflegestufe = pflegestufe;
    }

    public String getSozialversicherungsnummer() {
        return sozialversicherungsnummer;
    }

    public void setSozialversicherungsnummer(String sozialversicherungsnummer) {
        this.sozialversicherungsnummer = sozialversicherungsnummer;
    }

    public String getSozialversicherungstraeger() {
        return sozialversicherungstraeger;
    }

    public void setSozialversicherungstraeger(String sozialversicherungstraeger) {
        this.sozialversicherungstraeger = sozialversicherungstraeger;
    }

    public String getDiagnose() {
        return diagnose;
    }

    public void setDiagnose(String diagnose) {
        this.diagnose = diagnose;
    }

    public String getDiagnoseschluessel() {
        return diagnoseschluessel;
    }

    public void setDiagnoseschluessel(String diagnoseschluessel) {
        this.diagnoseschluessel = diagnoseschluessel;
    }

    public String getMutterVertreterin() {
        return mutterVertreterin;
    }

    public void setMutterVertreterin(String mutterVertreterin) {
        this.mutterVertreterin = mutterVertreterin;
    }

    public String getMutterName() {
        return mutterName;
    }

    public void setMutterName(String mutterName) {
        this.mutterName = mutterName;
    }

    public LocalDate getMutterGeburtsdatum() {
        return mutterGeburtsdatum;
    }

    public void setMutterGeburtsdatum(LocalDate mutterGeburtsdatum) {
        this.mutterGeburtsdatum = mutterGeburtsdatum;
    }

    public String getMutterTelefonnummer() {
        return mutterTelefonnummer;
    }

    public void setMutterTelefonnummer(String mutterTelefonnummer) {
        this.mutterTelefonnummer = mutterTelefonnummer;
    }

    public String getVaterVertreter() {
        return vaterVertreter;
    }

    public void setVaterVertreter(String vaterVertreter) {
        this.vaterVertreter = vaterVertreter;
    }

    public String getVaterName() {
        return vaterName;
    }

    public void setVaterName(String vaterName) {
        this.vaterName = vaterName;
    }

    public LocalDate getVaterGeburtsdatum() {
        return vaterGeburtsdatum;
    }

    public void setVaterGeburtsdatum(LocalDate vaterGeburtsdatum) {
        this.vaterGeburtsdatum = vaterGeburtsdatum;
    }

    public String getVaterTelefonnummer() {
        return vaterTelefonnummer;
    }

    public void setVaterTelefonnummer(String vaterTelefonnummer) {
        this.vaterTelefonnummer = vaterTelefonnummer;
    }

    public String getSonstigeVertreter() {
        return sonstigeVertreter;
    }

    public void setSonstigeVertreter(String sonstigeVertreter) {
        this.sonstigeVertreter = sonstigeVertreter;
    }

    public String getSonstigeName() {
        return sonstigeName;
    }

    public void setSonstigeName(String sonstigeName) {
        this.sonstigeName = sonstigeName;
    }

    public LocalDate getSonstigeGeburtsdatum() {
        return sonstigeGeburtsdatum;
    }

    public void setSonstigeGeburtsdatum(LocalDate sonstigeGeburtsdatum) {
        this.sonstigeGeburtsdatum = sonstigeGeburtsdatum;
    }

    public String getSonstigeTelefonnummer() {
        return sonstigeTelefonnummer;
    }

    public void setSonstigeTelefonnummer(String sonstigeTelefonnummer) {
        this.sonstigeTelefonnummer = sonstigeTelefonnummer;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getMutterEmail() {
        return mutterEmail;
    }

    public void setMutterEmail(String mutterEmail) {
        this.mutterEmail = mutterEmail;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getVaterEmail() {
        return vaterEmail;
    }

    public void setVaterEmail(String vaterEmail) {
        this.vaterEmail = vaterEmail;
    }

    public String getHauptwohnsitz() {
        return hauptwohnsitz;
    }

    public void setHauptwohnsitz(String hauptwohnsitz) {
        this.hauptwohnsitz = hauptwohnsitz;
    }

    public String getZuweisendeStelleKH() {
        return zuweisendeStelleKH;
    }

    public void setZuweisendeStelleKH(String zuweisendeStelleKH) {
        this.zuweisendeStelleKH = zuweisendeStelleKH;
    }

    public String getZuweisendeStelleKJH() {
        return zuweisendeStelleKJH;
    }

    public void setZuweisendeStelleKJH(String zuweisendeStelleKJH) {
        this.zuweisendeStelleKJH = zuweisendeStelleKJH;
    }

    public String getZuweisendeStelleSonstige() {
        return zuweisendeStelleSonstige;
    }

    public void setZuweisendeStelleSonstige(String zuweisendeStelleSonstige) {
        this.zuweisendeStelleSonstige = zuweisendeStelleSonstige;
    }

    public String getKinderfacharztName() {
        return kinderfacharztName;
    }

    public void setKinderfacharztName(String kinderfacharztName) {
        this.kinderfacharztName = kinderfacharztName;
    }

    public String getKinderfacharztTelefonnummer() {
        return kinderfacharztTelefonnummer;
    }

    public void setKinderfacharztTelefonnummer(String kinderfacharztTelefonnummer) {
        this.kinderfacharztTelefonnummer = kinderfacharztTelefonnummer;
    }

    public String getBetreuendeKrankenpflegerin() {
        return betreuendeKrankenpflegerin;
    }

    public void setBetreuendeKrankenpflegerin(String betreuendeKrankenpflegerin) {
        this.betreuendeKrankenpflegerin = betreuendeKrankenpflegerin;
    }

    public String getWeitereKrankenpflegerinnen() {
        return weitereKrankenpflegerinnen;
    }

    public void setWeitereKrankenpflegerinnen(String weitereKrankenpflegerinnen) {
        this.weitereKrankenpflegerinnen = weitereKrankenpflegerinnen;
    }

    public LocalDate getBetreuungsbeginn() {
        return betreuungsbeginn;
    }

    public void setBetreuungsbeginn(LocalDate betreuungsbeginn) {
        this.betreuungsbeginn = betreuungsbeginn;
    }

    public LocalDate getBetreuungsende() {
        return betreuungsende;
    }

    public void setBetreuungsende(LocalDate betreuungsende) {
        this.betreuungsende = betreuungsende;
    }

    public String getKmProHb() {
        return kmProHb;
    }

    public void setKmProHb(String kmProHb) {
        this.kmProHb = kmProHb;
    }
}