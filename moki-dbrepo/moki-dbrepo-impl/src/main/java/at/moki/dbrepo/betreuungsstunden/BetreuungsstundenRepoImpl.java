package at.moki.dbrepo.betreuungsstunden;

import at.moki.dbrepo.entitymanager.producer.MokiEntityManager;
import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.exception.MokiDbRepoException;
import at.moki.security.MokiRolesAllowed;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Green Arrow on 25.02.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class BetreuungsstundenRepoImpl implements BetreuungsstundenRepo, Serializable {

    private static final long serialVersionUID = 2298043439878950883L;

    @Inject
    @MokiEntityManager
    private EntityManager entityManager;


    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN,
            MokiUserRole.VERWALTUNG})
    @Override
    public List<at.moki.domainmodel.betreuungsstunden.Betreuungsstunden> findByKlientInId(String klientInId) {
        try {
            List<at.moki.dbrepo.entity.Betreuungsstunden> betreuungsstundenList = this.entityManager
                    .createNamedQuery(at.moki.dbrepo.entity.Betreuungsstunden.QUERY_FIND_BY_KLIENTINID,
                            at.moki.dbrepo.entity.Betreuungsstunden.class)
                    .setParameter("klientInId", klientInId)
                    .getResultList();

            return betreuungsstundenList.stream()
                    .map(this::mapToDomainmodel)
                    .sorted(Comparator.comparing(Betreuungsstunden::getJahr))
                    .collect(Collectors.toList());
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Diese betreuungsstunden existieren nicht, bitte ueberprüfen Sie ihre eingaben", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN,
            MokiUserRole.VERWALTUNG})
    @Override
    public void merge(Betreuungsstunden betreuungsstunden) {
        try {
            at.moki.dbrepo.entity.Betreuungsstunden betreuungsstundenEntity = mapToEntity(betreuungsstunden);
            this.entityManager.merge(betreuungsstundenEntity);
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Betreuungsstunden konnten nicht aktualisiert werden", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.VERWALTUNG})
    @Override
    public void remove(String klientInId) {
        try {
            this.entityManager.createNamedQuery(at.moki.dbrepo.entity.Betreuungsstunden.QUERY_REMOVE)
                    .setParameter("klientInId", klientInId)
                    .executeUpdate();
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Betreuungsstunden konnten nicht gelöscht werden", e);
        }
    }

    private at.moki.dbrepo.entity.Betreuungsstunden mapToEntity(Betreuungsstunden betreuungsstunden) {
        at.moki.dbrepo.entity.Betreuungsstunden betreuungsstundenEntity = new at.moki.dbrepo.entity.Betreuungsstunden();

        betreuungsstundenEntity.setId(betreuungsstunden.getId());
        betreuungsstundenEntity.setJahr(betreuungsstunden.getJahr());
        betreuungsstundenEntity.setStunden(betreuungsstunden.getStunden());
        betreuungsstundenEntity.setKlientInId(betreuungsstunden.getKlientInId());

        return betreuungsstundenEntity;
    }

    private Betreuungsstunden mapToDomainmodel(at.moki.dbrepo.entity.Betreuungsstunden betreuungsstundenEntity) {
        Betreuungsstunden betreuungsstunden = new Betreuungsstunden();

        betreuungsstunden.setId(betreuungsstundenEntity.getId());
        betreuungsstunden.setJahr(betreuungsstundenEntity.getJahr());
        betreuungsstunden.setStunden(betreuungsstundenEntity.getStunden());
        betreuungsstunden.setKlientInId(betreuungsstundenEntity.getKlientInId());

        return betreuungsstunden;
    }
}
