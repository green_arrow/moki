package at.moki.dbrepo.klientin;

import at.moki.dbrepo.entitymanager.producer.MokiEntityManager;
import at.moki.domainmodel.klientin.KlientInZusaetzlicheDokumente;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.exception.MokiDbRepoException;
import at.moki.security.MokiRolesAllowed;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Green Arrow on 27.01.2019.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class KlientInZusaetzlicheDokumenteRepoImpl implements KlientInZusaetzlicheDokumenteRepo {

    @Inject
    @MokiEntityManager
    private EntityManager entityManager;

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN,
            MokiUserRole.VERWALTUNG})
    @Override
    public List<KlientInZusaetzlicheDokumente> findAllByKlientInId(String klientInId) {
        try {
            List<at.moki.dbrepo.entity.KlientInZusaetzlicheDokumente> klientInZusaetzlicheDokumenteList = this.entityManager
                    .createNamedQuery(at.moki.dbrepo.entity.KlientInZusaetzlicheDokumente.QUERY_FIND_BY_KLIENTINID,
                            at.moki.dbrepo.entity.KlientInZusaetzlicheDokumente.class)
                    .setParameter("klientInId", klientInId)
                    .getResultList();

            return klientInZusaetzlicheDokumenteList.stream()
                    .map(this::mapToDomainmodel)
                    .sorted(Comparator.comparing(KlientInZusaetzlicheDokumente::getErstellungszeitpunkt)
                            .reversed())
                    .collect(Collectors.toList());
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("KlientInZusaetzlicheDokumente konnten nicht gelesen werden.", e);
        }
    }


    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN,
            MokiUserRole.VERWALTUNG})
    @Override
    public void merge(KlientInZusaetzlicheDokumente klientInZusaetzlicheDokumente) {
        try {
            at.moki.dbrepo.entity.KlientInZusaetzlicheDokumente klientInZusaetzlicheDokumenteEntity = mapToEntity(klientInZusaetzlicheDokumente);
            this.entityManager.merge(klientInZusaetzlicheDokumenteEntity);
            this.entityManager.flush();
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("KlientInZusaetzlicheDokumente konnte nicht aktualisiert werden.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN,
            MokiUserRole.VERWALTUNG})
    @Override
    public void removeById(String id) {
        try {
            this.entityManager.createNamedQuery(at.moki.dbrepo.entity.KlientInZusaetzlicheDokumente.QUERY_REMOVE_BY_ID)
                    .setParameter("id", id)
                    .executeUpdate();
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Die zu loeschenden KlientInZusaetzlicheDokumente existiert nicht.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN,
            MokiUserRole.VERWALTUNG})
    @Override
    public void removeAllByKlientInId(String klientInId) {
        try {
            this.entityManager.createNamedQuery(at.moki.dbrepo.entity.KlientInZusaetzlicheDokumente.QUERY_REMOVE_ALL_BY_KLIENTIN_ID)
                    .setParameter("klientInId", klientInId)
                    .executeUpdate();
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Die zu loeschenden KlientInZusaetzlicheDokumente existiert nicht.", e);
        }
    }

    /**
     * this method maps the entity to the domainmodel. however, it does not map the actual media ({@link javax.persistence.Lob}).
     * This is due to performance increase since the actual media file is only used when the user wants to download it.
     * The getter of the entity {@link at.moki.dbrepo.entity.KlientInZusaetzlicheDokumente} is only used in findById method.
     *
     * @param klientInZusaetzlicheDokumenteEntity entity to be mapped
     * @return the domainmodel
     */
    private KlientInZusaetzlicheDokumente mapToDomainmodel(at.moki.dbrepo.entity.KlientInZusaetzlicheDokumente klientInZusaetzlicheDokumenteEntity) {

        KlientInZusaetzlicheDokumente klientInZusaetzlicheDokumente = new KlientInZusaetzlicheDokumente();

        klientInZusaetzlicheDokumente.setId(klientInZusaetzlicheDokumenteEntity.getId());
        klientInZusaetzlicheDokumente.setDateiname(klientInZusaetzlicheDokumenteEntity.getDateiname());
        klientInZusaetzlicheDokumente.setErstellungszeitpunkt(klientInZusaetzlicheDokumenteEntity.getErstellungszeitpunkt());
        klientInZusaetzlicheDokumente.setKlientInId(klientInZusaetzlicheDokumenteEntity.getKlientInId());

        return klientInZusaetzlicheDokumente;
    }

    private at.moki.dbrepo.entity.KlientInZusaetzlicheDokumente mapToEntity(KlientInZusaetzlicheDokumente klientInZusaetzlicheDokumente) {
        at.moki.dbrepo.entity.KlientInZusaetzlicheDokumente entity = new at.moki.dbrepo.entity.KlientInZusaetzlicheDokumente();

        entity.setId(klientInZusaetzlicheDokumente.getId());
        entity.setDateiname(klientInZusaetzlicheDokumente.getDateiname());
        entity.setErstellungszeitpunkt(klientInZusaetzlicheDokumente.getErstellungszeitpunkt());
        entity.setKlientInId(klientInZusaetzlicheDokumente.getKlientInId());

        return entity;
    }
}
