package at.moki.dbrepo.finanzierung;

import at.moki.dbrepo.entitymanager.producer.MokiEntityManager;
import at.moki.domainmodel.finanzierung.Finanzierung;
import at.moki.domainmodel.finanzierung.Institutions;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.exception.MokiDbRepoException;
import at.moki.logger.MokiLogger;
import at.moki.security.MokiRolesAllowed;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Green Arrow on 25.02.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class FinanzierungRepoImpl implements FinanzierungRepo, Serializable {

    private static final long serialVersionUID = -3070641393288671132L;

    @Inject
    @MokiLogger
    private Logger logger;

    @Inject
    @MokiEntityManager
    private EntityManager entityManager;


    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN,
            MokiUserRole.VERWALTUNG})
    @Override
    public List<Finanzierung> findAll() {
        try {
            List<at.moki.dbrepo.entity.Finanzierung> userList = this.entityManager
                    .createNamedQuery(at.moki.dbrepo.entity.Finanzierung.QUERY_FIND_ALL,
                            at.moki.dbrepo.entity.Finanzierung.class)
                    .getResultList();

            return userList.stream()
                    .map(this::mapToDomainmodel)
                    .sorted(Comparator.comparing(Finanzierung::getInstitution))
                    .collect(Collectors.toList());
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Finanzierung Entities konnten nicht gelesen werden.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN,
            MokiUserRole.VERWALTUNG})
    @Override
    public List<Finanzierung> findByKlientInId(String klientInId) {
        try {
            List<at.moki.dbrepo.entity.Finanzierung> userList = this.entityManager
                    .createNamedQuery(at.moki.dbrepo.entity.Finanzierung.QUERY_FIND_BY_KLIENTINID,
                            at.moki.dbrepo.entity.Finanzierung.class)
                    .setParameter("klientInId", klientInId)
                    .getResultList();

            return userList.stream()
                    .map(this::mapToDomainmodel)
                    .collect(Collectors.toList());
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Finanzierungen konnten nicht gelesen werden.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN,
            MokiUserRole.VERWALTUNG})
    @Override
    public void merge(Finanzierung finanzierung) {
        try {
            at.moki.dbrepo.entity.Finanzierung finanzierungEntity = mapToEntity(finanzierung);
            this.entityManager.merge(finanzierungEntity);
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Finanzierung konnte nicht aktualisiert werden.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.VERWALTUNG})
    @Override
    public void removeAllby(String klientInId) {
        try {
            this.entityManager.createNamedQuery(at.moki.dbrepo.entity.Finanzierung.QUERY_DELETE_BY_KLIENTINID)
                    .setParameter("klientInId", klientInId)
                    .executeUpdate();
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Die zu loeschenden Finanzierungen existieren nicht.", e);
        }
    }

    private at.moki.dbrepo.entity.Finanzierung mapToEntity(Finanzierung finanzierung) {
        at.moki.dbrepo.entity.Finanzierung finanzierungEntity = new at.moki.dbrepo.entity.Finanzierung();

        finanzierungEntity.setId(finanzierung.getId());
        finanzierungEntity.setInstitution(finanzierung.getInstitution().getInstitutionFullName());
        finanzierungEntity.setKosten(convertBooleanToString(finanzierung.isKostenUebernahme()));
        finanzierungEntity.setSelbstbehalt(convertBooleanToString(finanzierung.isKostenSelbstbehalt()));
        finanzierungEntity.setKlientInId(finanzierung.getKlientInId());

        return finanzierungEntity;
    }

    private Finanzierung mapToDomainmodel(at.moki.dbrepo.entity.Finanzierung finanzierungEntity) {
        Finanzierung finanzierung = new Finanzierung();

        finanzierung.setId(finanzierungEntity.getId());
        finanzierung.setInstitution(convertToInstitution(finanzierungEntity.getInstitution()));
        finanzierung.setKostenUebernahme(convertStringToBoolean(finanzierungEntity.getKosten()));
        finanzierung.setKostenSelbstbehalt(convertStringToBoolean(finanzierungEntity.getSelbstbehalt()));
        finanzierung.setKlientInId(finanzierungEntity.getKlientInId());

        return finanzierung;
    }

    private String convertBooleanToString(boolean value) {
        return value ? "J" : "N";
    }

    private boolean convertStringToBoolean(String value) {
        return StringUtils.equalsIgnoreCase(value, "J");
    }

    private Institutions convertToInstitution(String institution) {

        if (StringUtils.equals(institution, Institutions.LAND.getInstitutionFullName())) {
            return Institutions.LAND;
        } else if (StringUtils.equals(institution, Institutions.MUKI.getInstitutionFullName())) {
            return Institutions.MUKI;
        } else if (StringUtils.equals(institution, Institutions.KINDER_JUGENDHILFE.getInstitutionFullName())) {
            return Institutions.KINDER_JUGENDHILFE;
        } else if (StringUtils.equals(institution, Institutions.VERSICHERUNG.getInstitutionFullName())) {
            return Institutions.VERSICHERUNG;
        } else if (StringUtils.equals(institution, Institutions.HOSPIZ.getInstitutionFullName())) {
            return Institutions.HOSPIZ;
        } else if (StringUtils.equals(institution, Institutions.SPENDEN.getInstitutionFullName())) {
            return Institutions.SPENDEN;
        } else if (StringUtils.equals(institution, Institutions.SOZIALVERSICHERUNG.getInstitutionFullName())) {
            return Institutions.SOZIALVERSICHERUNG;
        } else if (StringUtils.equals(institution, Institutions.PRIVAT.getInstitutionFullName())) {
            return Institutions.PRIVAT;
        } else if (StringUtils.equals(institution, Institutions.KIB.getInstitutionFullName())) {
            return Institutions.KIB;
        } else if (StringUtils.equals(institution, Institutions.ANDERE.getInstitutionFullName())) {
            return Institutions.ANDERE;
        } else {
            this.logger.error("Konnte daten zur Institution nicht mappen.");
            return null;
        }
    }
}
