package at.moki.dbrepo.user;

import at.moki.dbrepo.entitymanager.producer.MokiEntityManager;
import at.moki.domainmodel.user.MokiUser;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.exception.MokiDbRepoException;
import at.moki.security.MokiRolesAllowed;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Green Arrow on 22.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class UserRepoImpl implements UserRepo, Serializable {

    private static final long serialVersionUID = 1470558966210390532L;

    @Inject
    @MokiEntityManager
    private EntityManager entityManager;

    /**
     * no roles allowed here since this method is used over the log in display
     * @return List<MokiUser>
     */
    @Override
    public List<MokiUser> findAll() {
        try {
            List<at.moki.dbrepo.entity.User> userList = this.entityManager
                    .createNamedQuery(at.moki.dbrepo.entity.User.QUERY_FIND_ALL,
                            at.moki.dbrepo.entity.User.class)
                    .getResultList();

            return userList.stream()
                    .map(this::mapToDomainmodel)
                    .sorted(Comparator.comparing(MokiUser::getUsername))
                    .collect(Collectors.toList());
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("MokiUser konnten nicht gelesen werden", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR})
    @Override
    public List<MokiUser> findByUsername(String username) {
        try {
            List<at.moki.dbrepo.entity.User> userList = this.entityManager
                    .createNamedQuery(at.moki.dbrepo.entity.User.QUERY_FIND_BY_USERNAME,
                            at.moki.dbrepo.entity.User.class)
                    .setParameter("username", username)
                    .getResultList();

            return userList.stream()
                    .map(this::mapToDomainmodel)
                    .collect(Collectors.toList());
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Benutzername und/oder Passwort ist falsch, bitte ueberprüfen Sie ihre eingaben", e);
        }
    }

    /**
     * no roles allowed here since this method is used over the log in display
     * @param mokiUser to be merged
     */
    @Override
    public void merge(MokiUser mokiUser) {
        try {
            at.moki.dbrepo.entity.User userEntity = mapToEntity(mokiUser);
            this.entityManager.merge(userEntity);
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Die Daten des users konnte nicht aktualisiert werden", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR})
    @Override
    public void remove(String userId) {
        try {
            this.entityManager.createNamedQuery(at.moki.dbrepo.entity.User.QUERY_REMOVE)
                    .setParameter("id", userId)
                    .executeUpdate();
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Der zu loeschende MokiUser existiert nicht", e);
        }
    }

    private MokiUser mapToDomainmodel(at.moki.dbrepo.entity.User userEntity) {
        MokiUser mokiUser = new MokiUser();

        mokiUser.setId(userEntity.getId());
        mokiUser.setUsername(userEntity.getUsername());
        mokiUser.setPassword(userEntity.getPassword());
        mokiUser.setGesperrt(convertStringToBoolean(userEntity.getGesperrt()));
        mokiUser.setUserrechteId(userEntity.getUserrechteId());
        mokiUser.setKrankenpflegerinId(userEntity.getKrankenpflegerinId());
        mokiUser.setSperrungCounter(userEntity.getSperrungCounter());

        return mokiUser;
    }

    private at.moki.dbrepo.entity.User mapToEntity(MokiUser mokiUser) {
        at.moki.dbrepo.entity.User userEntity = new at.moki.dbrepo.entity.User();

        userEntity.setId(mokiUser.getId());
        userEntity.setUsername(mokiUser.getUsername());
        userEntity.setPassword(mokiUser.getPassword());
        userEntity.setGesperrt(convertBooleanToString(mokiUser.isGesperrt()));
        userEntity.setUserrechteId(mokiUser.getUserrechteId());
        userEntity.setKrankenpflegerinId(mokiUser.getKrankenpflegerinId());
        userEntity.setSperrungCounter(mokiUser.getSperrungCounter());

        return userEntity;
    }

    private String convertBooleanToString(boolean value) {
        return value ? "J" : "N";
    }

    private boolean convertStringToBoolean(String value) {
        return StringUtils.equalsIgnoreCase(value, "J");
    }
}
