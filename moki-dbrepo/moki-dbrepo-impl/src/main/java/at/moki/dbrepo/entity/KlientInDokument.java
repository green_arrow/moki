package at.moki.dbrepo.entity;

import javax.persistence.*;

/**
 * Created by Green Arrow on 05.07.2019.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Entity
@Table(name = "KLIENTIN_DOKUMENT")
@NamedQueries({
        @NamedQuery(name = KlientInDokument.QUERY_FIND_BY_ID, query = "SELECT k FROM KlientInDokument k WHERE k.klientInZuseatzlicheDokumenteId = :klientInZuseatzlicheDokumenteId"),
})
public class KlientInDokument {

    public static final String QUERY_FIND_BY_ID = "KlientInDokument.findById";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false, length = 36)
    private String id;

    @Lob
    @Column(name = "DATEI", nullable = false, length = Integer.MAX_VALUE)
    private byte[] datei;

    @Column(name = "KLIENTIN_ZUSAETZLICHE_DOKUMENTE_ID", nullable = false, length = 36)
    private String klientInZuseatzlicheDokumenteId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getDatei() {
        return datei;
    }

    public void setDatei(byte[] datei) {
        this.datei = datei;
    }

    public String getKlientInZuseatzlicheDokumenteId() {
        return klientInZuseatzlicheDokumenteId;
    }

    public void setKlientInZuseatzlicheDokumenteId(String klientInZuseatzlicheDokumenteId) {
        this.klientInZuseatzlicheDokumenteId = klientInZuseatzlicheDokumenteId;
    }
}
