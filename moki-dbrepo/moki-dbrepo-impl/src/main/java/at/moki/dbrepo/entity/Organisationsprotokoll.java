package at.moki.dbrepo.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by Green Arrow on 31.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Entity
@Table(name = "ORGANISATIONSPROTOKOLL")
@NamedQueries({
        @NamedQuery(name = Organisationsprotokoll.QUERY_FIND_BY_KLIENTINID, query = "SELECT o FROM Organisationsprotokoll o where o.klientInId = :klientInId"),
        @NamedQuery(name = Organisationsprotokoll.QUERY_REMOVE_BY_KLIENTINID, query = "DELETE from Organisationsprotokoll where klientInId = :klientInId")
})
public class Organisationsprotokoll implements Serializable {

    private static final long serialVersionUID = 8496574671827133472L;

    public static final String QUERY_FIND_BY_KLIENTINID = "Organisationsprotokoll.findByKlientInId";

    public static final String QUERY_REMOVE_BY_KLIENTINID = "Organisationsprotokoll.deleteByKlientInId";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false, length = 36)
    private String id;

    @Column(name = "TEXT", length = 200)
    private String text;

    @Column(name = "HANDSCHRIFT_DGKP", length = 45)
    private String handschriftDgkp;

    @Column(name = "AENDERUNGSZEITPUNKT")
    private LocalDateTime aenderungszeitpunkt;

    @Column(name = "KLIENTIN_ID", nullable = false, length = 36)
    private String klientInId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getHandschriftDgkp() {
        return handschriftDgkp;
    }

    public void setHandschriftDgkp(String handschriftDgkp) {
        this.handschriftDgkp = handschriftDgkp;
    }

    public LocalDateTime getAenderungszeitpunkt() {
        return aenderungszeitpunkt;
    }

    public void setAenderungszeitpunkt(LocalDateTime aenderungszeitpunkt) {
        this.aenderungszeitpunkt = aenderungszeitpunkt;
    }

    public String getKlientInId() {
        return klientInId;
    }

    public void setKlientInId(String klientInId) {
        this.klientInId = klientInId;
    }
}