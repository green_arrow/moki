package at.moki.dbrepo.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Green Arrow on 11.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Entity
@Table(name = "VERWANDTE")
@NamedQueries({
        @NamedQuery(name = Verwandte.QUERY_FIND_BY_ID, query = "SELECT v FROM Verwandte v WHERE v.klientInId = :klientInId"),
        @NamedQuery(name = Verwandte.QUERY_DELETE_BY_KLIENTIND, query = "DELETE from Verwandte where klientInId = :klientInId")
})
public class Verwandte implements Serializable {

    private static final long serialVersionUID = 4236368960682858594L;

    public static final String QUERY_FIND_BY_ID = "Verwandte.findById";

    public static final String QUERY_DELETE_BY_KLIENTIND = "Verwandte.deleteByKlientnInId";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false, length = 36)
    private String id;

    @Column(name = "KLIENTIN_ID", nullable = false, length = 36)
    private String klientInId;

    @Column(name = "VERWANDTE_1_ID", unique = true, length = 36)
    private String verwandte1Id;

    @Column(name = "VERWANDTE_2_ID", unique = true, length = 36)
    private String verwandte2Id;

    @Column(name = "VERWANDTE_3_ID", unique = true, length = 36)
    private String verwandte3Id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKlientInId() {
        return klientInId;
    }

    public void setKlientInId(String klientInId) {
        this.klientInId = klientInId;
    }

    public String getVerwandte1Id() {
        return verwandte1Id;
    }

    public void setVerwandte1Id(String verwandte1Id) {
        this.verwandte1Id = verwandte1Id;
    }

    public String getVerwandte2Id() {
        return verwandte2Id;
    }

    public void setVerwandte2Id(String verwandte2Id) {
        this.verwandte2Id = verwandte2Id;
    }

    public String getVerwandte3Id() {
        return verwandte3Id;
    }

    public void setVerwandte3Id(String verwandte3Id) {
        this.verwandte3Id = verwandte3Id;
    }
}