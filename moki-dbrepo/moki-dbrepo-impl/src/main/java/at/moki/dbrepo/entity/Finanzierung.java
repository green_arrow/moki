package at.moki.dbrepo.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Green Arrow on 31.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Entity
@Table(name = "FINANZIERUNG")
@NamedQueries({
        @NamedQuery(name = Finanzierung.QUERY_FIND_ALL, query = "SELECT f FROM Finanzierung f"),
        @NamedQuery(name = Finanzierung.QUERY_FIND_BY_KLIENTINID, query = "SELECT f FROM Finanzierung f where f.klientInId = :klientInId"),
        @NamedQuery(name = Finanzierung.QUERY_DELETE_BY_KLIENTINID, query = "DELETE from Finanzierung where klientInId = :klientInId")
})
public class Finanzierung implements Serializable {

    private static final long serialVersionUID = -4901405363743620989L;

    public static final String QUERY_FIND_ALL = "Finanzierung.findAll";

    public static final String QUERY_FIND_BY_KLIENTINID = "Finanzierung.findByKlientInId";

    public static final String QUERY_DELETE_BY_KLIENTINID = "Finanzierung.deleteByKlientInId";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false, length = 36)
    private String id;

    @Column(name = "INSTITUTION", length = 45)
    private String institution;

    @Column(name = "KOSTEN", length = 1)
    private String kosten;

    @Column(name = "SELBSTBEHALT", length = 1)
    private String selbstbehalt;

    @Column(name = "KLIENTIN_ID", nullable = false, length = 36)
    private String klientInId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getKosten() {
        return kosten;
    }

    public void setKosten(String kosten) {
        this.kosten = kosten;
    }

    public String getSelbstbehalt() {
        return selbstbehalt;
    }

    public void setSelbstbehalt(String selbstbehalt) {
        this.selbstbehalt = selbstbehalt;
    }

    public String getKlientInId() {
        return klientInId;
    }

    public void setKlientInId(String klientInId) {
        this.klientInId = klientInId;
    }
}