package at.moki.dbrepo.organisationsprotokoll;

import at.moki.dbrepo.entitymanager.producer.MokiEntityManager;
import at.moki.domainmodel.organisationsprotokoll.Organisationsprotokoll;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.exception.MokiDbRepoException;
import at.moki.security.MokiRolesAllowed;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Green Arrow on 18.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class OrganisationsprotokollRepoImpl implements OrganisationsprotokollRepo, Serializable {

    private static final long serialVersionUID = 475297493883789812L;

    @Inject
    @MokiEntityManager
    private EntityManager entityManager;


    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.VERWALTUNG,
            MokiUserRole.KRANKENPFLEGERIN})
    @Override
    public List<Organisationsprotokoll> findByKlientInId(String klientInId) {
        try {
            List<at.moki.dbrepo.entity.Organisationsprotokoll> organisationsprotokollList = this.entityManager
                    .createNamedQuery(at.moki.dbrepo.entity.Organisationsprotokoll.QUERY_FIND_BY_KLIENTINID,
                            at.moki.dbrepo.entity.Organisationsprotokoll.class)
                    .setParameter("klientInId", klientInId)
                    .getResultList();

            return organisationsprotokollList.stream()
                    .map(this::mapToDomainmodel)
                    .sorted(Comparator.comparing(Organisationsprotokoll::getAenderungszeitpunkt).reversed())
                    .collect(Collectors.toList());
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Organisationsprotokoll konnte nicht gelesen werden.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.VERWALTUNG,
            MokiUserRole.KRANKENPFLEGERIN})
    @Override
    public void merge(Organisationsprotokoll organisationsprotokoll) {
        try {
            at.moki.dbrepo.entity.Organisationsprotokoll organisationsprotokollEntity = mapToEntity(organisationsprotokoll);
            this.entityManager.merge(organisationsprotokollEntity);
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Organisationsprotokoll konnte nicht aktualisiert werden.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.VERWALTUNG})
    @Override
    public void removeByKlientInId(String klientInId) {
        try {
            this.entityManager.createNamedQuery(at.moki.dbrepo.entity.Organisationsprotokoll.QUERY_REMOVE_BY_KLIENTINID)
                    .setParameter("klientInId", klientInId)
                    .executeUpdate();
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Das zu loeschende Organisationsprotokoll existiert nicht.", e);
        }
    }

    private at.moki.dbrepo.entity.Organisationsprotokoll mapToEntity(Organisationsprotokoll organisationsprotokoll) {
        at.moki.dbrepo.entity.Organisationsprotokoll organisationsprotokollEntity = new at.moki.dbrepo.entity.Organisationsprotokoll();

        organisationsprotokollEntity.setId(organisationsprotokoll.getId());
        organisationsprotokollEntity.setText(organisationsprotokoll.getText());
        organisationsprotokollEntity.setHandschriftDgkp(organisationsprotokoll.getHandschriftDgkp());
        organisationsprotokollEntity.setAenderungszeitpunkt(organisationsprotokoll.getAenderungszeitpunkt());
        organisationsprotokollEntity.setKlientInId(organisationsprotokoll.getKlientInId());

        return organisationsprotokollEntity;
    }

    private Organisationsprotokoll mapToDomainmodel(at.moki.dbrepo.entity.Organisationsprotokoll organisationsprotokollEntity) {
        Organisationsprotokoll organisationsprotokoll = new Organisationsprotokoll();

        organisationsprotokoll.setId(organisationsprotokollEntity.getId());
        organisationsprotokoll.setText(organisationsprotokollEntity.getText());
        organisationsprotokoll.setHandschriftDgkp(organisationsprotokollEntity.getHandschriftDgkp());
        organisationsprotokoll.setAenderungszeitpunkt(organisationsprotokollEntity.getAenderungszeitpunkt());
        organisationsprotokoll.setKlientInId(organisationsprotokollEntity.getKlientInId());

        return organisationsprotokoll;
    }
}
