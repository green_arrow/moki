package at.moki.dbrepo.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Green Arrow on 31.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Entity
@Table(name = "USER")
@NamedQueries({
        @NamedQuery(name = User.QUERY_FIND_ALL, query = "SELECT u FROM User u"),
        @NamedQuery(name = User.QUERY_FIND_BY_USERNAME, query = "SELECT u FROM User u WHERE u.username = :username"),
        @NamedQuery(name = User.QUERY_REMOVE, query = "DELETE from User where id = :id")
})
public class User implements Serializable {

    private static final long serialVersionUID = -9091676822274789787L;

    public static final String QUERY_FIND_ALL = "User.findAll";

    public static final String QUERY_FIND_BY_USERNAME = "User.findByUsername";

    public static final String QUERY_REMOVE = "User.remove";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false, length = 36)
    private String id;

    @Column(name = "USERNAME", length = 45)
    private String username;

    @Column(name = "PASSWORD", length = 45)
    private String password;

    @Column(name = "GESPERRT", length = 45)
    private String gesperrt;

    @Column(name = "USERRECHTE_ID", length = 45, nullable = false)
    private String userrechteId;

    @Column(name = "KRANKENPFLEGERIN_ID", length = 45)
    private String krankenpflegerinId;

    @Column(name = "SPERRUNG_COUNTER", length = 45)
    private Integer sperrungCounter;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGesperrt() {
        return gesperrt;
    }

    public void setGesperrt(String gesperrt) {
        this.gesperrt = gesperrt;
    }

    public String getUserrechteId() {
        return userrechteId;
    }

    public void setUserrechteId(String userrechteId) {
        this.userrechteId = userrechteId;
    }

    public String getKrankenpflegerinId() {
        return krankenpflegerinId;
    }

    public void setKrankenpflegerinId(String krankenpflegerinId) {
        this.krankenpflegerinId = krankenpflegerinId;
    }

    public Integer getSperrungCounter() {
        return sperrungCounter;
    }

    public void setSperrungCounter(Integer sperrungCounter) {
        this.sperrungCounter = sperrungCounter;
    }
}