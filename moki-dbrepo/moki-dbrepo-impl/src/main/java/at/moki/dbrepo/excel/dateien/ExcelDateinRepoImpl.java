package at.moki.dbrepo.excel.dateien;

import at.moki.dbrepo.entitymanager.producer.MokiEntityManager;
import at.moki.domainmodel.excel.dateien.ExcelDatei;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.exception.MokiDbRepoException;
import at.moki.logger.MokiLogger;
import at.moki.security.MokiRolesAllowed;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import java.io.Serializable;

/**
 * @author Green Arrow
 * @date 17.08.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
public class ExcelDateinRepoImpl implements ExcelDateinRepo, Serializable {

    private static final long serialVersionUID = 1353529727285911274L;

    @Inject
    @MokiLogger
    private Logger logger;

    @Inject
    @MokiEntityManager
    private EntityManager entityManager;

    @Transactional
    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.VERWALTUNG})
    @Override
    public void merge(ExcelDatei excelDatei) {
        try {
            this.entityManager.merge(mapToEntity(excelDatei));
        } catch (Exception e) {
            throw new MokiDbRepoException("ExcelDatei konnte nicht aktualisiert werden.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.VERWALTUNG})
    @Override
    public ExcelDatei findByTaetigkeitsfeldId(String taetigkeitsfeldId) {
        try {
            at.moki.dbrepo.entity.ExcelDatei excelDatei = this.entityManager.createNamedQuery(
                    at.moki.dbrepo.entity.ExcelDatei.QUERY_FIND_BY_TAETIGKEITSFELD_ID, at.moki.dbrepo.entity.ExcelDatei.class)
                    .setParameter("taetigkeitsfeldId", taetigkeitsfeldId)
                    .getSingleResult();
            return mapToDomainmodel(excelDatei);
        } catch (PersistenceException e) {
            return null;
        }
    }

    private at.moki.dbrepo.entity.ExcelDatei mapToEntity(ExcelDatei domainmodel) {
        at.moki.dbrepo.entity.ExcelDatei entity = new at.moki.dbrepo.entity.ExcelDatei();

        entity.setId(domainmodel.getId());
        entity.setDateiname(domainmodel.getDateiname());
        entity.setDatei(domainmodel.getDatei());
        entity.setTaetigkeitsfeldId(domainmodel.getTaetigkeitsfeldId());

        return entity;
    }

    private ExcelDatei mapToDomainmodel(at.moki.dbrepo.entity.ExcelDatei entity) {
        ExcelDatei domainmodel = new ExcelDatei();

        domainmodel.setId(entity.getId());
        domainmodel.setDateiname(entity.getDateiname());
        domainmodel.setDatei(entity.getDatei());
        domainmodel.setTaetigkeitsfeldId(entity.getTaetigkeitsfeldId());

        return domainmodel;
    }
}
