package at.moki.dbrepo.verrechnung;

import at.moki.dbrepo.entitymanager.producer.MokiEntityManager;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.domainmodel.verrechnung.Verrechnung;
import at.moki.exception.MokiDbRepoException;
import at.moki.security.MokiRolesAllowed;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Green Arrow
 * @date 21.07.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
public class VerrechnungRepoImpl implements VerrechnungRepo, Serializable {

    private static final long serialVersionUID = 1818458634271367452L;

    @Inject
    @MokiEntityManager
    private EntityManager entityManager;

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.VERWALTUNG})
    @Override
    public List<Verrechnung> findAll() {
        try {
            return this.entityManager.createNamedQuery(at.moki.dbrepo.entity.Verrechnung.QUERY_FIND_ALL, at.moki.dbrepo.entity.Verrechnung.class)
                    .getResultList()
                    .stream()
                    .map(this::mapToDomainmodel)
                    .collect(Collectors.toList());
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Verrechnung existiern nicht, bitte ueberprüfen Sie ihre Eingaben.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.VERWALTUNG})
    @Override
    public Verrechnung findByKlienInId(String id) {
        try {
            at.moki.dbrepo.entity.Verrechnung verrechnungEntity = this.entityManager.createNamedQuery(at.moki.dbrepo.entity.Verrechnung.QUERY_FIND_BY_ID, at.moki.dbrepo.entity.Verrechnung.class)
                    .setParameter("id", id)
                    .getSingleResult();

            return mapToDomainmodel(verrechnungEntity);
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Verrechnung existiern nicht, bitte ueberprüfen Sie ihre Eingaben.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.VERWALTUNG})
    @Transactional
    @Override
    public void merge(Verrechnung verrechnung) {
        try {
            at.moki.dbrepo.entity.Verrechnung verrechnungEntity = mapToEntity(verrechnung);
            this.entityManager.merge(verrechnungEntity);
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Die Daten der Verrechnung konnten nicht aktualisiert werden", e);
        }
    }

    private Verrechnung mapToDomainmodel(at.moki.dbrepo.entity.Verrechnung verrechnungEntity) {

        Verrechnung verrechnung = new Verrechnung();

        verrechnung.setId(verrechnungEntity.getId());
        verrechnung.setBezeichnung(verrechnungEntity.getBezeichnung());

        return verrechnung;

    }

    private at.moki.dbrepo.entity.Verrechnung mapToEntity(Verrechnung verrechnung) {

        at.moki.dbrepo.entity.Verrechnung verrechnungEntity = new at.moki.dbrepo.entity.Verrechnung();

        verrechnungEntity.setId(verrechnung.getId());
        verrechnungEntity.setBezeichnung(verrechnung.getBezeichnung());

        return verrechnungEntity;
    }
}
