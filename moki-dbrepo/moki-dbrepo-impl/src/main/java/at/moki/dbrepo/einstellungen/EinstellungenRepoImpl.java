package at.moki.dbrepo.einstellungen;

import at.moki.dbrepo.entitymanager.producer.MokiEntityManager;
import at.moki.einstellungen.Einstellungen;
import at.moki.exception.MokiDbRepoException;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;

/**
 * @author Green Arrow
 * @date 08.08.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
public class EinstellungenRepoImpl implements EinstellungenRepo, Serializable {

    private static final long serialVersionUID = 78022915137798374L;

    @Inject
    @MokiEntityManager
    private EntityManager entityManager;

    @Override
    public Einstellungen findByTaetigkeitsfeld() {
        try {
            at.moki.dbrepo.entity.Einstellungen entity = this.entityManager.createNamedQuery(at.moki.dbrepo.entity.Einstellungen.QUERY_FIND_BY_TAETIGKEITSFELD,
                    at.moki.dbrepo.entity.Einstellungen.class)
                    .getSingleResult();
            return mapToDomainmodel(entity);
        } catch (NoResultException e) {
            return null;
        }
    }

    @Transactional
    @Override
    public void merge(Einstellungen einstellungen) {
        try {
            at.moki.dbrepo.entity.Einstellungen krankenpflegerinEntity = mapToEntity(einstellungen);
            this.entityManager.merge(krankenpflegerinEntity);
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Die Daten der Einstellungen konnten nicht aktualisiert werden.", e);
        }
    }

    private at.moki.dbrepo.entity.Einstellungen mapToEntity(Einstellungen domainmodel) {

        at.moki.dbrepo.entity.Einstellungen entity = new at.moki.dbrepo.entity.Einstellungen();

        entity.setId(domainmodel.getId());
        entity.setFachlichkeit(domainmodel.getFachlichkeit());
        entity.setInhalt(domainmodel.getInhalt().getBytes(StandardCharsets.UTF_8));

        return entity;
    }

    private Einstellungen mapToDomainmodel(at.moki.dbrepo.entity.Einstellungen entity) {
        Einstellungen einstellungen = new Einstellungen();

        einstellungen.setId(entity.getId());
        einstellungen.setFachlichkeit(entity.getFachlichkeit());
        einstellungen.setInhalt(new String(entity.getInhalt()));

        return einstellungen;
    }
}
