package at.moki.dbrepo.klientin;

import at.moki.dbrepo.entity.KlientInDokument;
import at.moki.dbrepo.entitymanager.producer.MokiEntityManager;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.exception.MokiDbRepoException;
import at.moki.security.MokiRolesAllowed;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

/**
 * Created by Green Arrow on 07.07.19.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class KlientInDokumentRepoImpl implements KlientInDokumentRepo {

    @Inject
    @MokiEntityManager
    private EntityManager entityManager;

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN,
            MokiUserRole.VERWALTUNG})
    @Override
    public byte[] findById(String id) {
        try {
            return this.entityManager
                    .createNamedQuery(at.moki.dbrepo.entity.KlientInDokument.QUERY_FIND_BY_ID,
                            at.moki.dbrepo.entity.KlientInDokument.class)
                    .setParameter("klientInZuseatzlicheDokumenteId", id)
                    .getSingleResult()
                    .getDatei();
        } catch (PersistenceException e) {
            throw new MokiDbRepoException(String.format("KlientInDokument mit der id %s konnte nicht geladen werden.", id), e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN,
            MokiUserRole.VERWALTUNG})
    @Override
    public void merge(String id, byte[] dokument, String klientInZuseatzlicheDokumenteId) {
        try {
            KlientInDokument klientInDokument = new KlientInDokument();
            klientInDokument.setId(id);
            klientInDokument.setDatei(dokument);
            klientInDokument.setKlientInZuseatzlicheDokumenteId(klientInZuseatzlicheDokumenteId);

            this.entityManager.merge(klientInDokument);
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("KlientInDokument konnte nicht aktualisiert werden.", e);
        }
    }

}
