package at.moki.dbrepo.entity;

import javax.persistence.*;

/**
 * @author Green Arrow
 * @date 03.08.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Entity
@Table(name = "TAETIGKEITSFELD")
@NamedQueries({
        @NamedQuery(name = Taetigkeitsfeld.QUERY_FIND_ALL, query = "SELECT t FROM Taetigkeitsfeld t"),
        @NamedQuery(name = Taetigkeitsfeld.QUERY_FIND_BY_TAETIGKEITSFELD_ID, query = "SELECT t FROM Taetigkeitsfeld t WHERE t.id = :id"),
        @NamedQuery(name = Taetigkeitsfeld.QUERY_FIND_BY_TAETIGKEITSFELD_BEZEICHNUNG, query = "SELECT t FROM Taetigkeitsfeld t WHERE t.bezeichnung = :bezeichnung"),
})
public class Taetigkeitsfeld {

    public static final String QUERY_FIND_ALL = "Taetigkeitsfeld.findAll";
    public static final String QUERY_FIND_BY_TAETIGKEITSFELD_ID = "Taetigkeitsfeld.findByTaetigkeitsfeldId";
    public static final String QUERY_FIND_BY_TAETIGKEITSFELD_BEZEICHNUNG = "Taetigkeitsfeld.findByTaetigkeitsfeldBezeichnung";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", unique = true, nullable = false, length = 36)
    private String id;

    @Column(name = "BEZEICHNUNG", unique = true, nullable = false, length = 10)
    private String bezeichnung;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }
}
