package at.moki.dbrepo.userrechte;

import at.moki.dbrepo.entitymanager.producer.MokiEntityManager;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.domainmodel.userrechte.Userrechte;
import at.moki.exception.MokiDbRepoException;
import at.moki.security.MokiRolesAllowed;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Green Arrow on 25.02.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class UserrechteRepoImpl implements UserrechteRepo, Serializable {

    private static final long serialVersionUID = 4160496699229049247L;

    @Inject
    @MokiEntityManager
    private EntityManager entityManager;


    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR})
    @Override
    public List<Userrechte> findAll() {
        try {
            List<at.moki.dbrepo.entity.Userrechte> userList = this.entityManager
                    .createNamedQuery(at.moki.dbrepo.entity.Userrechte.QUERY_FIND_ALL,
                            at.moki.dbrepo.entity.Userrechte.class)
                    .getResultList();

            return userList.stream()
                    .map(this::mapToDomainmodel)
                    .collect(Collectors.toList());
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Userrechte konnten nicht gelesen werden.", e);
        }
    }

    /**
     * no roles allowed here since this method is used over the log in display
     * @param id to be searched
     * @return Userrechte
     */
    @Override
    public Userrechte findById(String id) {
        try {
            at.moki.dbrepo.entity.Userrechte userrechte = (at.moki.dbrepo.entity.Userrechte) this.entityManager
                    .createNamedQuery(at.moki.dbrepo.entity.Userrechte.QUERY_FIND_BY_ID)
                    .setParameter("id", id)
                    .getSingleResult();

            return mapToDomainmodel(userrechte);
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Userrechte mit angegebener existiern nicht, bitte ueberprüfen Sie ihre Eingaben.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR})
    @Override
    public Userrechte findByBezeichnung(MokiUserRole mokiUserRole) {
        try {
            at.moki.dbrepo.entity.Userrechte userrechte = (at.moki.dbrepo.entity.Userrechte) this.entityManager
                    .createNamedQuery(at.moki.dbrepo.entity.Userrechte.QUERY_FIND_BY_BEZEICHNUNG)
                    .setParameter("bezeichnung", mokiUserRole.getRolename())
                    .getSingleResult();

            return mapToDomainmodel(userrechte);
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Userrechte existieren nicht, bitte ueberprüfen Sie ihre Eingaben.", e);
        }
    }

    private Userrechte mapToDomainmodel(at.moki.dbrepo.entity.Userrechte userrechteEntity) {
        Userrechte userrechte = new Userrechte();

        userrechte.setId(userrechteEntity.getId());
        userrechte.setMokiUserRole(MokiUserRole.valueOf(userrechteEntity.getBezeichnung()));

        return userrechte;
    }
}
