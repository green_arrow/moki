package at.moki.dbrepo.verwandte;

import at.moki.dbrepo.entitymanager.producer.MokiEntityManager;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.domainmodel.verwandte.Verwandte;
import at.moki.exception.MokiDbRepoException;
import at.moki.security.MokiRolesAllowed;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Green Arrow on 12.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class VerwandteRepoImpl implements VerwandteRepo, Serializable {

    private static final long serialVersionUID = -5405519550291182064L;

    @Inject
    @MokiEntityManager
    private EntityManager entityManager;


    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN,
            MokiUserRole.VERWALTUNG})
    @Override
    public Verwandte findByKlientInId(String klientInId) {
        try {
            at.moki.dbrepo.entity.Verwandte verwandte = (at.moki.dbrepo.entity.Verwandte) this.entityManager
                    .createNamedQuery(at.moki.dbrepo.entity.Verwandte.QUERY_FIND_BY_ID)
                    .setParameter("klientInId", klientInId)
                    .getSingleResult();

            return mapToDomainmodel(verwandte);
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Verwandte existieren nicht, bitte ueberprüfen Sie ihre Eingaben.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN,
            MokiUserRole.VERWALTUNG})
    @Override
    public void merge(Verwandte verwandte) {
        try {
            at.moki.dbrepo.entity.Verwandte verwandteEntity = mapToEntity(verwandte);
            this.entityManager.merge(verwandteEntity);
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Die Daten der Verwandten konnten nicht aktualisiert werden.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.VERWALTUNG})
    @Override
    public void removeByKlientInId(String klientInId) {
        try {
            this.entityManager.createNamedQuery(at.moki.dbrepo.entity.Verwandte.QUERY_DELETE_BY_KLIENTIND)
                    .setParameter("klientInId", klientInId)
                    .executeUpdate();
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Die zu loeschenden Verwandten mit der id existieren nicht.", e);
        }
    }

    private at.moki.dbrepo.entity.Verwandte mapToEntity(Verwandte verwandte) {
        at.moki.dbrepo.entity.Verwandte verwandteEntity = new at.moki.dbrepo.entity.Verwandte();

        verwandteEntity.setId(verwandte.getId());
        verwandteEntity.setKlientInId(verwandte.getKlientInId());
        verwandteEntity.setVerwandte1Id(verwandte.getVerwandteList().get(0));
        verwandteEntity.setVerwandte2Id(verwandte.getVerwandteList().get(1));

        if (verwandte.getVerwandteList().size() == 3) {
            verwandteEntity.setVerwandte3Id(verwandte.getVerwandteList().get(2));
        }

        return verwandteEntity;
    }

    private Verwandte mapToDomainmodel(at.moki.dbrepo.entity.Verwandte verwandteEntity) {
        Verwandte verwandte = new Verwandte();

        verwandte.setId(verwandteEntity.getId());
        verwandte.setKlientInId(verwandteEntity.getKlientInId());

        verwandte.setVerwandteList(convertVerwandteToList(
                verwandteEntity.getVerwandte1Id(),
                verwandteEntity.getVerwandte2Id(),
                verwandteEntity.getVerwandte3Id()));

        return verwandte;
    }

    private List<String> convertVerwandteToList(String verwandte1, String verwandte2, String verwandte3) {
        return Stream.of(verwandte1, verwandte2, verwandte3)
                .filter(StringUtils::isNotEmpty)
                .collect(Collectors.toList());
    }
}
