package at.moki.dbrepo.klientin;

import at.moki.dbrepo.entitymanager.producer.MokiEntityManager;
import at.moki.domainmodel.klientin.Taetigkeitsfeld;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.exception.MokiDbRepoException;
import at.moki.security.MokiRolesAllowed;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Green Arrow
 * @date 03.08.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
public class TaetigkeitsfeldRepoImpl implements TaetigkeitsfeldRepo, Serializable {

    private static final long serialVersionUID = 4798388876708482460L;

    @Inject
    @MokiEntityManager
    private EntityManager entityManager;

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.VERWALTUNG})
    @Override
    public List<Taetigkeitsfeld> findAll() {
        try {
            return this.entityManager.createNamedQuery(at.moki.dbrepo.entity.Taetigkeitsfeld.QUERY_FIND_ALL, at.moki.dbrepo.entity.Taetigkeitsfeld.class)
                    .getResultList()
                    .stream()
                    .map(this::mapToDomainmodel)
                    .sorted(Comparator.comparing(Taetigkeitsfeld::getBezeichnung))
                    .collect(Collectors.toList());
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Taetigkeitsfeld konnten nicht gelesen werden.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.VERWALTUNG})
    @Override
    public Taetigkeitsfeld findById(String taetigkeitsfeldId) {
        try {
            at.moki.dbrepo.entity.Taetigkeitsfeld klientIn = (at.moki.dbrepo.entity.Taetigkeitsfeld) this.entityManager
                    .createNamedQuery(at.moki.dbrepo.entity.Taetigkeitsfeld.QUERY_FIND_BY_TAETIGKEITSFELD_ID)
                    .setParameter("id", taetigkeitsfeldId)
                    .getSingleResult();

            return mapToDomainmodel(klientIn);
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Taetigkeitsfeld existiert nicht, bitte ueberprüfen Sie ihre Eingaben.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.VERWALTUNG})
    @Override
    public Taetigkeitsfeld findByBezeichnung(String bezeichnung) {
        try {
            at.moki.dbrepo.entity.Taetigkeitsfeld klientIn = (at.moki.dbrepo.entity.Taetigkeitsfeld) this.entityManager
                    .createNamedQuery(at.moki.dbrepo.entity.Taetigkeitsfeld.QUERY_FIND_BY_TAETIGKEITSFELD_BEZEICHNUNG)
                    .setParameter("bezeichnung", bezeichnung)
                    .getSingleResult();

            return mapToDomainmodel(klientIn);
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Taetigkeitsfeld existiert nicht, bitte ueberprüfen Sie ihre Eingaben.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.VERWALTUNG
    })
    @Transactional
    @Override
    public void merge(Taetigkeitsfeld taetigkeitsfeld) {
        try {
            this.entityManager.merge(mapToEntity(taetigkeitsfeld));
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Taetigkeitsfeld mit der konnte nicht aktualisiert werden.", e);
        }
    }

    private at.moki.dbrepo.entity.Taetigkeitsfeld mapToEntity(Taetigkeitsfeld taetigkeitsfeld) {

        at.moki.dbrepo.entity.Taetigkeitsfeld entity = new at.moki.dbrepo.entity.Taetigkeitsfeld();

        entity.setId(taetigkeitsfeld.getId());
        entity.setBezeichnung(taetigkeitsfeld.getBezeichnung());

        return entity;
    }

    private Taetigkeitsfeld mapToDomainmodel(at.moki.dbrepo.entity.Taetigkeitsfeld entity) {

        Taetigkeitsfeld taetigkeitsfeld = new Taetigkeitsfeld();

        taetigkeitsfeld.setId(entity.getId());
        taetigkeitsfeld.setBezeichnung(entity.getBezeichnung());

        return taetigkeitsfeld;
    }
}
