package at.moki.dbrepo.entity;

import javax.persistence.*;

/**
 * @author Green Arrow
 * @date 21.07.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Entity
@Table(name = "VERRECHNUNG")
@NamedQueries({
        @NamedQuery(name = Verrechnung.QUERY_FIND_ALL, query = "SELECT verrechnung FROM Verrechnung verrechnung"),
        @NamedQuery(name = Verrechnung.QUERY_FIND_BY_ID, query = "SELECT verrechnung FROM Verrechnung verrechnung where verrechnung.id = :id")
})
public class Verrechnung {

    public static final String QUERY_FIND_ALL = "Verrechnung.findAll";
    public static final String QUERY_FIND_BY_ID = "Verrechnung.findById";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false, length = 36)
    private String id;

    @Column(name = "BEZEICHNUNG", length = 45)
    private String bezeichnung;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }
}
