package at.moki.dbrepo.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Green Arrow on 31.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Entity
@Table(name = "KRANKENPFLEGERIN")
@NamedQueries({
        @NamedQuery(name = Krankenpflegerin.QUERY_FIND_ALL, query = "SELECT k FROM Krankenpflegerin k"),
        @NamedQuery(name = Krankenpflegerin.QUERY_FIND_BY_KLIENTINID, query = "SELECT k FROM Krankenpflegerin k WHERE k.id = :id"),
        @NamedQuery(name = Krankenpflegerin.QUERY_REMOVE, query = "DELETE from Krankenpflegerin where id = :id")
})
public class Krankenpflegerin implements Serializable {

    private static final long serialVersionUID = 360993502752078417L;

    public static final String QUERY_FIND_ALL = "Krankenpflegerin.findAll";

    public static final String QUERY_FIND_BY_KLIENTINID = "Krankenpflegerin.findbyKlientInId";

    public static final String QUERY_REMOVE = "Krankenpflegerin.remove";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false, length = 36)
    private String id;

    @Column(length = 45)
    private String vorname;

    @Column(length = 45)
    private String nachname;

    @Column(length = 45)
    private String bezeichnung;

    @Column(length = 45)
    private String strasse;

    @Column(length = 45)
    private String postleitzahl;

    @Column(length = 45)
    private String telefonnummer;

    @Column(length = 45)
    private String bankverbindung;

    @Column(length = 45)
    private String iban;

    @Column(length = 45)
    private String bic;

    @Column(length = 45)
    private String email;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getPostleitzahl() {
        return postleitzahl;
    }

    public void setPostleitzahl(String postleitzahl) {
        this.postleitzahl = postleitzahl;
    }

    public String getTelefonnummer() {
        return telefonnummer;
    }

    public void setTelefonnummer(String telefonnummer) {
        this.telefonnummer = telefonnummer;
    }

    public String getBankverbindung() {
        return bankverbindung;
    }

    public void setBankverbindung(String bankverbindung) {
        this.bankverbindung = bankverbindung;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}