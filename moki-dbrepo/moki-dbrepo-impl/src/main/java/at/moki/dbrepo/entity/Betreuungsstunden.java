package at.moki.dbrepo.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Green Arrow on 31.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Entity
@Table(name = "BETREUUNGSSTUNDEN")
@NamedQueries({
        @NamedQuery(name = Betreuungsstunden.QUERY_FIND_BY_KLIENTINID, query = "SELECT b FROM Betreuungsstunden b WHERE b.klientInId = :klientInId"),
        @NamedQuery(name = Betreuungsstunden.QUERY_REMOVE, query = "DELETE from Betreuungsstunden where klientInId = :klientInId"),
})
public class Betreuungsstunden implements Serializable {

    private static final long serialVersionUID = 5587815053503824350L;

    public static final String QUERY_FIND_BY_KLIENTINID = "Betreuungsstunden.findByKlientInId";

    public static final String QUERY_REMOVE = "Betreuungsstunden.remove";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false, length = 36)
    private String id;

    @Column(name = "JAHR", length = 45)
    private int jahr;

    @Column(name = "STUNDEN", length = 45)
    private String stunden;

    @Column(name = "KLIENTIN_ID", length = 36)
    private String klientInId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getJahr() {
        return jahr;
    }

    public void setJahr(int jahr) {
        this.jahr = jahr;
    }

    public String getStunden() {
        return stunden;
    }

    public void setStunden(String stunden) {
        this.stunden = stunden;
    }

    public String getKlientInId() {
        return klientInId;
    }

    public void setKlientInId(String klientInId) {
        this.klientInId = klientInId;
    }
}