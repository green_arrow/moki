package at.moki.dbrepo.entity;

import javax.persistence.*;

/**
 * @author Green Arrow
 * @date 08.08.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Entity
@Table(name = "EINSTELLUNGEN")
@NamedQueries({
        @NamedQuery(name = Einstellungen.QUERY_FIND_BY_TAETIGKEITSFELD, query = "SELECT e FROM Einstellungen e where e.fachlichkeit = 'TAETIGKEITSFELDER'"),
})
public class Einstellungen {

    public static final String QUERY_FIND_BY_TAETIGKEITSFELD = "Einstellungen.findByTaetigkeitsfeld";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", unique = true, nullable = false, length = 36)
    private String id;

    @Column(name = "FACHLICHKEIT", unique = true, nullable = false, length = 45)
    private String fachlichkeit;

    @Lob
    @Column(name = "INHALT", nullable = false, length = Integer.MAX_VALUE)
    private byte[] inhalt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFachlichkeit() {
        return fachlichkeit;
    }

    public void setFachlichkeit(String fachlichkeit) {
        this.fachlichkeit = fachlichkeit;
    }

    public byte[] getInhalt() {
        return inhalt;
    }

    public void setInhalt(byte[] inhalt) {
        this.inhalt = inhalt;
    }
}

