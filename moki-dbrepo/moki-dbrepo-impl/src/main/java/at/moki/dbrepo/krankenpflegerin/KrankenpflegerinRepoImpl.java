package at.moki.dbrepo.krankenpflegerin;

import at.moki.dbrepo.entitymanager.producer.MokiEntityManager;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.exception.MokiDbRepoException;
import at.moki.security.MokiRolesAllowed;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Green Arrow on 01.01.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class KrankenpflegerinRepoImpl implements KrankenpflegerinRepo, Serializable {

    private static final long serialVersionUID = 4806669789343293260L;

    @Inject
    @MokiEntityManager
    private EntityManager entityManager;


    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN})
    @Override
    public List<Krankenpflegerin> findAll() {
        try {
            List<at.moki.dbrepo.entity.Krankenpflegerin> krankenpflegerinList = this.entityManager
                    .createNamedQuery(at.moki.dbrepo.entity.Krankenpflegerin.QUERY_FIND_ALL,
                            at.moki.dbrepo.entity.Krankenpflegerin.class)
                    .getResultList();

            return krankenpflegerinList.stream()
                    .map(this::mapToDomainmodel)
                    .sorted(Comparator.comparing(Krankenpflegerin::getVorname)
                            .thenComparing(Krankenpflegerin::getNachname))
                    .collect(Collectors.toList());
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Krankenpflegerinnen konnten nicht gelesen werden.", e);
        }
    }

    /**
     * no roles allowed here since this method is used over the log in display
     * @param id to be searched
     * @return Krankenpflegerin
     */
    @Override
    public Krankenpflegerin findByKlientInId(String id) {
        try {
            at.moki.dbrepo.entity.Krankenpflegerin krankenpflegerin = (at.moki.dbrepo.entity.Krankenpflegerin)
                    this.entityManager.createNamedQuery(at.moki.dbrepo.entity.Krankenpflegerin.QUERY_FIND_BY_KLIENTINID)
                            .setParameter("id", id)
                            .getSingleResult();
            return mapToDomainmodel(krankenpflegerin);
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Krankenpflegerin existiert nicht, bitte ueberprüfen Sie ihre eingaben.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN})
    @Override
    public void merge(Krankenpflegerin krankenpflegerin) {
        try {
            at.moki.dbrepo.entity.Krankenpflegerin krankenpflegerinEntity = mapToEntity(krankenpflegerin);
            this.entityManager.merge(krankenpflegerinEntity);
            this.entityManager.flush();
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Die Daten der Krankenpflegerin konnten nicht aktualisiert werden.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR})
    @Override
    public void remove(String krankenpflegerinId) {
        try {
            this.entityManager.createNamedQuery(at.moki.dbrepo.entity.Krankenpflegerin.QUERY_REMOVE)
                    .setParameter("id", krankenpflegerinId)
                    .executeUpdate();
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Die zu loeschende Krankenpflegerin existiert nicht.", e);
        }
    }

    private Krankenpflegerin mapToDomainmodel(at.moki.dbrepo.entity.Krankenpflegerin krankenpflegerinEntity) {
        Krankenpflegerin krankenpflegerin = new Krankenpflegerin();

        krankenpflegerin.setId(krankenpflegerinEntity.getId());
        krankenpflegerin.setVorname(krankenpflegerinEntity.getVorname());
        krankenpflegerin.setNachname(krankenpflegerinEntity.getNachname());
        krankenpflegerin.setBezeichnung(krankenpflegerinEntity.getBezeichnung());
        krankenpflegerin.setStrasse(krankenpflegerinEntity.getStrasse());
        krankenpflegerin.setPostleitzahl(krankenpflegerinEntity.getPostleitzahl());
        krankenpflegerin.setTelefonnummer(krankenpflegerinEntity.getTelefonnummer());
        krankenpflegerin.setBankverbindung(krankenpflegerinEntity.getBankverbindung());
        krankenpflegerin.setIban(krankenpflegerinEntity.getIban());
        krankenpflegerin.setBic(krankenpflegerinEntity.getBic());
        krankenpflegerin.setEmail(krankenpflegerinEntity.getEmail());

        return krankenpflegerin;
    }

    private at.moki.dbrepo.entity.Krankenpflegerin mapToEntity(Krankenpflegerin krankenpflegerin) {
        at.moki.dbrepo.entity.Krankenpflegerin krankenpflegerinEntity = new at.moki.dbrepo.entity.Krankenpflegerin();

        krankenpflegerinEntity.setId(krankenpflegerin.getId());
        krankenpflegerinEntity.setVorname(krankenpflegerin.getVorname());
        krankenpflegerinEntity.setNachname(krankenpflegerin.getNachname());
        krankenpflegerinEntity.setBezeichnung(krankenpflegerin.getBezeichnung());
        krankenpflegerinEntity.setStrasse(krankenpflegerin.getStrasse());
        krankenpflegerinEntity.setPostleitzahl(krankenpflegerin.getPostleitzahl());
        krankenpflegerinEntity.setTelefonnummer(krankenpflegerin.getTelefonnummer());
        krankenpflegerinEntity.setBankverbindung(krankenpflegerin.getBankverbindung());
        krankenpflegerinEntity.setIban(krankenpflegerin.getIban());
        krankenpflegerinEntity.setBic(krankenpflegerin.getBic());
        krankenpflegerinEntity.setEmail(krankenpflegerin.getEmail());

        return krankenpflegerinEntity;
    }
}
