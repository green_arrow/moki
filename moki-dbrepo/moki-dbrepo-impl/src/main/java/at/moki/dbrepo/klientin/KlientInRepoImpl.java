package at.moki.dbrepo.klientin;

import at.moki.dbrepo.entitymanager.producer.MokiEntityManager;
import at.moki.domainmodel.klientin.Geschlecht;
import at.moki.domainmodel.klientin.KlientIn;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.exception.MokiDbRepoException;
import at.moki.security.MokiRolesAllowed;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Green Arrow on 01.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class KlientInRepoImpl implements KlientInRepo, Serializable {

    private static final long serialVersionUID = -2276658415760737267L;

    @Inject
    @MokiEntityManager
    private EntityManager entityManager;

    @Inject
    private TaetigkeitsfeldRepo taetigkeitsfeldRepo;


    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN,
            MokiUserRole.VERWALTUNG})
    @Override
    public List<KlientIn> findAll() {
        try {
            List<at.moki.dbrepo.entity.KlientIn> klientInList = this.entityManager
                    .createNamedQuery(at.moki.dbrepo.entity.KlientIn.QUERY_FIND_ALL,
                            at.moki.dbrepo.entity.KlientIn.class)
                    .getResultList();

            return klientInList.stream()
                    .map(this::mapToDomainmodel)
                    .sorted(Comparator.comparing(KlientIn::getKlientenNummer).reversed()
                            .thenComparing(KlientIn::getNachname)
                            .thenComparing(KlientIn::getVorname))
                    .collect(Collectors.toList());
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("KlientIn konnten nicht gelesen werden.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN,
            MokiUserRole.VERWALTUNG})
    @Override
    public KlientIn findByKlientInId(String id) {
        try {
            at.moki.dbrepo.entity.KlientIn klientIn = (at.moki.dbrepo.entity.KlientIn) this.entityManager
                    .createNamedQuery(at.moki.dbrepo.entity.KlientIn.QUERY_FIND_BY_KLIENTINID)
                    .setParameter("id", id)
                    .getSingleResult();

            return mapToDomainmodel(klientIn);
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("KlientIn existiert nicht, bitte ueberprüfen Sie ihre Eingaben.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.KRANKENPFLEGERIN,
            MokiUserRole.VERWALTUNG})
    @Override
    public void merge(KlientIn klientIn) {
        try {
            at.moki.dbrepo.entity.KlientIn klientInEntity = mapToEntity(klientIn);
            this.entityManager.merge(klientInEntity);
            this.entityManager.flush();
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("KlientIn konnte nicht aktualisiert werden.", e);
        }
    }

    @MokiRolesAllowed(userRoles = {
            MokiUserRole.ADMINISTRATOR,
            MokiUserRole.VERWALTUNG})
    @Override
    public void remove(String klientIn) {
        try {
            this.entityManager.createNamedQuery(at.moki.dbrepo.entity.KlientIn.QUERY_REMOVE)
                    .setParameter("id", klientIn)
                    .executeUpdate();
        } catch (PersistenceException e) {
            throw new MokiDbRepoException("Die zu loeschenden KlientIn existiert nicht.", e);
        }
    }

    private at.moki.dbrepo.entity.KlientIn mapToEntity(KlientIn klientIn) {
        at.moki.dbrepo.entity.KlientIn klientInEntity = new at.moki.dbrepo.entity.KlientIn();

        klientInEntity.setId(klientIn.getId());
        klientInEntity.setKlientenNummer(klientIn.getKlientenNummer());
        klientInEntity.setVerrechnungId(klientIn.getVerrechnungId());
        klientInEntity.setVerrechnungNummer(klientIn.getVerrechnungNummer());
        klientInEntity.setTaetigkeitsfeldId(this.taetigkeitsfeldRepo.findByBezeichnung(klientIn.getTaetigkeitsfeld()).getId());
        klientInEntity.setTaetigkeitsfeldNummer(klientIn.getTaetigkeitsfeldNummer());
        klientInEntity.setWeitereKontaktdaten(klientIn.getWeitereKontaktdaten());
        klientInEntity.setSpende(klientIn.getSpende());
        klientInEntity.setWerkvertragsNummer(klientIn.getWerkvertragsNummer());
        klientInEntity.setVorname(klientIn.getVorname());
        klientInEntity.setNachname(klientIn.getNachname());
        klientInEntity.setGeschlecht(klientIn.getGeschlecht() == null ? null : klientIn.getGeschlecht().getGeschlecht());
        klientInEntity.setImpacctGruppe(klientIn.getImpacctGruppe());
        klientInEntity.setPflegestufe(klientIn.getPflegestufe());
        klientInEntity.setSozialversicherungsnummer(klientIn.getSozialversicherungsnummer());
        klientInEntity.setGeburtsdatum(klientIn.getGeburtsdatum());
        klientInEntity.setSozialversicherungstraeger(klientIn.getSozialversicherungstraeger());
        klientInEntity.setDiagnose(klientIn.getDiagnose());
        klientInEntity.setDiagnoseschluessel(klientIn.getDiagnoseschluessel());
        klientInEntity.setSterbedatum(klientIn.getSterbedatum());
        klientInEntity.setMutterVertreterin(convertBooleanToString(klientIn.isMutterVertreterin()));
        klientInEntity.setMutterName(klientIn.getMutterName());
        klientInEntity.setMutterGeburtsdatum(klientIn.getMutterGeburtsdatum());
        klientInEntity.setMutterTelefonnummer(klientIn.getMutterTelefonnummer());
        klientInEntity.setVaterVertreter(convertBooleanToString(klientIn.isVaterVertreter()));
        klientInEntity.setVaterName(klientIn.getVaterName());
        klientInEntity.setVaterGeburtsdatum(klientIn.getVaterGeburtsdatum());
        klientInEntity.setVaterTelefonnummer(klientIn.getVaterTelefonnummer());
        klientInEntity.setSonstigeVertreter(convertBooleanToString(klientIn.isSonstigeVertreter()));
        klientInEntity.setSonstigeName(klientIn.getSonstigeName());
        klientInEntity.setSonstigeGeburtsdatum(klientIn.getSonstigeGeburtsdatum());
        klientInEntity.setSonstigeTelefonnummer(klientIn.getSonstigeTelefonnummer());
        klientInEntity.setStrasse(klientIn.getStrasse());
        klientInEntity.setMutterEmail(klientIn.getMutterEmail());
        klientInEntity.setOrt(klientIn.getOrt());
        klientInEntity.setVaterEmail(klientIn.getVaterEmail());
        klientInEntity.setHauptwohnsitz(convertBooleanToString(klientIn.isHauptwohnsitz()));
        klientInEntity.setZuweisendeStelleKH(klientIn.getZuweisendeStelleKH());
        klientInEntity.setZuweisendeStelleKJH(klientIn.getZuweisendeStelleKJH());
        klientInEntity.setZuweisendeStelleSonstige(klientIn.getZuweisendeStelleSonstige());
        klientInEntity.setKinderfacharztName(klientIn.getKinderfacharztName());
        klientInEntity.setKinderfacharztTelefonnummer(klientIn.getKinderfacharztTelefonnummer());
        klientInEntity.setBetreuendeKrankenpflegerin(klientIn.getBetreuendeKrankenpflegerin());
        klientInEntity.setWeitereKrankenpflegerinnen(klientIn.getWeitereKrankenpflegerinnen());
        klientInEntity.setBetreuungsbeginn(klientIn.getBetreuungsbeginn());
        klientInEntity.setBetreuungsende(klientIn.getBetreuungsende());
        klientInEntity.setKmProHb(klientIn.getKmProHb());

        return klientInEntity;
    }

    private KlientIn mapToDomainmodel(at.moki.dbrepo.entity.KlientIn klientInEntity) {
        KlientIn klientIn = new KlientIn();

        klientIn.setId(klientInEntity.getId());
        klientIn.setKlientenNummer(klientInEntity.getKlientenNummer());
        klientIn.setVerrechnungId(klientInEntity.getVerrechnungId());
        klientIn.setVerrechnungNummer(klientInEntity.getVerrechnungNummer());
        klientIn.setTaetigkeitsfeldId(klientInEntity.getTaetigkeitsfeldId());
        klientIn.setTaetigkeitsfeld(this.taetigkeitsfeldRepo.findById(klientInEntity.getTaetigkeitsfeldId()).getBezeichnung());
        klientIn.setTaetigkeitsfeldNummer(klientInEntity.getTaetigkeitsfeldNummer());
        klientIn.setWeitereKontaktdaten(klientInEntity.getWeitereKontaktdaten());
        klientIn.setSpende(klientInEntity.getSpende());
        klientIn.setWerkvertragsNummer(klientInEntity.getWerkvertragsNummer());
        klientIn.setVorname(klientInEntity.getVorname());
        klientIn.setNachname(klientInEntity.getNachname());

        if (StringUtils.equals(klientInEntity.getGeschlecht(), Geschlecht.WEIBLICH.getGeschlecht())) {
            klientIn.setGeschlecht(Geschlecht.WEIBLICH);
        } else if (StringUtils.equals(klientInEntity.getGeschlecht(), Geschlecht.MAENNLICH.getGeschlecht())) {
            klientIn.setGeschlecht(Geschlecht.MAENNLICH);
        }

        klientIn.setImpacctGruppe(klientInEntity.getImpacctGruppe());
        klientIn.setPflegestufe(klientInEntity.getPflegestufe());
        klientIn.setSozialversicherungsnummer(klientInEntity.getSozialversicherungsnummer());
        klientIn.setGeburtsdatum(klientInEntity.getGeburtsdatum());
        klientIn.setSozialversicherungstraeger(klientInEntity.getSozialversicherungstraeger());
        klientIn.setDiagnose(klientInEntity.getDiagnose());
        klientIn.setDiagnoseschluessel(klientInEntity.getDiagnoseschluessel());
        klientIn.setSterbedatum(klientInEntity.getSterbedatum());
        klientIn.setMutterVertreterin(convertStringToBoolean(klientInEntity.getMutterVertreterin()));
        klientIn.setMutterName(klientInEntity.getMutterName());
        klientIn.setMutterGeburtsdatum(klientInEntity.getMutterGeburtsdatum());
        klientIn.setMutterTelefonnummer(klientInEntity.getMutterTelefonnummer());
        klientIn.setVaterVertreter(convertStringToBoolean(klientInEntity.getVaterVertreter()));
        klientIn.setVaterName(klientInEntity.getVaterName());
        klientIn.setVaterGeburtsdatum(klientInEntity.getVaterGeburtsdatum());
        klientIn.setVaterTelefonnummer(klientInEntity.getVaterTelefonnummer());
        klientIn.setSonstigeVertreter(convertStringToBoolean(klientInEntity.getSonstigeVertreter()));
        klientIn.setSonstigeName(klientInEntity.getSonstigeName());
        klientIn.setSonstigeGeburtsdatum(klientInEntity.getSonstigeGeburtsdatum());
        klientIn.setSonstigeTelefonnummer(klientInEntity.getSonstigeTelefonnummer());
        klientIn.setStrasse(klientInEntity.getStrasse());
        klientIn.setMutterEmail(klientInEntity.getMutterEmail());
        klientIn.setOrt(klientInEntity.getOrt());
        klientIn.setVaterEmail(klientInEntity.getVaterEmail());
        klientIn.setHauptwohnsitz(convertStringToBoolean(klientInEntity.getHauptwohnsitz()));
        klientIn.setZuweisendeStelleKH(klientInEntity.getZuweisendeStelleKH());
        klientIn.setZuweisendeStelleKJH(klientInEntity.getZuweisendeStelleKJH());
        klientIn.setZuweisendeStelleSonstige(klientInEntity.getZuweisendeStelleSonstige());
        klientIn.setKinderfacharztName(klientInEntity.getKinderfacharztName());
        klientIn.setKinderfacharztTelefonnummer(klientInEntity.getKinderfacharztTelefonnummer());
        klientIn.setBetreuendeKrankenpflegerin(klientInEntity.getBetreuendeKrankenpflegerin());
        klientIn.setWeitereKrankenpflegerinnen(klientInEntity.getWeitereKrankenpflegerinnen());
        klientIn.setBetreuungsbeginn(klientInEntity.getBetreuungsbeginn());
        klientIn.setBetreuungsende(klientInEntity.getBetreuungsende());
        klientIn.setKmProHb(klientInEntity.getKmProHb());

        return klientIn;
    }

    private String convertBooleanToString(boolean value) {
        return value ? "J" : "N";
    }

    private boolean convertStringToBoolean(String value) {
        return StringUtils.equalsIgnoreCase(value, "J");
    }
}
