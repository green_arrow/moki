package at.moki.business.krankenpflegerin;

import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;

import java.util.List;

/**
 * Created by Green Arrow on 01.01.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public interface KrankenpflegerinBusiness {

    List<Krankenpflegerin> getData(String vorname, String nachname);

    void save(Krankenpflegerin krankenpflegerin, String username);

    void update(Krankenpflegerin krankenpflegerin);

}
