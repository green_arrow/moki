package at.moki.business.klientin;

import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.domainmodel.finanzierung.Finanzierung;
import at.moki.domainmodel.klientin.FK;
import at.moki.domainmodel.klientin.KJH;
import at.moki.domainmodel.klientin.KlientIn;

import java.util.List;

/**
 * Created by Green Arrow on 01.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public interface KlientInBusiness {

    List<KlientIn> getData(String klientenNummerFilter, String vornameFilter, String nachnameFilter, String klientInTaetigkeitsfeldFilter);

    List<String> getTaetigkeitsfelderAsList();

    void save(KlientIn klientIn);

    void save(KlientIn klientIn, List<Betreuungsstunden> betreuungsstunden, List<Finanzierung> finanzierungList);

    void saveWithGeschwister(FK fk);

    void saveWithKinder(KJH kjh);

    void update(KlientIn klientIn, List<Betreuungsstunden> betreuungsstunden, List<Finanzierung> finanzierungList);

    void updateWithGeschwister(FK fk);

    void updateWithKinder(KJH kjh);

    List<KlientIn> getKlientInGeschwister(KlientIn klientIn);

    List<KlientIn> getKlientInWithKinder(KlientIn klientIn);

    void delete(KlientIn klientIn);

}
