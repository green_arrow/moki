package at.moki.business.excel;

import at.moki.domainmodel.klientin.KlientIn;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;

import java.io.ByteArrayOutputStream;

/**
 * Created by Green Arrow on 03.05.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public interface ExcelExportBusiness {

    ByteArrayOutputStream exportSingleKlientIn(Krankenpflegerin krankenpflegerin, KlientIn klientIn);

    ByteArrayOutputStream exportFk(Krankenpflegerin krankenpflegerin, KlientIn klientIn, KlientIn zwilling, KlientIn drilling);

    ByteArrayOutputStream exportKjh(Krankenpflegerin krankenpflegerin, KlientIn klientIn, KlientIn zweitesKind, KlientIn drittesKind, KlientIn viertesKind);

}
