package at.moki.business.organisationsprotokoll;

import at.moki.domainmodel.organisationsprotokoll.Organisationsprotokoll;

import java.util.List;

/**
 * Created by Green Arrow on 18.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public interface OrganisationsprotokollBusiness {

    List<Organisationsprotokoll> getData(String klientInId);

    void save(Organisationsprotokoll organisationsprotokoll);

}
