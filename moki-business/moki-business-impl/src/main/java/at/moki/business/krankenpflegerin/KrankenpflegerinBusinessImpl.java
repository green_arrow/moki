package at.moki.business.krankenpflegerin;

import at.moki.dbrepo.krankenpflegerin.KrankenpflegerinRepo;
import at.moki.dbrepo.user.UserRepo;
import at.moki.dbrepo.userrechte.UserrechteRepo;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;
import at.moki.domainmodel.user.MokiUser;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.domainmodel.userrechte.Userrechte;
import at.moki.encryption.MokiEncryption;
import at.moki.exception.MokiBusinessException;
import at.moki.exception.MokiDbRepoException;
import at.moki.logger.MokiLogger;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Green Arrow on 01.01.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
@Transactional
public class KrankenpflegerinBusinessImpl implements KrankenpflegerinBusiness, Serializable {

    private static final long serialVersionUID = 935380816726919688L;

    @Inject
    @MokiLogger
    private Logger logger;

    @Inject
    private UserRepo userRepo;

    @Inject
    private MokiEncryption mokiEncryption;

    @Inject
    private UserrechteRepo userrechteRepo;

    @Inject
    private KrankenpflegerinRepo krankenpflegerinRepo;

    @Override
    public List<Krankenpflegerin> getData(String vorname, String nachname) {
        this.logger.info("Krankenpflegerinnen werden geladen und anhand von " +
                "vorname und nachname gefiltert.");

        try {
            Stream<Krankenpflegerin> stream = this.krankenpflegerinRepo.findAll().stream();

            if (StringUtils.isNotEmpty(vorname)) {
                stream = stream.filter(e -> StringUtils.containsIgnoreCase(e.getVorname(), vorname));
            }

            if (StringUtils.isNotEmpty(nachname)) {
                stream = stream.filter(e -> StringUtils.containsIgnoreCase(e.getNachname(), nachname));
            }

            return stream.collect(Collectors.toList());
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public void save(Krankenpflegerin krankenpflegerin, String username) {
        try {
            this.logger.info("Krankenpflegerin wird angelegt.");

            if (isUsernameAlreadyAvailable(username)) {
                throw new MokiBusinessException("Dieser Username existiert bereits, bitte wählen Sie einen anderen");
            }

            Userrechte userrechte = this.userrechteRepo.findByBezeichnung(MokiUserRole.KRANKENPFLEGERIN);

            String krankenpflegerinUUID = UUID.randomUUID().toString();
            krankenpflegerin.setId(krankenpflegerinUUID);
            this.krankenpflegerinRepo.merge(krankenpflegerin);

            //create new pseudo mokiUser
            MokiUser mokiUser = new MokiUser();
            mokiUser.setId(UUID.randomUUID().toString());
            mokiUser.setUsername(username);
            // genrate default password
            mokiUser.setPassword(this.mokiEncryption.generateHashPassword(
                    "s3cret", RandomStringUtils.random(15, true, true)));
            mokiUser.setMokiUserRole(userrechte.getMokiUserRole());
            mokiUser.setKrankenpflegerinId(krankenpflegerinUUID);
            mokiUser.setUserrechteId(userrechte.getId());

            this.userRepo.merge(mokiUser);

        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public void update(Krankenpflegerin krankenpflegerin) {
        this.logger.info("Daten der Krankenpflegerin werden aktualisiert.");

        try {
            this.krankenpflegerinRepo.merge(krankenpflegerin);
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    private boolean isUsernameAlreadyAvailable(String username) {
        Optional<MokiUser> userList = this.userRepo.findByUsername(username).stream()
                .filter(e -> StringUtils.equals(username, e.getUsername()))
                .findAny();

        return userList.isPresent();
    }
}
