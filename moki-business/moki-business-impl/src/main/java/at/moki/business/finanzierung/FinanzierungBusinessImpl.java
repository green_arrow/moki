package at.moki.business.finanzierung;

import at.moki.dbrepo.finanzierung.FinanzierungRepo;
import at.moki.domainmodel.finanzierung.Finanzierung;
import at.moki.domainmodel.finanzierung.Institutions;
import at.moki.exception.MokiBusinessException;
import at.moki.exception.MokiDbRepoException;
import at.moki.logger.MokiLogger;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Green Arrow on 08.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
@Transactional
public class FinanzierungBusinessImpl implements FinanzierungBusiness, Serializable {

    private static final long serialVersionUID = 4905219223245484921L;

    @Inject
    @MokiLogger
    private Logger logger;

    @Inject
    private FinanzierungRepo finanzierungRepo;

    @Override
    public List<Finanzierung> getData(String klientInId) {
        this.logger.info("Finanzierungen werden geladen.");

        try {
            Stream<Finanzierung> stream = this.finanzierungRepo.findAll().stream();

            if (StringUtils.isNotEmpty(klientInId)) {
                stream = stream.filter(e -> StringUtils.equals(e.getKlientInId(), klientInId));
            }

            return stream.collect(Collectors.toList());
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public Map<Institutions, String> getDataAsMap(String klientInId) {
        this.logger.info("Finanzierungen werden geladen und auf Institutions gemappt.");

        EnumMap<Institutions, String> institutionsStringMap = new EnumMap<>(Institutions.class);

        this.finanzierungRepo.findByKlientInId(klientInId).forEach(finanzierung ->
                institutionsStringMap.put(finanzierung.getInstitution(), finanzierung.getId()));

        return institutionsStringMap;
    }
}
