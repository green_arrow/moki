package at.moki.business.betreuungsstunden;

import at.moki.dbrepo.betreuungsstunden.BetreuungsstundenRepo;
import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.exception.MokiBusinessException;
import at.moki.exception.MokiDbRepoException;
import at.moki.logger.MokiLogger;
import at.moki.util.MokiDateUtil;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Green Arrow on 09.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
@Transactional
public class BetreuungsstundenBusinessImpl implements BetreuungsstundenBusiness, Serializable {

    private static final long serialVersionUID = 5747526596910113406L;

    @Inject
    @MokiLogger
    private Logger logger;

    @Inject
    private BetreuungsstundenRepo betreuungsstundenRepo;


    @Override
    public List<Betreuungsstunden> getDataBy(String klientInId) {
        this.logger.info("Betreuungsstunden zu einer klientInId werden geladen");

        try {
            int minYear = MokiDateUtil.getYearNow();
            int maxYear = MokiDateUtil.getplusTwoYears();

            return this.betreuungsstundenRepo.findByKlientInId(klientInId).stream()
                    .filter(betreuungsstunden -> betreuungsstunden.getJahr() >= minYear && betreuungsstunden.getJahr() <= maxYear)
                    .distinct()
                    .collect(Collectors.toList());
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }
}
