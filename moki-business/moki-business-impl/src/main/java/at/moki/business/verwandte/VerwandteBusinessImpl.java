package at.moki.business.verwandte;

import at.moki.dbrepo.verwandte.VerwandteRepo;
import at.moki.domainmodel.verwandte.Verwandte;
import at.moki.exception.MokiBusinessException;
import at.moki.exception.MokiDbRepoException;
import at.moki.logger.MokiLogger;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Green Arrow on 13.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
@Transactional
public class VerwandteBusinessImpl implements VerwandteBusiness, Serializable {

    private static final long serialVersionUID = -5950091653417754106L;

    @Inject
    @MokiLogger
    private Logger logger;

    @Inject
    private VerwandteRepo verwandteRepo;

    @Override
    public Verwandte getDataByKlientInId(String klientInId) {
        this.logger.info("Verwandte zu einer klientInId werden geladen.");

        try {
            return verwandteRepo.findByKlientInId(klientInId);
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public void save(Verwandte verwandte) {
        this.logger.info("Verwandte werden angelegt.");

        try {
            verwandte.setId(UUID.randomUUID().toString());
            this.verwandteRepo.merge(verwandte);
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }
}
