package at.moki.business.excel;

import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.domainmodel.finanzierung.Finanzierung;
import at.moki.domainmodel.finanzierung.Institutions;
import at.moki.domainmodel.klientin.Geschlecht;
import at.moki.domainmodel.klientin.KlientIn;
import at.moki.domainmodel.klientin.KlientInExcelContainerModel;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;
import at.moki.einstellungen.TaetigkeitsfeldJson;
import at.moki.exception.MokiBusinessException;
import at.moki.logger.MokiLogger;
import at.moki.util.MokiDateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Green Arrow on 01.09.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
public class ExcelImportBusinessImpl extends AbsExcelBusiness implements ExcelImportBusiness {

    @Inject
    @MokiLogger
    private Logger logger;

    @Override
    public List<KlientInExcelContainerModel> transformExcelToDomainmodel(String xlsxName, InputStream xlsxInputStream, String klientInTaetigkeitsfeld) {

        try {
            // create a new xssf workbook for xlsx format
            XSSFWorkbook xssfWorkbook = new XSSFWorkbook(xlsxInputStream);

            TaetigkeitsfeldJson taetigkeitsfeldJson = super.getTaetigkeitsfeldJson(klientInTaetigkeitsfeld);

            if (StringUtils.equals("FK", klientInTaetigkeitsfeld)) {
                return mapKlientInFk(xlsxName, klientInTaetigkeitsfeld, xssfWorkbook, taetigkeitsfeldJson);
            } else if (StringUtils.equals("KJH", klientInTaetigkeitsfeld)) {
                return mapKlientInKjh(xlsxName, klientInTaetigkeitsfeld, xssfWorkbook, taetigkeitsfeldJson);
            } else {
                return mapKlientIn(xlsxName, xssfWorkbook, klientInTaetigkeitsfeld, taetigkeitsfeldJson.getStammblatt(), taetigkeitsfeldJson.getBlattList().get(0), true);
            }
        } catch (IOException e) {
            this.logger.error("Fehler beim Laden des Excel Files");
            throw new MokiBusinessException(e);
        }
    }

    private List<KlientInExcelContainerModel> mapKlientIn(String xlsxName, XSSFWorkbook xssfWorkbook, String klientInTaetigkeitsfeld, String stammblatt, String klientenblatt, boolean isHauptKlientIn) {

        KlientInExcelContainerModel klientInExcelContainerModel = new KlientInExcelContainerModel();
        XSSFSheet xssfSheet = getMokiSheet(xlsxName, xssfWorkbook, stammblatt);
        klientInExcelContainerModel.setKrankenpflegerin(transformKrankenpflegerin(xssfSheet));

        xssfSheet = getMokiSheet(xlsxName, xssfWorkbook, klientenblatt);
        klientInExcelContainerModel.setKlientIn(transformKlientIn(xssfSheet, klientInTaetigkeitsfeld));
        klientInExcelContainerModel.setBetreuungsstundenList(transformBetreuungsstunden(xssfSheet));

        if (isHauptKlientIn) {
            klientInExcelContainerModel.setFinanzierungList(transformFinanzierung(xssfSheet));
        }

        return Collections.singletonList(klientInExcelContainerModel);
    }

    private List<KlientInExcelContainerModel> mapKlientInFk(String xlsxName, String klientInTaetigkeitsfeld, XSSFWorkbook xssfWorkbook, TaetigkeitsfeldJson taetigkeitsfeldJson) {
        String stammblatt = taetigkeitsfeldJson.getStammblatt();
        List<String> blattList = taetigkeitsfeldJson.getBlattList();

        return Stream.of(
                mapKlientIn(xlsxName, xssfWorkbook, klientInTaetigkeitsfeld, stammblatt, blattList.get(0), true),
                mapKlientIn(xlsxName, xssfWorkbook, klientInTaetigkeitsfeld, stammblatt, blattList.get(1), false),
                mapKlientIn(xlsxName, xssfWorkbook, klientInTaetigkeitsfeld, stammblatt, blattList.get(2), false))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private List<KlientInExcelContainerModel> mapKlientInKjh(String xlsxName, String klientInTaetigkeitsfeld, XSSFWorkbook xssfWorkbook, TaetigkeitsfeldJson taetigkeitsfeldJson) {
        String stammblatt = taetigkeitsfeldJson.getStammblatt();
        List<String> blattList = taetigkeitsfeldJson.getBlattList();

        return Stream.of(
                mapKlientIn(xlsxName, xssfWorkbook, klientInTaetigkeitsfeld, stammblatt, blattList.get(0), true),
                mapKlientIn(xlsxName, xssfWorkbook, klientInTaetigkeitsfeld, stammblatt, blattList.get(1), false),
                mapKlientIn(xlsxName, xssfWorkbook, klientInTaetigkeitsfeld, stammblatt, blattList.get(2), false),
                mapKlientIn(xlsxName, xssfWorkbook, klientInTaetigkeitsfeld, stammblatt, blattList.get(3), false))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private Krankenpflegerin transformKrankenpflegerin(XSSFSheet xssfSheet) {

        Krankenpflegerin krankenpflegerin = new Krankenpflegerin();

        // set name of the krankenpflegerin
        Row row = xssfSheet.getRow(0);

        setNameOfKrankenpflegerin(krankenpflegerin, extractCellValue(row.getCell(1)));

        // set bezeichnung
        row = xssfSheet.getRow(1);
        krankenpflegerin.setBezeichnung(extractCellValue(row.getCell(1)));
        // set strasse
        row = xssfSheet.getRow(2);
        krankenpflegerin.setStrasse(extractCellValue(row.getCell(1)));
        // set plz
        row = xssfSheet.getRow(3);
        krankenpflegerin.setPostleitzahl(extractCellValue(row.getCell(1)));
        // set telefon
        row = xssfSheet.getRow(4);
        krankenpflegerin.setTelefonnummer(extractCellValue(row.getCell(1)));
        // set bank
        row = xssfSheet.getRow(8);
        krankenpflegerin.setBankverbindung(extractCellValue(row.getCell(1)));
        // set iban
        row = xssfSheet.getRow(9);
        krankenpflegerin.setIban(extractCellValue(row.getCell(1)));
        // set bic
        row = xssfSheet.getRow(10);
        krankenpflegerin.setBic(extractCellValue(row.getCell(1)));

        return krankenpflegerin;
    }

    private KlientIn transformKlientIn(XSSFSheet xssfSheet, String klientInTaetigkeitsfeld) {

        KlientIn klientIn = new KlientIn();

        Row row = xssfSheet.getRow(0);
        klientIn.setKlientenNummer(extractCellValue(row.getCell(0)));

        // Tätigkeitsfeld
        row = xssfSheet.getRow(1);
        klientIn.setTaetigkeitsfeldNummer(extractCellValue(row.getCell(3)));

        klientIn.setVerrechnungNummer(extractCellValue(row.getCell(7)));

        klientIn.setSpende(extractCellValue(row.getCell(10)));
        klientIn.setWerkvertragsNummer(extractCellValue(row.getCell(12)));

        // Klientenname (Nachname, Vorname)
        row = xssfSheet.getRow(2);

        setNamesOfKlientIn(klientIn, extractCellValue(row.getCell(3)));

        // Geschlecht
        row = xssfSheet.getRow(3);
        String geschlecht = extractCellValue(row.getCell(1));
        if (StringUtils.equalsIgnoreCase(geschlecht, "x")) {
            klientIn.setGeschlecht(Geschlecht.WEIBLICH);
        } else {
            klientIn.setGeschlecht(Geschlecht.MAENNLICH);
        }

        // ImpacctGruppe
        klientIn.setImpacctGruppe(extractCellValue(row.getCell(9)));
        // Pflegestufe
        klientIn.setPflegestufe(extractCellValue(row.getCell(12)));

        // Sozialversicherungsnr:
        row = xssfSheet.getRow(4);
        klientIn.setSozialversicherungsnummer(extractCellValue(row.getCell(1)));
        klientIn.setGeburtsdatum(extractLocalDateFromCell(row.getCell(5)));
        klientIn.setSozialversicherungstraeger(extractCellValue(row.getCell(12)));

        // Diagnose
        row = xssfSheet.getRow(5);
        klientIn.setDiagnose(extractCellValue(row.getCell(1)));
        klientIn.setDiagnoseschluessel(extractCellValue(row.getCell(12)));

        // Daten der Eltern / gesetzlicher Vertreter
        row = xssfSheet.getRow(6);
        klientIn.setSterbedatum(extractLocalDateFromCell(row.getCell(12)));

        // Mutter
        row = xssfSheet.getRow(7);
        klientIn.setMutterVertreterin(StringUtils.equalsAnyIgnoreCase(extractCellValue(row.getCell(1)), "X"));
        klientIn.setMutterName(extractCellValue(row.getCell(3)));
        klientIn.setMutterGeburtsdatum(extractLocalDateFromCell(row.getCell(9)));
        klientIn.setMutterTelefonnummer(extractCellValue(row.getCell(11)));

        // Vater
        row = xssfSheet.getRow(8);
        klientIn.setVaterVertreter(StringUtils.equalsAnyIgnoreCase(extractCellValue(row.getCell(1)), "X"));
        klientIn.setVaterName(extractCellValue(row.getCell(3)));
        klientIn.setVaterGeburtsdatum(extractLocalDateFromCell(row.getCell(9)));
        klientIn.setVaterTelefonnummer(extractCellValue(row.getCell(11)));

        // Sonstige
        row = xssfSheet.getRow(9);
        klientIn.setSonstigeVertreter(StringUtils.equalsAnyIgnoreCase(extractCellValue(row.getCell(1)), "X"));
        klientIn.setSonstigeName(extractCellValue(row.getCell(3)));
        klientIn.setSonstigeGeburtsdatum(extractLocalDateFromCell(row.getCell(9)));
        klientIn.setSonstigeTelefonnummer(extractCellValue(row.getCell(11)));

        // Straße
        row = xssfSheet.getRow(10);
        klientIn.setStrasse(extractCellValue(row.getCell(1)));
        klientIn.setMutterEmail(extractCellValue(row.getCell(8)));

        // Hauptwohnsitz
        klientIn.setHauptwohnsitz(extractBoolean(extractCellValue(row.getCell(12))));


        //PLZ + Ort
        row = xssfSheet.getRow(11);
        klientIn.setOrt(extractCellValue(row.getCell(1)));
        klientIn.setVaterEmail(extractCellValue(row.getCell(8)));

        // Zuweisende Stellen
        row = xssfSheet.getRow(12);
        klientIn.setZuweisendeStelleKH(extractCellValue(row.getCell(2)));
        klientIn.setZuweisendeStelleKJH(extractCellValue(row.getCell(7)));
        klientIn.setZuweisendeStelleSonstige(extractCellValue(row.getCell(11)));

        // Kinderfacharzt
        row = xssfSheet.getRow(13);
        klientIn.setKinderfacharztName(extractCellValue(row.getCell(1)));
        klientIn.setKinderfacharztTelefonnummer(extractCellValue(row.getCell(9)));

        // KM
        klientIn.setKmProHb(extractCellValue(row.getCell(3)));
        klientIn.setKmProHb(extractCellValue(row.getCell(6)));
        klientIn.setKmProHb(extractCellValue(row.getCell(9)));

        // betreuende DGKP
        row = xssfSheet.getRow(20);
        klientIn.setBetreuendeKrankenpflegerin(extractCellValue(row.getCell(1)));
        klientIn.setWeitereKrankenpflegerinnen(extractCellValue(row.getCell(9)));

        // Betreuungsbeginn
        row = xssfSheet.getRow(21);
        klientIn.setBetreuungsbeginn(extractLocalDateFromCell(row.getCell(1)));
        klientIn.setBetreuungsende(extractLocalDateFromCell(row.getCell(7)));
        klientIn.setKmProHb(extractCellValue(row.getCell(12)));

        return klientIn;
    }

    private List<Betreuungsstunden> transformBetreuungsstunden(XSSFSheet xssfSheet) {

        List<Betreuungsstunden> betreuungsstundenList = new ArrayList<>();

        // Betreuungsstunden
        Row row = xssfSheet.getRow(22);

        betreuungsstundenList.add(new Betreuungsstunden(null, MokiDateUtil.getYearNow(),
                parseBetreuungsstundenValue(extractCellValue(row.getCell(3))), null));

        betreuungsstundenList.add(new Betreuungsstunden(null, MokiDateUtil.getPlusOneYear(),
                parseBetreuungsstundenValue(extractCellValue(row.getCell(6))), null));

        betreuungsstundenList.add(new Betreuungsstunden(null, MokiDateUtil.getplusTwoYears(),
                parseBetreuungsstundenValue(extractCellValue(row.getCell(9))), null));

        return betreuungsstundenList;
    }

    private String parseBetreuungsstundenValue(String cellValue) {
        return NumberUtils.isParsable(cellValue) ? replaceCommasWithDots(cellValue) : null;
    }

    private String replaceCommasWithDots(String value) {
        return StringUtils.replace(value, ",", ".");
    }

    private List<Finanzierung> transformFinanzierung(XSSFSheet xssfSheet) {

        Row row;

        List<Finanzierung> finanzierungList = new ArrayList<>();

        Finanzierung finanzierung = new Finanzierung();

        // LAND
        row = xssfSheet.getRow(15);
        finanzierung.setInstitution(Institutions.LAND);
        finanzierung.setKostenUebernahme(extractBoolean(extractCellValue(row.getCell(1))));
        finanzierung.setKostenSelbstbehalt(extractBoolean(extractCellValue(row.getCell(7))));
        finanzierungList.add(finanzierung);

        // MUKI
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.MUKI);
        finanzierung.setKostenUebernahme(extractBoolean(extractCellValue(row.getCell(9))));
        finanzierung.setKostenSelbstbehalt(extractBoolean(extractCellValue(row.getCell(12))));
        finanzierungList.add(finanzierung);

        // KINDER_JUGENDHILFE
        finanzierung = new Finanzierung();
        row = xssfSheet.getRow(16);
        finanzierung.setInstitution(Institutions.KINDER_JUGENDHILFE);
        finanzierung.setKostenUebernahme(extractBoolean(extractCellValue(row.getCell(1))));
        finanzierung.setKostenSelbstbehalt(extractBoolean(extractCellValue(row.getCell(7))));
        finanzierungList.add(finanzierung);

        // VERSICHERUNG
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.VERSICHERUNG);
        finanzierung.setKostenUebernahme(extractBoolean(extractCellValue(row.getCell(9))));
        finanzierung.setKostenSelbstbehalt(extractBoolean(extractCellValue(row.getCell(12))));
        finanzierungList.add(finanzierung);

        // HOSPIZ
        finanzierung = new Finanzierung();
        row = xssfSheet.getRow(17);
        finanzierung.setInstitution(Institutions.HOSPIZ);
        finanzierung.setKostenUebernahme(extractBoolean(extractCellValue(row.getCell(1))));
        finanzierung.setKostenSelbstbehalt(extractBoolean(extractCellValue(row.getCell(7))));
        finanzierungList.add(finanzierung);

        // SPENDEN
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.SPENDEN);
        finanzierung.setKostenUebernahme(extractBoolean(extractCellValue(row.getCell(9))));
        finanzierung.setKostenSelbstbehalt(extractBoolean(extractCellValue(row.getCell(12))));
        finanzierungList.add(finanzierung);

        // SOZIALVERSICHERUNG
        finanzierung = new Finanzierung();
        row = xssfSheet.getRow(18);
        finanzierung.setInstitution(Institutions.SOZIALVERSICHERUNG);
        finanzierung.setKostenUebernahme(extractBoolean(extractCellValue(row.getCell(1))));
        finanzierung.setKostenSelbstbehalt(extractBoolean(extractCellValue(row.getCell(7))));
        finanzierungList.add(finanzierung);

        // PRIVAT
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.PRIVAT);
        finanzierung.setKostenUebernahme(extractBoolean(extractCellValue(row.getCell(9))));
        finanzierung.setKostenSelbstbehalt(extractBoolean(extractCellValue(row.getCell(12))));
        finanzierungList.add(finanzierung);

        // KIB
        finanzierung = new Finanzierung();
        row = xssfSheet.getRow(19);
        finanzierung.setInstitution(Institutions.KIB);
        finanzierung.setKostenUebernahme(extractBoolean(extractCellValue(row.getCell(1))));
        finanzierung.setKostenSelbstbehalt(extractBoolean(extractCellValue(row.getCell(7))));
        finanzierungList.add(finanzierung);

        // ANDERE
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.ANDERE);
        finanzierung.setKostenUebernahme(extractBoolean(extractCellValue(row.getCell(9))));
        finanzierung.setKostenSelbstbehalt(extractBoolean(extractCellValue(row.getCell(12))));
        finanzierungList.add(finanzierung);

        return finanzierungList;
    }

    private String extractCellValue(Cell cell) {
        if (cell == null) {
            return null;
        }
        cell.setCellType(CellType.STRING);
        return StringUtils.isEmpty(cell.getStringCellValue()) ? null : cell.getStringCellValue();
    }

    private boolean extractBoolean(String booleanValue) {
        return StringUtils.equalsIgnoreCase(booleanValue, "X");
    }

    private LocalDate extractLocalDateFromCell(Cell cell) {
        try {
            Date dateCellValue = cell.getDateCellValue();
            return dateCellValue == null ? null : dateCellValue.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        } catch (IllegalStateException e) {
            return null;
        }
    }

    void setNameOfKrankenpflegerin(Krankenpflegerin krankenpflegerin, String vornameNachname) {
        String[] fullName = StringUtils.split(vornameNachname, " ", 2);

        if (fullName != null) {
            if (fullName.length == 1) {
                krankenpflegerin.setVorname(fullName[0]);
            } else if (fullName.length > 1) {
                krankenpflegerin.setVorname(fullName[0]);
                krankenpflegerin.setNachname(fullName[1]);
            }
        }
    }

    void setNamesOfKlientIn(KlientIn klientIn, String nachnameVorname) {
        String[] fullName = StringUtils.split(nachnameVorname, " ", 2);

        if (fullName != null) {
            if (fullName.length == 1) {
                klientIn.setNachname(fullName[0]);
            } else if (fullName.length > 1) {
                klientIn.setNachname(fullName[0]);
                klientIn.setVorname(fullName[1]);
            }
        }
    }

    private XSSFSheet getMokiSheet(String xlsxName, XSSFWorkbook xssfWorkbook, String mokiSheetName) {
        XSSFSheet sheet = xssfWorkbook.getSheet(mokiSheetName);

        if (sheet == null) {
            throw new MokiBusinessException(String.format("Im Excelfile \"%s\" konnte das Sheet \"%s\" nicht gefunden werden",
                    xlsxName, mokiSheetName));
        }

        return sheet;
    }
}
