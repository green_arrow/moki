package at.moki.business.excel;

import at.moki.business.betreuungsstunden.BetreuungsstundenBusiness;
import at.moki.business.finanzierung.FinanzierungBusiness;
import at.moki.business.organisationsprotokoll.OrganisationsprotokollBusiness;
import at.moki.dbrepo.excel.dateien.ExcelDateinRepo;
import at.moki.dbrepo.klientin.TaetigkeitsfeldRepo;
import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.domainmodel.excel.dateien.ExcelDatei;
import at.moki.domainmodel.finanzierung.Finanzierung;
import at.moki.domainmodel.klientin.Geschlecht;
import at.moki.domainmodel.klientin.KlientIn;
import at.moki.domainmodel.klientin.Taetigkeitsfeld;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;
import at.moki.domainmodel.organisationsprotokoll.Organisationsprotokoll;
import at.moki.einstellungen.TaetigkeitsfeldJson;
import at.moki.exception.MokiBusinessException;
import at.moki.formatter.MokiDateFormatter;
import at.moki.logger.MokiLogger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by Green Arrow on 14.04.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
public class ExcelExportBusinessImpl extends AbsExcelBusiness implements ExcelExportBusiness {

    private static final int ROW_INDEX = 7;
    private static final String KONNTE_EXCEL_SHEET_NICHT_LADEN = "konnte excel sheet nicht laden";
    private static final String KONNTE_RESOURCE_FUER_EXCEL_NICHT_LADEN = "Konnte Resource fuer Excel nicht laden.";

    @Inject
    @MokiLogger
    private Logger logger;

    @Inject
    private ExcelDateinRepo excelDateinRepo;

    @Inject
    private TaetigkeitsfeldRepo taetigkeitsfeldRepo;

    @Inject
    private FinanzierungBusiness finanzierungBusiness;

    @Inject
    private BetreuungsstundenBusiness betreuungsstundenBusiness;

    @Inject
    private OrganisationsprotokollBusiness organisationsprotokollBusiness;

    @Override
    public ByteArrayOutputStream exportSingleKlientIn(Krankenpflegerin krankenpflegerin, KlientIn klientIn) {

        this.logger.info("Erstelle Excel File SingleKlientIn.");

        try {

            List<Finanzierung> finanzierungList = finanzierungBusiness.getData(klientIn.getId());
            List<Betreuungsstunden> betreuungsstunden = betreuungsstundenBusiness.getDataBy(klientIn.getId());
            List<Organisationsprotokoll> organisationsprotokollList = organisationsprotokollBusiness.getData(klientIn.getId());

            String klientInTaetigkeitsfeld = klientIn.getTaetigkeitsfeld();

            InputStream inputStream = getExcelFromDbAsInputStream(klientInTaetigkeitsfeld);

            TaetigkeitsfeldJson taetigkeitsfeldJson = super.getTaetigkeitsfeldJson(klientIn.getTaetigkeitsfeld());

            // create a new xssf workbook for xlsx format
            XSSFWorkbook xssfWorkbook = new XSSFWorkbook(inputStream);

            // get sheet with stammblatt
            XSSFSheet xssfSheet;
            xssfSheet = xssfWorkbook.getSheet(taetigkeitsfeldJson.getStammblatt());

            if (xssfSheet == null) {
                throw new MokiBusinessException(KONNTE_EXCEL_SHEET_NICHT_LADEN);
            }

            addKrankenpflegerinData(xssfSheet, krankenpflegerin);

            // get sheet with Klientenblatt
            xssfSheet = xssfWorkbook.getSheet(taetigkeitsfeldJson.getBlattList().get(0));

            if (xssfSheet == null) {
                throw new MokiBusinessException(KONNTE_EXCEL_SHEET_NICHT_LADEN);
            }

            addKlientInData(xssfSheet, klientIn, finanzierungList, betreuungsstunden);

            xssfSheet = xssfWorkbook.getSheet(taetigkeitsfeldJson.getOrgprot());

            addOrganisationsprotokoll(xssfSheet, klientIn.getWeitereKontaktdaten(), organisationsprotokollList);

            inputStream.close();

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            XSSFFormulaEvaluator.evaluateAllFormulaCells(xssfWorkbook);

            //write changes and close the workbook
            xssfWorkbook.write(byteArrayOutputStream);
            xssfWorkbook.close();

            return byteArrayOutputStream;

        } catch (Exception e) {
            throw new MokiBusinessException(KONNTE_RESOURCE_FUER_EXCEL_NICHT_LADEN, e);
        }
    }

    @Override
    public ByteArrayOutputStream exportFk(Krankenpflegerin krankenpflegerin, KlientIn klientIn, KlientIn zwilling, KlientIn drilling) {

        this.logger.info("Erstelle Excel File FK.");

        try {

            List<Finanzierung> finanzierungList = finanzierungBusiness.getData(klientIn.getId());
            List<Betreuungsstunden> betreuungsstundenKlientIn = betreuungsstundenBusiness.getDataBy(klientIn.getId());
            List<Betreuungsstunden> betreuungsstundenZwilling = betreuungsstundenBusiness.getDataBy(zwilling.getId());
            List<Betreuungsstunden> betreuungsstundenDrilling = betreuungsstundenBusiness.getDataBy(drilling.getId());
            List<Organisationsprotokoll> organisationsprotokollList = organisationsprotokollBusiness.getData(klientIn.getId());

            String klientInTaetigkeitsfeld = klientIn.getTaetigkeitsfeld();

            InputStream inputStream = getExcelFromDbAsInputStream(klientInTaetigkeitsfeld);

            TaetigkeitsfeldJson taetigkeitsfeldJson = super.getTaetigkeitsfeldJson(klientIn.getTaetigkeitsfeld());

            // create a new xssf workbook for xlsx format
            XSSFWorkbook xssfWorkbook = new XSSFWorkbook(inputStream);

            // get sheet with stammblatt
            XSSFSheet xssfSheet = xssfWorkbook.getSheet(taetigkeitsfeldJson.getStammblatt());


            if (xssfSheet == null) {
                throw new MokiBusinessException(KONNTE_EXCEL_SHEET_NICHT_LADEN);
            }

            addKrankenpflegerinData(xssfSheet, krankenpflegerin);

            // add first klientin blatt
            xssfSheet = xssfWorkbook.getSheet(taetigkeitsfeldJson.getBlattList().get(0));

            if (xssfSheet == null) {
                throw new MokiBusinessException(KONNTE_EXCEL_SHEET_NICHT_LADEN);
            }

            addKlientInData(xssfSheet, klientIn, finanzierungList, betreuungsstundenKlientIn);

            // add zwilling
            xssfSheet = xssfWorkbook.getSheet(taetigkeitsfeldJson.getBlattList().get(1));

            if (xssfSheet == null) {
                throw new MokiBusinessException(KONNTE_EXCEL_SHEET_NICHT_LADEN);
            }

            addKlientInData(xssfSheet, zwilling, finanzierungList, betreuungsstundenZwilling);

            // add drilling
            xssfSheet = xssfWorkbook.getSheet(taetigkeitsfeldJson.getBlattList().get(2));

            if (xssfSheet == null) {
                throw new MokiBusinessException(KONNTE_EXCEL_SHEET_NICHT_LADEN);
            }

            addKlientInData(xssfSheet, drilling, finanzierungList, betreuungsstundenDrilling);

            xssfSheet = xssfWorkbook.getSheet(taetigkeitsfeldJson.getOrgprot());

            addOrganisationsprotokoll(xssfSheet, klientIn.getWeitereKontaktdaten(), organisationsprotokollList);

            inputStream.close();

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            XSSFFormulaEvaluator.evaluateAllFormulaCells(xssfWorkbook);

            //write changes and close the workbook
            xssfWorkbook.write(byteArrayOutputStream);
            xssfWorkbook.close();

            return byteArrayOutputStream;

        } catch (Exception e) {
            throw new MokiBusinessException(KONNTE_RESOURCE_FUER_EXCEL_NICHT_LADEN, e);
        }
    }

    @Override
    public ByteArrayOutputStream exportKjh(Krankenpflegerin krankenpflegerin, KlientIn klientIn, KlientIn zweitesKind, KlientIn drittesKind, KlientIn viertesKind) {

        this.logger.info("Erstelle Excel File KJH.");

        try {

            List<Finanzierung> finanzierungList = finanzierungBusiness.getData(klientIn.getId());
            List<Betreuungsstunden> betreuungsstundenKlientIn = betreuungsstundenBusiness.getDataBy(klientIn.getId());
            List<Betreuungsstunden> betreuungsstundenZweitesKind = betreuungsstundenBusiness.getDataBy(zweitesKind.getId());
            List<Betreuungsstunden> betreuungsstundenDritterKind = betreuungsstundenBusiness.getDataBy(drittesKind.getId());
            List<Betreuungsstunden> betreuungsstundenViertesKind = betreuungsstundenBusiness.getDataBy(viertesKind.getId());
            List<Organisationsprotokoll> organisationsprotokollList = organisationsprotokollBusiness.getData(klientIn.getId());

            String klientInTaetigkeitsfeld = klientIn.getTaetigkeitsfeld();

            InputStream inputStream = getExcelFromDbAsInputStream(klientInTaetigkeitsfeld);

            TaetigkeitsfeldJson taetigkeitsfeldJson = super.getTaetigkeitsfeldJson(klientIn.getTaetigkeitsfeld());

            // create a new xssf workbook for xlsx format
            XSSFWorkbook xssfWorkbook = new XSSFWorkbook(inputStream);

            // get sheet with stammblatt
            XSSFSheet xssfSheet = xssfWorkbook.getSheet(taetigkeitsfeldJson.getStammblatt());

            if (xssfSheet == null) {
                throw new MokiBusinessException(KONNTE_EXCEL_SHEET_NICHT_LADEN);
            }

            addKrankenpflegerinData(xssfSheet, krankenpflegerin);

            // add klientin
            xssfSheet = xssfWorkbook.getSheet(taetigkeitsfeldJson.getBlattList().get(0));

            if (xssfSheet == null) {
                throw new MokiBusinessException(KONNTE_EXCEL_SHEET_NICHT_LADEN);
            }

            addKlientInData(xssfSheet, klientIn, finanzierungList, betreuungsstundenKlientIn);

            // add zweites kind
            xssfSheet = xssfWorkbook.getSheet(taetigkeitsfeldJson.getBlattList().get(1));

            if (xssfSheet == null) {
                throw new MokiBusinessException(KONNTE_EXCEL_SHEET_NICHT_LADEN);
            }

            addKlientInData(xssfSheet, zweitesKind, finanzierungList, betreuungsstundenZweitesKind);

            // add drittes kind
            xssfSheet = xssfWorkbook.getSheet(taetigkeitsfeldJson.getBlattList().get(2));

            if (xssfSheet == null) {
                throw new MokiBusinessException(KONNTE_EXCEL_SHEET_NICHT_LADEN);
            }

            addKlientInData(xssfSheet, drittesKind, finanzierungList, betreuungsstundenDritterKind);

            // add viertes kind
            xssfSheet = xssfWorkbook.getSheet(taetigkeitsfeldJson.getBlattList().get(3));

            if (xssfSheet == null) {
                throw new MokiBusinessException(KONNTE_EXCEL_SHEET_NICHT_LADEN);
            }

            addKlientInData(xssfSheet, viertesKind, finanzierungList, betreuungsstundenViertesKind);

            xssfSheet = xssfWorkbook.getSheet(taetigkeitsfeldJson.getOrgprot());

            addOrganisationsprotokoll(xssfSheet, klientIn.getWeitereKontaktdaten(), organisationsprotokollList);

            inputStream.close();

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            XSSFFormulaEvaluator.evaluateAllFormulaCells(xssfWorkbook);

            //write changes and close the workbook
            xssfWorkbook.write(byteArrayOutputStream);
            xssfWorkbook.close();

            return byteArrayOutputStream;

        } catch (Exception e) {
            throw new MokiBusinessException(KONNTE_RESOURCE_FUER_EXCEL_NICHT_LADEN, e);
        }
    }

    private InputStream getExcelFromDbAsInputStream(String klientInTaetigkeitsfeld) {
        Taetigkeitsfeld taetigkeitsfeldFromDb = this.taetigkeitsfeldRepo.findByBezeichnung(klientInTaetigkeitsfeld);
        ExcelDatei excelDatei = this.excelDateinRepo.findByTaetigkeitsfeldId(taetigkeitsfeldFromDb.getId());

        return new ByteArrayInputStream(excelDatei.getDatei());
    }

    private void addKrankenpflegerinData(XSSFSheet xssfSheet, Krankenpflegerin krankenpflegerin) {
        if (krankenpflegerin != null) {
            // set name of the krankenpflegerin
            Row row = xssfSheet.getRow(0);
            addCell(row.createCell(1), krankenpflegerin.getVorname() + " " + krankenpflegerin.getNachname());
            // set bezeichnung
            row = xssfSheet.getRow(1);
            addCell(row.createCell(1), krankenpflegerin.getBezeichnung());
            // set strasse
            row = xssfSheet.getRow(2);
            addCell(row.createCell(1), krankenpflegerin.getStrasse());
            // set plz
            row = xssfSheet.getRow(3);
            addCell(row.createCell(1), krankenpflegerin.getPostleitzahl());
            // set telefon
            row = xssfSheet.getRow(4);
            addCell(row.createCell(1), krankenpflegerin.getTelefonnummer());

            // set bank
            row = xssfSheet.getRow(8);
            addCell(row.createCell(1), krankenpflegerin.getBankverbindung());
            // set iban
            row = xssfSheet.getRow(9);
            addCell(row.createCell(1), krankenpflegerin.getIban());
            // set bic
            row = xssfSheet.getRow(10);
            addCell(row.createCell(1), krankenpflegerin.getBic());
        } else {
            this.logger.info("exportiere Daten fuer admin");
        }
    }

    private void addKlientInData(XSSFSheet xssfSheet, KlientIn klientIn,
                                 List<Finanzierung> finanzierungList,
                                 List<Betreuungsstunden> betreuungsstunden) {
        if (klientIn != null) {
            Row row = xssfSheet.getRow(0);
            addCell(row.getCell(0), klientIn.getKlientenNummer());

            // Tätigkeitsfeld
            row = xssfSheet.getRow(1);
            addCell(row.getCell(3), klientIn.getTaetigkeitsfeldNummer());

            addCell(row.getCell(7), klientIn.getVerrechnungNummer());
            addCell(row.getCell(10), klientIn.getSpende());
            addCell(row.getCell(12), klientIn.getWerkvertragsNummer());

            // Klientenname (Nachname, Vorname)
            row = xssfSheet.getRow(2);
            addCell(row.getCell(3), klientIn.getNachname() + " " + klientIn.getVorname());

            // Geschlecht
            row = xssfSheet.getRow(3);
            if (Geschlecht.WEIBLICH == klientIn.getGeschlecht()) {
                addCell(row.getCell(1), "X");
            } else {
                addCell(row.getCell(3), "X");
            }

            // ImpacctGruppe
            addCell(row.getCell(9), klientIn.getImpacctGruppe());
            // Pflegestufe
            addCell(row.getCell(12), klientIn.getPflegestufe());

            // Sozialversicherungsnr:
            row = xssfSheet.getRow(4);
            addCell(row.getCell(1), klientIn.getSozialversicherungsnummer());
            addCell(row.getCell(1), klientIn.getPflegestufe());
            addCell(row.getCell(5), dateFormat(klientIn.getGeburtsdatum()));
            addCell(row.getCell(12), klientIn.getSozialversicherungstraeger());

            // Diagnose
            row = xssfSheet.getRow(5);
            addCell(row.getCell(1), klientIn.getDiagnose());
            addCell(row.getCell(12), klientIn.getDiagnoseschluessel());

            // Daten der Eltern / gesetzlicher Vertreter
            row = xssfSheet.getRow(6);
            addCell(row.getCell(12), dateFormat(klientIn.getSterbedatum()));

            // Mutter
            row = xssfSheet.getRow(7);
            addCell(row.getCell(1), klientIn.isMutterVertreterin() ? "X" : "O");
            addCell(row.getCell(3), klientIn.getMutterName());
            addCell(row.getCell(9), dateFormat(klientIn.getMutterGeburtsdatum()));
            addCell(row.getCell(11), klientIn.getMutterTelefonnummer());
            // Vater
            row = xssfSheet.getRow(8);
            addCell(row.getCell(1), klientIn.isVaterVertreter() ? "X" : "O");
            addCell(row.getCell(3), klientIn.getVaterName());
            addCell(row.getCell(9), dateFormat(klientIn.getVaterGeburtsdatum()));
            addCell(row.getCell(11), klientIn.getVaterTelefonnummer());
            // Sonstige
            row = xssfSheet.getRow(9);
            addCell(row.getCell(1), klientIn.isSonstigeVertreter() ? "X" : "O");
            addCell(row.getCell(3), klientIn.getSonstigeName());
            addCell(row.getCell(9), dateFormat(klientIn.getSonstigeGeburtsdatum()));
            addCell(row.getCell(11), klientIn.getSonstigeTelefonnummer());

            // Straße
            row = xssfSheet.getRow(10);
            addCell(row.getCell(1), klientIn.getStrasse());
            addCell(row.getCell(8), klientIn.getMutterEmail());

            // Hauptwohnsitz
            if (klientIn.isHauptwohnsitz()) {
                addCell(row.getCell(12), "X");
            } else {
                row = xssfSheet.getRow(11);
                addCell(row.getCell(12), "X");
            }

            //PLZ + Ort
            row = xssfSheet.getRow(11);
            addCell(row.getCell(1), klientIn.getOrt());
            addCell(row.getCell(8), klientIn.getVaterEmail());

            // Zuweisende Stellen
            row = xssfSheet.getRow(12);
            addCell(row.getCell(2), klientIn.getZuweisendeStelleKH());
            addCell(row.getCell(7), klientIn.getZuweisendeStelleKJH());
            addCell(row.getCell(11), klientIn.getZuweisendeStelleSonstige());

            // Kinderfacharzt
            row = xssfSheet.getRow(13);
            addCell(row.getCell(1), klientIn.getKinderfacharztName());
            addCell(row.getCell(9), klientIn.getKinderfacharztTelefonnummer());

            // Finanzierung
            addFinanzierung(xssfSheet, finanzierungList);

            // betreuende DGKP
            row = xssfSheet.getRow(20);
            addCell(row.getCell(9), klientIn.getWeitereKrankenpflegerinnen());

            // Betreuungsbeginn
            row = xssfSheet.getRow(21);
            addCell(row.getCell(1), dateFormat(klientIn.getBetreuungsbeginn()));
            addCell(row.getCell(7), dateFormat(klientIn.getBetreuungsende()));
            addCell(row.getCell(12), klientIn.getKmProHb());

            // Betreuungsstunden
            row = xssfSheet.getRow(22);

            addCell(row.getCell(3), betreuungsstunden.get(0).getStunden());
            addCell(row.getCell(6), betreuungsstunden.get(1).getStunden());
            addCell(row.getCell(9), betreuungsstunden.get(2).getStunden());

        } else {
            String error = "KlientIn object ist Null.";
            this.logger.error(error);
            throw new MokiBusinessException(error);
        }
    }

    private void addFinanzierung(XSSFSheet xssfSheet, List<Finanzierung> finanzierungList) {

        finanzierungList.forEach(finanzierung -> {
            Row rowFinanzierung;
            switch (finanzierung.getInstitution()) {
                case LAND:
                    rowFinanzierung = xssfSheet.getRow(15);
                    addCell(rowFinanzierung.getCell(1), finanzierung.isKostenUebernahme() ? "X" : "O");
                    addCell(rowFinanzierung.getCell(7), finanzierung.isKostenSelbstbehalt() ? "X" : "O");
                    break;
                case MUKI:
                    rowFinanzierung = xssfSheet.getRow(15);
                    addCell(rowFinanzierung.getCell(9), finanzierung.isKostenUebernahme() ? "X" : "O");
                    addCell(rowFinanzierung.getCell(12), finanzierung.isKostenSelbstbehalt() ? "X" : "O");
                    break;
                case KINDER_JUGENDHILFE:
                    rowFinanzierung = xssfSheet.getRow(16);
                    addCell(rowFinanzierung.getCell(1), finanzierung.isKostenUebernahme() ? "X" : "O");
                    addCell(rowFinanzierung.getCell(7), finanzierung.isKostenSelbstbehalt() ? "X" : "O");
                    break;
                case VERSICHERUNG:
                    rowFinanzierung = xssfSheet.getRow(16);
                    addCell(rowFinanzierung.getCell(9), finanzierung.isKostenUebernahme() ? "X" : "O");
                    addCell(rowFinanzierung.getCell(12), finanzierung.isKostenSelbstbehalt() ? "X" : "O");
                    break;
                case HOSPIZ:
                    rowFinanzierung = xssfSheet.getRow(17);
                    addCell(rowFinanzierung.getCell(1), finanzierung.isKostenUebernahme() ? "X" : "O");
                    addCell(rowFinanzierung.getCell(7), finanzierung.isKostenSelbstbehalt() ? "X" : "O");
                    break;
                case SPENDEN:
                    rowFinanzierung = xssfSheet.getRow(17);
                    addCell(rowFinanzierung.getCell(9), finanzierung.isKostenUebernahme() ? "X" : "O");
                    addCell(rowFinanzierung.getCell(12), finanzierung.isKostenSelbstbehalt() ? "X" : "O");
                    break;
                case SOZIALVERSICHERUNG:
                    rowFinanzierung = xssfSheet.getRow(18);
                    addCell(rowFinanzierung.getCell(1), finanzierung.isKostenUebernahme() ? "X" : "O");
                    addCell(rowFinanzierung.getCell(7), finanzierung.isKostenSelbstbehalt() ? "X" : "O");
                    break;
                case PRIVAT:
                    rowFinanzierung = xssfSheet.getRow(18);
                    addCell(rowFinanzierung.getCell(9), finanzierung.isKostenUebernahme() ? "X" : "O");
                    addCell(rowFinanzierung.getCell(12), finanzierung.isKostenSelbstbehalt() ? "X" : "O");
                    break;
                case KIB:
                    rowFinanzierung = xssfSheet.getRow(19);
                    addCell(rowFinanzierung.getCell(1), finanzierung.isKostenUebernahme() ? "X" : "O");
                    addCell(rowFinanzierung.getCell(7), finanzierung.isKostenSelbstbehalt() ? "X" : "O");
                    break;
                case ANDERE:
                    rowFinanzierung = xssfSheet.getRow(19);
                    addCell(rowFinanzierung.getCell(9), finanzierung.isKostenUebernahme() ? "X" : "O");
                    addCell(rowFinanzierung.getCell(12), finanzierung.isKostenSelbstbehalt() ? "X" : "O");
                    break;
                default:
                    this.logger.error("Taetigkeitsfeld nicht gesetzt.");
                    throw new UnsupportedOperationException("Taetigkeitsfeld nicht gesetzt.");
            }
        });
    }

    private void addOrganisationsprotokoll(XSSFSheet xssfSheet, String weitereKontaktdaten, List<Organisationsprotokoll> organisationsprotokollList) {

        Row row = xssfSheet.getRow(4);
        Cell cell = row.getCell(2);

        addCell(cell, row, weitereKontaktdaten, 3);

        // export excel
        for (int i = 0; i < organisationsprotokollList.size(); i++) {

            // Get Row at index 1
            row = xssfSheet.getRow(ROW_INDEX + i);

            if (row == null) {
                row = xssfSheet.createRow(ROW_INDEX + i);
            }

            Organisationsprotokoll organisationsprotokoll = organisationsprotokollList.get(i);

            // Datum
            cell = row.getCell(0);
            addCell(cell, row, organisationsprotokoll.getAenderungszeitpunkt()
                    .format(MokiDateFormatter.DATE_FORMATER), 0);

            // Zeit
            cell = row.getCell(1);
            addCell(cell, row, organisationsprotokoll.getAenderungszeitpunkt()
                    .format(MokiDateFormatter.TIME_FORMATER), 1);

            // Text
            cell = row.getCell(2);
            addCell(cell, row, organisationsprotokoll.getText(), 2);

            // Handschrift
            cell = row.getCell(10);
            addCell(cell, row, organisationsprotokoll.getHandschriftDgkp(), 10);
        }
    }

    private void addCell(Cell cell, String value) {
        if (cell != null) {
            cell.setCellValue(value == null ? "" : value);
        } else {
            this.logger.error("Konnte Daten nicht in Zelle schreiben.");
            throw new MokiBusinessException("Konnte Daten nicht in Zelle schreiben.");
        }
    }

    private void addCell(Cell cell, Row row, String value, int column) {
        if (cell != null) {
            cell.setCellValue(value);
        } else {
            row.createCell(column, CellType.STRING).setCellValue(value);
        }
    }

    private String dateFormat(LocalDate localDate) {
        return localDate == null ? null : localDate.format(MokiDateFormatter.DATE_FORMATER);
    }
}
