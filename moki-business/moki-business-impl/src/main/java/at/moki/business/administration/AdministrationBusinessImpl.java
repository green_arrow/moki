package at.moki.business.administration;

import at.moki.dbrepo.user.UserRepo;
import at.moki.dbrepo.userrechte.UserrechteRepo;
import at.moki.domainmodel.user.MokiUser;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.domainmodel.userrechte.Userrechte;
import at.moki.exception.MokiBusinessException;
import at.moki.exception.MokiDbRepoException;
import at.moki.logger.MokiLogger;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Green Arrow on 06.02.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
@Transactional
public class AdministrationBusinessImpl implements AdministrationBusiness, Serializable {

    private static final long serialVersionUID = 7683118780468903934L;

    @Inject
    @MokiLogger
    private Logger logger;

    @Inject
    private UserRepo userRepo;

    @Inject
    private UserrechteRepo userrechteRepo;

    @Override
    public List<MokiUser> getAllUsersWithUserrechte() {
        this.logger.info("Userrechte werden geladen.");

        try {
            // get all userrechte
            List<Userrechte> userrechteList = this.userrechteRepo.findAll();

            // map userrechte to map
            Map<Object, MokiUserRole> userrechteMap = userrechteList.stream().collect(
                    Collectors.toMap(Userrechte::getId, Userrechte::getMokiUserRole));

            // get all users
            List<MokiUser> mokiUserList = this.userRepo.findAll();

            // iterate through users and set user role according to id
            mokiUserList.forEach(item -> item.setMokiUserRole(userrechteMap.get(item.getUserrechteId())));

            return mokiUserList;
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }
}
