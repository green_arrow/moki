package at.moki.business.klientin;

import at.moki.business.betreuungsstunden.BetreuungsstundenBusiness;
import at.moki.business.verwandte.VerwandteBusiness;
import at.moki.dbrepo.betreuungsstunden.BetreuungsstundenRepo;
import at.moki.dbrepo.finanzierung.FinanzierungRepo;
import at.moki.dbrepo.klientin.KlientInRepo;
import at.moki.dbrepo.klientin.KlientInZusaetzlicheDokumenteRepo;
import at.moki.dbrepo.klientin.TaetigkeitsfeldRepo;
import at.moki.dbrepo.organisationsprotokoll.OrganisationsprotokollRepo;
import at.moki.dbrepo.verwandte.VerwandteRepo;
import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.domainmodel.finanzierung.Finanzierung;
import at.moki.domainmodel.klientin.FK;
import at.moki.domainmodel.klientin.KJH;
import at.moki.domainmodel.klientin.KlientIn;
import at.moki.domainmodel.klientin.Taetigkeitsfeld;
import at.moki.domainmodel.verwandte.Verwandte;
import at.moki.exception.MokiBusinessException;
import at.moki.exception.MokiDbRepoException;
import at.moki.logger.MokiLogger;
import at.moki.util.MokiFilterUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Green Arrow on 01.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
@Transactional
public class KlientInBusinessImpl implements KlientInBusiness, Serializable {

    private static final long serialVersionUID = 4060537654798955241L;

    private static final String KLIENTEN_NUMMER_NOT_EMPTY = "Klienten Nummer darf nicht fehlen";

    @Inject
    @MokiLogger
    private Logger logger;

    @Inject
    private KlientInRepo klientInRepo;

    @Inject
    private FinanzierungRepo finanzierungRepo;

    @Inject
    private VerwandteBusiness verwandteBusiness;

    @Inject
    private BetreuungsstundenRepo betreuungsstundenRepo;

    @Inject
    private KlientInZusaetzlicheDokumenteRepo klientInZusaetzlicheDokumenteRepo;

    @Inject
    private VerwandteRepo verwandteRepo;

    @Inject
    private OrganisationsprotokollRepo organisationsprotokollRepo;

    @Inject
    private TaetigkeitsfeldRepo taetigkeitsfeldRepo;

    @Inject
    private BetreuungsstundenBusiness betreuungsstundenBusiness;

    @Override
    public List<KlientIn> getData(String klientenNummerFilter, String vornameFilter, String nachnameFilter, String klientInTaetigkeitsfeldFilter) {
        this.logger.info("KlientInnen werden geladen und anhand von klientenNummerFilter, " +
                "vornameFilter und nachnameFilter gefiltert.");
        try {
            return this.klientInRepo.findAll().stream()
                    .filter(e -> MokiFilterUtil.containsIgnoreCaseMatch(e.getKlientenNummer(), klientenNummerFilter))
                    .filter(e -> MokiFilterUtil.containsIgnoreCaseMatch(e.getVorname(), vornameFilter))
                    .filter(e -> MokiFilterUtil.containsIgnoreCaseMatch(e.getNachname(), nachnameFilter))
                    .filter(e -> MokiFilterUtil.containsIgnoreCaseMatch(e.getTaetigkeitsfeld(), klientInTaetigkeitsfeldFilter))
                    .collect(Collectors.toList());
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public List<String> getTaetigkeitsfelderAsList() {
        return this.taetigkeitsfeldRepo.findAll().stream()
                .map(Taetigkeitsfeld::getBezeichnung)
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public void save(KlientIn klientIn) {
        try {
            this.klientInRepo.merge(klientIn);
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public void save(KlientIn klientIn, List<Betreuungsstunden> betreuungsstunden, List<Finanzierung> finanzierungList) {
        this.logger.info("Klientin wird angelegt.");

        validateKlientInNummer(klientIn.getKlientenNummer());

        String klientInUuid = saveKlientIn(klientIn, betreuungsstunden);
        saveFinanzierung(klientInUuid, finanzierungList);
    }

    @Override
    public void saveWithGeschwister(FK fk) {

        this.logger.info("Klientin wird mit Geschwistern angelegt.");

        validateKlientInNummer(fk.getKlientIn().getKlientenNummer());

        String klientInUuid = saveKlientIn(fk.getKlientIn(), fk.getBetreuungsstundenKlientIn());
        saveFinanzierung(klientInUuid, fk.getFinanzierungList());

        String zwillingUuid = saveKlientIn(fk.getZwilling(), fk.getBetreuungsstundenZwilling());
        String drillingUuid = saveKlientIn(fk.getDrilling(), fk.getBetreuungsstundenDrilling());

        saveVerwandte(klientInUuid, zwillingUuid, drillingUuid);
    }

    @Override
    public void saveWithKinder(KJH kjh) {

        this.logger.info("Klientin wird mit Kindern angelegt.");

        validateKlientInNummer(kjh.getKlientIn().getKlientenNummer());

        String klientInUuid = saveKlientIn(kjh.getKlientIn(), kjh.getBetreuungsstundenKlientIn());
        saveFinanzierung(klientInUuid, kjh.getFinanzierungList());

        String zweitesKindUuid = saveKlientIn(kjh.getZweitesKind(), kjh.getBetreuungsstundenZweitesKind());
        String drittesKindUuid = saveKlientIn(kjh.getDrittesKind(), kjh.getBetreuungsstundenDrittesKind());
        String viertesKindUuid = saveKlientIn(kjh.getViertesKind(), kjh.getBetreuungsstundenViertesKind());

        saveVerwandte(klientInUuid, zweitesKindUuid, drittesKindUuid, viertesKindUuid);
    }

    @Override
    public void update(KlientIn klientIn, List<Betreuungsstunden> betreuungsstundenKlientIn, List<Finanzierung> finanzierungList) {
        this.logger.info("Daten der KlientIn werden aktualisiert.");

        try {
            this.klientInRepo.merge(klientIn);
            updateBetreuungsstunden(klientIn.getId(), this.betreuungsstundenBusiness.getDataBy(klientIn.getId()), betreuungsstundenKlientIn);
            updateFinanzierung(klientIn.getId(), finanzierungList);
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public void updateWithGeschwister(FK fk) {

        this.logger.info("Daten der KlientIn und deren Geschwister werden aktualisiert.");

        try {
            this.klientInRepo.merge(fk.getKlientIn());
            this.klientInRepo.merge(fk.getZwilling());
            this.klientInRepo.merge(fk.getDrilling());

            updateBetreuungsstunden(fk.getKlientIn().getId(), this.betreuungsstundenBusiness.getDataBy(fk.getKlientIn().getId()), fk.getBetreuungsstundenKlientIn());
            updateBetreuungsstunden(fk.getZwilling().getId(), this.betreuungsstundenBusiness.getDataBy(fk.getZwilling().getId()), fk.getBetreuungsstundenZwilling());
            updateBetreuungsstunden(fk.getDrilling().getId(), this.betreuungsstundenBusiness.getDataBy(fk.getDrilling().getId()), fk.getBetreuungsstundenDrilling());

            updateFinanzierung(fk.getKlientIn().getId(), fk.getFinanzierungList());
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public void updateWithKinder(KJH kjh) {

        this.logger.info("Daten der KlientIn mit Kindern werden aktualisiert.");

        try {
            this.klientInRepo.merge(kjh.getKlientIn());
            this.klientInRepo.merge(kjh.getZweitesKind());
            this.klientInRepo.merge(kjh.getDrittesKind());
            this.klientInRepo.merge(kjh.getViertesKind());

            updateBetreuungsstunden(kjh.getKlientIn().getId(), this.betreuungsstundenBusiness.getDataBy(kjh.getKlientIn().getId()), kjh.getBetreuungsstundenKlientIn());
            updateBetreuungsstunden(kjh.getZweitesKind().getId(), this.betreuungsstundenBusiness.getDataBy(kjh.getZweitesKind().getId()), kjh.getBetreuungsstundenZweitesKind());
            updateBetreuungsstunden(kjh.getDrittesKind().getId(), this.betreuungsstundenBusiness.getDataBy(kjh.getDrittesKind().getId()), kjh.getBetreuungsstundenDrittesKind());
            updateBetreuungsstunden(kjh.getViertesKind().getId(), this.betreuungsstundenBusiness.getDataBy(kjh.getViertesKind().getId()), kjh.getBetreuungsstundenViertesKind());

            updateFinanzierung(kjh.getKlientIn().getId(), kjh.getFinanzierungList());
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public List<KlientIn> getKlientInGeschwister(KlientIn klientIn) {
        this.logger.info("KlientInnen mit Geschwister werden geladen.");

        try {
            List<KlientIn> klientInList = new ArrayList<>();

            Verwandte verwandte = this.verwandteBusiness.getDataByKlientInId(klientIn.getId());

            List<String> verwandteList = verwandte.getVerwandteList();

            // since it is FK the size MUST be 2!
            if (verwandteList.size() != 2) {
                throw new MokiBusinessException("Konnte Geschwister zu KlientIn FK nicht laden.");
            }

            KlientIn zwilling = this.klientInRepo.findByKlientInId(verwandteList.get(0));
            klientInList.add(zwilling);
            KlientIn drilling = this.klientInRepo.findByKlientInId(verwandteList.get(1));
            klientInList.add(drilling);

            return klientInList;
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public List<KlientIn> getKlientInWithKinder(KlientIn klientIn) {
        this.logger.info("KlientInnen mit Kindern werden geladen.");

        try {
            List<KlientIn> klientInList = new ArrayList<>();

            Verwandte verwandte = this.verwandteBusiness.getDataByKlientInId(klientIn.getId());

            List<String> verwandteList = verwandte.getVerwandteList();

            // since it is KJH the size MUST be 3!
            if (verwandteList.size() != 3) {
                throw new MokiBusinessException("Konnte Verwandte zu KlientIn KJH nicht laden.");
            }

            KlientIn erstesKind = this.klientInRepo.findByKlientInId(verwandteList.get(0));
            klientInList.add(erstesKind);
            KlientIn zweitesKind = this.klientInRepo.findByKlientInId(verwandteList.get(1));
            klientInList.add(zweitesKind);
            KlientIn drittesKind = this.klientInRepo.findByKlientInId(verwandteList.get(2));
            klientInList.add(drittesKind);

            return klientInList;
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public void delete(KlientIn klientIn) {
        String klientInId = klientIn.getId();

        this.organisationsprotokollRepo.removeByKlientInId(klientInId);
        this.finanzierungRepo.removeAllby(klientInId);
        this.betreuungsstundenRepo.remove(klientInId);
        this.klientInZusaetzlicheDokumenteRepo.removeAllByKlientInId(klientInId);

        if (StringUtils.equalsAnyIgnoreCase(klientIn.getTaetigkeitsfeld(), "FK", "KJH")) {
            Verwandte verwandte = this.verwandteBusiness.getDataByKlientInId(klientInId);
            cleanupVerwandte(verwandte);
        }

        this.klientInRepo.remove(klientIn.getId());
    }

    private String saveKlientIn(KlientIn klientIn, List<Betreuungsstunden> betreuungsstunden) {

        String klientInUuid = UUID.randomUUID().toString();
        klientIn.setId(klientInUuid);

        try {
            this.klientInRepo.merge(klientIn);
            saveBetreuungsstunden(klientInUuid, betreuungsstunden);
            return klientInUuid;
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    private void cleanupVerwandte(Verwandte verwandte) {

        this.verwandteRepo.removeByKlientInId(verwandte.getKlientInId());

        String verwandte1Id = verwandte.getVerwandteList().get(0);
        this.finanzierungRepo.removeAllby(verwandte1Id);
        this.betreuungsstundenRepo.remove(verwandte1Id);
        this.klientInRepo.remove(verwandte1Id);

        String verwandte2Id = verwandte.getVerwandteList().get(1);
        this.finanzierungRepo.removeAllby(verwandte2Id);
        this.betreuungsstundenRepo.remove(verwandte2Id);
        this.klientInRepo.remove(verwandte2Id);

        if (verwandte.getVerwandteList().size() == 3) {
            String verwandte3Id = verwandte.getVerwandteList().get(2);
            this.finanzierungRepo.removeAllby(verwandte3Id);
            this.betreuungsstundenRepo.remove(verwandte3Id);
            this.klientInRepo.remove(verwandte3Id);
        }
    }

    private void saveBetreuungsstunden(String klientInId, List<Betreuungsstunden> betreuungsstunden) {
        betreuungsstunden.forEach(betreuungsstundenKlientIn -> {
            betreuungsstundenKlientIn.setId(UUID.randomUUID().toString());
            betreuungsstundenKlientIn.setKlientInId(klientInId);

            this.betreuungsstundenRepo.merge(betreuungsstundenKlientIn);
        });
    }

    private void updateBetreuungsstunden(String klientInId, List<Betreuungsstunden> betreuungsstundenOriginalList,
                                         List<Betreuungsstunden> betreuungsstundenUpdated) {

        for (int i = 0; i < betreuungsstundenUpdated.size(); i++) {
            Betreuungsstunden betreuungsstunden = betreuungsstundenUpdated.get(i);
            Betreuungsstunden betreuungsstundenOriginal = betreuungsstundenOriginalList.get(i);

            betreuungsstunden.setId(betreuungsstundenOriginal.getId());
            betreuungsstunden.setKlientInId(klientInId);

            this.betreuungsstundenRepo.merge(betreuungsstunden);
        }
    }

    private void saveFinanzierung(String klientInUuid, List<Finanzierung> finanzierungList) {
        finanzierungList.forEach(finanzierung -> {
            finanzierung.setId(UUID.randomUUID().toString());
            finanzierung.setKlientInId(klientInUuid);

            this.finanzierungRepo.merge(finanzierung);
        });
    }

    private void updateFinanzierung(String klientInUuid, List<Finanzierung> finanzierungList) {
        finanzierungList.forEach(finanzierung -> {
            finanzierung.setKlientInId(klientInUuid);

            this.finanzierungRepo.merge(finanzierung);
        });
    }

    private void saveVerwandte(String klientInUuid, String... kinder) {
        Verwandte verwandte = new Verwandte();
        verwandte.setId(UUID.randomUUID().toString());
        verwandte.setKlientInId(klientInUuid);
        verwandte.setVerwandteList(Stream.of(kinder).collect(Collectors.toList()));
        this.verwandteBusiness.save(verwandte);
    }

    private void validateKlientInNummer(String klientenNummer) {
        if (StringUtils.isEmpty(klientenNummer)) {
            throw new MokiBusinessException(KLIENTEN_NUMMER_NOT_EMPTY);
        }
    }
}
