package at.moki.business.excel;

import at.moki.dbrepo.einstellungen.EinstellungenRepo;
import at.moki.einstellungen.TaetigkeitsfeldJson;
import at.moki.exception.MokiBusinessException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.util.List;

/**
 * @author Green Arrow
 * @date 13.08.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
class AbsExcelBusiness {

    @Inject
    private EinstellungenRepo einstellungenRepo;

    TaetigkeitsfeldJson getTaetigkeitsfeldJson(String taetigkeitsfeld) throws java.io.IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        List<TaetigkeitsfeldJson> list = objectMapper.readValue(this.einstellungenRepo.findByTaetigkeitsfeld().getInhalt(),
                new TypeReference<List<TaetigkeitsfeldJson>>() {
                });

        return list.stream()
                .filter(json -> StringUtils.equals(taetigkeitsfeld, json.getTaetigkietsfeld()))
                .findFirst()
                .orElseThrow(() -> new MokiBusinessException("Taetigkeitsfeld konnte aus Datenbank nicht geladen werden"));
    }

}
