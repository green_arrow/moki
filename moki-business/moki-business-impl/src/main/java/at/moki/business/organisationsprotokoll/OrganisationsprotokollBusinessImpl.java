package at.moki.business.organisationsprotokoll;

import at.moki.dbrepo.organisationsprotokoll.OrganisationsprotokollRepo;
import at.moki.domainmodel.organisationsprotokoll.Organisationsprotokoll;
import at.moki.exception.MokiBusinessException;
import at.moki.exception.MokiDbRepoException;
import at.moki.logger.MokiLogger;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * Created by Green Arrow on 18.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
@Transactional
public class OrganisationsprotokollBusinessImpl implements OrganisationsprotokollBusiness, Serializable {

    private static final long serialVersionUID = -3761947366920474969L;

    @Inject
    @MokiLogger
    private Logger logger;

    @Inject
    private OrganisationsprotokollRepo organisationsprotokollRepo;

    @Override
    public List<Organisationsprotokoll> getData(String klientInId) {
        this.logger.info("Organisationsprotokoll-Daten zur klientInId werden geladen.");

        try {
            return this.organisationsprotokollRepo.findByKlientInId(klientInId);
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public void save(Organisationsprotokoll organisationsprotokoll) {
        this.logger.info("Organisationsprotokoll wird angelegt.");

        organisationsprotokoll.setId(UUID.randomUUID().toString());
        organisationsprotokoll.setAenderungszeitpunkt(LocalDateTime.now());

        try {
            this.organisationsprotokollRepo.merge(organisationsprotokoll);
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }
}
