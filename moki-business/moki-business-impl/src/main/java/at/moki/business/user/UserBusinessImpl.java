package at.moki.business.user;

import at.moki.dbrepo.krankenpflegerin.KrankenpflegerinRepo;
import at.moki.dbrepo.user.UserRepo;
import at.moki.dbrepo.userrechte.UserrechteRepo;
import at.moki.domainmodel.user.MokiUser;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.domainmodel.userrechte.Userrechte;
import at.moki.encryption.MokiEncryption;
import at.moki.exception.MokiBusinessException;
import at.moki.exception.MokiDbRepoException;
import at.moki.exception.MokiSecurityException;
import at.moki.logger.MokiLogger;
import at.moki.session.SessionModel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by Green Arrow on 22.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
@Transactional
public class UserBusinessImpl implements UserBusiness, Serializable {

    private static final long serialVersionUID = 1499551988948563773L;

    @Inject
    @MokiLogger
    private Logger logger;

    @Inject
    private UserRepo userRepo;

    @Inject
    private UserrechteRepo userrechteRepo;

    @Inject
    private SessionModel sessionModel;

    @Inject
    private KrankenpflegerinRepo krankenpflegerinRepo;

    @Inject
    private MokiEncryption mokiEncryption;

    @Override
    public List<MokiUser> getData() {
        return this.userRepo.findAll();
    }

    @Override
    public MokiUser getUser(String username, String password) {
        this.logger.info("Login daten werden gelesen.");

        try {
            MokiUser mokiUser = checkIfUserIsAvailable(username);
            // mokiUser exists
            if (mokiUser != null) {
                if (mokiUser.isGesperrt()) {
                    throw new MokiSecurityException("MokiUser ist bereits gesperrt!");
                }

                // extract password
                String passwordAndSaltFromDb = mokiUser.getPassword();
                String saltFromDb = StringUtils.substring(passwordAndSaltFromDb, 32, 47);
                String passwordActual = this.mokiEncryption.generateHashPassword(password, saltFromDb);

                // mokiUser gave a wrong password
                if (!StringUtils.equals(passwordAndSaltFromDb, passwordActual)) {
                    Integer counter = mokiUser.getSperrungCounter();
                    if (counter == null || counter == 0) {
                        mokiUser.setSperrungCounter(1);
                    } else if (counter != 3) {
                        mokiUser.setSperrungCounter(counter + 1);
                    }

                    if (mokiUser.getSperrungCounter() == 3) {
                        mokiUser.setGesperrt(true);
                    }

                    // update db
                    update(mokiUser);

                    return mokiUser;

                } else {    // username and passsword are correct, programm continues

                    mokiUser.setSperrungCounter(null); // reset counter, mokiUser gave correct username and password

                    // update db
                    update(mokiUser);

                    Userrechte userrechte = this.userrechteRepo.findById(mokiUser.getUserrechteId());
                    mokiUser.setMokiUserRole(userrechte.getMokiUserRole());

                    if (MokiUserRole.KRANKENPFLEGERIN == mokiUser.getMokiUserRole()) {
                        // only for a krankenpflegerin the session model is set
                        this.sessionModel.setKrankenpflegerin(this.krankenpflegerinRepo.findByKlientInId(mokiUser.getKrankenpflegerinId()));
                    } else {
                        // logged in mokiUser is admin or verwaltung
                        this.sessionModel.setKrankenpflegerin(null);
                    }
                    this.sessionModel.setMokiUser(mokiUser);

                    return mokiUser;
                }
            } else {
                throw new MokiBusinessException("Benutzername und/oder Passwort ist falsch, bitte Überprüfen Sie Ihre Eingaben");
            }
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public void save(MokiUser mokiUser) {
        this.logger.info("Neuer mokiUser wird angelegt.");

        try {
            mokiUser.setId(UUID.randomUUID().toString());
            Userrechte userrechte = this.userrechteRepo.findByBezeichnung(MokiUserRole.VERWALTUNG);
            mokiUser.setUserrechteId(userrechte.getId());
            this.userRepo.merge(mokiUser);

        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public void update(MokiUser mokiUser) {
        this.logger.info("Daten werden fuer MokiUser werden aktualisiert.");

        try {
            this.userRepo.merge(mokiUser);
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public void delete(MokiUser mokiUser) {
        this.logger.info("Der MokiUser wird mit der Krankenpflegerin geloescht.");

        this.userRepo.remove(mokiUser.getId());
        this.krankenpflegerinRepo.remove(mokiUser.getKrankenpflegerinId());
    }

    MokiUser checkIfUserIsAvailable(String username) {
        Optional<MokiUser> userOptional = getData().stream()
                .filter(e -> StringUtils.equals(e.getUsername(), username))
                .findAny();

        return userOptional.orElse(null);
    }
}
