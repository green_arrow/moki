package at.moki.business.klientin;

import at.moki.dbrepo.klientin.KlientInDokumentRepo;
import at.moki.dbrepo.klientin.KlientInZusaetzlicheDokumenteRepo;
import at.moki.domainmodel.klientin.KlientInZusaetzlicheDokumente;
import at.moki.exception.MokiBusinessException;
import at.moki.exception.MokiDbRepoException;
import at.moki.logger.MokiLogger;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * Created by Green Arrow on 01.02.2019.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@Named
@Transactional
public class KlientInZusaetzlicheDokumenteBusinessImpl implements KlientInZusaetzlicheDokumenteBusiness, Serializable {

    private static final long serialVersionUID = -2264660381790845967L;

    @Inject
    @MokiLogger
    private Logger logger;

    @Inject
    private KlientInDokumentRepo klientInDokumentRepo;

    @Inject
    private KlientInZusaetzlicheDokumenteRepo klientInZusaetzlicheDokumenteRepo;

    @Override
    public List<KlientInZusaetzlicheDokumente> findAllByKlientInId(String klientInId) {
        try {
            return this.klientInZusaetzlicheDokumenteRepo.findAllByKlientInId(klientInId);
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public byte[] getById(String id) {
        try {
            return this.klientInDokumentRepo.findById(id);
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public void save(KlientInZusaetzlicheDokumente klientInZusaetzlicheDokumente, byte[] byteData) {
        try {
            String klientInZuseatzlicheDokumenteId = UUID.randomUUID().toString();
            klientInZusaetzlicheDokumente.setId(klientInZuseatzlicheDokumenteId);
            klientInZusaetzlicheDokumente.setErstellungszeitpunkt(LocalDateTime.now());
            this.klientInZusaetzlicheDokumenteRepo.merge(klientInZusaetzlicheDokumente);
            this.klientInDokumentRepo.merge(UUID.randomUUID().toString(), byteData, klientInZuseatzlicheDokumenteId);
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }

    @Override
    public void deleteById(String id) {
        try {
            this.klientInZusaetzlicheDokumenteRepo.removeById(id);
        } catch (MokiDbRepoException e) {
            throw new MokiBusinessException(e);
        }
    }
}
