package at.moki.business.excel;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Green Arrow on 26.08.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
abstract class AbsExcelExportBusinessTest {

    boolean excelFilesIdentical(InputStream inputStreamExpected, InputStream inputStreamActual) throws IOException {

        XSSFWorkbook xssfWorkbookExpected = new XSSFWorkbook(inputStreamExpected);
        XSSFWorkbook xssfWorkbookActual = new XSSFWorkbook(inputStreamActual);

        List<String> excelDifference = ExcelComparator.compare(xssfWorkbookExpected, xssfWorkbookActual);

        if (excelDifference.isEmpty()) {
            return true;
        } else {
            excelDifference.forEach(System.out::println);
            return false;
        }
    }
}
