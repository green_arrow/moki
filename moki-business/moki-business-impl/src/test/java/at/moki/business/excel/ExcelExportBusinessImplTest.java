package at.moki.business.excel;

import at.moki.business.betreuungsstunden.BetreuungsstundenBusiness;
import at.moki.business.finanzierung.FinanzierungBusiness;
import at.moki.business.organisationsprotokoll.OrganisationsprotokollBusiness;
import at.moki.dbrepo.einstellungen.EinstellungenRepo;
import at.moki.dbrepo.excel.dateien.ExcelDateinRepo;
import at.moki.dbrepo.klientin.TaetigkeitsfeldRepo;
import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.domainmodel.excel.dateien.ExcelDatei;
import at.moki.domainmodel.finanzierung.Finanzierung;
import at.moki.domainmodel.finanzierung.Institutions;
import at.moki.domainmodel.klientin.KlientIn;
import at.moki.domainmodel.klientin.Taetigkeitsfeld;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;
import at.moki.domainmodel.organisationsprotokoll.Organisationsprotokoll;
import at.moki.einstellungen.Einstellungen;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import java.awt.*;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Green Arrow on 25.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(MockitoJUnitRunner.class)
public class ExcelExportBusinessImplTest extends AbsExcelExportBusinessTest {

    private boolean PRINT_EXCEL = false;

    private static final String TEMP_DIRECTORY = System.getProperty("java.io.tmpdir");

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();

    @Mock
    private Logger logger;

    @Mock
    private ExcelDateinRepo excelDateinRepo;

    @Mock
    private EinstellungenRepo einstellungenRepo;

    @Mock
    private TaetigkeitsfeldRepo taetigkeitsfeldRepo;

    @Mock
    private FinanzierungBusiness finanzierungBusiness;

    @Mock
    private BetreuungsstundenBusiness betreuungsstundenBusiness;

    @Mock
    private OrganisationsprotokollBusiness organisationsprotokollBusiness;

    @InjectMocks
    private ExcelExportBusinessImpl excelExportBusiness = new ExcelExportBusinessImpl();

    @Before
    public void initExternalResources() throws IOException {
        configureTaetigkeitsfelder();

        Mockito.when(this.finanzierungBusiness.getData(Mockito.anyString())).thenReturn(createFinanzierungsList());
        Mockito.when(this.organisationsprotokollBusiness.getData(Mockito.anyString())).thenReturn(createOrganisationsprotokollList());

        Einstellungen einstellungen = new Einstellungen();
        einstellungen.setFachlichkeit("TAETIGKEITSFELDER");
        einstellungen.setInhalt(FileUtils.readFileToString(new File("src/test/resources/excel/export/compare/taetigkeitsfelder.json"), StandardCharsets.UTF_8));
        Mockito.when(this.einstellungenRepo.findByTaetigkeitsfeld()).thenReturn(einstellungen);
    }

    @Test
    public void testExcelExportIch() throws IOException {

        InputStream excelInputStream = this.getClass().getResourceAsStream("/excel/export/compare/ICH.xlsx");

        ExcelDatei excelDatei = new ExcelDatei();
        excelDatei.setDateiname("ICH.xlsx");
        excelDatei.setDatei(IOUtils.toByteArray(excelInputStream));

        excelInputStream.close();

        Mockito.when(this.excelDateinRepo.findByTaetigkeitsfeldId(Mockito.anyString())).thenReturn(excelDatei);
        Mockito.when(this.betreuungsstundenBusiness.getDataBy(Mockito.eq("fcbcd6f8-5041-424d-a41f-715eefb6ee7a"))).thenReturn(createBetreuungsstunden());

        ByteArrayOutputStream excelActual = this.excelExportBusiness.exportSingleKlientIn(
                createKrankenpflegerin(), createKlientIn("ICH"));

        Assert.assertNotNull(excelActual);

        print(excelActual, TEMP_DIRECTORY + "ICH.xlsx");

        File actualExcelFile = this.testFolder.newFile("ICH.xlsx");
        FileUtils.writeByteArrayToFile(actualExcelFile, excelActual.toByteArray());

        Assert.assertEquals(true, super.excelFilesIdentical(this.getClass().getResourceAsStream(
                "/excel/export/compare/ICH.xlsx"), new FileInputStream(actualExcelFile)));
    }

    @Test
    public void testExcelExportFrz() throws IOException {

        InputStream excelInputStream = this.getClass().getResourceAsStream("/excel/export/compare/FRZ.xlsx");

        ExcelDatei excelDatei = new ExcelDatei();
        excelDatei.setDateiname("FRZ.xlsx");
        excelDatei.setDatei(IOUtils.toByteArray(excelInputStream));

        excelInputStream.close();

        Mockito.when(this.excelDateinRepo.findByTaetigkeitsfeldId(Mockito.anyString())).thenReturn(excelDatei);
        Mockito.when(this.betreuungsstundenBusiness.getDataBy(Mockito.eq("fcbcd6f8-5041-424d-a41f-715eefb6ee7a"))).thenReturn(createBetreuungsstunden());

        ByteArrayOutputStream excelActual = this.excelExportBusiness.exportSingleKlientIn(
                createKrankenpflegerin(), createKlientIn("FRZ"));

        Assert.assertNotNull(excelActual);

        print(excelActual, TEMP_DIRECTORY + "FRZ.xlsx");

        File actualExcelFile = this.testFolder.newFile("FRZ.xlsx");
        FileUtils.writeByteArrayToFile(actualExcelFile, excelActual.toByteArray());

        Assert.assertEquals(true, super.excelFilesIdentical(this.getClass().getResourceAsStream(
                "/excel/export/compare/FRZ.xlsx"), new FileInputStream(actualExcelFile)));
    }

    @Test
    public void testExcelExportMpt() throws IOException {

        InputStream excelInputStream = this.getClass().getResourceAsStream("/excel/export/compare/MPT.xlsx");

        ExcelDatei excelDatei = new ExcelDatei();
        excelDatei.setDateiname("MPT.xlsx");
        excelDatei.setDatei(IOUtils.toByteArray(excelInputStream));

        excelInputStream.close();

        Mockito.when(this.excelDateinRepo.findByTaetigkeitsfeldId(Mockito.anyString())).thenReturn(excelDatei);
        Mockito.when(this.betreuungsstundenBusiness.getDataBy(Mockito.eq("fcbcd6f8-5041-424d-a41f-715eefb6ee7a"))).thenReturn(createBetreuungsstunden());

        ByteArrayOutputStream excelActual = this.excelExportBusiness.exportSingleKlientIn(
                createKrankenpflegerin(), createKlientIn("MPT"));

        Assert.assertNotNull(excelActual);

        print(excelActual, TEMP_DIRECTORY + "MPT.xlsx");

        File actualExcelFile = this.testFolder.newFile("MPT.xlsx");
        FileUtils.writeByteArrayToFile(actualExcelFile, excelActual.toByteArray());

        Assert.assertEquals(true, super.excelFilesIdentical(this.getClass().getResourceAsStream(
                "/excel/export/compare/MPT.xlsx"), new FileInputStream(actualExcelFile)));
    }

    @Test
    public void testExcelExportTs() throws IOException {

        InputStream excelInputStream = this.getClass().getResourceAsStream("/excel/export/compare/TS.xlsx");

        ExcelDatei excelDatei = new ExcelDatei();
        excelDatei.setDateiname("TS.xlsx");
        excelDatei.setDatei(IOUtils.toByteArray(excelInputStream));

        excelInputStream.close();

        Mockito.when(this.excelDateinRepo.findByTaetigkeitsfeldId(Mockito.anyString())).thenReturn(excelDatei);
        Mockito.when(this.betreuungsstundenBusiness.getDataBy(Mockito.eq("fcbcd6f8-5041-424d-a41f-715eefb6ee7a"))).thenReturn(createBetreuungsstunden());

        KlientIn klientIn = createKlientIn("TS");
        klientIn.setHauptwohnsitz(true);
        ByteArrayOutputStream excelActual = this.excelExportBusiness.exportSingleKlientIn(
                createKrankenpflegerin(), klientIn);

        Assert.assertNotNull(excelActual);

        print(excelActual, TEMP_DIRECTORY + "TS.xlsx");

        File actualExcelFile = this.testFolder.newFile("TS.xlsx");
        FileUtils.writeByteArrayToFile(actualExcelFile, excelActual.toByteArray());

        Assert.assertEquals(true, super.excelFilesIdentical(this.getClass().getResourceAsStream(
                "/excel/export/compare/TS.xlsx"), new FileInputStream(actualExcelFile)));
    }

    @Test
    public void testExcelExportFrzWithNoKrankenpflegerin() throws IOException {
        InputStream excelInputStream = this.getClass().getResourceAsStream("/excel/export/compare/IchFrzMptTs.xlsx");

        ExcelDatei excelDatei = new ExcelDatei();
        excelDatei.setDateiname("IchFrzMptTs.xlsx");
        excelDatei.setDatei(IOUtils.toByteArray(excelInputStream));

        excelInputStream.close();

        Mockito.when(this.excelDateinRepo.findByTaetigkeitsfeldId(Mockito.anyString())).thenReturn(excelDatei);
        Mockito.when(this.betreuungsstundenBusiness.getDataBy(Mockito.eq("fcbcd6f8-5041-424d-a41f-715eefb6ee7a"))).thenReturn(createBetreuungsstunden());

        ByteArrayOutputStream excelActual = this.excelExportBusiness.exportSingleKlientIn(
                null, createKlientIn("FRZ"));

        Assert.assertNotNull(excelActual);

        print(excelActual, TEMP_DIRECTORY + "IchFrzMptTs.xlsx");

        File actualExcelFile = this.testFolder.newFile("IchFrzMptTs.xlsx");
        FileUtils.writeByteArrayToFile(actualExcelFile, excelActual.toByteArray());

        Assert.assertEquals(true, super.excelFilesIdentical(this.getClass().getResourceAsStream(
                "/excel/export/compare/IchFrzMptTs.xlsx"), new FileInputStream(actualExcelFile)));
    }

    @Test
    public void testExcelExportFk() throws IOException {

        InputStream excelInputStream = this.getClass().getResourceAsStream("/excel/export/compare/FK.xlsx");

        ExcelDatei excelDatei = new ExcelDatei();
        excelDatei.setDateiname("FK.xlsx");
        excelDatei.setDatei(IOUtils.toByteArray(excelInputStream));

        excelInputStream.close();

        Mockito.when(this.excelDateinRepo.findByTaetigkeitsfeldId(Mockito.anyString())).thenReturn(excelDatei);
        Mockito.when(this.betreuungsstundenBusiness.getDataBy(Mockito.eq("fcbcd6f8-5041-424d-a41f-715eefb6ee7a"))).thenReturn(createBetreuungsstunden());
        Mockito.when(this.betreuungsstundenBusiness.getDataBy(Mockito.eq("2a8ce1dc-678b-42f4-a233-dba777a661da"))).thenReturn(createBetreuungsstunden());
        Mockito.when(this.betreuungsstundenBusiness.getDataBy(Mockito.eq("89ae1a18-98ff-4473-b72a-1d487418c961"))).thenReturn(createBetreuungsstunden());


        KlientIn klientIn = createKlientIn("FK");

        KlientIn zwilling = createKlientIn("FK");
        zwilling.setId("2a8ce1dc-678b-42f4-a233-dba777a661da");

        KlientIn drilling = createKlientIn("FK");
        drilling.setId("89ae1a18-98ff-4473-b72a-1d487418c961");

        ByteArrayOutputStream excelActual = this.excelExportBusiness.exportFk(createKrankenpflegerin(), klientIn, zwilling, drilling);

        Assert.assertNotNull(excelActual);

        print(excelActual, TEMP_DIRECTORY + "FK.xlsx");

        File actualExcelFile = this.testFolder.newFile("FK.xlsx");
        FileUtils.writeByteArrayToFile(actualExcelFile, excelActual.toByteArray());

        Assert.assertEquals(true, super.excelFilesIdentical(this.getClass().getResourceAsStream(
                "/excel/export/compare/FK.xlsx"), new FileInputStream(actualExcelFile)));
    }

    @Test
    public void testExcelExportKjh() throws IOException {

        InputStream excelInputStream = this.getClass().getResourceAsStream("/excel/export/compare/KJH.xlsx");

        ExcelDatei excelDatei = new ExcelDatei();
        excelDatei.setDateiname("KJH.xlsx");
        excelDatei.setDatei(IOUtils.toByteArray(excelInputStream));

        excelInputStream.close();

        Mockito.when(this.excelDateinRepo.findByTaetigkeitsfeldId(Mockito.anyString())).thenReturn(excelDatei);
        Mockito.when(this.betreuungsstundenBusiness.getDataBy(Mockito.eq("fcbcd6f8-5041-424d-a41f-715eefb6ee7a"))).thenReturn(createBetreuungsstunden());
        Mockito.when(this.betreuungsstundenBusiness.getDataBy(Mockito.eq("383ae93e-3f07-49dc-9b02-4a0c29d9fafd"))).thenReturn(createBetreuungsstunden());
        Mockito.when(this.betreuungsstundenBusiness.getDataBy(Mockito.eq("2d8d249b-d3f1-4bd7-b5bc-3b94497779aa"))).thenReturn(createBetreuungsstunden());
        Mockito.when(this.betreuungsstundenBusiness.getDataBy(Mockito.eq("e6dd0bc2-60c2-4427-b6a5-c9264fecf00b"))).thenReturn(createBetreuungsstunden());

        KlientIn klientIn = createKlientIn("KJH");

        KlientIn zweitesKind = createKlientIn("FK");
        zweitesKind.setId("383ae93e-3f07-49dc-9b02-4a0c29d9fafd");

        KlientIn drittesKind = createKlientIn("FK");
        drittesKind.setId("2d8d249b-d3f1-4bd7-b5bc-3b94497779aa");

        KlientIn viertesKind = createKlientIn("FK");
        viertesKind.setId("e6dd0bc2-60c2-4427-b6a5-c9264fecf00b");

        ByteArrayOutputStream excelActual = this.excelExportBusiness.exportKjh(createKrankenpflegerin(),
                klientIn, zweitesKind, drittesKind, viertesKind);

        Assert.assertNotNull(excelActual);

        print(excelActual, TEMP_DIRECTORY + "KJH.xlsx");

        File actualExcelFile = this.testFolder.newFile("KJH.xlsx");
        FileUtils.writeByteArrayToFile(actualExcelFile, excelActual.toByteArray());

        Assert.assertEquals(true, super.excelFilesIdentical(this.getClass().getResourceAsStream(
                "/excel/export/compare/KJH.xlsx"), new FileInputStream(actualExcelFile)));
    }

    private Krankenpflegerin createKrankenpflegerin() {
        // create krankenpflegerin
        Krankenpflegerin krankenpflegerin = new Krankenpflegerin();
        krankenpflegerin.setVorname("Anja");
        krankenpflegerin.setNachname("K.");
        krankenpflegerin.setStrasse("Deutschland");
        krankenpflegerin.setPostleitzahl("2500");
        krankenpflegerin.setBezeichnung("MOBILE KINDERKRANKENPFLEGE");
        krankenpflegerin.setTelefonnummer("0123456789");

        return krankenpflegerin;
    }

    private KlientIn createKlientIn(String klientInTaetigkeitsfeld) {
        // create klientin
        KlientIn klientIn = new KlientIn();
        klientIn.setId("fcbcd6f8-5041-424d-a41f-715eefb6ee7a");
        klientIn.setTaetigkeitsfeld(klientInTaetigkeitsfeld);
        klientIn.setVorname("Anja");
        klientIn.setNachname("K.");
        klientIn.setWeitereKontaktdaten("Weitere Daten fuer Anja");
        klientIn.setGeburtsdatum(LocalDate.of(1985, 5, 31));
        klientIn.setDiagnose("ICD-10");
        klientIn.setStrasse("Linz");
        klientIn.setBetreuungsbeginn(LocalDate.of(1990, 1, 1));
        klientIn.setBetreuungsende(LocalDate.of(1995, 1, 31));
        klientIn.setBetreuendeKrankenpflegerin("vorname nachname");
        klientIn.setSpende("4561");
        klientIn.setWerkvertragsNummer("9876");
        klientIn.setKmProHb("20");
        klientIn.setSterbedatum(LocalDate.of(2015, 5, 5));
        klientIn.setDiagnoseschluessel("jo");
        klientIn.setMutterVertreterin(true);
        klientIn.setVaterVertreter(false);
        klientIn.setSonstigeVertreter(true);

        return klientIn;
    }

    private List<Finanzierung> createFinanzierungsList() {

        List<Finanzierung> finanzierungList = new ArrayList<>();

        Finanzierung finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.HOSPIZ);
        finanzierung.setKostenSelbstbehalt(true);
        finanzierung.setKostenUebernahme(true);

        finanzierungList.add(finanzierung);

        return finanzierungList;
    }

    private List<Betreuungsstunden> createBetreuungsstunden() {

        List<Betreuungsstunden> betreuungsstundenMap = new ArrayList<>();

        Betreuungsstunden betreuungsstunden = new Betreuungsstunden();
        betreuungsstunden.setJahr(2017);
        betreuungsstunden.setStunden("20");

        betreuungsstundenMap.add(betreuungsstunden);

        betreuungsstunden = new Betreuungsstunden();
        betreuungsstunden.setJahr(2018);
        betreuungsstunden.setStunden("20");

        betreuungsstundenMap.add(betreuungsstunden);

        betreuungsstunden = new Betreuungsstunden();
        betreuungsstunden.setJahr(2019);
        betreuungsstunden.setStunden("20");

        betreuungsstundenMap.add(betreuungsstunden);

        return betreuungsstundenMap;
    }

    private List<Organisationsprotokoll> createOrganisationsprotokollList() {
        List<Organisationsprotokoll> organisationsprotokollList = new ArrayList<>();

        for (int i = 0; i < 30; i++) {
            Organisationsprotokoll organisationsprotokoll = new Organisationsprotokoll();
            organisationsprotokoll.setAenderungszeitpunkt(LocalDateTime.of(2018, 9, 14, 22, 39, 22));

            organisationsprotokoll.setText("Text " + i);
            organisationsprotokoll.setHandschriftDgkp("Anja");

            organisationsprotokollList.add(organisationsprotokoll);
        }

        return organisationsprotokollList;
    }

    private void print(ByteArrayOutputStream generatedExcelStream, String path) throws IOException {
        if (this.PRINT_EXCEL) {
            OutputStream outputStream = new FileOutputStream(path);
            generatedExcelStream.writeTo(outputStream);
            outputStream.close();
            try {
                Desktop.getDesktop().browse(new URI(StringUtils.replace(path, "\\", "/")));
            } catch (IOException | URISyntaxException e1) {
                e1.printStackTrace();
            }
            System.out.println("Opened document in:");
            System.out.println(path);
        }
    }

    private void configureTaetigkeitsfelder() {
        Taetigkeitsfeld taetigkeitsfeldFk = new Taetigkeitsfeld();
        taetigkeitsfeldFk.setBezeichnung("FK");
        Mockito.when(this.taetigkeitsfeldRepo.findByBezeichnung(Mockito.eq("FK"))).thenReturn(taetigkeitsfeldFk);

        Taetigkeitsfeld taetigkeitsfeldFRZ = new Taetigkeitsfeld();
        taetigkeitsfeldFRZ.setBezeichnung("FRZ");
        Mockito.when(this.taetigkeitsfeldRepo.findByBezeichnung(Mockito.eq("FRZ"))).thenReturn(taetigkeitsfeldFRZ);

        Taetigkeitsfeld taetigkeitsfeldICH = new Taetigkeitsfeld();
        taetigkeitsfeldICH.setBezeichnung("ICH");
        Mockito.when(this.taetigkeitsfeldRepo.findByBezeichnung(Mockito.eq("ICH"))).thenReturn(taetigkeitsfeldICH);

        Taetigkeitsfeld taetigkeitsfeldKJH = new Taetigkeitsfeld();
        taetigkeitsfeldKJH.setBezeichnung("KJH");
        Mockito.when(this.taetigkeitsfeldRepo.findByBezeichnung(Mockito.eq("KJH"))).thenReturn(taetigkeitsfeldKJH);

        Taetigkeitsfeld taetigkeitsfeldMPT = new Taetigkeitsfeld();
        taetigkeitsfeldMPT.setBezeichnung("MPT");
        Mockito.when(this.taetigkeitsfeldRepo.findByBezeichnung(Mockito.eq("MPT"))).thenReturn(taetigkeitsfeldMPT);

        Taetigkeitsfeld taetigkeitsfeldTS = new Taetigkeitsfeld();
        taetigkeitsfeldTS.setBezeichnung("TS");
        Mockito.when(this.taetigkeitsfeldRepo.findByBezeichnung(Mockito.eq("TS"))).thenReturn(taetigkeitsfeldTS);
    }
}
