package at.moki.business.administration;

import at.moki.dbrepo.user.UserRepo;
import at.moki.dbrepo.userrechte.UserrechteRepo;
import at.moki.domainmodel.user.MokiUser;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.domainmodel.userrechte.Userrechte;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.unitils.reflectionassert.ReflectionAssert;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Green Arrow on 20.02.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(MockitoJUnitRunner.class)
public class AdministrationBusinessImplTest {

    @Mock
    private Logger logger;

    @Mock
    private UserRepo userRepo;

    @Mock
    private UserrechteRepo userrechteRepo;

    @InjectMocks
    private AdministrationBusiness administrationBusiness = new AdministrationBusinessImpl();

    private static String USER1_ID = UUID.randomUUID().toString();
    private static String USER2_ID = UUID.randomUUID().toString();
    private static String USER3_ID = UUID.randomUUID().toString();

    private static String USERRECHTE_1ID = UUID.randomUUID().toString();
    private static String USERRECHTE_2ID = UUID.randomUUID().toString();
    private static String USERRECHTE_3ID = UUID.randomUUID().toString();

    @Test
    public void testGetAllUsersWithUserrechte() {
        Mockito.when(this.userrechteRepo.findAll()).thenReturn(generateFakeUserrechteData());
        Mockito.when(this.userRepo.findAll()).thenReturn(genreateUserData());

        List<MokiUser> mokiUserList = new ArrayList<>();

        MokiUser mokiUser = new MokiUser();
        mokiUser.setId(USER1_ID);
        mokiUser.setUsername("vorname1");
        mokiUser.setMokiUserRole(MokiUserRole.ADMINISTRATOR);
        mokiUser.setUserrechteId(USERRECHTE_1ID);
        mokiUserList.add(mokiUser);

        mokiUser = new MokiUser();
        mokiUser.setId(USER2_ID);
        mokiUser.setUsername("vorname2");
        mokiUser.setMokiUserRole(MokiUserRole.KRANKENPFLEGERIN);
        mokiUser.setUserrechteId(USERRECHTE_2ID);
        mokiUserList.add(mokiUser);

        mokiUser = new MokiUser();
        mokiUser.setId(USER3_ID);
        mokiUser.setUsername("vorname3");
        mokiUser.setMokiUserRole(MokiUserRole.VERWALTUNG);
        mokiUser.setUserrechteId(USERRECHTE_3ID);
        mokiUserList.add(mokiUser);

        ReflectionAssert.assertReflectionEquals(mokiUserList, this.administrationBusiness.getAllUsersWithUserrechte());
    }

    private List<Userrechte> generateFakeUserrechteData() {

        List<Userrechte> userrechteList = new ArrayList<>();

        Userrechte userrechte = new Userrechte();
        userrechte.setId(USERRECHTE_1ID);
        userrechte.setMokiUserRole(MokiUserRole.ADMINISTRATOR);
        userrechteList.add(userrechte);

        userrechte = new Userrechte();
        userrechte.setId(USERRECHTE_2ID);
        userrechte.setMokiUserRole(MokiUserRole.KRANKENPFLEGERIN);
        userrechteList.add(userrechte);

        userrechte = new Userrechte();
        userrechte.setId(USERRECHTE_3ID);
        userrechte.setMokiUserRole(MokiUserRole.VERWALTUNG);
        userrechteList.add(userrechte);

        return userrechteList;
    }

    private List<MokiUser> genreateUserData() {
        List<MokiUser> mokiUserList = new ArrayList<>();

        MokiUser mokiUser = new MokiUser();
        mokiUser.setId(USER1_ID);
        mokiUser.setUsername("vorname1");
        mokiUser.setMokiUserRole(null);
        mokiUser.setUserrechteId(USERRECHTE_1ID);
        mokiUserList.add(mokiUser);

        mokiUser = new MokiUser();
        mokiUser.setId(USER2_ID);
        mokiUser.setUsername("vorname2");
        mokiUser.setMokiUserRole(null);
        mokiUser.setUserrechteId(USERRECHTE_2ID);
        mokiUserList.add(mokiUser);

        mokiUser = new MokiUser();
        mokiUser.setId(USER3_ID);
        mokiUser.setUsername("vorname3");
        mokiUser.setMokiUserRole(null);
        mokiUser.setUserrechteId(USERRECHTE_3ID);
        mokiUserList.add(mokiUser);

        return mokiUserList;
    }
}
