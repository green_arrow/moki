package at.moki.business.excel;

import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.domainmodel.finanzierung.Finanzierung;
import at.moki.domainmodel.finanzierung.Institutions;
import at.moki.domainmodel.klientin.Geschlecht;
import at.moki.domainmodel.klientin.KlientIn;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;

import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Green Arrow on 08.09.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
abstract class AbsExcelImportBusinessTest {

    Krankenpflegerin generateExpectedKrankenpflegerinInData() {

        Krankenpflegerin krankenpflegerin = new Krankenpflegerin();

        krankenpflegerin.setVorname("Anja");
        krankenpflegerin.setNachname("K.");
        krankenpflegerin.setBezeichnung("Mobile Kinderkrankenpflege");
        krankenpflegerin.setStrasse("Straße und Hausnummer");
        krankenpflegerin.setPostleitzahl("94094, Rotthalmünster");
        krankenpflegerin.setTelefonnummer("8533");
        krankenpflegerin.setBankverbindung("Deutsche Bank");
        krankenpflegerin.setIban(null);
        krankenpflegerin.setBic(null);

        return krankenpflegerin;
    }


    KlientIn generateExpectedKlientInData() {

        KlientIn klientIn = new KlientIn();

        klientIn.setTaetigkeitsfeldNummer("12345");
        klientIn.setVerrechnungNummer("122");
        klientIn.setSpende("2");
        klientIn.setWerkvertragsNummer("3");
        klientIn.setWeitereKontaktdaten(null);
        klientIn.setGeschlecht(Geschlecht.WEIBLICH);
        klientIn.setImpacctGruppe("IMPaCCT Gruppe");
        klientIn.setPflegestufe("5");
        klientIn.setSozialversicherungsnummer("1234567890");
        klientIn.setGeburtsdatum(LocalDate.of(1985, 5, 1));
        klientIn.setSozialversicherungstraeger("OÖGKK");
        klientIn.setDiagnose("Diagnose");
        klientIn.setDiagnoseschluessel("Diagnoseschlüssel");
        klientIn.setSterbedatum(null);
        klientIn.setMutterVertreterin(true);
        klientIn.setMutterName("Mutter");
        klientIn.setMutterGeburtsdatum(LocalDate.of(1985, 5, 31));
        klientIn.setMutterTelefonnummer("??");
        klientIn.setVaterVertreter(false);
        klientIn.setVaterName("Vater");
        klientIn.setVaterGeburtsdatum(LocalDate.of(1985, 5, 1));
        klientIn.setVaterTelefonnummer("??");
        klientIn.setSonstigeVertreter(false);
        klientIn.setSonstigeName("Sonstige");
        klientIn.setSonstigeGeburtsdatum(null);
        klientIn.setSonstigeTelefonnummer("??");
        klientIn.setStrasse("Straße");
        klientIn.setMutterEmail("E-Mail Mutter");
        klientIn.setOrt("Plz + Ort");
        klientIn.setVaterEmail("E-Mail Vater");
        klientIn.setHauptwohnsitz(true);
        klientIn.setZuweisendeStelleKH("KH");
        klientIn.setZuweisendeStelleKJH("KJH");
        klientIn.setZuweisendeStelleSonstige("sonstige");
        klientIn.setKinderfacharztName("Kinderfacharzt");
        klientIn.setKinderfacharztTelefonnummer(null);
        klientIn.setBetreuendeKrankenpflegerin("Anja K.");
        klientIn.setWeitereKrankenpflegerinnen("weitere DGKP");
        klientIn.setBetreuungsbeginn(null);
        klientIn.setBetreuungsende(LocalDate.of(2019, 1, 4));
        klientIn.setKmProHb("40");

        return klientIn;
    }

    List<Betreuungsstunden> generateExpectedBetreuungsstundenData() {

        List<Betreuungsstunden> betreuungsstundenList = new ArrayList<>();

        betreuungsstundenList.add(new Betreuungsstunden(null, Year.now().getValue(), "4.5", null));
        betreuungsstundenList.add(new Betreuungsstunden(null, Year.now().plusYears(1).getValue(), "10", null));
        betreuungsstundenList.add(new Betreuungsstunden(null, Year.now().plusYears(2).getValue(), "14.89", null));

        return betreuungsstundenList;
    }

    List<Finanzierung> generateExpectedFinanzierungData() {

        List<Finanzierung> finanzierungList = new ArrayList<>();

        Finanzierung finanzierung = new Finanzierung();

        // LAND
        finanzierung.setInstitution(Institutions.LAND);
        finanzierung.setKostenUebernahme(true);
        finanzierung.setKostenSelbstbehalt(false);
        finanzierungList.add(finanzierung);

        // MUKI
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.MUKI);
        finanzierung.setKostenUebernahme(false);
        finanzierung.setKostenSelbstbehalt(false);
        finanzierungList.add(finanzierung);

        // KINDER_JUGENDHILFE
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.KINDER_JUGENDHILFE);
        finanzierung.setKostenUebernahme(false);
        finanzierung.setKostenSelbstbehalt(false);
        finanzierungList.add(finanzierung);

        // VERSICHERUNG
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.VERSICHERUNG);
        finanzierung.setKostenUebernahme(false);
        finanzierung.setKostenSelbstbehalt(true);
        finanzierungList.add(finanzierung);

        // HOSPIZ
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.HOSPIZ);
        finanzierung.setKostenUebernahme(false);
        finanzierung.setKostenSelbstbehalt(false);
        finanzierungList.add(finanzierung);

        // SPENDEN
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.SPENDEN);
        finanzierung.setKostenUebernahme(true);
        finanzierung.setKostenSelbstbehalt(false);
        finanzierungList.add(finanzierung);

        // SOZIALVERSICHERUNG
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.SOZIALVERSICHERUNG);
        finanzierung.setKostenUebernahme(true);
        finanzierung.setKostenSelbstbehalt(false);
        finanzierungList.add(finanzierung);

        // PRIVAT
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.PRIVAT);
        finanzierung.setKostenUebernahme(false);
        finanzierung.setKostenSelbstbehalt(false);
        finanzierungList.add(finanzierung);

        // KIB
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.KIB);
        finanzierung.setKostenUebernahme(true);
        finanzierung.setKostenSelbstbehalt(true);
        finanzierungList.add(finanzierung);

        // ANDERE
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.ANDERE);
        finanzierung.setKostenUebernahme(false);
        finanzierung.setKostenSelbstbehalt(true);
        finanzierungList.add(finanzierung);

        return finanzierungList;
    }

}
