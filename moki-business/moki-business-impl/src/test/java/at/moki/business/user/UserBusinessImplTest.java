package at.moki.business.user;

import at.moki.dbrepo.krankenpflegerin.KrankenpflegerinRepo;
import at.moki.dbrepo.user.UserRepo;
import at.moki.dbrepo.userrechte.UserrechteRepo;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;
import at.moki.domainmodel.user.MokiUser;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.domainmodel.userrechte.Userrechte;
import at.moki.encryption.MokiEncryption;
import at.moki.exception.MokiBusinessException;
import at.moki.exception.MokiSecurityException;
import at.moki.session.SessionModel;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Green Arrow on 20.02.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserBusinessImplTest {

    @Mock
    private Logger logger;

    @Mock
    private UserRepo userRepo;

    @Mock
    private UserrechteRepo userrechteRepo;

    @Mock
    private KrankenpflegerinRepo krankenpflegerinRepo;

    @Spy
    private SessionModel sessionModel = new SessionModel();

    @Spy
    private MokiEncryption mokiEncryption = new MokiEncryption();

    @InjectMocks
    private UserBusinessImpl userBusiness = new UserBusinessImpl();

    @Test(expected = MokiBusinessException.class)
    public void testGetUserNotAvailable() {
        Mockito.when(this.userRepo.findAll()).thenReturn(generateTestUserList());

        this.userBusiness.getUser("katya", "password");
    }

    @Test(expected = MokiSecurityException.class)
    public void testGetUserGesperrt() {
        Mockito.when(this.userRepo.findAll()).thenReturn(generateTestUserList());

        this.userBusiness.getUser("edith", "password");
    }

    @Test
    public void testGetUserWrongPassword() {

        Mockito.when(this.userRepo.findAll()).thenReturn(generateTestUserList());

        // first login request
        MokiUser mokiUserExpected = this.userBusiness.getUser("astrid", "falsches passwort");
        Assert.assertEquals(Integer.valueOf(1), mokiUserExpected.getSperrungCounter());

        // second login request
        mokiUserExpected = this.userBusiness.getUser("astrid", "falsches passwort");
        Assert.assertEquals(Integer.valueOf(2), mokiUserExpected.getSperrungCounter());

        // third login request
        mokiUserExpected = this.userBusiness.getUser("astrid", "falsches passwort");
        Assert.assertEquals(Integer.valueOf(3), mokiUserExpected.getSperrungCounter());
        Assert.assertEquals(true, mokiUserExpected.isGesperrt());
    }

    @Test
    public void testGetUserCorrectPassword() {

        // mock userRepo
        Mockito.when(this.userRepo.findAll()).thenReturn(generateTestUserList());

        // mock userrechteRepo
        Userrechte userrechte = new Userrechte();
        userrechte.setMokiUserRole(MokiUserRole.KRANKENPFLEGERIN);
        Mockito.when(this.userrechteRepo.findById(Mockito.eq("astrid userrechte id"))).thenReturn(userrechte);

        // mock krankenpflegerinRepo
        Krankenpflegerin krankenpflegerin = new Krankenpflegerin();
        krankenpflegerin.setId("astrid krankenpflegerin id");
        Mockito.when(this.krankenpflegerinRepo.findByKlientInId(Mockito.eq("astrid krankenpflegerin id"))).thenReturn(krankenpflegerin);

        // first login request
        MokiUser mokiUserExpected = this.userBusiness.getUser("astrid", "falsches passwort");
        Assert.assertEquals(Integer.valueOf(1), mokiUserExpected.getSperrungCounter());

        // second login request
        mokiUserExpected = this.userBusiness.getUser("astrid", "falsches passwort");
        Assert.assertEquals(Integer.valueOf(2), mokiUserExpected.getSperrungCounter());
        Assert.assertEquals(false, mokiUserExpected.isGesperrt());

        // correct password
        mokiUserExpected = this.userBusiness.getUser("astrid", "Astrid");
        Assert.assertEquals(null, mokiUserExpected.getSperrungCounter());
        Assert.assertEquals(false, mokiUserExpected.isGesperrt());
    }

    @Test
    public void testSave() {

        Userrechte userrechteMock = new Userrechte();
        userrechteMock.setId("userrechteId");
        userrechteMock.setMokiUserRole(MokiUserRole.VERWALTUNG);

        Mockito.when(this.userrechteRepo.findByBezeichnung(MokiUserRole.VERWALTUNG)).thenReturn(userrechteMock);

        MokiUser mokiUser = new MokiUser();
        mokiUser.setUsername("anja");
        mokiUser.setPassword("password");
        mokiUser.setKrankenpflegerinId("krankenpflegerinId");
        mokiUser.setSperrungCounter(1);
        mokiUser.setGesperrt(false);

        this.userBusiness.save(mokiUser);

        ArgumentCaptor<MokiUser> captor = ArgumentCaptor.forClass(MokiUser.class);
        Mockito.verify(this.userRepo, Mockito.times(1))
                .merge(captor.capture());

        Assert.assertNotNull(captor.getValue().getId());
        Assert.assertEquals("userrechteId", captor.getValue().getUserrechteId());
        Assert.assertEquals("anja", captor.getValue().getUsername());
        Assert.assertEquals("password", captor.getValue().getPassword());
        Assert.assertEquals("krankenpflegerinId", captor.getValue().getKrankenpflegerinId());
        Assert.assertEquals(Integer.valueOf(1), captor.getValue().getSperrungCounter());
        Assert.assertEquals(false, captor.getValue().isGesperrt());
    }

    @Test
    public void testCheckIfUserIsAvailable() {

        Mockito.when(this.userRepo.findAll()).thenReturn(generateTestUserList());

        Assert.assertEquals("anja", this.userBusiness.checkIfUserIsAvailable("anja").getUsername());
        Assert.assertEquals(null, this.userBusiness.checkIfUserIsAvailable("katya"));
    }

    private List<MokiUser> generateTestUserList() {
        List<MokiUser> mokiUserList = new ArrayList<>();
        MokiUser mokiUserAnja = new MokiUser();
        mokiUserAnja.setUsername("anja");
        mokiUserList.add(mokiUserAnja);

        MokiUser mokiUserEdith = new MokiUser();
        mokiUserEdith.setUsername("edith");
        mokiUserEdith.setGesperrt(true);
        mokiUserList.add(mokiUserEdith);

        MokiUser mokiUserMegan = new MokiUser();
        mokiUserMegan.setUsername("megan");
        mokiUserList.add(mokiUserMegan);

        MokiUser mokiUserAstrid = new MokiUser();
        mokiUserAstrid.setUsername("astrid");
        mokiUserAstrid.setPassword("5487a608ede54ef8055f1e1577a4c244123456789012345");
        mokiUserAstrid.setUserrechteId("astrid userrechte id");
        mokiUserAstrid.setKrankenpflegerinId("astrid krankenpflegerin id");
        mokiUserList.add(mokiUserAstrid);

        return mokiUserList;
    }
}
