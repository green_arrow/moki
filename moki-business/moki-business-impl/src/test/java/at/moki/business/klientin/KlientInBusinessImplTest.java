package at.moki.business.klientin;

import at.moki.business.betreuungsstunden.BetreuungsstundenBusiness;
import at.moki.business.verwandte.VerwandteBusiness;
import at.moki.dbrepo.betreuungsstunden.BetreuungsstundenRepo;
import at.moki.dbrepo.finanzierung.FinanzierungRepo;
import at.moki.dbrepo.klientin.KlientInRepo;
import at.moki.dbrepo.klientin.TaetigkeitsfeldRepo;
import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.domainmodel.finanzierung.Finanzierung;
import at.moki.domainmodel.finanzierung.Institutions;
import at.moki.domainmodel.klientin.FK;
import at.moki.domainmodel.klientin.KJH;
import at.moki.domainmodel.klientin.KlientIn;
import at.moki.exception.MokiBusinessException;
import at.moki.exception.MokiDbRepoException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Green Arrow on 01.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(MockitoJUnitRunner.class)
public class KlientInBusinessImplTest {

    @Mock
    private Logger logger;

    @Mock
    private KlientInRepo klientInRepo;

    @Mock
    private FinanzierungRepo finanzierungRepo;

    @Mock
    private BetreuungsstundenRepo betreuungsstundenRepo;

    @Mock
    private VerwandteBusiness verwandteBusiness;

    @Mock
    private BetreuungsstundenBusiness betreuungsstundenBusiness;

    @Mock
    private TaetigkeitsfeldRepo taetigkeitsfeldRepo;

    @InjectMocks
    private KlientInBusinessImpl klientInBusiness = new KlientInBusinessImpl();

    @Test
    public void testSearchKlientIn() {

        Mockito.when(this.klientInRepo.findAll()).thenReturn(genreateKlientInData());
        List<KlientIn> klientInList = this.klientInBusiness.getData("123456", "Anja", null, null);

        Assert.assertEquals(3, klientInList.size());

        klientInList = this.klientInBusiness.getData(null, "Anja", "", null);
        Assert.assertEquals(3, klientInList.size());

        klientInList = this.klientInBusiness.getData(null, "Anja", "", "KJH");
        Assert.assertEquals(2, klientInList.size());

        klientInList = this.klientInBusiness.getData(null, "Anja", "", "TS");
        Assert.assertEquals(1, klientInList.size());
    }

    @Test
    public void testSaveKlientIn() {

        Mockito.when(this.klientInRepo.findAll()).thenReturn(genreateKlientInData());

        KlientIn klientIn = new KlientIn();
        klientIn.setKlientenNummer("1234567890");
        klientIn.setVorname("Astrid");
        klientIn.setNachname("J.");

        this.klientInBusiness.save(klientIn, generateBetreuungsstunden(), generateFinanzierungsList());

        Mockito.verify(this.klientInRepo, Mockito.times(1)).merge(Mockito.any());
        Mockito.verify(this.betreuungsstundenRepo, Mockito.times(3)).merge(Mockito.any());
        Mockito.verify(this.finanzierungRepo, Mockito.times(10)).merge(Mockito.any());
    }

    @Test
    public void testUpdateKlientIn() {

        Mockito.when(this.betreuungsstundenBusiness.getDataBy(Mockito.anyString())).thenReturn(generateBetreuungsstunden());

        KlientIn klientIn = new KlientIn();
        klientIn.setKlientenNummer("1234567890");
        klientIn.setVorname("Astrid");
        klientIn.setNachname("J.");

        this.klientInBusiness.update(klientIn, generateBetreuungsstunden(), generateFinanzierungsList());

        Mockito.verify(this.klientInRepo, Mockito.times(1)).merge(Mockito.any());
        Mockito.verify(this.betreuungsstundenRepo, Mockito.times(3)).merge(Mockito.any());
        Mockito.verify(this.finanzierungRepo, Mockito.times(10)).merge(Mockito.any());
    }

    @Test
    public void testSaveKlientInWithGeschwister() {

        Mockito.when(this.klientInRepo.findAll()).thenReturn(genreateKlientInData());

        KlientIn klientIn = new KlientIn();
        klientIn.setKlientenNummer("1234567890");
        klientIn.setVorname("Astrid");
        klientIn.setNachname("J.");

        KlientIn zwilling = new KlientIn();
        zwilling.setKlientenNummer("1234567891");
        zwilling.setVorname("Hilde");
        zwilling.setNachname("D.");

        KlientIn drilling = new KlientIn();
        drilling.setKlientenNummer("1234567892");
        drilling.setVorname("Megan");
        drilling.setNachname("S.");

        FK fk = new FK();
        fk.setKlientIn(klientIn);
        fk.setZwilling(zwilling);
        fk.setDrilling(drilling);
        fk.setBetreuungsstundenKlientIn(generateBetreuungsstunden());
        fk.setBetreuungsstundenZwilling(generateBetreuungsstunden());
        fk.setBetreuungsstundenDrilling(generateBetreuungsstunden());
        fk.setFinanzierungList(generateFinanzierungsList());

        this.klientInBusiness.saveWithGeschwister(fk);

        Mockito.verify(this.klientInRepo, Mockito.times(3)).merge(Mockito.any());
        Mockito.verify(this.betreuungsstundenRepo, Mockito.times(9)).merge(Mockito.any());
        Mockito.verify(this.finanzierungRepo, Mockito.times(10)).merge(Mockito.any());
        Mockito.verify(this.verwandteBusiness, Mockito.times(1)).save(Mockito.any());
    }

    @Test
    public void testUpdateKlientInWithGeschwister() {

        Mockito.when(this.betreuungsstundenBusiness.getDataBy(Mockito.anyString())).thenReturn(generateBetreuungsstunden());

        KlientIn klientIn = new KlientIn();
        klientIn.setKlientenNummer("1234567890");
        klientIn.setVorname("Astrid");
        klientIn.setNachname("J.");

        KlientIn zwilling = new KlientIn();
        zwilling.setKlientenNummer("1234567891");
        zwilling.setVorname("Hilde");
        zwilling.setNachname("D.");

        KlientIn drilling = new KlientIn();
        drilling.setKlientenNummer("1234567892");
        drilling.setVorname("Megan");
        drilling.setNachname("S.");

        FK fk = new FK();
        fk.setKlientIn(klientIn);
        fk.setZwilling(zwilling);
        fk.setDrilling(drilling);
        fk.setBetreuungsstundenKlientIn(generateBetreuungsstunden());
        fk.setBetreuungsstundenZwilling(generateBetreuungsstunden());
        fk.setBetreuungsstundenDrilling(generateBetreuungsstunden());
        fk.setFinanzierungList(generateFinanzierungsList());

        this.klientInBusiness.updateWithGeschwister(fk);

        Mockito.verify(this.klientInRepo, Mockito.times(3)).merge(Mockito.any());
        Mockito.verify(this.betreuungsstundenRepo, Mockito.times(9)).merge(Mockito.any());
        Mockito.verify(this.finanzierungRepo, Mockito.times(10)).merge(Mockito.any());
    }

    @Test
    public void testSaveKlientInWithKinder() {

        Mockito.when(this.klientInRepo.findAll()).thenReturn(genreateKlientInData());

        KlientIn klientIn = new KlientIn();
        klientIn.setKlientenNummer("1234567890");
        klientIn.setVorname("Astrid");
        klientIn.setNachname("J.");

        KlientIn zweitesKind = new KlientIn();
        zweitesKind.setKlientenNummer("1234567891");
        zweitesKind.setVorname("Hilde");
        zweitesKind.setNachname("D.");

        KlientIn drittesKind = new KlientIn();
        drittesKind.setKlientenNummer("1234567892");
        drittesKind.setVorname("Megan");
        drittesKind.setNachname("S.");

        KlientIn viertesKind = new KlientIn();
        viertesKind.setKlientenNummer("1234567893");
        viertesKind.setVorname("Whitney");
        viertesKind.setNachname("C.");

        KJH kjh = new KJH();
        kjh.setKlientIn(klientIn);
        kjh.setZweitesKind(zweitesKind);
        kjh.setDrittesKind(drittesKind);
        kjh.setViertesKind(viertesKind);
        kjh.setBetreuungsstundenKlientIn(generateBetreuungsstunden());
        kjh.setBetreuungsstundenZweitesKind(generateBetreuungsstunden());
        kjh.setBetreuungsstundenDrittesKind(generateBetreuungsstunden());
        kjh.setBetreuungsstundenViertesKind(generateBetreuungsstunden());
        kjh.setFinanzierungList(generateFinanzierungsList());

        this.klientInBusiness.saveWithKinder(kjh);

        Mockito.verify(this.klientInRepo, Mockito.times(4)).merge(Mockito.any());
        Mockito.verify(this.betreuungsstundenRepo, Mockito.times(12)).merge(Mockito.any());
        Mockito.verify(this.finanzierungRepo, Mockito.times(10)).merge(Mockito.any());
        Mockito.verify(this.verwandteBusiness, Mockito.times(1)).save(Mockito.any());
    }

    @Test
    public void testUpdateKlientInWithKinder() {

        Mockito.when(this.betreuungsstundenBusiness.getDataBy(Mockito.anyString())).thenReturn(generateBetreuungsstunden());

        KlientIn klientIn = new KlientIn();
        klientIn.setKlientenNummer("1234567890");
        klientIn.setVorname("Astrid");
        klientIn.setNachname("J.");

        KlientIn zweitesKind = new KlientIn();
        zweitesKind.setKlientenNummer("1234567891");
        zweitesKind.setVorname("Hilde");
        zweitesKind.setNachname("D.");

        KlientIn drittesKind = new KlientIn();
        drittesKind.setKlientenNummer("1234567892");
        drittesKind.setVorname("Megan");
        drittesKind.setNachname("S.");

        KlientIn viertesKind = new KlientIn();
        viertesKind.setKlientenNummer("1234567893");
        viertesKind.setVorname("Whitney");
        viertesKind.setNachname("C.");

        KJH kjh = new KJH();
        kjh.setKlientIn(klientIn);
        kjh.setZweitesKind(zweitesKind);
        kjh.setDrittesKind(drittesKind);
        kjh.setViertesKind(viertesKind);
        kjh.setBetreuungsstundenKlientIn(generateBetreuungsstunden());
        kjh.setBetreuungsstundenZweitesKind(generateBetreuungsstunden());
        kjh.setBetreuungsstundenDrittesKind(generateBetreuungsstunden());
        kjh.setBetreuungsstundenViertesKind(generateBetreuungsstunden());
        kjh.setFinanzierungList(generateFinanzierungsList());

        this.klientInBusiness.updateWithKinder(kjh);

        Mockito.verify(this.klientInRepo, Mockito.times(4)).merge(Mockito.any());
        Mockito.verify(this.betreuungsstundenRepo, Mockito.times(12)).merge(Mockito.any());
        Mockito.verify(this.finanzierungRepo, Mockito.times(10)).merge(Mockito.any());
    }

    @Test(expected = MokiBusinessException.class)
    public void testSaveWithSaveException() {

        Mockito.doThrow(new MokiDbRepoException()).when(klientInRepo).merge(Mockito.any());

        this.klientInBusiness.save(new KlientIn(), generateBetreuungsstunden(), generateFinanzierungsList());
    }

    private List<KlientIn> genreateKlientInData() {

        List<KlientIn> klientInList = new ArrayList<>();

        KlientIn klientIn = new KlientIn();
        klientIn.setKlientenNummer("123456");
        klientIn.setVorname("Anja2");
        klientIn.setNachname("");
        klientIn.setTaetigkeitsfeld("KJH");
        klientInList.add(klientIn);

        klientIn = new KlientIn();
        klientIn.setKlientenNummer("1234567");
        klientIn.setVorname("anja2");
        klientIn.setNachname(null);
        klientIn.setTaetigkeitsfeld("KJH");
        klientInList.add(klientIn);

        klientIn = new KlientIn();
        klientIn.setKlientenNummer("12345678");
        klientIn.setVorname("Anja");
        klientIn.setNachname("K.");
        klientIn.setTaetigkeitsfeld("TS");
        klientInList.add(klientIn);

        klientIn = new KlientIn();
        klientIn.setKlientenNummer("123456789");
        klientIn.setVorname("edith");
        klientIn.setNachname("");
        klientIn.setTaetigkeitsfeld("KJH");
        klientInList.add(klientIn);

        klientIn = new KlientIn();
        klientIn.setKlientenNummer("12345678910");
        klientIn.setVorname("Edith");
        klientIn.setNachname(null);
        klientIn.setTaetigkeitsfeld("FRZ");
        klientInList.add(klientIn);

        return klientInList;
    }

    private List<Betreuungsstunden> generateBetreuungsstunden() {

        List<Betreuungsstunden> betreuungsstundenMap = new ArrayList<>();

        Betreuungsstunden betreuungsstunden = new Betreuungsstunden();
        betreuungsstunden.setId("betreuungsstunden1");
        betreuungsstunden.setJahr(2017);
        betreuungsstunden.setStunden("20");

        betreuungsstundenMap.add(betreuungsstunden);

        betreuungsstunden.setId("betreuungsstunden2");
        betreuungsstunden = new Betreuungsstunden();
        betreuungsstunden.setJahr(2018);
        betreuungsstunden.setStunden("40");

        betreuungsstundenMap.add(betreuungsstunden);

        betreuungsstunden = new Betreuungsstunden();
        betreuungsstunden.setId("betreuungsstunden3");
        betreuungsstunden.setJahr(2019);
        betreuungsstunden.setStunden("60");

        betreuungsstundenMap.add(betreuungsstunden);

        return betreuungsstundenMap;
    }

    private List<Finanzierung> generateFinanzierungsList() {

        List<Finanzierung> finanzierungList = new ArrayList<>();

        Finanzierung finanzierung = new Finanzierung();

        // LAND
        finanzierung.setInstitution(Institutions.LAND);
        finanzierung.setKostenUebernahme(true);
        finanzierung.setKostenSelbstbehalt(false);
        finanzierungList.add(finanzierung);

        // MUKI
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.MUKI);
        finanzierung.setKostenUebernahme(false);
        finanzierung.setKostenSelbstbehalt(false);
        finanzierungList.add(finanzierung);

        // KINDER_JUGENDHILFE
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.KINDER_JUGENDHILFE);
        finanzierung.setKostenUebernahme(false);
        finanzierung.setKostenSelbstbehalt(false);
        finanzierungList.add(finanzierung);

        // VERSICHERUNG
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.VERSICHERUNG);
        finanzierung.setKostenUebernahme(false);
        finanzierung.setKostenSelbstbehalt(true);
        finanzierungList.add(finanzierung);

        // HOSPIZ
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.HOSPIZ);
        finanzierung.setKostenUebernahme(false);
        finanzierung.setKostenSelbstbehalt(false);
        finanzierungList.add(finanzierung);

        // SPENDEN
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.SPENDEN);
        finanzierung.setKostenUebernahme(true);
        finanzierung.setKostenSelbstbehalt(false);
        finanzierungList.add(finanzierung);

        // SOZIALVERSICHERUNG
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.SOZIALVERSICHERUNG);
        finanzierung.setKostenUebernahme(true);
        finanzierung.setKostenSelbstbehalt(false);
        finanzierungList.add(finanzierung);

        // PRIVAT
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.PRIVAT);
        finanzierung.setKostenUebernahme(false);
        finanzierung.setKostenSelbstbehalt(false);
        finanzierungList.add(finanzierung);

        // KIB
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.KIB);
        finanzierung.setKostenUebernahme(true);
        finanzierung.setKostenSelbstbehalt(true);
        finanzierungList.add(finanzierung);

        // ANDERE
        finanzierung = new Finanzierung();
        finanzierung.setInstitution(Institutions.ANDERE);
        finanzierung.setKostenUebernahme(false);
        finanzierung.setKostenSelbstbehalt(true);
        finanzierungList.add(finanzierung);

        return finanzierungList;
    }
}
