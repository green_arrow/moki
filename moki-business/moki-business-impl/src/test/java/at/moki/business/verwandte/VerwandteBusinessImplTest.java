package at.moki.business.verwandte;

import at.moki.dbrepo.verwandte.VerwandteRepo;
import at.moki.domainmodel.verwandte.Verwandte;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import java.util.ArrayList;

/**
 * Created by Green Arrow on 22.09.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(MockitoJUnitRunner.class)
public class VerwandteBusinessImplTest {

    @Mock
    private Logger logger;

    @Mock
    private VerwandteRepo verwandteRepo;

    @InjectMocks
    private VerwandteBusinessImpl verwandteBusiness = new VerwandteBusinessImpl();

    @Test
    public void testSave() {

        Verwandte verwandte = new Verwandte();
        verwandte.setKlientInId("klientinId");
        verwandte.setVerwandteList(new ArrayList<>());

        this.verwandteBusiness.save(verwandte);

        ArgumentCaptor<Verwandte> captor = ArgumentCaptor.forClass(Verwandte.class);
        Mockito.verify(this.verwandteRepo, Mockito.times(1))
                .merge(captor.capture());

        Assert.assertEquals("klientinId", captor.getValue().getKlientInId());
        Assert.assertEquals(true, captor.getValue().getVerwandteList().isEmpty());
        Assert.assertNotNull(captor.getValue().getId());
    }
}
