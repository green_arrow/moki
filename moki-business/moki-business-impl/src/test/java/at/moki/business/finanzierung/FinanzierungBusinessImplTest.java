package at.moki.business.finanzierung;

import at.moki.dbrepo.finanzierung.FinanzierungRepo;
import at.moki.dbrepo.user.UserRepo;
import at.moki.domainmodel.finanzierung.Finanzierung;
import at.moki.domainmodel.finanzierung.Institutions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Green Arrow on 27.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(MockitoJUnitRunner.class)
public class FinanzierungBusinessImplTest {

    @Mock
    private Logger logger;

    @Mock
    private UserRepo userRepo;

    @Mock
    private FinanzierungRepo finanzierungRepo;

    @InjectMocks
    private FinanzierungBusinessImpl finanzierungBusiness = new FinanzierungBusinessImpl();

    @Test
    public void testSearchFinanzierung() {

        Mockito.when(this.finanzierungRepo.findAll()).thenReturn(generateFinanzierungData());

        List<Finanzierung> finanzierungList = this.finanzierungBusiness.getData("123456");

        Assert.assertEquals(3, finanzierungList.size());
    }

    private List<Finanzierung> generateFinanzierungData() {

        List<Finanzierung> finanzierungList = new ArrayList<>();

        Finanzierung finanzierung = new Finanzierung();
        finanzierung.setId("some id");
        finanzierung.setKostenUebernahme(true);
        finanzierung.setInstitution(Institutions.LAND);
        finanzierung.setKlientInId("123456");
        finanzierungList.add(finanzierung);

        finanzierung = new Finanzierung();
        finanzierung.setId("some id 2");
        finanzierung.setKostenUebernahme(true);
        finanzierung.setInstitution(Institutions.MUKI);
        finanzierung.setKlientInId("123456");
        finanzierungList.add(finanzierung);

        finanzierung = new Finanzierung();
        finanzierung.setId("some id 3");
        finanzierung.setKostenUebernahme(true);
        finanzierung.setInstitution(Institutions.SOZIALVERSICHERUNG);
        finanzierung.setKlientInId("123456");
        finanzierungList.add(finanzierung);

        finanzierung = new Finanzierung();
        finanzierung.setId("some id 4");
        finanzierung.setKostenUebernahme(true);
        finanzierung.setInstitution(Institutions.HOSPIZ);
        finanzierung.setKlientInId("1234567");
        finanzierungList.add(finanzierung);

        return finanzierungList;
    }
}
