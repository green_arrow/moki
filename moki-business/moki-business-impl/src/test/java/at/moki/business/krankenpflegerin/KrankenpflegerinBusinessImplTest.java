package at.moki.business.krankenpflegerin;

import at.moki.dbrepo.krankenpflegerin.KrankenpflegerinRepo;
import at.moki.dbrepo.user.UserRepo;
import at.moki.dbrepo.userrechte.UserrechteRepo;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;
import at.moki.domainmodel.user.MokiUser;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.domainmodel.userrechte.Userrechte;
import at.moki.encryption.MokiEncryption;
import at.moki.exception.MokiBusinessException;
import at.moki.exception.MokiDbRepoException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Green Arrow on 25.01.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(MockitoJUnitRunner.class)
public class KrankenpflegerinBusinessImplTest {

    @Mock
    private Logger logger;

    @Mock
    private UserRepo userRepo;

    @Mock
    private MokiEncryption mokiEncryption;

    @Mock
    private KrankenpflegerinRepo krankenpflegerinRepo;

    @Mock
    private UserrechteRepo userrechteRepo;

    @InjectMocks
    private KrankenpflegerinBusiness krankenpflegerinBusiness = new KrankenpflegerinBusinessImpl();

    @Test
    public void testSearchKrankenpflegrin() {

        Mockito.when(this.krankenpflegerinRepo.findAll()).thenReturn(genreateKrankenpflegerinData());

        List<Krankenpflegerin> krankenpflegerinList = this.krankenpflegerinBusiness.getData("Anja", null);

        Assert.assertEquals(3, krankenpflegerinList.size());

        krankenpflegerinList = this.krankenpflegerinBusiness.getData("Anja", "k");

        Assert.assertEquals(1, krankenpflegerinList.size());
    }

    @Test(expected = MokiBusinessException.class)
    public void testSaveWithSaveException() {

        Mockito.when(this.userRepo.findByUsername(Mockito.eq("Anja"))).thenReturn(genreateUserData());
        Mockito.doThrow(new MokiDbRepoException()).when(userRepo).merge(Mockito.any());

        Userrechte userrechte = new Userrechte();
        userrechte.setId(UUID.randomUUID().toString());
        userrechte.setMokiUserRole(MokiUserRole.KRANKENPFLEGERIN);

        Mockito.when(this.userrechteRepo.findByBezeichnung(Mockito.eq(MokiUserRole.KRANKENPFLEGERIN))).thenReturn(userrechte);

        this.krankenpflegerinBusiness.save(new Krankenpflegerin(), "Katya");
    }

    @Test(expected = MokiBusinessException.class)
    public void testSaveWithUsernameIsAlreadyAvailable() {

        Mockito.when(this.userRepo.findByUsername(Mockito.eq("Anja"))).thenReturn(genreateUserData());

        Krankenpflegerin krankenpflegerin = new Krankenpflegerin();

        this.krankenpflegerinBusiness.save(krankenpflegerin, "Anja");
    }

    @Test(expected = MokiBusinessException.class)
    public void testUpdateWithException() {

        Mockito.doThrow(new MokiDbRepoException()).when(krankenpflegerinRepo).merge(Mockito.any());

        this.krankenpflegerinBusiness.save(new Krankenpflegerin(), null);
    }

    private List<MokiUser> genreateUserData() {

        List<MokiUser> mokiUserList = new ArrayList<>();

        MokiUser mokiUser = new MokiUser();
        mokiUser.setId("123");
        mokiUser.setUsername("Anja");
        mokiUserList.add(mokiUser);

        mokiUser = new MokiUser();
        mokiUser.setId("124");
        mokiUser.setUsername("Edith");
        mokiUserList.add(mokiUser);

        mokiUser = new MokiUser();
        mokiUser.setId("125");
        mokiUser.setUsername("Astrid");
        mokiUserList.add(mokiUser);

        return mokiUserList;
    }

    private List<Krankenpflegerin> genreateKrankenpflegerinData() {

        List<Krankenpflegerin> krankenpflegerinList = new ArrayList<>();

        Krankenpflegerin krankenpflegerin = new Krankenpflegerin();
        krankenpflegerin.setVorname("Test2");
        krankenpflegerin.setNachname("");
        krankenpflegerinList.add(krankenpflegerin);

        krankenpflegerin = new Krankenpflegerin();
        krankenpflegerin.setVorname("test2");
        krankenpflegerin.setNachname(null);
        krankenpflegerinList.add(krankenpflegerin);

        krankenpflegerin = new Krankenpflegerin();
        krankenpflegerin.setVorname("Anja");
        krankenpflegerin.setNachname("K.");
        krankenpflegerinList.add(krankenpflegerin);

        krankenpflegerin = new Krankenpflegerin();
        krankenpflegerin.setVorname("Anja");
        krankenpflegerin.setNachname("");
        krankenpflegerinList.add(krankenpflegerin);

        krankenpflegerin = new Krankenpflegerin();
        krankenpflegerin.setVorname("Anja");
        krankenpflegerin.setNachname(null);
        krankenpflegerinList.add(krankenpflegerin);

        return krankenpflegerinList;
    }
}
