package at.moki.business.excel;

import at.moki.dbrepo.einstellungen.EinstellungenRepo;
import at.moki.domainmodel.klientin.KlientIn;
import at.moki.domainmodel.klientin.KlientInExcelContainerModel;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;
import at.moki.einstellungen.Einstellungen;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.unitils.reflectionassert.ReflectionAssert;

import java.io.InputStream;
import java.util.List;

/**
 * Created by Green Arrow on 01.09.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(MockitoJUnitRunner.class)
public class ExcelImportBusinessImplTest extends AbsExcelImportBusinessTest {

    @Mock
    private Logger logger;

    @Mock
    private EinstellungenRepo einstellungenRepo;

    @InjectMocks
    private ExcelImportBusinessImpl excelImportBusiness = new ExcelImportBusinessImpl();

    @Before
    public void initEinstellungen() {
        Einstellungen einstellungen = new Einstellungen();

        einstellungen.setId("3073b4dc-7331-4f22-8994-5a6859a36149");
        einstellungen.setFachlichkeit("TAETIGKEITSFELDER");
        einstellungen.setInhalt("[{\"taetigkietsfeld\":\"FK\",\"orgprot\":\"Org. Prot.\",\"stammblatt\":\"FK Stammblatt\",\"blattList\":[\"Klientenblatt FK und EPP\",\"Klientenblatt Zwilling\",\"Klientenblatt Drilling\"]},{\"taetigkietsfeld\":\"FRZ\",\"orgprot\":\"Org. Prot.\",\"stammblatt\":\"FRZ Stammblatt\",\"blattList\":[\"Klientenblatt FRZ\"]},{\"taetigkietsfeld\":\"ICH\",\"orgprot\":\"Org. Prot.\",\"stammblatt\":\"Ich Stammblatt\",\"blattList\":[\"Klientenblatt ICH\"]},{\"taetigkietsfeld\":\"KJH\",\"orgprot\":\"Org. Prot.\",\"stammblatt\":\"Stammblatt MK\",\"blattList\":[\"Klientenblatt 1 MK\",\"Klientenblatt 2 MK\",\"Klientenblatt 3 MK\",\"Klientenblatt 4 MK\"]},{\"taetigkietsfeld\":\"LZP\",\"orgprot\":\"2\",\"stammblatt\":\"3\",\"blattList\":[\"1\",\"4\"]},{\"taetigkietsfeld\":\"MPT\",\"orgprot\":\"Org. Prot.\",\"stammblatt\":\"KI-JU-PALL Stammblatt\",\"blattList\":[\"Klientenblatt KI-JU-PALL\"]},{\"taetigkietsfeld\":\"TS\",\"orgprot\":\"Org. Prot.\",\"stammblatt\":\"TS Stammblatt\",\"blattList\":[\"Klientenblatt TS\"]}]");

        Mockito.when(this.einstellungenRepo.findByTaetigkeitsfeld()).thenReturn(einstellungen);
    }

    @Test
    public void testExcelImportFk() {

        List<KlientInExcelContainerModel> klientInExcelContainerModelList = this.excelImportBusiness.transformExcelToDomainmodel(
                "Klientendatei und HN FK 2017.xlsx", this.getClass().getResourceAsStream(
                        "/excel/import/compare/Klientendatei und HN FK 2017.xlsx"), "FK");

        KlientInExcelContainerModel klientInExcelContainerModelKlientIn = klientInExcelContainerModelList.get(0);
        // assert Krankenpflegerin
        ReflectionAssert.assertReflectionEquals(generateExpectedKrankenpflegerinInData(), klientInExcelContainerModelKlientIn.getKrankenpflegerin());

        // assert KlientIn
        KlientIn klientInExpectedFk = generateExpectedKlientInData();
        klientInExpectedFk.setKlientenNummer("2017000FK");
        klientInExpectedFk.setVorname("Edith");
        klientInExpectedFk.setNachname("B.");
        ReflectionAssert.assertReflectionEquals(klientInExpectedFk, klientInExcelContainerModelKlientIn.getKlientIn());

        Assert.assertEquals(3, klientInExcelContainerModelList.size());

        KlientIn klientInExpectedFkZwilling = generateExpectedKlientInData();
        klientInExpectedFkZwilling.setKlientenNummer("2017000FKZWILLING");
        klientInExpectedFkZwilling.setVorname("Katja");
        klientInExpectedFkZwilling.setNachname("C.");
        klientInExpectedFkZwilling.setKinderfacharztTelefonnummer("0");
        ReflectionAssert.assertReflectionEquals(klientInExpectedFkZwilling, klientInExcelContainerModelList.get(1).getKlientIn());

        KlientIn klientInExpectedFkDrilling = generateExpectedKlientInData();
        klientInExpectedFkDrilling.setKlientenNummer("2017000FKDRILLING");
        klientInExpectedFkDrilling.setVorname("Uma");
        klientInExpectedFkDrilling.setNachname("J.");
        klientInExpectedFkDrilling.setKinderfacharztTelefonnummer("0");
        ReflectionAssert.assertReflectionEquals(klientInExpectedFkDrilling, klientInExcelContainerModelList.get(2).getKlientIn());

        // assert finanzierung
        ReflectionAssert.assertReflectionEquals(generateExpectedFinanzierungData(), klientInExcelContainerModelKlientIn.getFinanzierungList());

        // assert betreuungsstunden
        ReflectionAssert.assertReflectionEquals(generateExpectedBetreuungsstundenData(), klientInExcelContainerModelKlientIn.getBetreuungsstundenList());
    }

    @Test
    public void testExcelImportFrz() {
        assertGeneralData("Klientendatei und HN FREIZEIT 2017.xlsx", this.getClass().getResourceAsStream(
                "/excel/import/compare/Klientendatei und HN FREIZEIT 2017.xlsx"), "2017000FRZ", "FRZ");
    }

    @Test
    public void testExcelImportIch() {
        assertGeneralData("Klientendatei und HN ICH 2017.xlsx", this.getClass().getResourceAsStream(
                "/excel/import/compare/Klientendatei und HN ICH 2017.xlsx"), "2017000ICH", "ICH");
    }

    @Test
    public void testExcelImportMpt() {
        assertGeneralData("Klientendatei und HN LISA-MARIE 2017.xlsx", this.getClass().getResourceAsStream(
                "/excel/import/compare/Klientendatei und HN LISA-MARIE 2017.xlsx"), "2017000MPT", "MPT");
    }

    @Test
    public void testExcelImportKjh() {

        List<KlientInExcelContainerModel> klientInExcelContainerModelList = this.excelImportBusiness.transformExcelToDomainmodel(
                "Klientendatei und HN MARIENKÄFER 2018.xlsx", this.getClass().getResourceAsStream(
                        "/excel/import/compare/Klientendatei und HN MARIENKÄFER 2018.xlsx"), "KJH");

        KlientInExcelContainerModel klientInExcelContainerModelKlientIn = klientInExcelContainerModelList.get(0);
        // assert Krankenpflegerin
        ReflectionAssert.assertReflectionEquals(generateExpectedKrankenpflegerinInData(), klientInExcelContainerModelKlientIn.getKrankenpflegerin());

        // assert KlientIn
        KlientIn klientInExpectedKjh = generateExpectedKlientInData();
        klientInExpectedKjh.setKlientenNummer("2018000KJH");
        klientInExpectedKjh.setVorname("Edith");
        klientInExpectedKjh.setNachname("B.");
        klientInExpectedKjh.setVerrechnungNummer("122");
        ReflectionAssert.assertReflectionEquals(klientInExpectedKjh, klientInExcelContainerModelKlientIn.getKlientIn());

        Assert.assertEquals(4, klientInExcelContainerModelList.size());

        KlientIn klientInExpectedKjhZweitesKind = generateExpectedKlientInData();
        klientInExpectedKjhZweitesKind.setKlientenNummer("2018000KJHZWEITESKIND");
        klientInExpectedKjhZweitesKind.setVorname("Katja");
        klientInExpectedKjhZweitesKind.setNachname("C.");
        klientInExpectedKjhZweitesKind.setVerrechnungNummer("122");
        klientInExpectedKjhZweitesKind.setKinderfacharztTelefonnummer("0");
        ReflectionAssert.assertReflectionEquals(klientInExpectedKjhZweitesKind, klientInExcelContainerModelList.get(1).getKlientIn());

        KlientIn klientInExpectedKjhDrittesKind = generateExpectedKlientInData();
        klientInExpectedKjhDrittesKind.setKlientenNummer("2018000KJHDRITTESKIND");
        klientInExpectedKjhDrittesKind.setVorname("Uma");
        klientInExpectedKjhDrittesKind.setNachname("J.");
        klientInExpectedKjhDrittesKind.setVerrechnungNummer("122");
        klientInExpectedKjhDrittesKind.setKinderfacharztTelefonnummer("0");
        ReflectionAssert.assertReflectionEquals(klientInExpectedKjhDrittesKind, klientInExcelContainerModelList.get(2).getKlientIn());

        KlientIn klientInExpectedKjhViertesKind = generateExpectedKlientInData();
        klientInExpectedKjhViertesKind.setKlientenNummer("2018000KJHVIERTESKIND");
        klientInExpectedKjhViertesKind.setVorname("Megan");
        klientInExpectedKjhViertesKind.setNachname("S.");
        klientInExpectedKjhViertesKind.setVerrechnungNummer("122");
        klientInExpectedKjhViertesKind.setKinderfacharztTelefonnummer("0");
        ReflectionAssert.assertReflectionEquals(klientInExpectedKjhViertesKind, klientInExcelContainerModelList.get(3).getKlientIn());

        // assert finanzierung
        ReflectionAssert.assertReflectionEquals(generateExpectedFinanzierungData(), klientInExcelContainerModelKlientIn.getFinanzierungList());

        // assert betreuungsstunden
        ReflectionAssert.assertReflectionEquals(generateExpectedBetreuungsstundenData(), klientInExcelContainerModelKlientIn.getBetreuungsstundenList());
    }

    @Test
    public void testExcelImportTs() {
        assertGeneralData("Klientendatei und HN TAPFERES SCHNEIDERLEIN 2017.xlsx", this.getClass().getResourceAsStream(
                "/excel/import/compare/Klientendatei und HN TAPFERES SCHNEIDERLEIN 2017.xlsx"), "2017000TS", "TS");
    }

    @Test
    public void testNameSetting() {
        KlientIn klientIn = new KlientIn();
        this.excelImportBusiness.setNamesOfKlientIn(klientIn, "L. Tera");

        Assert.assertEquals("Tera", klientIn.getVorname());
        Assert.assertEquals("L.", klientIn.getNachname());

        klientIn = new KlientIn();
        this.excelImportBusiness.setNamesOfKlientIn(klientIn, "Tera");

        Assert.assertEquals(null, klientIn.getVorname());
        Assert.assertEquals("Tera", klientIn.getNachname());

        klientIn = new KlientIn();
        this.excelImportBusiness.setNamesOfKlientIn(klientIn, "Tera_L");

        Assert.assertEquals(null, klientIn.getVorname());
        Assert.assertEquals("Tera_L", klientIn.getNachname());

        klientIn = new KlientIn();
        this.excelImportBusiness.setNamesOfKlientIn(klientIn, "");

        Assert.assertEquals(null, klientIn.getVorname());
        Assert.assertEquals(null, klientIn.getNachname());

        klientIn = new KlientIn();
        this.excelImportBusiness.setNamesOfKlientIn(klientIn, null);

        Assert.assertEquals(null, klientIn.getVorname());
        Assert.assertEquals(null, klientIn.getNachname());
    }

    @Test
    public void testNamesOfKlientIn() {

        KlientIn klientIn = new KlientIn();

        this.excelImportBusiness.setNamesOfKlientIn(klientIn, "Onori Agatha Florencia");
        Assert.assertEquals("Onori", klientIn.getNachname());
        Assert.assertEquals("Agatha Florencia", klientIn.getVorname());

        klientIn = new KlientIn();
        this.excelImportBusiness.setNamesOfKlientIn(klientIn, "Onori Agatha-Florencia");
        Assert.assertEquals("Onori", klientIn.getNachname());
        Assert.assertEquals("Agatha-Florencia", klientIn.getVorname());

        klientIn = new KlientIn();
        this.excelImportBusiness.setNamesOfKlientIn(klientIn, "Onori Florencia");
        Assert.assertEquals("Onori", klientIn.getNachname());
        Assert.assertEquals("Florencia", klientIn.getVorname());

        klientIn = new KlientIn();
        this.excelImportBusiness.setNamesOfKlientIn(klientIn, "Onori");
        Assert.assertEquals("Onori", klientIn.getNachname());
        Assert.assertEquals(null, klientIn.getVorname());

        klientIn = new KlientIn();
        this.excelImportBusiness.setNamesOfKlientIn(klientIn, "");
        Assert.assertEquals(null, klientIn.getNachname());
        Assert.assertEquals(null, klientIn.getVorname());

        klientIn = new KlientIn();
        this.excelImportBusiness.setNamesOfKlientIn(klientIn, null);
        Assert.assertEquals(null, klientIn.getNachname());
        Assert.assertEquals(null, klientIn.getVorname());
    }

    @Test
    public void testNameOfKrankenpflegerin() {

        Krankenpflegerin krankenpflegerin = new Krankenpflegerin();

        this.excelImportBusiness.setNameOfKrankenpflegerin(krankenpflegerin, "Agatha Onori");
        Assert.assertEquals("Agatha", krankenpflegerin.getVorname());
        Assert.assertEquals("Onori", krankenpflegerin.getNachname());

        krankenpflegerin = new Krankenpflegerin();
        this.excelImportBusiness.setNameOfKrankenpflegerin(krankenpflegerin, "Agatha-Florencia Onori");
        Assert.assertEquals("Agatha-Florencia", krankenpflegerin.getVorname());
        Assert.assertEquals("Onori", krankenpflegerin.getNachname());

        krankenpflegerin = new Krankenpflegerin();
        this.excelImportBusiness.setNameOfKrankenpflegerin(krankenpflegerin, "Florencia Onori");
        Assert.assertEquals("Florencia", krankenpflegerin.getVorname());
        Assert.assertEquals("Onori", krankenpflegerin.getNachname());

        krankenpflegerin = new Krankenpflegerin();
        this.excelImportBusiness.setNameOfKrankenpflegerin(krankenpflegerin, "Florencia");
        Assert.assertEquals("Florencia", krankenpflegerin.getVorname());
        Assert.assertEquals(null, krankenpflegerin.getNachname());

        krankenpflegerin = new Krankenpflegerin();
        this.excelImportBusiness.setNameOfKrankenpflegerin(krankenpflegerin, "");
        Assert.assertEquals(null, krankenpflegerin.getVorname());
        Assert.assertEquals(null, krankenpflegerin.getNachname());

        krankenpflegerin = new Krankenpflegerin();
        this.excelImportBusiness.setNameOfKrankenpflegerin(krankenpflegerin, null);
        Assert.assertEquals(null, krankenpflegerin.getVorname());
        Assert.assertEquals(null, krankenpflegerin.getNachname());
    }

    private void assertGeneralData(String xlsxName, InputStream inputStream, String klientenNummer, String klientInTaetigkeitsfeld) {

        List<KlientInExcelContainerModel> klientInExcelContainerModelList = this.excelImportBusiness.transformExcelToDomainmodel(xlsxName, inputStream, klientInTaetigkeitsfeld);

        Assert.assertEquals(1, klientInExcelContainerModelList.size());

        KlientInExcelContainerModel klientInExcelContainerModel = klientInExcelContainerModelList.get(0);

        // assert Krankenpflegerin
        ReflectionAssert.assertReflectionEquals(generateExpectedKrankenpflegerinInData(), klientInExcelContainerModel.getKrankenpflegerin());

        // assert KlientIn
        KlientIn klientInExpected = generateExpectedKlientInData();
        klientInExpected.setKlientenNummer(klientenNummer);
        klientInExpected.setVorname("Edith");
        klientInExpected.setNachname("B.");
        ReflectionAssert.assertReflectionEquals(klientInExpected, klientInExcelContainerModel.getKlientIn());

        // assert finanzierung
        ReflectionAssert.assertReflectionEquals(generateExpectedFinanzierungData(), klientInExcelContainerModel.getFinanzierungList());

        // assert betreuungsstunden
        ReflectionAssert.assertReflectionEquals(generateExpectedBetreuungsstundenData(), klientInExcelContainerModel.getBetreuungsstundenList());
    }
}
