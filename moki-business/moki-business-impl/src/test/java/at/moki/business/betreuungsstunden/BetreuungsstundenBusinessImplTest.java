package at.moki.business.betreuungsstunden;

import at.moki.dbrepo.betreuungsstunden.BetreuungsstundenRepo;
import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import java.time.Year;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * created by Green Arrow on 10.06.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@RunWith(MockitoJUnitRunner.class)
public class BetreuungsstundenBusinessImplTest {

    @Mock
    private Logger logger;

    @Mock
    private BetreuungsstundenRepo betreuungsstundenRepo;

    @InjectMocks
    private BetreuungsstundenBusinessImpl betreuungsstundenBusiness = new BetreuungsstundenBusinessImpl();

    @Test
    public void testFindByKlientInId() {

        Mockito.when(this.betreuungsstundenRepo.findByKlientInId(Mockito.anyString())).thenReturn(generateData());

        List<Betreuungsstunden> betreuungsstundenList = this.betreuungsstundenBusiness.getDataBy("anything");
        // rearange them since it was moved to the repo layer
        betreuungsstundenList.sort(Comparator.comparing(Betreuungsstunden::getJahr));

        Assert.assertEquals(3, betreuungsstundenList.size());

        Assert.assertEquals(Year.now().getValue(), betreuungsstundenList.get(0).getJahr());
        Assert.assertEquals(Year.now().plusYears(1).getValue(), betreuungsstundenList.get(1).getJahr());
        Assert.assertEquals(Year.now().plusYears(2).getValue(), betreuungsstundenList.get(2).getJahr());
    }

    private List<Betreuungsstunden> generateData() {
        List<Betreuungsstunden> betreuungsstundenList = new ArrayList<>();

        Betreuungsstunden betreuungsstunden = new Betreuungsstunden();
        betreuungsstunden.setJahr(2012);
        betreuungsstunden.setStunden("20");
        betreuungsstundenList.add(betreuungsstunden);

        betreuungsstunden = new Betreuungsstunden();
        betreuungsstunden.setJahr(2021);
        betreuungsstunden.setStunden("110");
        betreuungsstundenList.add(betreuungsstunden);

        betreuungsstunden = new Betreuungsstunden();
        betreuungsstunden.setJahr(2013);
        betreuungsstunden.setStunden("30");
        betreuungsstundenList.add(betreuungsstunden);

        betreuungsstunden = new Betreuungsstunden();
        betreuungsstunden.setJahr(2014);
        betreuungsstunden.setStunden("40");
        betreuungsstundenList.add(betreuungsstunden);

        betreuungsstunden = new Betreuungsstunden();
        betreuungsstunden.setJahr(2019);
        betreuungsstunden.setStunden("90");
        betreuungsstundenList.add(betreuungsstunden);

        betreuungsstunden = new Betreuungsstunden();
        betreuungsstunden.setJahr(2015);
        betreuungsstunden.setStunden("50");
        betreuungsstundenList.add(betreuungsstunden);

        betreuungsstunden = new Betreuungsstunden();
        betreuungsstunden.setJahr(2016);
        betreuungsstunden.setStunden("60");
        betreuungsstundenList.add(betreuungsstunden);

        betreuungsstunden = new Betreuungsstunden();
        betreuungsstunden.setJahr(2017);
        betreuungsstunden.setStunden("70");
        betreuungsstundenList.add(betreuungsstunden);

        betreuungsstunden = new Betreuungsstunden();
        betreuungsstunden.setJahr(2018);
        betreuungsstunden.setStunden("80");
        betreuungsstundenList.add(betreuungsstunden);

        betreuungsstunden = new Betreuungsstunden();
        betreuungsstunden.setJahr(2020);
        betreuungsstunden.setStunden("100");
        betreuungsstundenList.add(betreuungsstunden);

        return betreuungsstundenList;
    }
}
