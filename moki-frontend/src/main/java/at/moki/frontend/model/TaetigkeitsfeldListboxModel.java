package at.moki.frontend.model;

import at.moki.domainmodel.klientin.Taetigkeitsfeld;
import org.zkoss.zul.ListModelList;

import java.util.Collection;

/**
 * @author Green Arrow
 * @date 05.08.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class TaetigkeitsfeldListboxModel extends ListModelList<Taetigkeitsfeld> {

    private static final long serialVersionUID = 8836720881954451828L;

    public TaetigkeitsfeldListboxModel() {
        // empty constructor
    }

    public TaetigkeitsfeldListboxModel(Collection<? extends Taetigkeitsfeld> c) {
        super(c);
    }
}
