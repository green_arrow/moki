package at.moki.frontend.model;

import at.moki.domainmodel.klientin.KlientIn;
import org.zkoss.zul.ListModelList;

import java.util.Collection;

/**
 * Created by Green Arrow on 21.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class OrganisationsprotokollSearchListboxModel extends ListModelList<KlientIn> {

    private static final long serialVersionUID = 5490369493696844861L;

    public OrganisationsprotokollSearchListboxModel() {
        // empty constructor
    }

    public OrganisationsprotokollSearchListboxModel(Collection<? extends KlientIn> c) {
        super(c);
    }
}
