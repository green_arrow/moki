package at.moki.frontend.model;

import at.moki.domainmodel.verrechnung.Verrechnung;
import org.zkoss.zul.ListModelList;

import java.util.Collection;

/**
 * @author Green Arrow
 * @date 21.07.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class VerrechnungListboxModel extends ListModelList<Verrechnung> {

    private static final long serialVersionUID = 4139264871302773861L;

    public VerrechnungListboxModel() {
        // empty constructor
    }

    public VerrechnungListboxModel(Collection<? extends Verrechnung> c) {
        super(c);
    }
}
