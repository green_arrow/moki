package at.moki.frontend.component;

import at.moki.frontend.model.TaetigkeitsfeldListboxModel;
import org.zkoss.zul.Listbox;

/**
 * @author Green Arrow
 * @date 21.07.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class TaetigkeitsfeldListbox extends Listbox {

    private static final long serialVersionUID = -5269258585591009134L;

    public TaetigkeitsfeldListbox() {
        super();

        setModel(new TaetigkeitsfeldListboxModel());
    }
}
