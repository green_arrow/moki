package at.moki.frontend.controller;

import at.moki.business.krankenpflegerin.KrankenpflegerinBusiness;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;
import at.moki.session.SessionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Label;

/**
 * Created by Green Arrow on 22.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class ProfiledataController extends SelectorComposer<Component> {

    private static final long serialVersionUID = -2286106162640376008L;

    private static Logger logger = LoggerFactory.getLogger(ProfiledataController.class);

    @Wire("#lblVornameValue")
    private Label lblVornameValue;

    @Wire("#lblNachnameValue")
    private Label lblNachnameValue;

    @Wire("#lblBezeichnungValue")
    private Label lblBezeichnungValue;

    @Wire("#lblStrasseValue")
    private Label lblStrasseValue;

    @Wire("#lblPostleitzahlValue")
    private Label lblPostleitzahlValue;

    @Wire("#lblTelefonnummerValue")
    private Label lblTelefonnummerValue;

    @Wire("#lblBankValue")
    private Label lblBankValue;

    @Wire("#lblIbanValue")
    private Label lblIbanValue;

    @Wire("#lblBicValue")
    private Label lblBicValue;

    @Wire("#lblEmailValue")
    private Label lblEmailValue;

    @Wire("#lblEinzelstundenValue")
    private Label lblEinzelstundenValue;

    @Wire("#lblFeiertagsstundenValue")
    private Label lblFeiertagsstundenValue;

    @Wire("#lblNachtstundenValue")
    private Label lblNachtstundenValue;

    @Wire("#lblKmgeldValue")
    private Label lblKmgeldValue;

    @WireVariable("krankenpflegerinBusinessImpl")
    private KrankenpflegerinBusiness krankenpflegerinBusiness;

    @WireVariable("sessionModel")
    private SessionModel sessionModel;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        mapToFrontend(this.sessionModel.getKrankenpflegerin());
    }

    @Listen("onClick=#btnEdit")
    public void onEdit() {
        logger.info("erstelle krankenpflegerinUpdate.zul zum editieren von benutzerdaten");
        Executions.createComponents("/content/krankenpflegerinUpdate.zul", null, null);
    }

    private void mapToFrontend(Krankenpflegerin krankenpflegerin) {

        if (krankenpflegerin != null) {
            this.lblVornameValue.setValue(krankenpflegerin.getVorname());
            this.lblNachnameValue.setValue(krankenpflegerin.getNachname());
            this.lblBezeichnungValue.setValue(krankenpflegerin.getBezeichnung());
            this.lblStrasseValue.setValue(krankenpflegerin.getStrasse());
            this.lblPostleitzahlValue.setValue(krankenpflegerin.getPostleitzahl());
            this.lblTelefonnummerValue.setValue(krankenpflegerin.getTelefonnummer());
            this.lblBankValue.setValue(krankenpflegerin.getBankverbindung());
            this.lblIbanValue.setValue(krankenpflegerin.getIban());
            this.lblBicValue.setValue(krankenpflegerin.getBic());
            this.lblEmailValue.setValue(krankenpflegerin.getEmail());
        }
    }
}
