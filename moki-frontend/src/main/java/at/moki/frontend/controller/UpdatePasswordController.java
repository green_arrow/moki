package at.moki.frontend.controller;

import at.moki.business.user.UserBusiness;
import at.moki.domainmodel.user.MokiUser;
import at.moki.encryption.MokiEncryption;
import at.moki.exception.MokiBusinessException;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.session.SessionModel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Include;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

/**
 * Created by Green Arrow on 13.05.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class UpdatePasswordController extends SelectorComposer<Component> {

    private static final long serialVersionUID = 6474878285687436349L;

    private static Logger logger = LoggerFactory.getLogger(UpdateUsernameController.class);

    @Wire("#txtPasswordOld")
    private Textbox txtPasswordOld;

    @Wire("#txtPasswordNew")
    private Textbox txtPasswordNew;

    @Wire("#txtPasswordNewConfirmation")
    private Textbox txtPasswordNewConfirmation;

    @WireVariable("sessionModel")
    private SessionModel sessionModel;

    @WireVariable("userBusinessImpl")
    private UserBusiness userBusiness;

    @WireVariable("mokiEncryption")
    private MokiEncryption mokiEncryption;

    @Listen("onOK=#txtPasswordOld")
    public void onOKPasswordOld() {
        update();
    }

    @Listen("onOK=#txtPasswordNew")
    public void onOKPasswordNew() {
        update();
    }

    @Listen("onOK=#txtPasswordNewConfirmation")
    public void onOKPasswordNewConfirmation() {
        update();
    }

    @Listen("onClick=#btnUpdatePassword")
    public void onUpdatePassword() {
        update();
    }

    private void update() {
        try {
            logger.info("password wird aktualisiert");

            String passwordAndSaltFromDb = this.sessionModel.getMokiUser().getPassword();
            String saltFromDb = StringUtils.substring(passwordAndSaltFromDb, 32, 47);

            String passwordOld = this.mokiEncryption.generateHashPassword(this.txtPasswordOld.getValue(), saltFromDb);
            String passwordNew = this.mokiEncryption.generateHashPassword(this.txtPasswordNew.getValue(), saltFromDb);
            String passwordConfirmation = this.mokiEncryption.generateHashPassword(this.txtPasswordNew.getValue(), saltFromDb);

            if (!StringUtils.equals(passwordAndSaltFromDb, passwordOld)) {
                throw new WrongValueException(this.txtPasswordOld, Labels.getLabel("usersettings.display.info.wrong.input"));
            }

            if (!StringUtils.equals(passwordNew, passwordConfirmation)) {
                throw new WrongValueException(this.txtPasswordNewConfirmation, Labels.getLabel("usersettings.display.info.wrong.input"));
            }

            MokiUser mokiUser = this.sessionModel.getMokiUser();
            mokiUser.setPassword(passwordConfirmation);
            this.userBusiness.update(mokiUser);

            Include include = (Include) Path.getComponent(MokiFrontendConstants.INCLUDE);
            include.setSrc("");// reset include

            Messagebox.show(Labels.getLabel("usersettings.display.info.updatecomplete"), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);
        } catch (MokiBusinessException e) {
            logger.error("", e);
            Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }

    @Listen("onClick=#btnCancel")
    public void onCancel() {
        Include include = (Include) Path.getComponent(MokiFrontendConstants.INCLUDE);
        include.setSrc("");
    }
}
