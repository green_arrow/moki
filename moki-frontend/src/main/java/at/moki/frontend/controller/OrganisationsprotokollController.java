package at.moki.frontend.controller;

import at.moki.business.klientin.KlientInBusiness;
import at.moki.business.organisationsprotokoll.OrganisationsprotokollBusiness;
import at.moki.business.verwandte.VerwandteBusiness;
import at.moki.domainmodel.klientin.KlientIn;
import at.moki.domainmodel.organisationsprotokoll.Organisationsprotokoll;
import at.moki.exception.MokiBusinessException;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.frontend.model.OrganisationsprotokollListboxModel;
import at.moki.session.TransferModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import java.util.List;

/**
 * Created by Green Arrow on 22.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class OrganisationsprotokollController extends SelectorComposer<Component> {

    private static final long serialVersionUID = 2415682491764658343L;

    private static Logger logger = LoggerFactory.getLogger(OrganisationsprotokollController.class);

    @Wire("#lbOrgProtEntries")
    private Listbox lbOrgProtEntries;

    @Wire("#txtWeiterekontaktdaten")
    private Textbox txtWeiterekontaktdaten;

    @WireVariable("klientInBusinessImpl")
    private KlientInBusiness klientInBusiness;

    @WireVariable("verwandteBusinessImpl")
    private VerwandteBusiness verwandteBusiness;

    @WireVariable("organisationsprotokollBusinessImpl")
    private OrganisationsprotokollBusiness organisationsprotokollBusiness;

    @WireVariable("transferModel")
    private TransferModel transferModel;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        KlientIn klientInSelectedFromOrgProt = this.transferModel.getKlientInOrgProt();
        try {
            this.txtWeiterekontaktdaten.setValue(denull(klientInSelectedFromOrgProt.getWeitereKontaktdaten()));

            // read all org prots from klientin
            List<Organisationsprotokoll> organisationsprotokollList = this.organisationsprotokollBusiness.
                    getData(klientInSelectedFromOrgProt.getId());

            this.lbOrgProtEntries.setModel(new OrganisationsprotokollListboxModel(organisationsprotokollList));

        } catch (MokiBusinessException e) {
            logger.error("", e);
            Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }

    @Listen("onClick=#btnSave")
    public void onSave() {
        try {
            KlientIn klientIn = this.transferModel.getKlientInOrgProt();
            klientIn.setWeitereKontaktdaten(this.txtWeiterekontaktdaten.getValue());

            this.klientInBusiness.save(klientIn);

            Messagebox.show(Labels.getLabel("newentry.display.info.savecomplete"), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);
        } catch (MokiBusinessException e) {
            logger.error("", e);
            Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }

    @Listen("onClick=#btnAddNewEntry")
    public void onClickAddNewEntry() {
        Executions.createComponents("/content/organisationsprotokollNewEntry.zul", null, null);
    }

    private String denull(String weitereKontaktdaten) {
        return weitereKontaktdaten == null ? "" : weitereKontaktdaten;
    }
}
