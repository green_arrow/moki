package at.moki.frontend.controller;

import at.moki.business.betreuungsstunden.BetreuungsstundenBusiness;
import at.moki.business.excel.ExcelExportBusiness;
import at.moki.business.finanzierung.FinanzierungBusiness;
import at.moki.business.klientin.KlientInBusiness;
import at.moki.business.organisationsprotokoll.OrganisationsprotokollBusiness;
import at.moki.business.verwandte.VerwandteBusiness;
import at.moki.domainmodel.klientin.KlientIn;
import at.moki.exception.MokiBusinessException;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.frontend.model.KlientInSearchListboxModel;
import at.moki.session.SessionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Include;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import java.util.List;

/**
 * Created by Green Arrow on 25.04.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class ExcelExportController extends SelectorComposer<Component> {

    private static final long serialVersionUID = -7258407569718100154L;

    private static Logger logger = LoggerFactory.getLogger(ExcelExportController.class);

    @Wire("#txtKlientInNummer")
    private Textbox txtKlientInNummer;

    @Wire("#txtVorname")
    private Textbox txtVorname;

    @Wire("#txtNachname")
    private Textbox txtNachname;

    @Wire("#lboxKlientIn")
    private Listbox lboxKlientIn;

    @WireVariable("sessionModel")
    private SessionModel sessionModel;

    @WireVariable("excelExportBusinessImpl")
    private ExcelExportBusiness excelExportBusiness;

    @WireVariable("finanzierungBusinessImpl")
    private FinanzierungBusiness finanzierungBusiness;

    @WireVariable("betreuungsstundenBusinessImpl")
    private BetreuungsstundenBusiness betreuungsstundenBusiness;

    @WireVariable("verwandteBusinessImpl")
    private VerwandteBusiness verwandteBusiness;

    @WireVariable("klientInBusinessImpl")
    private KlientInBusiness klientInBusiness;

    @WireVariable("organisationsprotokollBusinessImpl")
    private OrganisationsprotokollBusiness organisationsprotokollBusiness;

    // wire the include into the class
    private Include include = (Include) Path.getComponent(MokiFrontendConstants.INCLUDE);

    @Listen("onClick=#btnSearch")
    public void onSearch() {
        search();
    }

    @Listen("onClick=#btnCancel")
    public void onCancel() {
        this.include.setSrc("");
    }

    @Listen("onOK=#txtKlientInNummer")
    public void onOkKlientInNummer() {
        search();
    }

    @Listen("onOK=#txtVorname")
    public void onOkVorname() {
        search();
    }

    @Listen("onOK=#txtNachname")
    public void onOkNachname() {
        search();
    }

    private void search() {
        String klientenNummer = this.txtKlientInNummer.getValue();
        String vorname = this.txtVorname.getValue();
        String nachname = this.txtNachname.getValue();

        try {
            List<KlientIn> klientInList = this.klientInBusiness.getData(klientenNummer, vorname, nachname, null);

            if (!klientInList.isEmpty()) {
                this.lboxKlientIn.setModel(new KlientInSearchListboxModel(klientInList));
                this.lboxKlientIn.setVisible(true);
            } else {
                this.lboxKlientIn.setVisible(false);
            }
        } catch (MokiBusinessException e) {
            logger.error("", e);
            Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }
}
