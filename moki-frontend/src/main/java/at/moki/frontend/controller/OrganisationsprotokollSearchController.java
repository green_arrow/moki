package at.moki.frontend.controller;

import at.moki.business.klientin.KlientInBusiness;
import at.moki.domainmodel.klientin.KlientIn;
import at.moki.exception.MokiBusinessException;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.frontend.model.OrganisationsprotokollSearchListboxModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import java.util.List;

/**
 * Created by Green Arrow on 21.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class OrganisationsprotokollSearchController extends SelectorComposer<Component> {

    private static final long serialVersionUID = 2415682491764658343L;

    private static Logger logger = LoggerFactory.getLogger(OrganisationsprotokollSearchController.class);

    @Wire("#winOrganisationsprotokollSearchWindow")
    private Window winOrganisationsprotokollSearchWindow;

    @Wire("#txtKlientInNummer")
    private Textbox txtKlientInNummer;

    @Wire("#txtVorname")
    private Textbox txtVorname;

    @Wire("#txtNachname")
    private Textbox txtNachname;

    @Wire("#lboxOrganisationsprotokoll")
    private Listbox lboxOrganisationsprotokoll;

    @WireVariable("klientInBusinessImpl")
    private KlientInBusiness klientInBusiness;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        this.winOrganisationsprotokollSearchWindow.doModal();
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {
        search();
    }

    @Listen("onOK=#txtKlientInNummer")
    public void onOkKlientInNummer() {
        search();
    }

    @Listen("onOK=#txtVorname")
    public void onOkVorname() {
        search();
    }

    @Listen("onOK=#txtNachname")
    public void onOkNachname() {
        search();
    }

    @Listen("onClose=#winOrganisationsprotokollSearchWindow")
    public void onCancel() {
        this.winOrganisationsprotokollSearchWindow.detach();
    }

    private void search() {
        String klientenNummer = this.txtKlientInNummer.getValue();
        String vorname = this.txtVorname.getValue();
        String nachname = this.txtNachname.getValue();

        try {
            List<KlientIn> klientInList = this.klientInBusiness.getData(klientenNummer, vorname, nachname, null);

            if (!klientInList.isEmpty()) {
                this.lboxOrganisationsprotokoll.setModel(new OrganisationsprotokollSearchListboxModel(klientInList));
                this.lboxOrganisationsprotokoll.setVisible(true);
            } else {
                Messagebox.show(Labels.getLabel("search.nolist"), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);
                this.lboxOrganisationsprotokoll.setVisible(false);
            }
        } catch (MokiBusinessException e) {
            logger.error("", e);
            Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }
}
