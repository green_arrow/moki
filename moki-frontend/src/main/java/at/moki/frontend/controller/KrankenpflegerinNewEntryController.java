package at.moki.frontend.controller;

import at.moki.business.krankenpflegerin.KrankenpflegerinBusiness;
import at.moki.business.user.UserBusiness;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;
import at.moki.domainmodel.user.MokiUser;
import at.moki.exception.MokiBusinessException;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.frontend.validation.MokiValidation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Include;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import java.util.Optional;

/**
 * Created by Green Arrow on 22.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class KrankenpflegerinNewEntryController extends SelectorComposer<Component> {

    private static final long serialVersionUID = 6592264021644755535L;

    private static final String NEWENTRY_MESSAGE_EMPTY_CHARACTER = "newentry.message.empty.character";
    private static final String NEWENTRY_MESSAGE_WRONG_CHARACTER = "newentry.message.wrong.character";

    private static Logger logger = LoggerFactory.getLogger(KrankenpflegerinNewEntryController.class);

    @Wire("#txtUsername")
    private Textbox txtUsername;

    @Wire("#txtVorname")
    private Textbox txtVorname;

    @Wire("#txtNachname")
    private Textbox txtNachname;

    @Wire("#txtBezeichnung")
    private Textbox txtBezeichnung;

    @Wire("#txtStrasse")
    private Textbox txtStrasse;

    @Wire("#txtPostleitzahl")
    private Textbox txtPostleitzahl;

    @Wire("#txtbTelefonnummer")
    private Textbox txtbTelefonnummer;

    @Wire("#txtBank")
    private Textbox txtBank;

    @Wire("#txtbIban")
    private Textbox txtbIban;

    @Wire("#txtbBic")
    private Textbox txtbBic;

    @Wire("#txtEmail")
    private Textbox txtEmail;

    @Wire("#txtbEinzelstunden")
    private Textbox txtbEinzelstunden;

    @Wire("#txtbFeiertagsstunden")
    private Textbox txtbFeiertagsstunden;

    @Wire("#txtbNachtstunden")
    private Textbox txtbNachtstunden;

    @Wire("#txtbKmgeld")
    private Textbox txtbKmgeld;

    @WireVariable("userBusinessImpl")
    private UserBusiness userBusiness;

    @WireVariable("krankenpflegerinBusinessImpl")
    private KrankenpflegerinBusiness krankenpflegerinBusiness;

    @Listen("onClick=#btnSave")
    public void onSave() {

        validate();

        try {
            logger.info("krankenpflegerin %s %s wird gespeichert");

            this.krankenpflegerinBusiness.save(mapToDomainmodel(), this.txtUsername.getValue());

            Include include = (Include) Path.getComponent(MokiFrontendConstants.INCLUDE);
            include.setSrc("");// reset include
            Messagebox.show(Labels.getLabel("krankenpflegerin.display.info.savecomplete"), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);
        } catch (MokiBusinessException e) {
            logger.error("", e);
            Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }

    @Listen("onClick=#btnCancel")
    public void onCancel() {
        Include include = (Include) Path.getComponent(MokiFrontendConstants.INCLUDE);
        include.setSrc("");
    }

    private void validate() {

        String username = this.txtUsername.getValue();
        String vorname = this.txtVorname.getValue();
        String nachname = this.txtNachname.getValue();

        // validate if input strings are empty
        if (StringUtils.isEmpty(username)) {
            throw new WrongValueException(this.txtUsername, Labels.getLabel(NEWENTRY_MESSAGE_EMPTY_CHARACTER));
        }

        if (StringUtils.isEmpty(vorname)) {
            throw new WrongValueException(this.txtVorname, Labels.getLabel(NEWENTRY_MESSAGE_EMPTY_CHARACTER));
        }

        if (StringUtils.isEmpty(nachname)) {
            throw new WrongValueException(this.txtNachname, Labels.getLabel(NEWENTRY_MESSAGE_EMPTY_CHARACTER));
        }

        // validate if input does not match the pattern
        if (!MokiValidation.isInputValid(username)) {
            throw new WrongValueException(this.txtEmail, Labels.getLabel(NEWENTRY_MESSAGE_WRONG_CHARACTER));
        }

        if (!MokiValidation.isInputValid(vorname)) {
            throw new WrongValueException(this.txtEmail, Labels.getLabel(NEWENTRY_MESSAGE_WRONG_CHARACTER));
        }

        if (!MokiValidation.isInputValid(nachname)) {
            throw new WrongValueException(this.txtEmail, Labels.getLabel(NEWENTRY_MESSAGE_WRONG_CHARACTER));
        }

        // validate if username already exist
        if (checkIfKrankenpflegerinExists(username)) {
            throw new WrongValueException(this.txtUsername, Labels.getLabel("newentry.message.user.exist"));
        }
    }

    private boolean checkIfKrankenpflegerinExists(String username) {

        Optional<MokiUser> userOptional = this.userBusiness.getData().stream()
                .filter(e -> StringUtils.equals(e.getUsername(), username))
                .findAny();

        return userOptional.isPresent();
    }

    private Krankenpflegerin mapToDomainmodel() {
        Krankenpflegerin krankenpflegerin = new Krankenpflegerin();

        krankenpflegerin.setVorname(this.txtVorname.getValue());
        krankenpflegerin.setNachname(this.txtNachname.getValue());
        krankenpflegerin.setBezeichnung(this.txtBezeichnung.getValue());
        krankenpflegerin.setStrasse(this.txtStrasse.getValue());
        krankenpflegerin.setPostleitzahl(this.txtPostleitzahl.getValue());
        krankenpflegerin.setTelefonnummer(this.txtbTelefonnummer.getValue());
        krankenpflegerin.setBankverbindung(this.txtBank.getValue());
        krankenpflegerin.setIban(this.txtbIban.getValue());
        krankenpflegerin.setBic(this.txtbBic.getValue());
        krankenpflegerin.setEmail(this.txtEmail.getValue());

        return krankenpflegerin;
    }
}
