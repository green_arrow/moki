package at.moki.frontend.controller;

import at.moki.business.krankenpflegerin.KrankenpflegerinBusiness;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;
import at.moki.exception.MokiBusinessException;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.session.SessionModel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Include;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 * Created by Green Arrow on 14.01.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class KrankenpflegerinUpdateController extends SelectorComposer<Component> {

    private static final long serialVersionUID = 8230165634344899670L;

    private static Logger logger = LoggerFactory.getLogger(KrankenpflegerinUpdateController.class);

    @Wire("#winUpdateKrankenpflegerin")
    private Window winUpdateKrankenpflegerin;

    @Wire("#txtVorname")
    private Textbox txtVorname;

    @Wire("#txtNachname")
    private Textbox txtNachname;

    @Wire("#txtBezeichnung")
    private Textbox txtBezeichnung;

    @Wire("#txtStrasse")
    private Textbox txtStrasse;

    @Wire("#txtPostleitzahl")
    private Textbox txtPostleitzahl;

    @Wire("#txtbTelefonnummer")
    private Textbox txtbTelefonnummer;

    @Wire("#txtBank")
    private Textbox txtBank;

    @Wire("#txtbIban")
    private Textbox txtbIban;

    @Wire("#txtbBic")
    private Textbox txtbBic;

    @Wire("#txtEmail")
    private Textbox txtEmail;

    @Wire("#txtbEinzelstunden")
    private Textbox txtbEinzelstunden;

    @Wire("#txtbFeiertagsstunden")
    private Textbox txtbFeiertagsstunden;

    @Wire("#txtbNachtstunden")
    private Textbox txtbNachtstunden;

    @Wire("#txtbKmgeld")
    private Textbox txtbKmgeld;

    @WireVariable("krankenpflegerinBusinessImpl")
    private KrankenpflegerinBusiness krankenpflegerinBusiness;

    @WireVariable("sessionModel")
    private SessionModel sessionModel;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        mapToFrontend(this.sessionModel.getKrankenpflegerin());
        this.winUpdateKrankenpflegerin.doModal();
    }

    @Listen("onClick=#btnSave")
    public void onSave() {

        validate();

        Krankenpflegerin krankenpflegerin = mapToDomainmodel();

        try {
            logger.info("daten zur krankenpflegerin werden aktualisiert");

            this.krankenpflegerinBusiness.update(krankenpflegerin);

            Include include = (Include) Path.getComponent(MokiFrontendConstants.INCLUDE);
            include.setSrc("");// reset include
            this.winUpdateKrankenpflegerin.detach();
            Messagebox.show(Labels.getLabel("krankenpflegerin.display.info.updatecomplete"),
                    MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);

            // update session model after successfull update
            this.sessionModel.setKrankenpflegerin(krankenpflegerin);
        } catch (MokiBusinessException e) {
            logger.error("", e);
            Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }

    @Listen("onClick=#btnCancel")
    public void onCancel() {
        this.winUpdateKrankenpflegerin.detach();
    }

    private void validate() {

        String vorname = this.txtVorname.getValue();
        String nachname = this.txtNachname.getValue();

        // validate if input strings are empty
        if (StringUtils.isEmpty(vorname)) {
            throw new WrongValueException(this.txtVorname, Labels.getLabel("newentry.message.empty.character"));
        }

        if (StringUtils.isEmpty(nachname)) {
            throw new WrongValueException(this.txtNachname, Labels.getLabel("newentry.message.empty.character"));
        }
    }

    private Krankenpflegerin mapToDomainmodel() {
        Krankenpflegerin krankenpflegerin = new Krankenpflegerin();

        krankenpflegerin.setId(this.sessionModel.getKrankenpflegerin().getId());
        krankenpflegerin.setVorname(this.txtVorname.getValue());
        krankenpflegerin.setNachname(this.txtNachname.getValue());
        krankenpflegerin.setBezeichnung(this.txtBezeichnung.getValue());
        krankenpflegerin.setStrasse(this.txtStrasse.getValue());
        krankenpflegerin.setPostleitzahl(this.txtPostleitzahl.getValue());
        krankenpflegerin.setTelefonnummer(this.txtbTelefonnummer.getValue());
        krankenpflegerin.setBankverbindung(this.txtBank.getValue());
        krankenpflegerin.setIban(this.txtbIban.getValue());
        krankenpflegerin.setBic(this.txtbBic.getValue());
        krankenpflegerin.setEmail(this.txtEmail.getValue());

        return krankenpflegerin;
    }

    private void mapToFrontend(Krankenpflegerin krankenpflegerin) {

        if (krankenpflegerin != null) {
            this.txtVorname.setValue(krankenpflegerin.getVorname());
            this.txtNachname.setValue(krankenpflegerin.getNachname());
            this.txtBezeichnung.setValue(krankenpflegerin.getBezeichnung());
            this.txtStrasse.setValue(krankenpflegerin.getStrasse());
            this.txtPostleitzahl.setValue(krankenpflegerin.getPostleitzahl());
            this.txtbTelefonnummer.setValue(krankenpflegerin.getTelefonnummer());
            this.txtBank.setValue(krankenpflegerin.getBankverbindung());
            this.txtbIban.setValue(krankenpflegerin.getIban());
            this.txtbBic.setValue(krankenpflegerin.getBic());
            this.txtEmail.setValue(krankenpflegerin.getEmail());
        }
    }
}
