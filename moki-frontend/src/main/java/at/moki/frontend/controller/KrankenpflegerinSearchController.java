package at.moki.frontend.controller;

import at.moki.business.krankenpflegerin.KrankenpflegerinBusiness;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;
import at.moki.domainmodel.user.MokiUser;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.exception.MokiBusinessException;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.frontend.model.KrankenpflegerinSearchListboxModel;
import at.moki.session.SessionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import java.util.List;

/**
 * Created by Green Arrow on 10.02.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class KrankenpflegerinSearchController extends SelectorComposer<Component> {

    private static final long serialVersionUID = 5013079097319929089L;

    private static Logger logger = LoggerFactory.getLogger(KrankenpflegerinSearchController.class);

    @Wire("#txtVorname")
    private Textbox txtVorname;

    @Wire("#txtNachname")
    private Textbox txtNachname;

    @Wire("#liBezeichnung")
    private Listheader liBezeichnung;

    @Wire("#liStrasse")
    private Listheader liStrasse;

    @Wire("#liPostleitzahl")
    private Listheader liPostleitzahl;

    @Wire("#liBankverbindung")
    private Listheader liBankverbindung;

    @Wire("#liIban")
    private Listheader liIban;

    @Wire("#liBic")
    private Listheader liBic;

    @Wire("#lboxKrankenpflegerin")
    private Listbox lboxKrankenpflegerin;

    @WireVariable("sessionModel")
    private SessionModel sessionModel;

    @WireVariable("krankenpflegerinBusinessImpl")
    private KrankenpflegerinBusiness krankenpflegerinBusiness;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        MokiUser loggedMokiUser = this.sessionModel.getMokiUser();

        if (loggedMokiUser != null) {
            if (MokiUserRole.KRANKENPFLEGERIN == loggedMokiUser.getMokiUserRole()) {
                this.liBezeichnung.detach();
                this.liStrasse.detach();
                this.liPostleitzahl.detach();
                this.liBankverbindung.detach();
                this.liIban.detach();
                this.liBic.detach();
            }
        } else {
            logger.error("User ist nicht gesetzt");
            throw new UnsupportedOperationException();
        }
    }

    @Listen("onClick=#btnSearch")
    public void onSearch() {
        search();
    }

    @Listen("onOK=#txtVorname")
    public void onOkVorname() {
        search();
    }

    @Listen("onOK=#txtNachname")
    public void onOkNachname() {
        search();
    }

    private void search() {
        String vorname = this.txtVorname.getValue();
        String nachname = this.txtNachname.getValue();

        try {
            List<Krankenpflegerin> krankenpflegerinList = this.krankenpflegerinBusiness.getData(vorname, nachname);

            if (!krankenpflegerinList.isEmpty()) {
                this.lboxKrankenpflegerin.setModel(new KrankenpflegerinSearchListboxModel(krankenpflegerinList));
                this.lboxKrankenpflegerin.setVisible(true);
            } else {
                Messagebox.show(Labels.getLabel("search.nolist"), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);
                this.lboxKrankenpflegerin.setVisible(false);
            }
        } catch (MokiBusinessException e) {
            logger.error("", e);
            Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }
}
