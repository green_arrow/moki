package at.moki.frontend.controller;

import at.moki.business.user.UserBusiness;
import at.moki.domainmodel.user.MokiUser;
import at.moki.exception.MokiBusinessException;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.session.SessionModel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Include;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class UpdateUsernameController extends SelectorComposer<Component> {

    private static final long serialVersionUID = -5572671754402147694L;

    private static Logger logger = LoggerFactory.getLogger(UpdateUsernameController.class);

    @Wire("#txtUsernameOld")
    private Textbox txtUsernameOld;

    @Wire("#txtUsernameNew")
    private Textbox txtUsernameNew;

    @Wire("#txtUsernameNewConfirmation")
    private Textbox txtUsernameNewConfirmation;

    @WireVariable("sessionModel")
    private SessionModel sessionModel;

    @WireVariable("userBusinessImpl")
    private UserBusiness userBusiness;

    @Listen("onOK=#txtUsernameOld")
    public void onOKUsernameOld() {
        update();
    }

    @Listen("onOK=#txtUsernameNew")
    public void onOKUsernameNew() {
        update();
    }

    @Listen("onOK=#txtUsernameNewConfirmation")
    public void onOKUsernameNewConfirmation() {
        update();
    }

    @Listen("onClick=#btnUpdateUsername")
    public void onUpdateUsername() {
        update();
    }

    private void update() {
        try {
            logger.info("username wird aktualisiert");
            String sessionUsername = this.sessionModel.getMokiUser().getUsername();
            String oroginInputUsername = this.txtUsernameOld.getValue();
            String newUsername = this.txtUsernameNew.getValue();
            String newUsernameConfirmation = this.txtUsernameNewConfirmation.getValue();

            if (!StringUtils.equals(sessionUsername, oroginInputUsername)) {
                throw new WrongValueException(this.txtUsernameOld, Labels.getLabel("usersettings.display.info.wrong.input"));
            }

            if (!StringUtils.equals(newUsername, newUsernameConfirmation)) {
                throw new WrongValueException(this.txtUsernameNewConfirmation, Labels.getLabel("usersettings.display.info.wrong.input"));
            }

            MokiUser mokiUser = this.sessionModel.getMokiUser();
            // set new username
            mokiUser.setUsername(newUsernameConfirmation);
            this.userBusiness.update(mokiUser);

            Include include = (Include) Path.getComponent(MokiFrontendConstants.INCLUDE);
            include.setSrc("");// reset include

            Messagebox.show(Labels.getLabel("usersettings.display.info.updatecomplete"), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);
        } catch (MokiBusinessException e) {
            logger.error("", e);
            Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }
}
