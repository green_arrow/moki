package at.moki.frontend.controller;

import at.moki.dbrepo.klientin.TaetigkeitsfeldRepo;
import at.moki.domainmodel.klientin.Taetigkeitsfeld;
import at.moki.exception.MokiBusinessException;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.frontend.model.TaetigkeitsfeldListboxModel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import java.util.UUID;

/**
 * Created by Green Arrow on 13.05.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class TaetigkeitsfeldVerwaltungController extends SelectorComposer<Component> {

    private static final long serialVersionUID = -5502445103561083809L;

    private static Logger logger = LoggerFactory.getLogger(TaetigkeitsfeldVerwaltungController.class);

    @Wire("#txtNeuesTaetigkeitsfeld")
    private Textbox txtNeuesTaetigkeitsfeld;

    @Wire("#lboxTaetigkietsfeld")
    private Listbox lboxTaetigkietsfeld;

    @WireVariable("taetigkeitsfeldRepoImpl")
    private TaetigkeitsfeldRepo taetigkeitsfeldRepo;

    @Listen("onOK=#txtNeuesTaetigkeitsfeld")
    public void onOKNeuesTaetigkeitsfeld() {
        save();
    }

    @Listen("onClick=#btnSave")
    public void onSave() {
        save();
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        logger.info("Lade alle Verrechnungen aus der Datenbank");

        loadTaetigkeitsfelderListbox();
    }

    private void save() {
        String taetigkeitsfeldBezeichnung = this.txtNeuesTaetigkeitsfeld.getValue();

        // validate if input strings are empty
        if (StringUtils.isEmpty(taetigkeitsfeldBezeichnung)) {
            throw new WrongValueException(this.txtNeuesTaetigkeitsfeld, Labels.getLabel("newentry.message.empty.character"));
        }


        try {
            Taetigkeitsfeld taetigkeitsfeldNewEntry = new Taetigkeitsfeld();
            taetigkeitsfeldNewEntry.setId(UUID.randomUUID().toString());
            taetigkeitsfeldNewEntry.setBezeichnung(taetigkeitsfeldBezeichnung);

            this.taetigkeitsfeldRepo.merge(taetigkeitsfeldNewEntry);

            Messagebox.show(Labels.getLabel("verrechnung.display.info.savecomplete"), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);

            // reload list
            loadTaetigkeitsfelderListbox();

        } catch (MokiBusinessException e) {
            logger.error("", e);
            Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }

    private void loadTaetigkeitsfelderListbox() {
        try {
            // init lboxTaetigkeitsfeld
            this.lboxTaetigkietsfeld.setModel(new TaetigkeitsfeldListboxModel(this.taetigkeitsfeldRepo.findAll()));
        } catch (MokiBusinessException e) {
            logger.error("", e);
            Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }
}
