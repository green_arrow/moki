package at.moki.frontend.controller;

import at.moki.business.excel.ExcelImportBusiness;
import at.moki.dbrepo.excel.dateien.ExcelDateinRepo;
import at.moki.dbrepo.klientin.TaetigkeitsfeldRepo;
import at.moki.domainmodel.klientin.KlientInExcelContainerModel;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.session.TransferModel;
import org.apache.commons.lang3.StringUtils;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.*;

import java.io.InputStream;
import java.util.List;

/**
 * Created by Green Arrow on 08.09.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class ExcelImportController extends SelectorComposer<Component> {

    private static final long serialVersionUID = -217845943604084550L;
    private static final String KLIENTIN_ZUL_FILE = "/content/klientIn.zul";
    private static final String KLIENTIN_FK_ZUL_FILE = "/content/klientInFk.zul";
    private static final String KLIENTIN_KJH_ZUL_FILE = "/content/klientInKjh.zul";

    @Wire("#winExcelImport")
    private Window winExcelImport;

    @Wire("#btnSelect")
    private Button btnSelect;

    @Wire("#rgTaetigkeitsfeld")
    private Radiogroup rgTaetigkeitsfeld;

    @WireVariable("transferModel")
    private TransferModel transferModel;

    @WireVariable("excelImportBusinessImpl")
    private ExcelImportBusiness excelImportBusiness;

    @WireVariable("taetigkeitsfeldRepoImpl")
    private TaetigkeitsfeldRepo taetigkeitsfeldRepo;

    @WireVariable("excelDateinRepoImpl")
    private ExcelDateinRepo excelDateinRepo;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        initRadiobuttons();
        this.winExcelImport.doModal();
    }

    private void initRadiobuttons() {
        this.taetigkeitsfeldRepo.findAll().forEach(taetigkeitsfeld -> {
            Radio radio = new Radio();
            radio.setParent(this.rgTaetigkeitsfeld);
            radio.setId(String.format("r%s", taetigkeitsfeld.getBezeichnung()));
            radio.setLabel(taetigkeitsfeld.getBezeichnung());
        });
    }

    @Listen("onClose=#winExcelImport")
    public void onCloseWindow() {
        Include include = (Include) Path.getComponent(MokiFrontendConstants.INCLUDE);
        include.setSrc("");
        this.winExcelImport.detach();
    }

    @Listen("onCheck=#rgTaetigkeitsfeld")
    public void onClickTaetigkeitsfeld() {
        if (!this.btnSelect.isVisible()) {
            this.btnSelect.setVisible(true);
        }
    }

    @Listen("onUpload=#btnSelect")
    public void onFileUpload(UploadEvent event) {

        Media media = event.getMedia();

        if (!StringUtils.equals("xlsx", media.getFormat())) {
            throw new UnsupportedOperationException("Die Datei ist kein gültiges Excel Format");
        }

        InputStream xlsxInputStream = media.getStreamData();
        String xlsxName = media.getName();

        Include include = (Include) Path.getComponent(MokiFrontendConstants.INCLUDE);
        Radio selectedRadio = this.rgTaetigkeitsfeld.getSelectedItem();

        String label = selectedRadio.getLabel();
        if (StringUtils.equals("FK", label)) {
            loadExcelIntoTransferModel(xlsxName, xlsxInputStream, "FK");
            include.setSrc(KLIENTIN_FK_ZUL_FILE);
        } else if (StringUtils.equals("KJH", label)) {
            loadExcelIntoTransferModel(xlsxName, xlsxInputStream, "KJH");
            include.setSrc(KLIENTIN_KJH_ZUL_FILE);
        } else {
            loadExcelIntoTransferModel(xlsxName, xlsxInputStream, label);
            include.setSrc(KLIENTIN_ZUL_FILE);
        }
    }

    private void loadExcelIntoTransferModel(String xlsxName, InputStream inputStream, String klientInTaetigkeitsfeld) {
        // load excel into object
        List<KlientInExcelContainerModel> klientInExcelContainerModelList = this.excelImportBusiness
                .transformExcelToDomainmodel(xlsxName, inputStream, klientInTaetigkeitsfeld);

        // store taetigkeitsfeld and KlientInExcelContainerModel into transfermodel and trigger rendering
        this.transferModel.setKlientInTaetigkeitsfeld(klientInTaetigkeitsfeld);
        this.transferModel.setKlientInExcelContainerModelList(klientInExcelContainerModelList);
    }
}
