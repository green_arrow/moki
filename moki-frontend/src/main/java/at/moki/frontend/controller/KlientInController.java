package at.moki.frontend.controller;

import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.domainmodel.finanzierung.Finanzierung;
import at.moki.domainmodel.klientin.KlientIn;
import at.moki.domainmodel.klientin.KlientInExcelContainerModel;
import at.moki.exception.MokiBusinessException;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.frontend.model.KlientInZusaetzlicheDokumenteSearchListboxModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Messagebox;

import java.util.List;

/**
 * Created by Green Arrow on 22.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class KlientInController extends AbsKlientInController {

    private static final long serialVersionUID = -2001642992036250133L;

    private static Logger logger = LoggerFactory.getLogger(KlientInController.class);

    @Override
    void performAfterCompose() {

        // calculate dates
        KlientInMapper.calculateBetreuungsstundenDates(super.lblNow, super.lblPlusOne, super.lblPlusTwo);

        String klientInTaetigkeitsfeld = super.transferModel.getKlientInTaetigkeitsfeld();
        super.lblTaetigkeitsfeld.setValue(klientInTaetigkeitsfeld + "-NR");

        super.initVerrechnungCombobox(super.cbbVerrechnungsnummer);

        // excel importer was used
        if (super.transferModel.getKlientInExcelContainerModelList() != null && !super.transferModel.getKlientInExcelContainerModelList().isEmpty()) {
            List<KlientInExcelContainerModel> klientInExcelContainerModel = super.transferModel.getKlientInExcelContainerModelList();

            KlientInExcelContainerModel klientInExcelContainerModelKlientIn = klientInExcelContainerModel.get(0);

            mapToFrontend(klientInExcelContainerModelKlientIn.getKlientIn(),
                    klientInExcelContainerModelKlientIn.getFinanzierungList(),
                    klientInExcelContainerModelKlientIn.getBetreuungsstundenList());


        } else { // klientin selected from search
            KlientIn klientInSelectedFromSearch = super.transferModel.getKlientInSearch();

            // distinguish between edit and new klientIn
            if (klientInSelectedFromSearch != null) {
                super.cbbVerrechnungsnummer.setValue(super.verrechnungRepo.findByKlienInId(klientInSelectedFromSearch.getVerrechnungId()).getBezeichnung());
                super.pnlKlientinZusaetlicheDokumente.setVisible(true);
                // load zusaetzliche dokumente
                super.lboxKlientinZusaetlicheDokumente.setModel(new KlientInZusaetzlicheDokumenteSearchListboxModel(
                        super.klientInZusaetzlicheDokumenteBusiness.findAllByKlientInId(klientInSelectedFromSearch.getId())));
                try {
                    List<Betreuungsstunden> betreuungsstundenSelectedFromSearch = super.betreuungsstundenBusiness.getDataBy(klientInSelectedFromSearch.getId());

                    List<Finanzierung> finanzierungFromSelectedSearchList = super.finanzierungBusiness.getData(klientInSelectedFromSearch.getId());
                    mapToFrontend(klientInSelectedFromSearch,
                            finanzierungFromSelectedSearchList,
                            betreuungsstundenSelectedFromSearch);
                } catch (MokiBusinessException e) {
                    // clear stored klientIn object
                    super.transferModel.clearKlientInSearch();
                    logger.error("", e);
                    Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
                }
            }
        }
    }

    @Listen("onClick=#btnSave")
    public void onSave() {

        try {
            logger.info("Ausgewaehlte KlientIn wird gespeichert");

            KlientIn klientInSelectedFromSearch = super.transferModel.getKlientInSearch();

            if (klientInSelectedFromSearch != null) {
                KlientIn klientInUpdated = mapToDomainmodelKlientIn();
                klientInUpdated.setId(klientInSelectedFromSearch.getId());
                klientInUpdated.setTaetigkeitsfeld(klientInSelectedFromSearch.getTaetigkeitsfeld());

                //update the data only
                super.klientInBusiness.update(
                        klientInUpdated,
                        mapToDomainmodelBetreuungsstunden(),
                        mapToDomainmodelFinanzierung());

            } else { // create a new entry if the data is coming from excel or new from frontend
                KlientIn klientIn = mapToDomainmodelKlientIn();
                klientIn.setTaetigkeitsfeld(super.transferModel.getKlientInTaetigkeitsfeld());
                // persist a new klientin object
                super.klientInBusiness.save(
                        klientIn,
                        mapToDomainmodelBetreuungsstunden(),
                        mapToDomainmodelFinanzierung());
            }

            clearTransferModel();
            Messagebox.show(Labels.getLabel("newentry.display.info.savecomplete"), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);
        } catch (MokiBusinessException e) {
            logger.error("", e);
            Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }

    private KlientIn mapToDomainmodelKlientIn() {

        return KlientInMapper.mapToDomainmodelKlientIn(transferModel.getKlientInTaetigkeitsfeld(),
                txtKlientenNummer, cbbVerrechnungsnummer, intVerrechnungsnummer, intTaetigkeitsfeldNummer, txtSpende, intWerkvertragsNummer, txtKlientVorname,
                txtKlientNachname, rWeiblich, rMaennlich, txtImpacct, intPflegestufe, intSozialversicherungsnummer, dbGebdatum,
                txtSvtrager, txtDiagnose, txtDiagnoseschluessel, dbSterbedatum, cbMutterVertreterin, txtMutterName,
                dbGebdatumMutter, txtTelefonMutter, cbVaterVertreter, txtVaterName, dbGebdatumVater, txtTelefonVater,
                cbSonstigeVertreter, txtSonstigeDateneltern, dbGebdatumSonstige, txtTelefonSonstige, txtStrassePlzOrtMutter,
                txtEmailmutter, txtStrassePlzOrtVater, txtEmailvater, rHauptwohnsitzJa, txtKh, txtKjh,
                txtSonstigeZuweisendeStellen, txtKinderfacharzt, txtTelefonKinderfacharzt, txtBetreuendeDGKP, txtWeitereDGKP,
                dbBetreuungsbeginn, dbBetreuungsende, txtKmprohb);
    }

    private List<Betreuungsstunden> mapToDomainmodelBetreuungsstunden() {

        return KlientInMapper.mapBetreuungsstundenMap(
                txtBetreuungsstundenNow,
                txtBetreuungsstundenPlusOne,
                txtBetreuungsstundenPlusTwo);
    }

    private List<Finanzierung> mapToDomainmodelFinanzierung() {

        return KlientInMapper.mapToDomainmodelFinanzierung(
                getInstitutions(),
                cbLandKosten, cbLandSelbstbehalt,
                cbMukiKosten, cbMukiSelbstbehalt,
                cbKinderJugendhilfeKosten, cbKinderJugendhilfeSelbstbehalt,
                cbVersicherungKosten, cbVersicherungSelbstbehalt,
                cbHospizKosten, cbHospizSelbstbehalt,
                cbSpendenKosten, cbSpendenSelbstbehalt,
                cbSozialversicherungKosten, cbSozialversicherungSelbstbehalt,
                cbPrivatKosten, cbPrivatSelbstbehalt,
                cbKibKosten, cbKibSelbstbehalt,
                cbAndereKosten, cbAndereSelbstbehalt);
    }

    private void mapToFrontend(KlientIn klientIn, List<Finanzierung> finanzierungList, List<Betreuungsstunden> betreuungsstunden) {
        try {
            KlientInMapper.mapToFrontendKlientIn(klientIn, txtKlientenNummer, intVerrechnungsnummer, intTaetigkeitsfeldNummer,
                    txtSpende, intWerkvertragsNummer, txtKlientVorname,
                    txtKlientNachname, rWeiblich, rMaennlich, txtImpacct, intPflegestufe, intSozialversicherungsnummer, dbGebdatum,
                    txtSvtrager, txtDiagnose, txtDiagnoseschluessel, dbSterbedatum, cbMutterVertreterin, txtMutterName,
                    dbGebdatumMutter, txtTelefonMutter, cbVaterVertreter, txtVaterName, dbGebdatumVater, txtTelefonVater,
                    cbSonstigeVertreter, txtSonstigeDateneltern, dbGebdatumSonstige, txtTelefonSonstige, txtStrassePlzOrtMutter,
                    txtEmailmutter, txtStrassePlzOrtVater, txtEmailvater, rHauptwohnsitzJa, rHauptwohnsitzNein, txtKh, txtKjh,
                    txtSonstigeZuweisendeStellen, txtKinderfacharzt, txtTelefonKinderfacharzt, txtBetreuendeDGKP, txtWeitereDGKP,
                    dbBetreuungsbeginn, dbBetreuungsende, txtKmprohb);

            setBetreuungsstunden(betreuungsstunden, super.txtBetreuungsstundenNow, super.txtBetreuungsstundenPlusOne,
                    super.txtBetreuungsstundenPlusTwo, super.lblStundenGesamtValue);

            setFinanzierungen(finanzierungList);
        } catch (WrongValueException e) {
            Messagebox.show(e.getComponent().getId() + " " + e.getMessage(), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);
        }
    }
}
