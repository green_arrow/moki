package at.moki.frontend.controller;

import at.moki.business.krankenpflegerin.KrankenpflegerinBusiness;
import at.moki.business.organisationsprotokoll.OrganisationsprotokollBusiness;
import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;
import at.moki.domainmodel.organisationsprotokoll.Organisationsprotokoll;
import at.moki.exception.MokiBusinessException;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.session.SessionModel;
import at.moki.session.TransferModel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Include;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 * Created by Green Arrow on 21.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class OrganisationsprotokollNewEntryController extends SelectorComposer<Component> {

    private static final long serialVersionUID = -1174109711162521213L;

    private static Logger logger = LoggerFactory.getLogger(OrganisationsprotokollNewEntryController.class);

    @Wire("#winOrganisationsprotokollNewEntry")
    private Window winOrganisationsprotokollNewEntry;

    @Wire("#txtText")
    private Textbox txtText;

    @WireVariable("krankenpflegerinBusinessImpl")
    private KrankenpflegerinBusiness krankenpflegerinBusiness;

    @WireVariable("organisationsprotokollBusinessImpl")
    private OrganisationsprotokollBusiness organisationsprotokollBusiness;

    @WireVariable("transferModel")
    private TransferModel transferModel;

    @WireVariable("sessionModel")
    private SessionModel sessionModel;

    // wire the include into the class
    private Include include = (Include) Path.getComponent(MokiFrontendConstants.INCLUDE);

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        this.winOrganisationsprotokollNewEntry.doModal();
    }

    @Listen("onClick=#btnSave")
    public void onSave() {
        save();
    }

    @Listen("onOK=#txtText")
    public void onOk() {
        save();
    }

    private void save() {
        validate();

        Organisationsprotokoll organisationsprotokoll = new Organisationsprotokoll();
        organisationsprotokoll.setText(this.txtText.getValue());

        // read krankenpflegrin from logged in user
        Krankenpflegerin krankenpflegerin = this.sessionModel.getKrankenpflegerin();
        if (krankenpflegerin != null) { // admin ans verwaltung has no krankenpflegerin
            organisationsprotokoll.setHandschriftDgkp(krankenpflegerin.getNachname() + " " + krankenpflegerin.getVorname());
        } else {
            organisationsprotokoll.setHandschriftDgkp(this.sessionModel.getMokiUser().getUsername());
        }
        organisationsprotokoll.setKlientInId(this.transferModel.getKlientInOrgProt().getId());

        try {
            this.organisationsprotokollBusiness.save(organisationsprotokoll);
            this.winOrganisationsprotokollNewEntry.detach();

            this.include.setSrc("");
            this.include.setSrc("/content/organisationsprotokoll.zul");
        } catch (MokiBusinessException e) {
            logger.error("", e);
            Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }

    @Listen("onCancel=#winOrganisationsprotokollNewEntry")
    public void onCancel() {
        this.winOrganisationsprotokollNewEntry.detach();
    }

    private void validate() {
        String text = this.txtText.getValue();

        if (StringUtils.isEmpty(text)) {
            throw new WrongValueException(this.txtText, Labels.getLabel("newentry.message.empty.character"));
        }
    }
}
