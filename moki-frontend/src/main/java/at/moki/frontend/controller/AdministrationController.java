package at.moki.frontend.controller;

import at.moki.business.administration.AdministrationBusiness;
import at.moki.exception.MokiBusinessException;
import at.moki.frontend.model.AdministrationListboxModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;

/**
 * Created by Green Arrow on 25.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class AdministrationController extends SelectorComposer<Component> {

    private static final long serialVersionUID = 8448404649317798475L;

    private static Logger logger = LoggerFactory.getLogger(AdministrationController.class);

    @Wire("#lboxUserdata")
    private Listbox lboxUserdata;

    @WireVariable("administrationBusinessImpl")
    private AdministrationBusiness administrationBusiness;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        logger.info("Lade alle nutzer und deren Rechte aus Datenbank");

        try {
            this.lboxUserdata.setModel(new AdministrationListboxModel(this.administrationBusiness.getAllUsersWithUserrechte()));
        } catch (MokiBusinessException e) {
            logger.error("", e);
            Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }
}
