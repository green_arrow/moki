package at.moki.frontend.controller;

import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.domainmodel.finanzierung.Finanzierung;
import at.moki.domainmodel.finanzierung.Institutions;
import at.moki.domainmodel.klientin.Geschlecht;
import at.moki.domainmodel.klientin.KlientIn;
import at.moki.domainmodel.verrechnung.Verrechnung;
import at.moki.util.MokiDateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.zkoss.zul.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Green Arrow on 24.07.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
final class KlientInMapper {

    private KlientInMapper() {
        // static class
    }

    static KlientIn mapToDomainmodelKlientIn(String klientInTaetigkeitsfeld, Textbox txtKlientenNummer,
                                             Combobox cbbVerrechnungsnummer, Intbox intVerrechnungsnummer, Intbox intTaetigkeitsfeldNummer, Textbox txtSpende,
                                             Intbox intWerkvertragsNummer, Textbox txtKlientVorname, Textbox txtKlientNachname,
                                             Radio rWeiblich, Radio rMaennlich, Textbox txtImpacct, Intbox intPflegestufe,
                                             Intbox intSozialversicherungsnummer, Datebox dbGebdatum, Textbox txtSvtrager,
                                             Textbox txtDiagnose, Textbox txtDiagnoseschluessel, Datebox dbSterbedatum,
                                             Checkbox cbMutterVertreterin, Textbox txtMutterName, Datebox dbGebdatumMutter,
                                             Textbox txtTelefonMutter, Checkbox cbVaterVertreter, Textbox txtVaterName,
                                             Datebox dbGebdatumVater, Textbox txtTelefonVater, Checkbox cbSonstigeVertreter,
                                             Textbox txtSonstigeDateneltern, Datebox dbGebdatumSonstige, Textbox txtTelefonSonstige,
                                             Textbox txtStrassePlzOrtMutter, Textbox txtEmailmutter, Textbox txtStrassePlzOrtVater,
                                             Textbox txtEmailvater, Radio rHauptwohnsitzJa, Textbox txtKh, Textbox txtKjh,
                                             Textbox txtSonstigeZuweisendeStellen, Textbox txtKinderfacharzt, Textbox txtTelefonKinderfacharzt,
                                             Textbox txtBetreuendeDGKP, Textbox txtWeitereDGKP, Datebox dbBetreuungsbeginn,
                                             Datebox dbBetreuungsende, Textbox txtKmprohb) {
        KlientIn klientIn = new KlientIn();

        klientIn.setKlientenNummer(txtKlientenNummer.getValue());
        klientIn.setVerrechnungId(getVerrechnungId(cbbVerrechnungsnummer));
        klientIn.setVerrechnungNummer(denullIntbox(intVerrechnungsnummer));
        klientIn.setTaetigkeitsfeld(klientInTaetigkeitsfeld);
        klientIn.setTaetigkeitsfeldNummer(denullIntbox(intTaetigkeitsfeldNummer));
        klientIn.setSpende(txtSpende.getValue());
        klientIn.setWerkvertragsNummer(denullIntbox(intWerkvertragsNummer));
        klientIn.setVorname(txtKlientVorname.getValue());
        klientIn.setNachname(txtKlientNachname.getValue());

        if (rWeiblich.isSelected()) {
            klientIn.setGeschlecht(Geschlecht.WEIBLICH);
        } else if (rMaennlich.isSelected()) {
            klientIn.setGeschlecht(Geschlecht.MAENNLICH);
        } else {
            klientIn.setGeschlecht(null);
        }

        klientIn.setImpacctGruppe(txtImpacct.getValue());
        klientIn.setPflegestufe(denullIntbox(intPflegestufe));
        klientIn.setSozialversicherungsnummer(denullIntbox(intSozialversicherungsnummer));
        klientIn.setGeburtsdatum(convertToLocalDate(dbGebdatum.getValue()));
        klientIn.setSozialversicherungstraeger(txtSvtrager.getValue());
        klientIn.setDiagnose(txtDiagnose.getValue());
        klientIn.setDiagnoseschluessel(txtDiagnoseschluessel.getValue());
        klientIn.setSterbedatum(convertToLocalDate(dbSterbedatum.getValue()));
        klientIn.setMutterVertreterin(cbMutterVertreterin.isChecked());
        klientIn.setMutterName(txtMutterName.getValue());
        klientIn.setMutterGeburtsdatum(convertToLocalDate(dbGebdatumMutter.getValue()));
        klientIn.setMutterTelefonnummer(txtTelefonMutter.getValue());
        klientIn.setVaterVertreter(cbVaterVertreter.isChecked());
        klientIn.setVaterName(txtVaterName.getValue());
        klientIn.setVaterGeburtsdatum(convertToLocalDate(dbGebdatumVater.getValue()));
        klientIn.setVaterTelefonnummer(txtTelefonVater.getValue());
        klientIn.setSonstigeVertreter(cbSonstigeVertreter.isChecked());
        klientIn.setSonstigeName(txtSonstigeDateneltern.getValue());
        klientIn.setSonstigeGeburtsdatum(convertToLocalDate(dbGebdatumSonstige.getValue()));
        klientIn.setSonstigeTelefonnummer(txtTelefonSonstige.getValue());
        klientIn.setStrasse(txtStrassePlzOrtMutter.getValue());
        klientIn.setMutterEmail(txtEmailmutter.getValue());
        klientIn.setOrt(txtStrassePlzOrtVater.getValue());
        klientIn.setVaterEmail(txtEmailvater.getValue());
        klientIn.setHauptwohnsitz(rHauptwohnsitzJa.isSelected());
        klientIn.setZuweisendeStelleKH(txtKh.getValue());
        klientIn.setZuweisendeStelleKJH(txtKjh.getValue());
        klientIn.setZuweisendeStelleSonstige(txtSonstigeZuweisendeStellen.getValue());
        klientIn.setKinderfacharztName(txtKinderfacharzt.getValue());
        klientIn.setKinderfacharztTelefonnummer(txtTelefonKinderfacharzt.getValue());
        klientIn.setBetreuendeKrankenpflegerin(txtBetreuendeDGKP.getValue());
        klientIn.setWeitereKrankenpflegerinnen(txtWeitereDGKP.getValue());
        klientIn.setBetreuungsbeginn(convertToLocalDate(dbBetreuungsbeginn.getValue()));
        klientIn.setBetreuungsende(convertToLocalDate(dbBetreuungsende.getValue()));
        klientIn.setKmProHb(txtKmprohb.getValue());

        return klientIn;
    }

    static void mapToFrontendKlientIn(KlientIn klientIn, Textbox txtKlientenNummer,
                                      Intbox intVerrechnungsnummer, Intbox intTaetigkeitsfeldNummer, Textbox txtSpende,
                                      Intbox intWerkvertragsNummer, Textbox txtKlientVorname, Textbox txtKlientNachname,
                                      Radio rWeiblich, Radio rMaennlich, Textbox txtImpacct, Intbox intPflegestufe,
                                      Intbox intSozialversicherungsnummer, Datebox dbGebdatum, Textbox txtSvtrager,
                                      Textbox txtDiagnose, Textbox txtDiagnoseschluessel, Datebox dbSterbedatum,
                                      Checkbox cbMutterVertreterin, Textbox txtMutterName, Datebox dbGebdatumMutter,
                                      Textbox txtTelefonMutter, Checkbox cbVaterVertreter, Textbox txtVaterName,
                                      Datebox dbGebdatumVater, Textbox txtTelefonVater, Checkbox cbSonstigeVertreter,
                                      Textbox txtSonstigeDateneltern, Datebox dbGebdatumSonstige, Textbox txtTelefonSonstige,
                                      Textbox txtStrassePlzOrtMutter, Textbox txtEmailmutter, Textbox txtStrassePlzOrtVater,
                                      Textbox txtEmailvater, Radio rHauptwohnsitzJa, Radio rHauptwohnsitzNein, Textbox txtKh, Textbox txtKjh,
                                      Textbox txtSonstigeZuweisendeStellen, Textbox txtKinderfacharzt, Textbox txtTelefonKinderfacharzt,
                                      Textbox txtBetreuendeDGKP, Textbox txtWeitereDGKP, Datebox dbBetreuungsbeginn,
                                      Datebox dbBetreuungsende, Textbox txtKmprohb) {

        txtKlientenNummer.setValue(klientIn.getKlientenNummer());
        intTaetigkeitsfeldNummer.setValue(NumberUtils.toInt(klientIn.getTaetigkeitsfeldNummer()));

        if (intVerrechnungsnummer != null) {
            intVerrechnungsnummer.setValue(Integer.valueOf(klientIn.getVerrechnungNummer()));
        }

        txtSpende.setValue(klientIn.getSpende());
        intWerkvertragsNummer.setValue(NumberUtils.toInt(klientIn.getWerkvertragsNummer()));
        txtKlientVorname.setValue(klientIn.getVorname());
        txtKlientNachname.setValue(klientIn.getNachname());

        if (Geschlecht.WEIBLICH == klientIn.getGeschlecht()) {
            rWeiblich.setSelected(true);
        } else if (Geschlecht.MAENNLICH == klientIn.getGeschlecht()) {
            rMaennlich.setSelected(true);
        }

        txtImpacct.setValue(klientIn.getImpacctGruppe());
        intPflegestufe.setValue(NumberUtils.toInt(klientIn.getPflegestufe()));
        intSozialversicherungsnummer.setValue(NumberUtils.toInt(klientIn.getSozialversicherungsnummer()));
        dbGebdatum.setValue(convertLocalDateToDate(klientIn.getGeburtsdatum()));
        txtSvtrager.setValue(klientIn.getSozialversicherungstraeger());
        txtDiagnose.setValue(klientIn.getDiagnose());
        txtDiagnoseschluessel.setValue(klientIn.getDiagnoseschluessel());
        dbSterbedatum.setValue(convertLocalDateToDate(klientIn.getSterbedatum()));
        cbMutterVertreterin.setChecked(klientIn.isMutterVertreterin());
        txtMutterName.setValue(klientIn.getMutterName());
        dbGebdatumMutter.setValue(convertLocalDateToDate(klientIn.getMutterGeburtsdatum()));
        txtTelefonMutter.setValue(klientIn.getMutterTelefonnummer());
        cbVaterVertreter.setChecked(klientIn.isVaterVertreter());
        txtVaterName.setValue(klientIn.getVaterName());
        dbGebdatumVater.setValue(convertLocalDateToDate(klientIn.getVaterGeburtsdatum()));
        txtTelefonVater.setValue(klientIn.getVaterTelefonnummer());
        cbSonstigeVertreter.setChecked(klientIn.isSonstigeVertreter());
        txtSonstigeDateneltern.setValue(klientIn.getSonstigeName());
        dbGebdatumSonstige.setValue(convertLocalDateToDate(klientIn.getSonstigeGeburtsdatum()));
        txtTelefonSonstige.setValue(klientIn.getSonstigeTelefonnummer());
        txtStrassePlzOrtMutter.setValue(klientIn.getStrasse());
        txtEmailmutter.setValue(klientIn.getMutterEmail());
        txtStrassePlzOrtVater.setValue(klientIn.getOrt());
        txtEmailvater.setValue(klientIn.getVaterEmail());

        if (klientIn.isHauptwohnsitz()) {
            rHauptwohnsitzJa.setSelected(true);
        } else {
            rHauptwohnsitzNein.setSelected(true);
        }

        txtKh.setValue(klientIn.getZuweisendeStelleKH());
        txtKjh.setValue(klientIn.getZuweisendeStelleKJH());
        txtSonstigeZuweisendeStellen.setValue(klientIn.getZuweisendeStelleSonstige());
        txtKinderfacharzt.setValue(klientIn.getKinderfacharztName());
        txtTelefonKinderfacharzt.setValue(klientIn.getKinderfacharztTelefonnummer());
        txtBetreuendeDGKP.setValue(klientIn.getBetreuendeKrankenpflegerin());
        txtWeitereDGKP.setValue(klientIn.getWeitereKrankenpflegerinnen());
        dbBetreuungsbeginn.setValue(convertLocalDateToDate(klientIn.getBetreuungsbeginn()));
        dbBetreuungsende.setValue(convertLocalDateToDate(klientIn.getBetreuungsende()));
        txtKmprohb.setValue(klientIn.getKmProHb());
    }

    static void mapToFrontendVerwandte(KlientIn klientIn, Textbox txtKlientenNummer, Intbox intTaetigkeitsfeldNummer,
                                       Intbox intVerrechnungsnummer, Textbox txtSpende, Intbox intWerkvertragsNummer,
                                       Textbox txtKlientVorname, Textbox txtKlientNachname, Radio rWeiblich, Radio rMaennlich,
                                       Textbox txtImpacct, Intbox intPflegestufe, Intbox intSozialversicherungsnummer,
                                       Datebox dbGebdatum, Textbox txtSvtrager, Textbox txtDiagnose, Textbox txtDiagnoseschluessel,
                                       Datebox dbSterbedatum) {

        txtKlientenNummer.setValue(klientIn.getKlientenNummer());
        intTaetigkeitsfeldNummer.setValue(NumberUtils.toInt(klientIn.getTaetigkeitsfeldNummer()));

        if (intVerrechnungsnummer != null) {
            intVerrechnungsnummer.setValue(Integer.valueOf(klientIn.getVerrechnungNummer()));
        }

        txtSpende.setValue(klientIn.getSpende());
        intWerkvertragsNummer.setValue(NumberUtils.toInt(klientIn.getWerkvertragsNummer()));
        txtKlientVorname.setValue(klientIn.getVorname());
        txtKlientNachname.setValue(klientIn.getNachname());

        if (Geschlecht.WEIBLICH == klientIn.getGeschlecht()) {
            rWeiblich.setSelected(true);
        } else if (Geschlecht.MAENNLICH == klientIn.getGeschlecht()) {
            rMaennlich.setSelected(true);
        }

        txtImpacct.setValue(klientIn.getImpacctGruppe());
        intPflegestufe.setValue(NumberUtils.toInt(klientIn.getPflegestufe()));
        intSozialversicherungsnummer.setValue(NumberUtils.toInt(klientIn.getSozialversicherungsnummer()));
        dbGebdatum.setValue(convertLocalDateToDate(klientIn.getGeburtsdatum()));
        txtSvtrager.setValue(klientIn.getSozialversicherungstraeger());
        txtDiagnose.setValue(klientIn.getDiagnose());
        txtDiagnoseschluessel.setValue(klientIn.getDiagnoseschluessel());
        dbSterbedatum.setValue(convertLocalDateToDate(klientIn.getSterbedatum()));
    }

    static void calculateBetreuungsstundenDates(Label lblNow, Label lblPlusOne, Label lblPlusTwo) {
        lblNow.setValue(String.valueOf(MokiDateUtil.getYearNow()));
        lblPlusOne.setValue(String.valueOf(MokiDateUtil.getPlusOneYear()));
        lblPlusTwo.setValue(String.valueOf(MokiDateUtil.getplusTwoYears()));
    }

    static List<Betreuungsstunden> mapBetreuungsstundenMap(Textbox txtBetreuungsstundenNow,
                                                           Textbox txtBetreuungsstundenPlusOne,
                                                           Textbox txtBetreuungsstundenPlusTwo) {

        List<Betreuungsstunden> betreuungsstunden = new ArrayList<>();

        Betreuungsstunden betreuungsstundenNow = new Betreuungsstunden();
        betreuungsstundenNow.setJahr(MokiDateUtil.getYearNow());
        betreuungsstundenNow.setStunden(convertEmptyToZero(txtBetreuungsstundenNow.getValue()));
        betreuungsstunden.add(betreuungsstundenNow);

        Betreuungsstunden betreuungsstundenPlusOne = new Betreuungsstunden();
        betreuungsstundenPlusOne.setJahr(MokiDateUtil.getPlusOneYear());
        betreuungsstundenPlusOne.setStunden(convertEmptyToZero(txtBetreuungsstundenPlusOne.getValue()));
        betreuungsstunden.add(betreuungsstundenPlusOne);

        Betreuungsstunden betreuungsstundenPlusTwo = new Betreuungsstunden();
        betreuungsstundenPlusTwo.setJahr(MokiDateUtil.getplusTwoYears());
        betreuungsstundenPlusTwo.setStunden(convertEmptyToZero(txtBetreuungsstundenPlusTwo.getValue()));
        betreuungsstunden.add(betreuungsstundenPlusTwo);

        return betreuungsstunden;
    }

    static List<Finanzierung> mapToDomainmodelFinanzierung(Map<Institutions, String> institutionsStringMap,
                                                           Checkbox cbLandKosten, Checkbox cbLandSelbstbehalt,
                                                           Checkbox cbMukiKosten, Checkbox cbMukiSelbstbehalt,
                                                           Checkbox cbKinderJugendhilfeKosten, Checkbox cbKinderJugendhilfeSelbstbehalt,
                                                           Checkbox cbVersicherungKosten, Checkbox cbVersicherungSelbstbehalt,
                                                           Checkbox cbHospizKosten, Checkbox cbHospizSelbstbehalt,
                                                           Checkbox cbSpendenKosten, Checkbox cbSpendenSelbstbehalt,
                                                           Checkbox cbSozialversicherungKosten, Checkbox cbSozialversicherungSelbstbehalt,
                                                           Checkbox cbPrivatKosten, Checkbox cbPrivatSelbstbehalt,
                                                           Checkbox cbKibKosten, Checkbox cbKibSelbstbehalt,
                                                           Checkbox cbAndereKosten, Checkbox cbAndereSelbstbehalt) {

        // read all finanzierungen from klientin

        List<Finanzierung> finanzierungList = new ArrayList<>();

        Finanzierung finanzierung = new Finanzierung();

        // Land NÖ
        finanzierung.setId(institutionsStringMap.get(Institutions.LAND));
        finanzierung.setInstitution(Institutions.LAND);
        finanzierung.setKostenUebernahme(cbLandKosten.isChecked());
        finanzierung.setKostenSelbstbehalt(cbLandSelbstbehalt.isChecked());
        finanzierungList.add(finanzierung);

        // Muki
        finanzierung = new Finanzierung();
        finanzierung.setId(institutionsStringMap.get(Institutions.MUKI));
        finanzierung.setInstitution(Institutions.MUKI);
        finanzierung.setKostenUebernahme(cbMukiKosten.isChecked());
        finanzierung.setKostenSelbstbehalt(cbMukiSelbstbehalt.isChecked());
        finanzierungList.add(finanzierung);

        // Kinder- und Jugendhilfe
        finanzierung = new Finanzierung();
        finanzierung.setId(institutionsStringMap.get(Institutions.KINDER_JUGENDHILFE));
        finanzierung.setInstitution(Institutions.KINDER_JUGENDHILFE);
        finanzierung.setKostenUebernahme(cbKinderJugendhilfeKosten.isChecked());
        finanzierung.setKostenSelbstbehalt(cbKinderJugendhilfeSelbstbehalt.isChecked());
        finanzierungList.add(finanzierung);

        // Versicherung
        finanzierung = new Finanzierung();
        finanzierung.setId(institutionsStringMap.get(Institutions.VERSICHERUNG));
        finanzierung.setInstitution(Institutions.VERSICHERUNG);
        finanzierung.setKostenUebernahme(cbVersicherungKosten.isChecked());
        finanzierung.setKostenSelbstbehalt(cbVersicherungSelbstbehalt.isChecked());
        finanzierungList.add(finanzierung);

        // Hospiz/MPT
        finanzierung = new Finanzierung();
        finanzierung.setId(institutionsStringMap.get(Institutions.HOSPIZ));
        finanzierung.setInstitution(Institutions.HOSPIZ);
        finanzierung.setKostenUebernahme(cbHospizKosten.isChecked());
        finanzierung.setKostenSelbstbehalt(cbHospizSelbstbehalt.isChecked());
        finanzierungList.add(finanzierung);

        // Spenden
        finanzierung = new Finanzierung();
        finanzierung.setId(institutionsStringMap.get(Institutions.SPENDEN));
        finanzierung.setInstitution(Institutions.SPENDEN);
        finanzierung.setKostenUebernahme(cbSpendenKosten.isChecked());
        finanzierung.setKostenSelbstbehalt(cbSpendenSelbstbehalt.isChecked());
        finanzierungList.add(finanzierung);

        // Sozialversicherung
        finanzierung = new Finanzierung();
        finanzierung.setId(institutionsStringMap.get(Institutions.SOZIALVERSICHERUNG));
        finanzierung.setInstitution(Institutions.SOZIALVERSICHERUNG);
        finanzierung.setKostenUebernahme(cbSozialversicherungKosten.isChecked());
        finanzierung.setKostenSelbstbehalt(cbSozialversicherungSelbstbehalt.isChecked());
        finanzierungList.add(finanzierung);

        // privat
        finanzierung = new Finanzierung();
        finanzierung.setId(institutionsStringMap.get(Institutions.PRIVAT));
        finanzierung.setInstitution(Institutions.PRIVAT);
        finanzierung.setKostenUebernahme(cbPrivatKosten.isChecked());
        finanzierung.setKostenSelbstbehalt(cbPrivatSelbstbehalt.isChecked());
        finanzierungList.add(finanzierung);

        // KiB
        finanzierung = new Finanzierung();
        finanzierung.setId(institutionsStringMap.get(Institutions.KIB));
        finanzierung.setInstitution(Institutions.KIB);
        finanzierung.setKostenUebernahme(cbKibKosten.isChecked());
        finanzierung.setKostenSelbstbehalt(cbKibSelbstbehalt.isChecked());
        finanzierungList.add(finanzierung);

        // andere
        finanzierung = new Finanzierung();
        finanzierung.setId(institutionsStringMap.get(Institutions.ANDERE));
        finanzierung.setInstitution(Institutions.ANDERE);
        finanzierung.setKostenUebernahme(cbAndereKosten.isChecked());
        finanzierung.setKostenSelbstbehalt(cbAndereSelbstbehalt.isChecked());
        finanzierungList.add(finanzierung);

        return finanzierungList;
    }

    private static String denullTextbox(Textbox textbox) {
        return (textbox == null || textbox.getValue() == null) ? null : textbox.getValue();
    }

    private static String denullIntbox(Intbox intbox) {
        return (intbox == null || intbox.getValue() == null) ? null : intbox.getValue().toString();
    }

    private static String convertEmptyToZero(String value) {
        return StringUtils.isEmpty(value) ? "0" : StringUtils.replace(value, ",", ".");
    }

    private static String getVerrechnungId(Combobox cbbVerrechnungsnummer) {
        return cbbVerrechnungsnummer.getSelectedItem() == null ? null : ((Verrechnung) cbbVerrechnungsnummer.getSelectedItem().getValue()).getId();
    }

    private static LocalDate convertToLocalDate(Date value) {
        return value == null ? null : value.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    private static Date convertLocalDateToDate(LocalDate value) {
        return value == null ? null : Date.from(value.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

}
