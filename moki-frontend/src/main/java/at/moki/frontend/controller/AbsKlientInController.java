package at.moki.frontend.controller;

import at.moki.business.betreuungsstunden.BetreuungsstundenBusiness;
import at.moki.business.finanzierung.FinanzierungBusiness;
import at.moki.business.klientin.KlientInBusiness;
import at.moki.business.klientin.KlientInZusaetzlicheDokumenteBusiness;
import at.moki.dbrepo.verrechnung.VerrechnungRepo;
import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.domainmodel.finanzierung.Finanzierung;
import at.moki.domainmodel.finanzierung.Institutions;
import at.moki.domainmodel.klientin.KlientInZusaetzlicheDokumente;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.domainmodel.verrechnung.Verrechnung;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.frontend.model.KlientInZusaetzlicheDokumenteSearchListboxModel;
import at.moki.session.SessionModel;
import at.moki.session.TransferModel;
import org.apache.commons.lang3.StringUtils;
import org.zkoss.util.media.Media;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Green Arrow on 10.09.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
abstract class AbsKlientInController extends SelectorComposer<Component> {

    private static final long serialVersionUID = -1666276949046317315L;

    /********************* WIRE KLIENTIN **********************/

    @Wire("#incKlientInModel #txtKlientenNummer")
    Textbox txtKlientenNummer;

    @Wire("#incKlientInModel #lblTaetigkeitsfeld")
    Label lblTaetigkeitsfeld;

    @Wire("#incKlientInModel #intTaetigkeitsfeldNummer")
    Intbox intTaetigkeitsfeldNummer;

    @Wire("#incKlientInModel #cbbVerrechnungsnummer")
    Combobox cbbVerrechnungsnummer;

    @Wire("#incKlientInModel #intVerrechnungsnummer")
    Intbox intVerrechnungsnummer;

    @Wire("#incKlientInModel #txtSpende")
    Textbox txtSpende;

    @Wire("#incKlientInModel #intWerkvertragsNummer")
    Intbox intWerkvertragsNummer;

    @Wire("#incKlientInModel #txtKlientVorname")
    Textbox txtKlientVorname;

    @Wire("#incKlientInModel #txtKlientNachname")
    Textbox txtKlientNachname;

    @Wire("#incKlientInModel #rWeiblich")
    Radio rWeiblich;

    @Wire("#incKlientInModel #rMaennlich")
    Radio rMaennlich;

    @Wire("#incKlientInModel #txtImpacct")
    Textbox txtImpacct;

    @Wire("#incKlientInModel #intPflegestufe")
    Intbox intPflegestufe;

    @Wire("#incKlientInModel #intSozialversicherungsnummer")
    Intbox intSozialversicherungsnummer;

    @Wire("#incKlientInModel #dbGebdatum")
    Datebox dbGebdatum;

    @Wire("#incKlientInModel #txtSvtrager")
    Textbox txtSvtrager;

    @Wire("#incKlientInModel #txtDiagnose")
    Textbox txtDiagnose;

    @Wire("#incKlientInModel #txtDiagnoseschluessel")
    Textbox txtDiagnoseschluessel;

    @Wire("#incKlientInModel #dbSterbedatum")
    Datebox dbSterbedatum;

    @Wire("#incKlientInZusaetlicheInformationen #cbMutterVertreterin")
    Checkbox cbMutterVertreterin;

    @Wire("#incKlientInZusaetlicheInformationen #txtMutterName")
    Textbox txtMutterName;

    @Wire("#incKlientInZusaetlicheInformationen #dbGebdatumMutter")
    Datebox dbGebdatumMutter;

    @Wire("#incKlientInZusaetlicheInformationen #txtTelefonMutter")
    Textbox txtTelefonMutter;

    @Wire("#incKlientInZusaetlicheInformationen #cbVaterVertreter")
    Checkbox cbVaterVertreter;

    @Wire("#incKlientInZusaetlicheInformationen #txtVaterName")
    Textbox txtVaterName;

    @Wire("#incKlientInZusaetlicheInformationen #dbGebdatumVater")
    Datebox dbGebdatumVater;

    @Wire("#incKlientInZusaetlicheInformationen #txtTelefonVater")
    Textbox txtTelefonVater;

    @Wire("#incKlientInZusaetlicheInformationen #cbSonstigeVertreter")
    Checkbox cbSonstigeVertreter;

    @Wire("#incKlientInZusaetlicheInformationen #txtSonstigeDateneltern")
    Textbox txtSonstigeDateneltern;

    @Wire("#incKlientInZusaetlicheInformationen #dbGebdatumSonstige")
    Datebox dbGebdatumSonstige;

    @Wire("#incKlientInZusaetlicheInformationen #txtTelefonSonstige")
    Textbox txtTelefonSonstige;

    @Wire("#incKlientInZusaetlicheInformationen #txtStrassePlzOrtMutter")
    Textbox txtStrassePlzOrtMutter;

    @Wire("#incKlientInZusaetlicheInformationen #txtEmailmutter")
    Textbox txtEmailmutter;

    @Wire("#incKlientInZusaetlicheInformationen #txtStrassePlzOrtVater")
    Textbox txtStrassePlzOrtVater;

    @Wire("#incKlientInZusaetlicheInformationen #txtEmailvater")
    Textbox txtEmailvater;

    @Wire("#incKlientInZusaetlicheInformationen #rHauptwohnsitzJa")
    Radio rHauptwohnsitzJa;

    @Wire("#incKlientInZusaetlicheInformationen #rHauptwohnsitzNein")
    Radio rHauptwohnsitzNein;

    @Wire("#incKlientInZusaetlicheInformationen #txtKh")
    Textbox txtKh;

    @Wire("#incKlientInZusaetlicheInformationen #txtKjh")
    Textbox txtKjh;

    @Wire("#incKlientInZusaetlicheInformationen #txtSonstigeZuweisendeStellen")
    Textbox txtSonstigeZuweisendeStellen;

    @Wire("#incKlientInZusaetlicheInformationen #txtKinderfacharzt")
    Textbox txtKinderfacharzt;

    @Wire("#incKlientInZusaetlicheInformationen #txtTelefonKinderfacharzt")
    Textbox txtTelefonKinderfacharzt;

    @Wire("#incKlientInZusaetlicheInformationen #cbLandKosten")
    Checkbox cbLandKosten;

    @Wire("#incKlientInZusaetlicheInformationen #cbLandSelbstbehalt")
    Checkbox cbLandSelbstbehalt;

    @Wire("#incKlientInZusaetlicheInformationen #cbMukiKosten")
    Checkbox cbMukiKosten;

    @Wire("#incKlientInZusaetlicheInformationen #cbMukiSelbstbehalt")
    Checkbox cbMukiSelbstbehalt;

    @Wire("#incKlientInZusaetlicheInformationen #cbKinderJugendhilfeKosten")
    Checkbox cbKinderJugendhilfeKosten;

    @Wire("#incKlientInZusaetlicheInformationen #cbKinderJugendhilfeSelbstbehalt")
    Checkbox cbKinderJugendhilfeSelbstbehalt;

    @Wire("#incKlientInZusaetlicheInformationen #cbVersicherungKosten")
    Checkbox cbVersicherungKosten;

    @Wire("#incKlientInZusaetlicheInformationen #cbVersicherungSelbstbehalt")
    Checkbox cbVersicherungSelbstbehalt;

    @Wire("#incKlientInZusaetlicheInformationen #cbHospizKosten")
    Checkbox cbHospizKosten;

    @Wire("#incKlientInZusaetlicheInformationen #cbHospizSelbstbehalt")
    Checkbox cbHospizSelbstbehalt;

    @Wire("#incKlientInZusaetlicheInformationen #cbSpendenKosten")
    Checkbox cbSpendenKosten;

    @Wire("#incKlientInZusaetlicheInformationen #cbSpendenSelbstbehalt")
    Checkbox cbSpendenSelbstbehalt;

    @Wire("#incKlientInZusaetlicheInformationen #cbSozialversicherungKosten")
    Checkbox cbSozialversicherungKosten;

    @Wire("#incKlientInZusaetlicheInformationen #cbSozialversicherungSelbstbehalt")
    Checkbox cbSozialversicherungSelbstbehalt;

    @Wire("#incKlientInZusaetlicheInformationen #cbPrivatKosten")
    Checkbox cbPrivatKosten;

    @Wire("#incKlientInZusaetlicheInformationen #cbPrivatSelbstbehalt")
    Checkbox cbPrivatSelbstbehalt;

    @Wire("#incKlientInZusaetlicheInformationen #cbKibKosten")
    Checkbox cbKibKosten;

    @Wire("#incKlientInZusaetlicheInformationen #cbKibSelbstbehalt")
    Checkbox cbKibSelbstbehalt;

    @Wire("#incKlientInZusaetlicheInformationen #cbAndereKosten")
    Checkbox cbAndereKosten;

    @Wire("#incKlientInZusaetlicheInformationen #cbAndereSelbstbehalt")
    Checkbox cbAndereSelbstbehalt;

    @Wire("#incKlientInZusaetlicheInformationen #txtBetreuendeDGKP")
    Textbox txtBetreuendeDGKP;

    @Wire("#incKlientInZusaetlicheInformationen #txtWeitereDGKP")
    Textbox txtWeitereDGKP;

    @Wire("#incKlientInZusaetlicheInformationen #dbBetreuungsbeginn")
    Datebox dbBetreuungsbeginn;

    @Wire("#incKlientInZusaetlicheInformationen #dbBetreuungsende")
    Datebox dbBetreuungsende;

    @Wire("#incKlientInZusaetlicheInformationen #txtKmprohb")
    Textbox txtKmprohb;

    @Wire("#incKlientInBetreuungsstunden #txtBetreuungsstundenNow")
    Textbox txtBetreuungsstundenNow;

    @Wire("#incKlientInBetreuungsstunden #txtBetreuungsstundenPlusOne")
    Textbox txtBetreuungsstundenPlusOne;

    @Wire("#incKlientInBetreuungsstunden #txtBetreuungsstundenPlusTwo")
    Textbox txtBetreuungsstundenPlusTwo;

    @Wire("#incKlientInBetreuungsstunden #lblNow")
    Label lblNow;

    @Wire("#incKlientInBetreuungsstunden #lblPlusOne")
    Label lblPlusOne;

    @Wire("#incKlientInBetreuungsstunden #lblPlusTwo")
    Label lblPlusTwo;

    @Wire("#incKlientInBetreuungsstunden #lblStundenGesamtValue")
    Label lblStundenGesamtValue;

    /********************* WIRE ZWILLING **********************/

    @Wire("#incZwilling #txtKlientenNummer")
    Textbox txtKlientenNummerZwilling;

    @Wire("#incZwilling #lblTaetigkeitsfeld")
    Label lblTaetigkeitsfeldZwilling;

    @Wire("#incZwilling #intTaetigkeitsfeldNummer")
    Intbox intTaetigkeitsfeldNummerZwilling;

    @Wire("#incZwilling #cbbVerrechnungsnummer")
    Combobox cbbVerrechnungsnummerZwilling;

    @Wire("#incZwilling #intVerrechnungsnummer")
    Intbox intVerrechnungsnummerZwilling;

    @Wire("#incZwilling #txtSpende")
    Textbox txtSpendeZwilling;

    @Wire("#incZwilling #intWerkvertragsNummer")
     Intbox intWerkvertragsNummerZwilling;

    @Wire("#incZwilling #txtKlientVorname")
    Textbox txtKlientVornameZwilling;

    @Wire("#incZwilling #txtKlientNachname")
    Textbox txtKlientNachnameZwilling;

    @Wire("#incZwilling #rWeiblich")
    Radio rWeiblichZwilling;

    @Wire("#incZwilling #rMaennlich")
    Radio rMaennlichZwilling;

    @Wire("#incZwilling #txtImpacct")
    Textbox txtImpacctZwilling;

    @Wire("#incZwilling #intPflegestufe")
    Intbox intPflegestufeZwilling;

    @Wire("#incZwilling #intSozialversicherungsnummer")
    Intbox intSozialversicherungsnummerZwilling;

    @Wire("#incZwilling #dbGebdatum")
    Datebox dbGebdatumZwilling;

    @Wire("#incZwilling #txtSvtrager")
    Textbox txtSvtragerZwilling;

    @Wire("#incZwilling #txtDiagnose")
    Textbox txtDiagnoseZwilling;

    @Wire("#incZwilling #txtDiagnoseschluessel")
    Textbox txtDiagnoseschluesselZwilling;

    @Wire("#incZwilling #dbSterbedatum")
    Datebox dbSterbedatumZwilling;

    @Wire("#incZwillingBetreuungsstunden #txtBetreuungsstundenNow")
    Textbox txtBetreuungsstundenNowZwilling;

    @Wire("#incZwillingBetreuungsstunden #txtBetreuungsstundenPlusOne")
    Textbox txtBetreuungsstundenPlusOneZwilling;

    @Wire("#incZwillingBetreuungsstunden #txtBetreuungsstundenPlusTwo")
    Textbox txtBetreuungsstundenPlusTwoZwilling;

    @Wire("#incZwillingBetreuungsstunden #lblNow")
    Label lblNowZwilling;

    @Wire("#incZwillingBetreuungsstunden #lblPlusOne")
    Label lblPlusOneZwilling;

    @Wire("#incZwillingBetreuungsstunden #lblPlusTwo")
    Label lblPlusTwoZwilling;

    @Wire("#incZwillingBetreuungsstunden #lblStundenGesamtValue")
    Label lblStundenGesamtZwillingValue;

    /********************* WIRE DRILLING **********************/

    @Wire("#incDrilling #txtKlientenNummer")
    Textbox txtKlientenNummerDrilling;

    @Wire("#incDrilling #lblTaetigkeitsfeld")
    Label lblTaetigkeitsfeldDrilling;

    @Wire("#incDrilling #intTaetigkeitsfeldNummer")
    Intbox intTaetigkeitsfeldNummerDrilling;

    @Wire("#incDrilling #cbbVerrechnungsnummer")
    Combobox cbbVerrechnungsnummerDrilling;

    @Wire("#incDrilling #intVerrechnungsnummer")
    Intbox intVerrechnungsnummerDrilling;

    @Wire("#incDrilling #txtSpende")
    Textbox txtSpendeDrilling;

    @Wire("#incDrilling #intWerkvertragsNummer")
    Intbox intWerkvertragsNummerDrilling;

    @Wire("#incDrilling #txtKlientVorname")
    Textbox txtKlientVornameDrilling;

    @Wire("#incDrilling #txtKlientNachname")
    Textbox txtKlientNachnameDrilling;

    @Wire("#incDrilling #rWeiblich")
    Radio rWeiblichDrilling;

    @Wire("#incDrilling #rMaennlich")
    Radio rMaennlichDrilling;

    @Wire("#incDrilling #txtImpacct")
    Textbox txtImpacctDrilling;

    @Wire("#incDrilling #intPflegestufe")
    Intbox intPflegestufeDrilling;

    @Wire("#incDrilling #intSozialversicherungsnummer")
    Intbox intSozialversicherungsnummerDrilling;

    @Wire("#incDrilling #dbGebdatum")
    Datebox dbGebdatumDrilling;

    @Wire("#incDrilling #txtSvtrager")
    Textbox txtSvtragerDrilling;

    @Wire("#incDrilling #txtDiagnose")
    Textbox txtDiagnoseDrilling;

    @Wire("#incDrilling #txtDiagnoseschluessel")
    Textbox txtDiagnoseschluesselDrilling;

    @Wire("#incDrilling #dbSterbedatum")
    Datebox dbSterbedatumDrilling;

    @Wire("#incDrillingBetreuungsstunden #txtBetreuungsstundenNow")
    Textbox txtBetreuungsstundenNowDrilling;

    @Wire("#incDrillingBetreuungsstunden #txtBetreuungsstundenPlusOne")
    Textbox txtBetreuungsstundenPlusOneDrilling;

    @Wire("#incDrillingBetreuungsstunden #txtBetreuungsstundenPlusTwo")
    Textbox txtBetreuungsstundenPlusTwoDrilling;

    @Wire("#incDrillingBetreuungsstunden #lblNow")
    Label lblNowDrilling;

    @Wire("#incDrillingBetreuungsstunden #lblPlusOne")
    Label lblPlusOneDrilling;

    @Wire("#incDrillingBetreuungsstunden #lblPlusTwo")
    Label lblPlusTwoDrilling;

    @Wire("#incDrillingBetreuungsstunden #lblStundenGesamtValue")
    Label lblStundenGesamtDrillingValue;

    /********************* WIRE Zweites Kind **********************/

    @Wire("#incZweitesKind #txtKlientenNummer")
    Textbox txtKlientenNummerZweitesKind;

    @Wire("#incZweitesKind #lblTaetigkeitsfeld")
    Label lblTaetigkeitsfeldZweitesKind;

    @Wire("#incZweitesKind #intTaetigkeitsfeldNummer")
    Intbox intTaetigkeitsfeldNummerZweitesKind;

    @Wire("#incZweitesKind #cbbVerrechnungsnummer")
    Combobox cbbVerrechnungsnummerZweitesKind;

    @Wire("#incZweitesKind #intVerrechnungsnummer")
    Intbox intVerrechnungsnummerZweitesKind;

    @Wire("#incZweitesKind #txtSpende")
    Textbox txtSpendeZweitesKind;

    @Wire("#incZweitesKind #intWerkvertragsNummer")
    Intbox intWerkvertragsNummerZweitesKind;

    @Wire("#incZweitesKind #txtKlientVorname")
    Textbox txtKlientVornameZweitesKind;

    @Wire("#incZweitesKind #txtKlientNachname")
    Textbox txtKlientNachnameZweitesKind;

    @Wire("#incZweitesKind #rWeiblich")
    Radio rWeiblichZweitesKind;

    @Wire("#incZweitesKind #rMaennlich")
    Radio rMaennlichZweitesKind;

    @Wire("#incZweitesKind #txtImpacct")
    Textbox txtImpacctZweitesKind;

    @Wire("#incZweitesKind #intPflegestufe")
    Intbox intPflegestufeZweitesKind;

    @Wire("#incZweitesKind #intSozialversicherungsnummer")
    Intbox intSozialversicherungsnummerZweitesKind;

    @Wire("#incZweitesKind #dbGebdatum")
    Datebox dbGebdatumZweitesKind;

    @Wire("#incZweitesKind #txtSvtrager")
    Textbox txtSvtragerZweitesKind;

    @Wire("#incZweitesKind #txtDiagnose")
    Textbox txtDiagnoseZweitesKind;

    @Wire("#incZweitesKind #txtDiagnoseschluessel")
    Textbox txtDiagnoseschluesselZweitesKind;

    @Wire("#incZweitesKind #dbSterbedatum")
    Datebox dbSterbedatumZweitesKind;

    @Wire("#incZweitesKindBetreuungsstunden #txtBetreuungsstundenNow")
    Textbox txtBetreuungsstundenNowZweitesKind;

    @Wire("#incZweitesKindBetreuungsstunden #txtBetreuungsstundenPlusOne")
    Textbox txtBetreuungsstundenPlusOneZweitesKind;

    @Wire("#incZweitesKindBetreuungsstunden #txtBetreuungsstundenPlusTwo")
    Textbox txtBetreuungsstundenPlusTwoZweitesKind;

    @Wire("#incZweitesKindBetreuungsstunden #lblNow")
    Label lblNowZweitesKind;

    @Wire("#incZweitesKindBetreuungsstunden #lblPlusOne")
    Label lblPlusOneZweitesKind;

    @Wire("#incZweitesKindBetreuungsstunden #lblPlusTwo")
    Label lblPlusTwoZweitesKind;

    @Wire("#incZweitesKindBetreuungsstunden #lblStundenGesamtValue")
    Label lblStundenGesamtZweitesKindValue;

    /********************* WIRE Drittes Kind **********************/

    @Wire("#incDrittesKind #txtKlientenNummer")
    Textbox txtKlientenNummerDrittesKind;

    @Wire("#incDrittesKind #lblTaetigkeitsfeld")
    Label lblTaetigkeitsfeldDrittesKind;

    @Wire("#incDrittesKind #intTaetigkeitsfeldNummer")
    Intbox intTaetigkeitsfeldNummerDrittesKind;

    @Wire("#incDrittesKind #cbbVerrechnungsnummer")
    Combobox cbbVerrechnungsnummerDrittesKind;

    @Wire("#incDrittesKind #intVerrechnungsnummer")
    Intbox intVerrechnungsnummerDrittesKind;

    @Wire("#incDrittesKind #txtSpende")
    Textbox txtSpendeDrittesKind;

    @Wire("#incDrittesKind #intWerkvertragsNummer")
    Intbox intWerkvertragsNummerDrittesKind;

    @Wire("#incDrittesKind #txtKlientVorname")
    Textbox txtKlientVornameDrittesKind;

    @Wire("#incDrittesKind #txtKlientNachname")
    Textbox txtKlientNachnameDrittesKind;

    @Wire("#incDrittesKind #rWeiblich")
    Radio rWeiblichDrittesKind;

    @Wire("#incDrittesKind #rMaennlich")
    Radio rMaennlichDrittesKind;

    @Wire("#incDrittesKind #txtImpacct")
    Textbox txtImpacctDrittesKind;

    @Wire("#incDrittesKind #intPflegestufe")
    Intbox intPflegestufeDrittesKind;

    @Wire("#incDrittesKind #intSozialversicherungsnummer")
    Intbox intSozialversicherungsnummerDrittesKind;

    @Wire("#incDrittesKind #dbGebdatum")
    Datebox dbGebdatumDrittesKind;

    @Wire("#incDrittesKind #txtSvtrager")
    Textbox txtSvtragerDrittesKind;

    @Wire("#incDrittesKind #txtDiagnose")
    Textbox txtDiagnoseDrittesKind;

    @Wire("#incDrittesKind #txtDiagnoseschluessel")
    Textbox txtDiagnoseschluesselDrittesKind;

    @Wire("#incDrittesKind #dbSterbedatum")
    Datebox dbSterbedatumDrittesKind;

    @Wire("#incDrittesKindBetreuungsstunden #txtBetreuungsstundenNow")
    Textbox txtBetreuungsstundenNowDrittesKind;

    @Wire("#incDrittesKindBetreuungsstunden #txtBetreuungsstundenPlusOne")
    Textbox txtBetreuungsstundenPlusOneDrittesKind;

    @Wire("#incDrittesKindBetreuungsstunden #txtBetreuungsstundenPlusTwo")
    Textbox txtBetreuungsstundenPlusTwoDrittesKind;

    @Wire("#incDrittesKindBetreuungsstunden #lblNow")
    Label lblNowDrittesKind;

    @Wire("#incDrittesKindBetreuungsstunden #lblPlusOne")
    Label lblPlusOneDrittesKind;

    @Wire("#incDrittesKindBetreuungsstunden #lblPlusTwo")
    Label lblPlusTwoDrittesKind;

    @Wire("#incDrittesKindBetreuungsstunden #lblStundenGesamtValue")
    Label lblStundenGesamtDrittesKindValue;

    /********************* WIRE Viertes Kind **********************/

    @Wire("#incViertesKind #txtKlientenNummer")
    Textbox txtKlientenNummerViertesKind;

    @Wire("#incViertesKind #lblTaetigkeitsfeld")
    Label lblTaetigkeitsfeldViertesKind;

    @Wire("#incViertesKind #intTaetigkeitsfeldNummer")
    Intbox intTaetigkeitsfeldNummerViertesKind;

    @Wire("#incViertesKind #cbbVerrechnungsnummer")
    Combobox cbbVerrechnungsnummerViertesKind;

    @Wire("#incViertesKind #intVerrechnungsnummer")
    Intbox intVerrechnungsnummerViertesKind;

    @Wire("#incViertesKind #txtSpende")
    Textbox txtSpendeViertesKind;

    @Wire("#incViertesKind #intWerkvertragsNummer")
    Intbox intWerkvertragsNummerViertesKind;

    @Wire("#incViertesKind #txtKlientVorname")
    Textbox txtKlientVornameViertesKind;

    @Wire("#incViertesKind #txtKlientNachname")
    Textbox txtKlientNachnameViertesKind;

    @Wire("#incViertesKind #rWeiblich")
    Radio rWeiblichViertesKind;

    @Wire("#incViertesKind #rMaennlich")
    Radio rMaennlichViertesKind;

    @Wire("#incViertesKind #txtImpacct")
    Textbox txtImpacctViertesKind;

    @Wire("#incViertesKind #intPflegestufe")
    Intbox intPflegestufeViertesKind;

    @Wire("#incViertesKind #intSozialversicherungsnummer")
    Intbox intSozialversicherungsnummerViertesKind;

    @Wire("#incViertesKind #dbGebdatum")
    Datebox dbGebdatumViertesKind;

    @Wire("#incViertesKind #txtSvtrager")
    Textbox txtSvtragerViertesKind;

    @Wire("#incViertesKind #txtDiagnose")
    Textbox txtDiagnoseViertesKind;

    @Wire("#incViertesKind #txtDiagnoseschluessel")
    Textbox txtDiagnoseschluesselViertesKind;

    @Wire("#incViertesKind #dbSterbedatum")
    Datebox dbSterbedatumViertesKind;

    @Wire("#incViertesKindBetreuungsstunden #txtBetreuungsstundenNow")
    Textbox txtBetreuungsstundenNowViertesKind;

    @Wire("#incViertesKindBetreuungsstunden #txtBetreuungsstundenPlusOne")
    Textbox txtBetreuungsstundenPlusOneViertesKind;

    @Wire("#incViertesKindBetreuungsstunden #txtBetreuungsstundenPlusTwo")
    Textbox txtBetreuungsstundenPlusTwoViertesKind;

    @Wire("#incViertesKindBetreuungsstunden #lblNow")
    Label lblNowViertesKind;

    @Wire("#incViertesKindBetreuungsstunden #lblPlusOne")
    Label lblPlusOneViertesKind;

    @Wire("#incViertesKindBetreuungsstunden #lblPlusTwo")
    Label lblPlusTwoViertesKind;

    @Wire("#incViertesKindBetreuungsstunden #lblStundenGesamtValue")
    Label lblStundenGesamtViertesKindValue;

    /****************** ZUSAETZLICHE DOKUMENTE ********************/

    @Wire("#incKlientinZusaetlicheDokumente #pnlKlientinZusaetlicheDokumente")
    Panel pnlKlientinZusaetlicheDokumente;

    @Wire("#incKlientinZusaetlicheDokumente #lboxKlientinZusaetlicheDokumente")
    Listbox lboxKlientinZusaetlicheDokumente;

    /************************ CDI Beans ***************************/

    @WireVariable("betreuungsstundenBusinessImpl")
    BetreuungsstundenBusiness betreuungsstundenBusiness;

    @WireVariable("finanzierungBusinessImpl")
    FinanzierungBusiness finanzierungBusiness;

    @WireVariable("klientInBusinessImpl")
    KlientInBusiness klientInBusiness;

    @WireVariable("klientInZusaetzlicheDokumenteBusinessImpl")
    KlientInZusaetzlicheDokumenteBusiness klientInZusaetzlicheDokumenteBusiness;

    @WireVariable("verrechnungRepoImpl")
    VerrechnungRepo verrechnungRepo;

    @WireVariable("transferModel")
    TransferModel transferModel;

    @WireVariable("sessionModel")
    private SessionModel sessionModel;

    // wire the include into the class
    private Include include = (Include) Path.getComponent(MokiFrontendConstants.INCLUDE);

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        // decide if klientennummer is disabled or not, only ADMINISTRATOR and VERWALTUNG users are allowed to
        // give and adapt it
        if (this.sessionModel.getMokiUser().getMokiUserRole() == MokiUserRole.KRANKENPFLEGERIN) {
            disableKlientenNummerTextfield();
        }

        performAfterCompose();
    }

    @Listen("onUpload=#incKlientinZusaetlicheDokumente #btnAddDocument")
    public void onAddDocument(UploadEvent event) {

        Media media = event.getMedia();

        if (!StringUtils.equals("pdf", media.getFormat())) {
            throw new UnsupportedOperationException("Die Datei ist kein PDF Format");
        }

        KlientInZusaetzlicheDokumente klientInZusaetzlicheDokumente = new KlientInZusaetzlicheDokumente();
        klientInZusaetzlicheDokumente.setDateiname(media.getName());
        String id = this.transferModel.getKlientInSearch().getId();
        klientInZusaetzlicheDokumente.setKlientInId(id);

        this.klientInZusaetzlicheDokumenteBusiness.save(klientInZusaetzlicheDokumente, media.getByteData());

        // reload list entries
        this.lboxKlientinZusaetlicheDokumente.setModel(new KlientInZusaetzlicheDokumenteSearchListboxModel(
                this.klientInZusaetzlicheDokumenteBusiness.findAllByKlientInId(id)));

        Messagebox.show(Labels.getLabel("klientin.zusaetzliche.dokumente.upload.successful"),
                MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);
    }

    private void disableKlientenNummerTextfield() {

        if (this.txtKlientenNummer != null) {
            this.txtKlientenNummer.setDisabled(true);
        }

        if (this.txtKlientenNummerZwilling != null) {
            this.txtKlientenNummerZwilling.setDisabled(true);
        }

        if (this.txtKlientenNummerDrilling != null) {
            this.txtKlientenNummerDrilling.setDisabled(true);
        }

        if (this.txtKlientenNummerZweitesKind != null) {
            this.txtKlientenNummerZweitesKind.setDisabled(true);
        }

        if (this.txtKlientenNummerDrittesKind != null) {
            this.txtKlientenNummerDrittesKind.setDisabled(true);
        }

        if (this.txtKlientenNummerViertesKind != null) {
            this.txtKlientenNummerViertesKind.setDisabled(true);
        }
    }

    abstract void performAfterCompose();

    void clearTransferModel() {
        // clear stored klientIn object
        transferModel.clearKlientInSearch();
        // clear mokidomainmodel
        transferModel.setKlientInExcelContainerModelList(null);
        include.setSrc("");// reset include
    }

    Map<Institutions, String> getInstitutions() {
        return this.transferModel.getKlientInSearch() == null ? new HashMap<>() :
                this.finanzierungBusiness.getDataAsMap(this.transferModel.getKlientInSearch().getId());
    }

    void setFinanzierungen(List<Finanzierung> finanzierungList) {
        finanzierungList.forEach(item -> {
            switch (item.getInstitution()) {
                case LAND:
                    cbLandKosten.setChecked(item.isKostenUebernahme());
                    cbLandSelbstbehalt.setChecked(item.isKostenSelbstbehalt());
                    break;
                case MUKI:
                    cbMukiKosten.setChecked(item.isKostenUebernahme());
                    cbMukiSelbstbehalt.setChecked(item.isKostenSelbstbehalt());
                    break;
                case KINDER_JUGENDHILFE:
                    cbKinderJugendhilfeKosten.setChecked(item.isKostenUebernahme());
                    cbKinderJugendhilfeSelbstbehalt.setChecked(item.isKostenSelbstbehalt());
                    break;
                case VERSICHERUNG:
                    cbVersicherungKosten.setChecked(item.isKostenUebernahme());
                    cbVersicherungSelbstbehalt.setChecked(item.isKostenSelbstbehalt());
                    break;
                case HOSPIZ:
                    cbHospizKosten.setChecked(item.isKostenUebernahme());
                    cbHospizSelbstbehalt.setChecked(item.isKostenSelbstbehalt());
                    break;
                case SPENDEN:
                    cbSpendenKosten.setChecked(item.isKostenUebernahme());
                    cbSpendenSelbstbehalt.setChecked(item.isKostenSelbstbehalt());
                    break;
                case SOZIALVERSICHERUNG:
                    cbSozialversicherungKosten.setChecked(item.isKostenUebernahme());
                    cbSozialversicherungSelbstbehalt.setChecked(item.isKostenSelbstbehalt());
                    break;
                case PRIVAT:
                    cbPrivatKosten.setChecked(item.isKostenUebernahme());
                    cbPrivatSelbstbehalt.setChecked(item.isKostenSelbstbehalt());
                    break;
                case KIB:
                    cbKibKosten.setChecked(item.isKostenUebernahme());
                    cbKibSelbstbehalt.setChecked(item.isKostenSelbstbehalt());
                    break;
                case ANDERE:
                    cbAndereKosten.setChecked(item.isKostenUebernahme());
                    cbAndereSelbstbehalt.setChecked(item.isKostenSelbstbehalt());
                    break;
                default:
                    throw new UnsupportedOperationException("Taetigkeitsfeld nicht gesetzt");
            }
        });
    }

    void setBetreuungsstunden(List<Betreuungsstunden> betreuungsstundenList, Textbox now, Textbox plusOne,
                              Textbox plusTwo, Label gesamtStunden) {

        now.setValue(convertDotsToCommas(betreuungsstundenList.get(0).getStunden()));
        plusOne.setValue(convertDotsToCommas(betreuungsstundenList.get(1).getStunden()));
        plusTwo.setValue(convertDotsToCommas(betreuungsstundenList.get(2).getStunden()));

        BigDecimal summe = BigDecimal.ZERO;

        for (Betreuungsstunden betreuungsstunden : betreuungsstundenList) {
            String stunden = betreuungsstunden.getStunden();
            if (stunden != null) {
                summe = summe.add(new BigDecimal(stunden));
            }
        }

        gesamtStunden.setValue(convertDotsToCommas(summe.toString()));
    }

    private String convertDotsToCommas(String value) {
        return StringUtils.replace(value, ".", ",");
    }

    void initVerrechnungCombobox(Combobox cbbVerrechnungsnummer) {
        List<Verrechnung> verrechnungList = verrechnungRepo.findAll();
        ListModelList verrechnungListModelList = new ListModelList<>(verrechnungList);
        cbbVerrechnungsnummer.setModel(verrechnungListModelList);
    }
}
