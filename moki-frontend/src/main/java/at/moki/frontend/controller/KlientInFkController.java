package at.moki.frontend.controller;

import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.domainmodel.finanzierung.Finanzierung;
import at.moki.domainmodel.klientin.FK;
import at.moki.domainmodel.klientin.KlientIn;
import at.moki.domainmodel.klientin.KlientInExcelContainerModel;
import at.moki.exception.MokiBusinessException;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.frontend.model.KlientInZusaetzlicheDokumenteSearchListboxModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Messagebox;

import java.util.List;

/**
 * Created by Green Arrow on 22.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class KlientInFkController extends AbsKlientInController {

    private static final long serialVersionUID = 1526929618800645233L;

    private static Logger logger = LoggerFactory.getLogger(KlientInFkController.class);

    private KlientIn zwillingSelected;

    private KlientIn drillingSelected;

    @Override
    void performAfterCompose() {

        super.lblTaetigkeitsfeld.setValue("FK" + "-NR");
        super.lblTaetigkeitsfeldZwilling.setValue("FK" + "-NR");
        super.lblTaetigkeitsfeldDrilling.setValue("FK" + "-NR");

        // calculate dates
        KlientInMapper.calculateBetreuungsstundenDates(super.lblNow, super.lblPlusOne, super.lblPlusTwo);
        KlientInMapper.calculateBetreuungsstundenDates(super.lblNowZwilling, super.lblPlusOneZwilling, super.lblPlusTwoZwilling);
        KlientInMapper.calculateBetreuungsstundenDates(super.lblNowDrilling, super.lblPlusOneDrilling, super.lblPlusTwoDrilling);

        super.initVerrechnungCombobox(super.cbbVerrechnungsnummer);
        super.initVerrechnungCombobox(super.cbbVerrechnungsnummerZwilling);
        super.initVerrechnungCombobox(super.cbbVerrechnungsnummerDrilling);

        // excel importer was used
        if (super.transferModel.getKlientInExcelContainerModelList() != null && !super.transferModel.getKlientInExcelContainerModelList().isEmpty()) {
            List<KlientInExcelContainerModel> klientInExcelContainerModel = super.transferModel.getKlientInExcelContainerModelList();

            mapToFrontend(
                    klientInExcelContainerModel.get(0).getKlientIn(),               // KlientIn
                    klientInExcelContainerModel.get(1).getKlientIn(),               // Zwilling
                    klientInExcelContainerModel.get(2).getKlientIn(),               // Drilling
                    klientInExcelContainerModel.get(0).getFinanzierungList(),       // Finanzierung from KlientIn
                    klientInExcelContainerModel.get(0).getBetreuungsstundenList(),  // Betreuungsstunden KlientIn
                    klientInExcelContainerModel.get(1).getBetreuungsstundenList(),  // Betreuungsstunden Zwilling
                    klientInExcelContainerModel.get(2).getBetreuungsstundenList()); // Betreuungsstunden Drilling

        } else { // klientin selected from search
            KlientIn klientInSelectedFromSearch = super.transferModel.getKlientInSearch();

            // distinguish between edit and new klientIn
            if (klientInSelectedFromSearch != null) {
                super.pnlKlientinZusaetlicheDokumente.setVisible(true);
                // load zusaetzliche dokumente
                super.lboxKlientinZusaetlicheDokumente.setModel(new KlientInZusaetzlicheDokumenteSearchListboxModel(
                        super.klientInZusaetzlicheDokumenteBusiness.findAllByKlientInId(klientInSelectedFromSearch.getId())));
                try {
                    List<KlientIn> getGeschwisterList = super.klientInBusiness.getKlientInGeschwister(klientInSelectedFromSearch);

                    KlientIn zwilling = getGeschwisterList.get(0);
                    this.zwillingSelected = zwilling;

                    KlientIn drilling = getGeschwisterList.get(1);
                    this.drillingSelected = drilling;

                    super.cbbVerrechnungsnummer.setValue(super.verrechnungRepo.findByKlienInId(klientInSelectedFromSearch.getVerrechnungId()).getBezeichnung());
                    super.cbbVerrechnungsnummerZwilling.setValue(super.verrechnungRepo.findByKlienInId(zwilling.getVerrechnungId()).getBezeichnung());
                    super.cbbVerrechnungsnummerDrilling.setValue(super.verrechnungRepo.findByKlienInId(drilling.getVerrechnungId()).getBezeichnung());


                    mapToFrontend(klientInSelectedFromSearch,
                            zwilling, // Zwilling
                            drilling, // Drilling
                            super.finanzierungBusiness.getData(klientInSelectedFromSearch.getId()),
                            super.betreuungsstundenBusiness.getDataBy(klientInSelectedFromSearch.getId()),
                            super.betreuungsstundenBusiness.getDataBy(zwilling.getId()),
                            super.betreuungsstundenBusiness.getDataBy(drilling.getId()));

                } catch (MokiBusinessException e) {
                    // clear stored klientIn object
                    super.transferModel.clearKlientInSearch();

                    logger.error("", e);
                    Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
                }
            }
        }
    }

    @Listen("onClick=#btnSave")
    public void onSave() {

        try {
            logger.info("Ausgewaehlte KlientIn wird gespeichert");

            KlientIn klientInSelectedFromSearch = super.transferModel.getKlientInSearch();

            // check if the klientin object was set to update or new created
            if (klientInSelectedFromSearch != null) {
                FK fk = new FK();

                KlientIn klientInUpdated = mapToDomainmodelKlientIn();
                klientInUpdated.setId(klientInSelectedFromSearch.getId());
                klientInUpdated.setTaetigkeitsfeld(klientInSelectedFromSearch.getTaetigkeitsfeld());
                fk.setKlientIn(klientInUpdated);

                // rewire origin zwilling id to new object
                KlientIn zwilling = mapToDomainmodelZwilling();
                zwilling.setId(this.zwillingSelected.getId());
                zwilling.setTaetigkeitsfeld(klientInSelectedFromSearch.getTaetigkeitsfeld());
                fk.setZwilling(zwilling);

                // rewire origin drilling id to new object
                KlientIn drilling = mapToDomainmodelDrilling();
                drilling.setId(this.drillingSelected.getId());
                drilling.setTaetigkeitsfeld(klientInSelectedFromSearch.getTaetigkeitsfeld());
                fk.setDrilling(drilling);

                fk.setBetreuungsstundenKlientIn(mapToDomainmodelBetreuungsstundenKlientIn());
                fk.setBetreuungsstundenZwilling(mapToDomainmodelBetreuungsstundenZwilling());
                fk.setBetreuungsstundenDrilling(mapToDomainmodelBetreuungsstundenDrilling());
                fk.setFinanzierungList(mapToDomainmodelFinanzierung());

                //update the data only
                super.klientInBusiness.updateWithGeschwister(fk);
            } else { // create a new entry if the data is coming from excel or new from frontend
                String klientInTaetigkeitsfeld = super.transferModel.getKlientInTaetigkeitsfeld();

                FK fk = new FK();
                fk.setKlientIn(mapToDomainmodelKlientIn());
                fk.setZwilling(mapToDomainmodelZwilling());
                fk.setDrilling(mapToDomainmodelDrilling());
                fk.setBetreuungsstundenKlientIn(mapToDomainmodelBetreuungsstundenKlientIn());
                fk.setBetreuungsstundenZwilling(mapToDomainmodelBetreuungsstundenZwilling());
                fk.setBetreuungsstundenDrilling(mapToDomainmodelBetreuungsstundenDrilling());
                fk.setFinanzierungList(mapToDomainmodelFinanzierung());

                // persist a new klientin object
                super.klientInBusiness.saveWithGeschwister(fk);
            }

            clearTransferModel();
            Messagebox.show(Labels.getLabel("newentry.display.info.savecomplete"), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);
        } catch (MokiBusinessException e) {
            // clear stored klientIn object
            super.transferModel.clearKlientInSearch();

            logger.error("", e);
            Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }

    private KlientIn mapToDomainmodelKlientIn() {

        return KlientInMapper.mapToDomainmodelKlientIn(transferModel.getKlientInTaetigkeitsfeld(),
                txtKlientenNummer, cbbVerrechnungsnummer, intVerrechnungsnummer, intTaetigkeitsfeldNummer, txtSpende, intWerkvertragsNummer, txtKlientVorname,
                txtKlientNachname, rWeiblich, rMaennlich, txtImpacct, intPflegestufe, intSozialversicherungsnummer, dbGebdatum,
                txtSvtrager, txtDiagnose, txtDiagnoseschluessel, dbSterbedatum, cbMutterVertreterin, txtMutterName,
                dbGebdatumMutter, txtTelefonMutter, cbVaterVertreter, txtVaterName, dbGebdatumVater, txtTelefonVater,
                cbSonstigeVertreter, txtSonstigeDateneltern, dbGebdatumSonstige, txtTelefonSonstige, txtStrassePlzOrtMutter,
                txtEmailmutter, txtStrassePlzOrtVater, txtEmailvater, rHauptwohnsitzJa, txtKh, txtKjh,
                txtSonstigeZuweisendeStellen, txtKinderfacharzt, txtTelefonKinderfacharzt, txtBetreuendeDGKP, txtWeitereDGKP,
                dbBetreuungsbeginn, dbBetreuungsende, txtKmprohb);
    }

    private KlientIn mapToDomainmodelZwilling() {

        return KlientInMapper.mapToDomainmodelKlientIn(transferModel.getKlientInTaetigkeitsfeld(),
                txtKlientenNummerZwilling, cbbVerrechnungsnummerZwilling, intVerrechnungsnummerZwilling, intTaetigkeitsfeldNummerZwilling, txtSpendeZwilling, intWerkvertragsNummerZwilling, txtKlientVornameZwilling,
                txtKlientNachnameZwilling, rWeiblichZwilling, rMaennlichZwilling, txtImpacctZwilling, intPflegestufeZwilling, intSozialversicherungsnummerZwilling, dbGebdatumZwilling,
                txtSvtragerZwilling, txtDiagnoseZwilling, txtDiagnoseschluesselZwilling, dbSterbedatumZwilling, cbMutterVertreterin, txtMutterName,
                dbGebdatumMutter, txtTelefonMutter, cbVaterVertreter, txtVaterName, dbGebdatumVater, txtTelefonVater,
                cbSonstigeVertreter, txtSonstigeDateneltern, dbGebdatumSonstige, txtTelefonSonstige, txtStrassePlzOrtMutter,
                txtEmailmutter, txtStrassePlzOrtVater, txtEmailvater, rHauptwohnsitzJa, txtKh, txtKjh,
                txtSonstigeZuweisendeStellen, txtKinderfacharzt, txtTelefonKinderfacharzt, txtBetreuendeDGKP, txtWeitereDGKP,
                dbBetreuungsbeginn, dbBetreuungsende, txtKmprohb);
    }

    private KlientIn mapToDomainmodelDrilling() {

        return KlientInMapper.mapToDomainmodelKlientIn(transferModel.getKlientInTaetigkeitsfeld(),
                txtKlientenNummerDrilling, cbbVerrechnungsnummerDrilling, intVerrechnungsnummerDrilling, intTaetigkeitsfeldNummerDrilling, txtSpendeDrilling, intWerkvertragsNummerDrilling, txtKlientVornameDrilling,
                txtKlientNachnameDrilling, rWeiblichDrilling, rMaennlichDrilling, txtImpacctDrilling, intPflegestufeDrilling, intSozialversicherungsnummerDrilling, dbGebdatumDrilling,
                txtSvtragerDrilling, txtDiagnoseDrilling, txtDiagnoseschluesselDrilling, dbSterbedatumDrilling, cbMutterVertreterin, txtMutterName,
                dbGebdatumMutter, txtTelefonMutter, cbVaterVertreter, txtVaterName, dbGebdatumVater, txtTelefonVater,
                cbSonstigeVertreter, txtSonstigeDateneltern, dbGebdatumSonstige, txtTelefonSonstige, txtStrassePlzOrtMutter,
                txtEmailmutter, txtStrassePlzOrtVater, txtEmailvater, rHauptwohnsitzJa, txtKh, txtKjh,
                txtSonstigeZuweisendeStellen, txtKinderfacharzt, txtTelefonKinderfacharzt, txtBetreuendeDGKP, txtWeitereDGKP,
                dbBetreuungsbeginn, dbBetreuungsende, txtKmprohb);
    }

    private List<Betreuungsstunden> mapToDomainmodelBetreuungsstundenKlientIn() {

        return KlientInMapper.mapBetreuungsstundenMap(
                txtBetreuungsstundenNow,
                txtBetreuungsstundenPlusOne,
                txtBetreuungsstundenPlusTwo);
    }

    private List<Betreuungsstunden> mapToDomainmodelBetreuungsstundenZwilling() {

        return KlientInMapper.mapBetreuungsstundenMap(
                txtBetreuungsstundenNowZwilling,
                txtBetreuungsstundenPlusOneZwilling,
                txtBetreuungsstundenPlusTwoZwilling);
    }

    private List<Betreuungsstunden> mapToDomainmodelBetreuungsstundenDrilling() {

        return KlientInMapper.mapBetreuungsstundenMap(
                txtBetreuungsstundenNowDrilling,
                txtBetreuungsstundenPlusOneDrilling,
                txtBetreuungsstundenPlusTwoDrilling);
    }

    private List<Finanzierung> mapToDomainmodelFinanzierung() {

        return KlientInMapper.mapToDomainmodelFinanzierung(
                getInstitutions(),
                cbLandKosten, cbLandSelbstbehalt,
                cbMukiKosten, cbMukiSelbstbehalt,
                cbKinderJugendhilfeKosten, cbKinderJugendhilfeSelbstbehalt,
                cbVersicherungKosten, cbVersicherungSelbstbehalt,
                cbHospizKosten, cbHospizSelbstbehalt,
                cbSpendenKosten, cbSpendenSelbstbehalt,
                cbSozialversicherungKosten, cbSozialversicherungSelbstbehalt,
                cbPrivatKosten, cbPrivatSelbstbehalt,
                cbKibKosten, cbKibSelbstbehalt,
                cbAndereKosten, cbAndereSelbstbehalt);
    }

    private void mapToFrontend(KlientIn klientIn, KlientIn zwilling, KlientIn drilling,
                               List<Finanzierung> finanzierungList,
                               List<Betreuungsstunden> betreuungsstundenKlientIn,
                               List<Betreuungsstunden> betreuungsstundenZwilling,
                               List<Betreuungsstunden> betreuungsstundenDrilling) {
        try {
            /* ************************************** Map KlientIn ************************************** */

            KlientInMapper.mapToFrontendKlientIn(klientIn, txtKlientenNummer, intVerrechnungsnummer, intTaetigkeitsfeldNummer,
                    txtSpende, intWerkvertragsNummer, txtKlientVorname,
                    txtKlientNachname, rWeiblich, rMaennlich, txtImpacct, intPflegestufe, intSozialversicherungsnummer, dbGebdatum,
                    txtSvtrager, txtDiagnose, txtDiagnoseschluessel, dbSterbedatum, cbMutterVertreterin, txtMutterName,
                    dbGebdatumMutter, txtTelefonMutter, cbVaterVertreter, txtVaterName, dbGebdatumVater, txtTelefonVater,
                    cbSonstigeVertreter, txtSonstigeDateneltern, dbGebdatumSonstige, txtTelefonSonstige, txtStrassePlzOrtMutter,
                    txtEmailmutter, txtStrassePlzOrtVater, txtEmailvater, rHauptwohnsitzJa, rHauptwohnsitzNein, txtKh, txtKjh,
                    txtSonstigeZuweisendeStellen, txtKinderfacharzt, txtTelefonKinderfacharzt, txtBetreuendeDGKP, txtWeitereDGKP,
                    dbBetreuungsbeginn, dbBetreuungsende, txtKmprohb);

            setBetreuungsstunden(betreuungsstundenKlientIn,
                    super.txtBetreuungsstundenNow,
                    super.txtBetreuungsstundenPlusOne,
                    super.txtBetreuungsstundenPlusTwo,
                    super.lblStundenGesamtValue);

            setFinanzierungen(finanzierungList);

            /* ************************************** Map Zwilling ************************************** */

            KlientInMapper.mapToFrontendVerwandte(zwilling, txtKlientenNummerZwilling, intTaetigkeitsfeldNummerZwilling,
                    intVerrechnungsnummerZwilling, txtSpendeZwilling, intWerkvertragsNummerZwilling, txtKlientVornameZwilling,
                    txtKlientNachnameZwilling, rWeiblichZwilling, rMaennlichZwilling, txtImpacctZwilling,
                    intPflegestufeZwilling, intSozialversicherungsnummerZwilling, dbGebdatumZwilling, txtSvtragerZwilling,
                    txtDiagnoseZwilling, txtDiagnoseschluesselZwilling, dbSterbedatumZwilling);

            setBetreuungsstunden(betreuungsstundenZwilling,
                    super.txtBetreuungsstundenNowZwilling,
                    super.txtBetreuungsstundenPlusOneZwilling,
                    super.txtBetreuungsstundenPlusTwoZwilling,
                    super.lblStundenGesamtZwillingValue);

            /* ************************************** Map Drilling ************************************** */

            KlientInMapper.mapToFrontendVerwandte(drilling, txtKlientenNummerDrilling, intTaetigkeitsfeldNummerDrilling,
                    intVerrechnungsnummerDrilling, txtSpendeDrilling, intWerkvertragsNummerDrilling, txtKlientVornameDrilling,
                    txtKlientNachnameDrilling, rWeiblichDrilling, rMaennlichDrilling, txtImpacctDrilling,
                    intPflegestufeDrilling, intSozialversicherungsnummerDrilling, dbGebdatumDrilling, txtSvtragerDrilling,
                    txtDiagnoseDrilling, txtDiagnoseschluesselDrilling, dbSterbedatumDrilling);

            setBetreuungsstunden(betreuungsstundenDrilling,
                    super.txtBetreuungsstundenNowDrilling,
                    super.txtBetreuungsstundenPlusOneDrilling,
                    super.txtBetreuungsstundenPlusTwoDrilling,
                    super.lblStundenGesamtDrillingValue);
        } catch (WrongValueException e) {
            Messagebox.show(e.getComponent().getId() + " " + e.getMessage(), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);
        }
    }
}
