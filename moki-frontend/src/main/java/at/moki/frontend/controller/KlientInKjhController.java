package at.moki.frontend.controller;

import at.moki.domainmodel.betreuungsstunden.Betreuungsstunden;
import at.moki.domainmodel.finanzierung.Finanzierung;
import at.moki.domainmodel.klientin.KJH;
import at.moki.domainmodel.klientin.KlientIn;
import at.moki.domainmodel.klientin.KlientInExcelContainerModel;
import at.moki.exception.MokiBusinessException;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.frontend.model.KlientInZusaetzlicheDokumenteSearchListboxModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Messagebox;

import java.util.List;

/**
 * Created by Green Arrow on 15.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class KlientInKjhController extends AbsKlientInController {

    private static final long serialVersionUID = 3121805679626951540L;

    private static Logger logger = LoggerFactory.getLogger(KlientInKjhController.class);

    private KlientIn zweitesKindSelected;

    private KlientIn drittesKindSelected;

    private KlientIn viertesKindSelected;

    @Override
    void performAfterCompose() {

        super.lblTaetigkeitsfeld.setValue("KJH" + "-NR");
        super.lblTaetigkeitsfeldZweitesKind.setValue("KJH" + "-NR");
        super.lblTaetigkeitsfeldDrittesKind.setValue("KJH" + "-NR");
        super.lblTaetigkeitsfeldViertesKind.setValue("KJH" + "-NR");

        // calculate dates
        KlientInMapper.calculateBetreuungsstundenDates(super.lblNow, super.lblPlusOne, super.lblPlusTwo);
        KlientInMapper.calculateBetreuungsstundenDates(super.lblNowZweitesKind, super.lblPlusOneZweitesKind, super.lblPlusTwoZweitesKind);
        KlientInMapper.calculateBetreuungsstundenDates(super.lblNowDrittesKind, super.lblPlusOneDrittesKind, super.lblPlusTwoDrittesKind);
        KlientInMapper.calculateBetreuungsstundenDates(super.lblNowViertesKind, super.lblPlusOneViertesKind, super.lblPlusTwoViertesKind);

        super.initVerrechnungCombobox(super.cbbVerrechnungsnummer);
        super.initVerrechnungCombobox(super.cbbVerrechnungsnummerZweitesKind);
        super.initVerrechnungCombobox(super.cbbVerrechnungsnummerDrittesKind);
        super.initVerrechnungCombobox(super.cbbVerrechnungsnummerViertesKind);

        // excel importer was used
        if (super.transferModel.getKlientInExcelContainerModelList() != null && !super.transferModel.getKlientInExcelContainerModelList().isEmpty()) {
            List<KlientInExcelContainerModel> klientInExcelContainerModel = super.transferModel.getKlientInExcelContainerModelList();

            mapToFrontend(
                    klientInExcelContainerModel.get(0).getKlientIn(),               // KlientIn
                    klientInExcelContainerModel.get(1).getKlientIn(),               // zweites Kind
                    klientInExcelContainerModel.get(2).getKlientIn(),               // drittes Kind
                    klientInExcelContainerModel.get(3).getKlientIn(),               // viertes Kind
                    klientInExcelContainerModel.get(0).getFinanzierungList(),       // Finanzierung from KlientIn
                    klientInExcelContainerModel.get(0).getBetreuungsstundenList(),  // Betreuungsstunden KlientIn
                    klientInExcelContainerModel.get(1).getBetreuungsstundenList(),  // Betreuungsstunden Zweites Kind
                    klientInExcelContainerModel.get(2).getBetreuungsstundenList(),  // Betreuungsstunden Drittes Kind
                    klientInExcelContainerModel.get(3).getBetreuungsstundenList()); // Betreuungsstunden Viertes Kind

        } else { // klientin selected from search
            KlientIn klientInSelectedFromSearch = super.transferModel.getKlientInSearch();

            // distinguish between edit and new klientIn
            if (klientInSelectedFromSearch != null) {
                super.pnlKlientinZusaetlicheDokumente.setVisible(true);
                // load zusaetzliche dokumente
                super.lboxKlientinZusaetlicheDokumente.setModel(new KlientInZusaetzlicheDokumenteSearchListboxModel(
                        super.klientInZusaetzlicheDokumenteBusiness.findAllByKlientInId(klientInSelectedFromSearch.getId())));
                try {
                    List<KlientIn> getVerwandteList = super.klientInBusiness.getKlientInWithKinder(klientInSelectedFromSearch);

                    KlientIn zweitesKind = getVerwandteList.get(0);
                    this.zweitesKindSelected = zweitesKind;

                    KlientIn drittesKind = getVerwandteList.get(1);
                    this.drittesKindSelected = drittesKind;

                    KlientIn viertesKind = getVerwandteList.get(2);
                    this.viertesKindSelected = viertesKind;

                    super.cbbVerrechnungsnummer.setValue(super.verrechnungRepo.findByKlienInId(klientInSelectedFromSearch.getVerrechnungId()).getBezeichnung());
                    super.cbbVerrechnungsnummerZweitesKind.setValue(super.verrechnungRepo.findByKlienInId(zweitesKind.getVerrechnungId()).getBezeichnung());
                    super.cbbVerrechnungsnummerDrittesKind.setValue(super.verrechnungRepo.findByKlienInId(drittesKind.getVerrechnungId()).getBezeichnung());
                    super.cbbVerrechnungsnummerViertesKind.setValue(super.verrechnungRepo.findByKlienInId(viertesKind.getVerrechnungId()).getBezeichnung());


                    mapToFrontend(klientInSelectedFromSearch,
                            zweitesKind, // zweites Kind
                            drittesKind, // drittes Kind
                            viertesKind, // viertes Kind
                            super.finanzierungBusiness.getData(klientInSelectedFromSearch.getId()),
                            super.betreuungsstundenBusiness.getDataBy(klientInSelectedFromSearch.getId()),
                            super.betreuungsstundenBusiness.getDataBy(zweitesKind.getId()),
                            super.betreuungsstundenBusiness.getDataBy(drittesKind.getId()),
                            super.betreuungsstundenBusiness.getDataBy(viertesKind.getId()));

                } catch (MokiBusinessException e) {
                    // clear stored klientIn object
                    super.transferModel.clearKlientInSearch();

                    logger.error("", e);
                    Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
                }
            }
        }
    }

    @Listen("onClick=#btnSave")
    public void onSave() {

        try {
            logger.info("Ausgewaehlte KlientIn wird gespeichert");

            KlientIn klientInSelectedFromSearch = super.transferModel.getKlientInSearch();

            // check if the klientin object was set to update or new created
            if (klientInSelectedFromSearch != null) {

                KJH kjh = new KJH();

                KlientIn klientInUpdated = mapToDomainmodelKlientIn();
                klientInUpdated.setId(klientInSelectedFromSearch.getId());
                klientInUpdated.setTaetigkeitsfeld(klientInSelectedFromSearch.getTaetigkeitsfeld());
                kjh.setKlientIn(klientInUpdated);

                // rewire origin zweitesKindSelected id to new object
                KlientIn zweitesKind = mapToDomainmodelZweitesKind();
                zweitesKind.setId(this.zweitesKindSelected.getId());
                zweitesKind.setTaetigkeitsfeld(klientInSelectedFromSearch.getTaetigkeitsfeld());
                kjh.setZweitesKind(zweitesKind);

                // rewire origin drittesKindSelected id to new object
                KlientIn drittesKind = mapToDomainmodelDrittesKind();
                drittesKind.setId(this.drittesKindSelected.getId());
                drittesKind.setTaetigkeitsfeld(klientInSelectedFromSearch.getTaetigkeitsfeld());
                kjh.setDrittesKind(drittesKind);

                // rewire origin viertesKindSelected id to new object
                KlientIn viertesKind = mapToDomainmodelViertesKind();
                viertesKind.setId(this.viertesKindSelected.getId());
                viertesKind.setTaetigkeitsfeld(klientInSelectedFromSearch.getTaetigkeitsfeld());
                kjh.setViertesKind(viertesKind);

                kjh.setBetreuungsstundenKlientIn(mapToDomainmodelBetreuungsstundenKlientIn());
                kjh.setBetreuungsstundenZweitesKind(mapToDomainmodelBetreuungsstundenZweitesKind());
                kjh.setBetreuungsstundenDrittesKind(mapToDomainmodelBetreuungsstundenDrittesKind());
                kjh.setBetreuungsstundenViertesKind(mapToDomainmodelBetreuungsstundenViertesKind());
                kjh.setFinanzierungList(mapToDomainmodelFinanzierung());

                //update the data only
                super.klientInBusiness.updateWithKinder(kjh);
            } else { // create a new entry if the data is coming from excel or new from frontend
                String klientInTaetigkeitsfeld = super.transferModel.getKlientInTaetigkeitsfeld();

                KJH kjh = new KJH();

                KlientIn klientInNew = mapToDomainmodelKlientIn();
                klientInNew.setTaetigkeitsfeld(klientInTaetigkeitsfeld);
                kjh.setKlientIn(klientInNew);

                KlientIn zweitesKind = mapToDomainmodelZweitesKind();
                zweitesKind.setTaetigkeitsfeld(klientInTaetigkeitsfeld);
                kjh.setZweitesKind(zweitesKind);

                KlientIn drittesKind = mapToDomainmodelDrittesKind();
                drittesKind.setTaetigkeitsfeld(klientInTaetigkeitsfeld);
                kjh.setDrittesKind(drittesKind);

                KlientIn viertesKind = mapToDomainmodelViertesKind();
                viertesKind.setTaetigkeitsfeld(klientInTaetigkeitsfeld);
                kjh.setViertesKind(viertesKind);

                kjh.setBetreuungsstundenKlientIn(mapToDomainmodelBetreuungsstundenKlientIn());
                kjh.setBetreuungsstundenZweitesKind(mapToDomainmodelBetreuungsstundenZweitesKind());
                kjh.setBetreuungsstundenDrittesKind(mapToDomainmodelBetreuungsstundenDrittesKind());
                kjh.setBetreuungsstundenViertesKind(mapToDomainmodelBetreuungsstundenViertesKind());
                kjh.setFinanzierungList(mapToDomainmodelFinanzierung());

                // persist a new klientin object
                super.klientInBusiness.saveWithKinder(kjh);
            }

            clearTransferModel();
            Messagebox.show(Labels.getLabel("newentry.display.info.savecomplete"), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);
        } catch (MokiBusinessException e) {
            // clear stored klientIn object
            super.transferModel.clearKlientInSearch();

            logger.error("", e);
            Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }

    private KlientIn mapToDomainmodelKlientIn() {

        return KlientInMapper.mapToDomainmodelKlientIn(transferModel.getKlientInTaetigkeitsfeld(),
                txtKlientenNummer, cbbVerrechnungsnummer, intVerrechnungsnummer, intTaetigkeitsfeldNummer, txtSpende, intWerkvertragsNummer, txtKlientVorname,
                txtKlientNachname, rWeiblich, rMaennlich, txtImpacct, intPflegestufe, intSozialversicherungsnummer, dbGebdatum,
                txtSvtrager, txtDiagnose, txtDiagnoseschluessel, dbSterbedatum, cbMutterVertreterin, txtMutterName,
                dbGebdatumMutter, txtTelefonMutter, cbVaterVertreter, txtVaterName, dbGebdatumVater, txtTelefonVater,
                cbSonstigeVertreter, txtSonstigeDateneltern, dbGebdatumSonstige, txtTelefonSonstige, txtStrassePlzOrtMutter,
                txtEmailmutter, txtStrassePlzOrtVater, txtEmailvater, rHauptwohnsitzJa, txtKh, txtKjh,
                txtSonstigeZuweisendeStellen, txtKinderfacharzt, txtTelefonKinderfacharzt, txtBetreuendeDGKP, txtWeitereDGKP,
                dbBetreuungsbeginn, dbBetreuungsende, txtKmprohb);
    }

    private KlientIn mapToDomainmodelZweitesKind() {

        return KlientInMapper.mapToDomainmodelKlientIn(transferModel.getKlientInTaetigkeitsfeld(),
                txtKlientenNummerZweitesKind, cbbVerrechnungsnummerZweitesKind, intVerrechnungsnummerZweitesKind, intTaetigkeitsfeldNummerZweitesKind, txtSpendeZweitesKind, intWerkvertragsNummerZweitesKind, txtKlientVornameZweitesKind,
                txtKlientNachnameZweitesKind, rWeiblichZweitesKind, rMaennlichZweitesKind, txtImpacctZweitesKind, intPflegestufeZweitesKind, intSozialversicherungsnummerZweitesKind, dbGebdatumZweitesKind,
                txtSvtragerZweitesKind, txtDiagnoseZweitesKind, txtDiagnoseschluesselZweitesKind, dbSterbedatumZweitesKind, cbMutterVertreterin, txtMutterName,
                dbGebdatumMutter, txtTelefonMutter, cbVaterVertreter, txtVaterName, dbGebdatumVater, txtTelefonVater,
                cbSonstigeVertreter, txtSonstigeDateneltern, dbGebdatumSonstige, txtTelefonSonstige, txtStrassePlzOrtMutter,
                txtEmailmutter, txtStrassePlzOrtVater, txtEmailvater, rHauptwohnsitzJa, txtKh, txtKjh,
                txtSonstigeZuweisendeStellen, txtKinderfacharzt, txtTelefonKinderfacharzt, txtBetreuendeDGKP, txtWeitereDGKP,
                dbBetreuungsbeginn, dbBetreuungsende, txtKmprohb);
    }

    private KlientIn mapToDomainmodelDrittesKind() {

        return KlientInMapper.mapToDomainmodelKlientIn(transferModel.getKlientInTaetigkeitsfeld(),
                txtKlientenNummerDrittesKind, cbbVerrechnungsnummerDrittesKind, intVerrechnungsnummerDrittesKind, intTaetigkeitsfeldNummerDrittesKind, txtSpendeDrittesKind, intWerkvertragsNummerDrittesKind, txtKlientVornameDrittesKind,
                txtKlientNachnameDrittesKind, rWeiblichDrittesKind, rMaennlichDrittesKind, txtImpacctDrittesKind, intPflegestufeDrittesKind, intSozialversicherungsnummerDrittesKind, dbGebdatumDrittesKind,
                txtSvtragerDrittesKind, txtDiagnoseDrittesKind, txtDiagnoseschluesselDrittesKind, dbSterbedatumDrittesKind, cbMutterVertreterin, txtMutterName,
                dbGebdatumMutter, txtTelefonMutter, cbVaterVertreter, txtVaterName, dbGebdatumVater, txtTelefonVater,
                cbSonstigeVertreter, txtSonstigeDateneltern, dbGebdatumSonstige, txtTelefonSonstige, txtStrassePlzOrtMutter,
                txtEmailmutter, txtStrassePlzOrtVater, txtEmailvater, rHauptwohnsitzJa, txtKh, txtKjh,
                txtSonstigeZuweisendeStellen, txtKinderfacharzt, txtTelefonKinderfacharzt, txtBetreuendeDGKP, txtWeitereDGKP,
                dbBetreuungsbeginn, dbBetreuungsende, txtKmprohb);
    }

    private KlientIn mapToDomainmodelViertesKind() {

        return KlientInMapper.mapToDomainmodelKlientIn(transferModel.getKlientInTaetigkeitsfeld(),
                txtKlientenNummerViertesKind, cbbVerrechnungsnummerViertesKind, intVerrechnungsnummerViertesKind, intTaetigkeitsfeldNummerViertesKind, txtSpendeViertesKind, intWerkvertragsNummerViertesKind, txtKlientVornameViertesKind,
                txtKlientNachnameViertesKind, rWeiblichViertesKind, rMaennlichViertesKind, txtImpacctViertesKind, intPflegestufeViertesKind, intSozialversicherungsnummerViertesKind, dbGebdatumViertesKind,
                txtSvtragerViertesKind, txtDiagnoseViertesKind, txtDiagnoseschluesselViertesKind, dbSterbedatumViertesKind, cbMutterVertreterin, txtMutterName,
                dbGebdatumMutter, txtTelefonMutter, cbVaterVertreter, txtVaterName, dbGebdatumVater, txtTelefonVater,
                cbSonstigeVertreter, txtSonstigeDateneltern, dbGebdatumSonstige, txtTelefonSonstige, txtStrassePlzOrtMutter,
                txtEmailmutter, txtStrassePlzOrtVater, txtEmailvater, rHauptwohnsitzJa, txtKh, txtKjh,
                txtSonstigeZuweisendeStellen, txtKinderfacharzt, txtTelefonKinderfacharzt, txtBetreuendeDGKP, txtWeitereDGKP,
                dbBetreuungsbeginn, dbBetreuungsende, txtKmprohb);
    }

    private List<Betreuungsstunden> mapToDomainmodelBetreuungsstundenKlientIn() {

        return KlientInMapper.mapBetreuungsstundenMap(
                txtBetreuungsstundenNow,
                txtBetreuungsstundenPlusOne,
                txtBetreuungsstundenPlusTwo);
    }

    private List<Betreuungsstunden> mapToDomainmodelBetreuungsstundenZweitesKind() {

        return KlientInMapper.mapBetreuungsstundenMap(
                txtBetreuungsstundenNowZweitesKind,
                txtBetreuungsstundenPlusOneZweitesKind,
                txtBetreuungsstundenPlusTwoZweitesKind);
    }

    private List<Betreuungsstunden> mapToDomainmodelBetreuungsstundenDrittesKind() {

        return KlientInMapper.mapBetreuungsstundenMap(
                txtBetreuungsstundenNowDrittesKind,
                txtBetreuungsstundenPlusOneDrittesKind,
                txtBetreuungsstundenPlusTwoDrittesKind);
    }

    private List<Betreuungsstunden> mapToDomainmodelBetreuungsstundenViertesKind() {

        return KlientInMapper.mapBetreuungsstundenMap(
                txtBetreuungsstundenNowViertesKind,
                txtBetreuungsstundenPlusOneViertesKind,
                txtBetreuungsstundenPlusTwoViertesKind);
    }

    private List<Finanzierung> mapToDomainmodelFinanzierung() {

        return KlientInMapper.mapToDomainmodelFinanzierung(
                getInstitutions(),
                cbLandKosten, cbLandSelbstbehalt,
                cbMukiKosten, cbMukiSelbstbehalt,
                cbKinderJugendhilfeKosten, cbKinderJugendhilfeSelbstbehalt,
                cbVersicherungKosten, cbVersicherungSelbstbehalt,
                cbHospizKosten, cbHospizSelbstbehalt,
                cbSpendenKosten, cbSpendenSelbstbehalt,
                cbSozialversicherungKosten, cbSozialversicherungSelbstbehalt,
                cbPrivatKosten, cbPrivatSelbstbehalt,
                cbKibKosten, cbKibSelbstbehalt,
                cbAndereKosten, cbAndereSelbstbehalt);
    }

    private void mapToFrontend(KlientIn klientIn, KlientIn zweitesKind, KlientIn drittesKind, KlientIn viertesKind,
                               List<Finanzierung> finanzierungList,
                               List<Betreuungsstunden> betreuungsstundenKlientIn,
                               List<Betreuungsstunden> betreuungsstundenZweitesKind,
                               List<Betreuungsstunden> betreuungsstundenDrittesKind,
                               List<Betreuungsstunden> betreuungsstundenViertesKind) {
        try {
            /* ************************************** Map KlientIn ************************************** */

            KlientInMapper.mapToFrontendKlientIn(klientIn, txtKlientenNummer, intVerrechnungsnummer, intTaetigkeitsfeldNummer,
                    txtSpende, intWerkvertragsNummer, txtKlientVorname,
                    txtKlientNachname, rWeiblich, rMaennlich, txtImpacct, intPflegestufe, intSozialversicherungsnummer, dbGebdatum,
                    txtSvtrager, txtDiagnose, txtDiagnoseschluessel, dbSterbedatum, cbMutterVertreterin, txtMutterName,
                    dbGebdatumMutter, txtTelefonMutter, cbVaterVertreter, txtVaterName, dbGebdatumVater, txtTelefonVater,
                    cbSonstigeVertreter, txtSonstigeDateneltern, dbGebdatumSonstige, txtTelefonSonstige, txtStrassePlzOrtMutter,
                    txtEmailmutter, txtStrassePlzOrtVater, txtEmailvater, rHauptwohnsitzJa, rHauptwohnsitzNein, txtKh, txtKjh,
                    txtSonstigeZuweisendeStellen, txtKinderfacharzt, txtTelefonKinderfacharzt, txtBetreuendeDGKP, txtWeitereDGKP,
                    dbBetreuungsbeginn, dbBetreuungsende, txtKmprohb);

            setBetreuungsstunden(betreuungsstundenKlientIn,
                    super.txtBetreuungsstundenNow,
                    super.txtBetreuungsstundenPlusOne,
                    super.txtBetreuungsstundenPlusTwo,
                    super.lblStundenGesamtValue);

            setFinanzierungen(finanzierungList);

            /* ************************************** Map Zweites Kind ************************************** */

            KlientInMapper.mapToFrontendVerwandte(zweitesKind, txtKlientenNummerZweitesKind, intTaetigkeitsfeldNummerZweitesKind,
                    intVerrechnungsnummerZweitesKind, txtSpendeZweitesKind, intWerkvertragsNummerZweitesKind, txtKlientVornameZweitesKind,
                    txtKlientNachnameZweitesKind, rWeiblichZweitesKind, rMaennlichZweitesKind, txtImpacctZweitesKind,
                    intPflegestufeZweitesKind, intSozialversicherungsnummerZweitesKind, dbGebdatumZweitesKind, txtSvtragerZweitesKind,
                    txtDiagnoseZweitesKind, txtDiagnoseschluesselZweitesKind, dbSterbedatumZweitesKind);

            setBetreuungsstunden(betreuungsstundenZweitesKind,
                    super.txtBetreuungsstundenNowZweitesKind,
                    super.txtBetreuungsstundenPlusOneZweitesKind,
                    super.txtBetreuungsstundenPlusTwoZweitesKind,
                    super.lblStundenGesamtZweitesKindValue);

            /* ************************************** Map Drittes Kind ************************************** */

            KlientInMapper.mapToFrontendVerwandte(drittesKind, txtKlientenNummerDrittesKind, intTaetigkeitsfeldNummerDrittesKind,
                    intVerrechnungsnummerDrittesKind, txtSpendeDrittesKind, intWerkvertragsNummerDrittesKind, txtKlientVornameDrittesKind,
                    txtKlientNachnameDrittesKind, rWeiblichDrittesKind, rMaennlichDrittesKind, txtImpacctDrittesKind,
                    intPflegestufeDrittesKind, intSozialversicherungsnummerDrittesKind, dbGebdatumDrittesKind, txtSvtragerDrittesKind,
                    txtDiagnoseDrittesKind, txtDiagnoseschluesselDrittesKind, dbSterbedatumDrittesKind);

            setBetreuungsstunden(betreuungsstundenDrittesKind,
                    super.txtBetreuungsstundenNowDrittesKind,
                    super.txtBetreuungsstundenPlusOneDrittesKind,
                    super.txtBetreuungsstundenPlusTwoDrittesKind,
                    super.lblStundenGesamtDrittesKindValue);

            /* ************************************** Map Viertes Kind ************************************** */

            KlientInMapper.mapToFrontendVerwandte(viertesKind, txtKlientenNummerViertesKind, intTaetigkeitsfeldNummerViertesKind,
                    intVerrechnungsnummerViertesKind, txtSpendeViertesKind, intWerkvertragsNummerViertesKind, txtKlientVornameViertesKind,
                    txtKlientNachnameViertesKind, rWeiblichViertesKind, rMaennlichViertesKind, txtImpacctViertesKind,
                    intPflegestufeViertesKind, intSozialversicherungsnummerViertesKind, dbGebdatumViertesKind, txtSvtragerViertesKind,
                    txtDiagnoseViertesKind, txtDiagnoseschluesselViertesKind, dbSterbedatumViertesKind);

            setBetreuungsstunden(betreuungsstundenViertesKind,
                    super.txtBetreuungsstundenNowViertesKind,
                    super.txtBetreuungsstundenPlusOneViertesKind,
                    super.txtBetreuungsstundenPlusTwoViertesKind,
                    super.lblStundenGesamtViertesKindValue);
        } catch (WrongValueException e) {
            Messagebox.show(e.getComponent().getId() + " " + e.getMessage(), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);
        }
    }
}
