package at.moki.frontend.controller;

import at.moki.business.user.UserBusiness;
import at.moki.domainmodel.user.MokiUser;
import at.moki.encryption.MokiEncryption;
import at.moki.exception.MokiBusinessException;
import at.moki.exception.MokiSecurityException;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.session.SessionModel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

/**
 * Created by Green Arrow on 22.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class LoginController extends SelectorComposer<Component> {

    private static final long serialVersionUID = -6560556669147155039L;

    @Wire("#btnLogin")
    private Button btnLogin;

    @Wire("#txtUsername")
    private Textbox txtUsername;

    @Wire("#txtPassword")
    private Textbox txtPassword;

    @WireVariable("userBusinessImpl")
    private UserBusiness userBusiness;

    @WireVariable("sessionModel")
    private SessionModel sessionModel;

    @WireVariable("mokiEncryption")
    private MokiEncryption mokiEncryption;

    private static Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Listen("onClick=#btnLogin")
    public void onLogin() {
        login();
    }

    @Listen("onOK=#txtUsername")
    public void onOkTxtUsername() {
        login();
    }

    @Listen("onOK=#txtPassword")
    public void onOkTxtPassword() {
        login();
    }

    private void login() {
        String username = this.txtUsername.getValue();
        String password = this.txtPassword.getValue();

        if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password)) {
            try {
                logger.info(String.format("informationen werden ueber eingeloggten mokiUser %s gelesen", username));
                MokiUser mokiUser = this.userBusiness.getUser(username, password);

                Integer sperrungCounter = mokiUser.getSperrungCounter();
                if (sperrungCounter != null) {
                    Messagebox.show(String.format(
                            "Benutzername und/oder Passwort ist falsch, Sie haben noch %s Versuche", 3 - sperrungCounter),
                            MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);
                } else {
                    Sessions.getCurrent().setAttribute("sessionUser", mokiUser);
                    Executions.sendRedirect("/content/index.zul");
                }
            } catch (MokiBusinessException e) {
                Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
            } catch (MokiSecurityException e) {
                Messagebox.show(e.getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
                logger.error(String.format("Falsches Passwort fuer %s eingegeben. IP: %s", username, Executions.getCurrent().getRemoteAddr()), e);
            }
        } else {
            throw new WrongValueException(this.txtUsername, Labels.getLabel("login.display.window.empty.input"));
        }
    }
}
