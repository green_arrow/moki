package at.moki.frontend.controller;

import at.moki.business.user.UserBusiness;
import at.moki.domainmodel.user.MokiUser;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.encryption.MokiEncryption;
import at.moki.exception.MokiBusinessException;
import at.moki.frontend.constants.MokiFrontendConstants;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Include;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

/**
 * Created by Green Arrow on 13.05.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class VerwaltungNewEntryController extends SelectorComposer<Component> {

    private static final long serialVersionUID = -8773577199927622461L;

    private static Logger logger = LoggerFactory.getLogger(VerwaltungNewEntryController.class);

    @Wire("#txtVerwaltungUsername")
    private Textbox txtVerwaltungUsername;

    @WireVariable("userBusinessImpl")
    private UserBusiness userBusiness;

    @WireVariable("mokiEncryption")
    private MokiEncryption mokiEncryption;

    @Listen("onOK=#txtVerwaltungUsername")
    public void onOKVerwaltungUsername() {
        save();
    }

    @Listen("onClick=#btnSave")
    public void onSave() {
        save();
    }

    private void save() {
        String username = this.txtVerwaltungUsername.getValue();

        // validate if input strings are empty
        if (StringUtils.isEmpty(username)) {
            throw new WrongValueException(this.txtVerwaltungUsername, Labels.getLabel("newentry.message.empty.character"));
        }

        try {
            MokiUser mokiUserVerwaltung = new MokiUser();
            mokiUserVerwaltung.setUsername(this.txtVerwaltungUsername.getValue());
            mokiUserVerwaltung.setMokiUserRole(MokiUserRole.VERWALTUNG);
            mokiUserVerwaltung.setPassword(this.mokiEncryption.generateHashPassword("s3cret",
                    RandomStringUtils.random(15, true, true)));

            this.userBusiness.save(mokiUserVerwaltung);

            Include include = (Include) Path.getComponent(MokiFrontendConstants.INCLUDE);
            include.setSrc("");// reset include
            Messagebox.show(Labels.getLabel("verwaltung.display.info.savecomplete"), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);

        } catch (MokiBusinessException e) {
            logger.error("", e);
            Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }
}
