package at.moki.frontend.controller;

import at.moki.dbrepo.klientin.TaetigkeitsfeldRepo;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.session.SessionModel;
import at.moki.session.TransferModel;
import org.apache.commons.lang3.StringUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Include;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;

/**
 * Created by Green Arrow on 22.12.2017.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class IndexController extends SelectorComposer<Component> {

    private static final long serialVersionUID = 6295479672572564990L;

    @Wire("#miPersonalData")
    private Menuitem miPersonalData;

    @Wire("#menuProfile")
    private Menu menuProfile;

    @Wire("#miUserLoginData")
    private Menuitem miUserLoginData;

    @Wire("#miSearchKrankenpflegerin")
    private Menuitem miSearchKrankenpflegerin;

    @Wire("#miEntryKrankenpflegerin")
    private Menuitem miEntryKrankenpflegerin;

    @Wire("#miImport")
    private Menuitem miImport;

    @Wire("#miUserdata")
    private Menuitem miUserdata;

    @Wire("#miEntryVerwalung")
    private Menuitem miEntryVerwalung;

    @Wire("#miEntryVerrechnung")
    private Menuitem miEntryVerrechnung;

    @Wire("#menuAdministration")
    private Menu menuAdministration;

    @Wire("#includeView")
    private Include includeView;

    @Wire("#mpEntryKlientIn")
    private Menupopup mpEntryKlientIn;

    @WireVariable("taetigkeitsfeldRepoImpl")
    private TaetigkeitsfeldRepo taetigkeitsfeldRepo;

    @WireVariable("sessionModel")
    private SessionModel sessionModel;

    @WireVariable("transferModel")
    private TransferModel transferModel;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        this.menuProfile.setLabel(this.menuProfile.getLabel() + " [" + this.sessionModel.getMokiUser().getUsername() + "]");
        // detach ZK components according to user rights
        applyUserRoleAccessRestrictions();
        initializeTaetigkeitsfelder();
    }

    /* ********************* Moki Logo ********************* */
    @Listen("onClick=#imLogo")
    public void onLogo() {
        this.transferModel.clearKlientInSearch();
        this.includeView.setSrc("");
    }

    /* ********************* MokiUser Data ********************* */
    @Listen("onClick=#miUserLoginUsername")
    public void onLoadUserLoginUsername() {
        this.transferModel.clearKlientInSearch();
        this.includeView.setSrc("/content/updateUsername.zul");
    }

    @Listen("onClick=#miUserLoginPassword")
    public void onLoadUserLoginPassword() {
        this.transferModel.clearKlientInSearch();
        this.includeView.setSrc("/content/updatePassword.zul");
    }

    @Listen("onClick=#miPersonalData")
    public void onLoadProfile() {
        this.transferModel.clearKlientInSearch();
        this.includeView.setSrc("/content/userProfiledata.zul");
    }

    /* ************** Organisationsprotokoll *************** */
    @Listen("onClick=#miOrgprot")
    public void onLoadOrganisationsprotokoll() {
        this.transferModel.clearKlientInSearch();
        this.includeView.setSrc("");
        Executions.createComponents("/content/organisationsprotokollSearch.zul", null, null);
    }

    /* ********************** Search *********************** */
    @Listen("onClick=#miSearchKrankenpflegerin")
    public void onLoadSearchKrankenpflegerin() {
        this.transferModel.clearKlientInSearch();
        this.includeView.setSrc("/content/krankenpflegerinSearch.zul");
    }

    @Listen("onClick=#miSearchKlientIn")
    public void onLoadSearchKlientIn() {
        this.transferModel.clearKlientInSearch();
        this.includeView.setSrc("");
        this.includeView.setSrc("/content/klientInSearch.zul");
    }

    /* *********************** Excel *********************** */
    @Listen("onClick=#miImport")
    public void onImportExcel() {
        this.transferModel.clearKlientInSearch();
        this.includeView.setSrc("/content/excelImport.zul");
    }

    @Listen("onClick=#miExport")
    public void onExportExcel() {
        this.transferModel.clearKlientInSearch();
        this.includeView.setSrc("/content/excelExport.zul");
    }

    /* *************** New Krankenpflegerin **************** */
    @Listen("onClick=#miEntryKrankenpflegerin")
    public void onLoadNewEntryKrankenpflegerin() {
        this.transferModel.clearKlientInSearch();
        this.includeView.setSrc("/content/krankenpflegerinNewEntry.zul");
    }

    /* ******************** Verwaltung ********************* */
    @Listen("onClick=#miEntryVerwalung")
    public void onLoadNewEntryVerwalung() {
        this.transferModel.clearKlientInSearch();
        this.includeView.setSrc("/content/verwaltungNewEntry.zul");
    }

    @Listen("onClick=#miUserdata")
    public void onLoadUserdata() {
        this.transferModel.clearKlientInSearch();
        this.includeView.setSrc("/content/administration.zul");
    }

    /* ******************* Verrechnung ********************* */
    @Listen("onClick=#miEntryVerrechnung")
    public void onLoadNewEntryVerrechnung() {
        this.transferModel.clearKlientInSearch();
        this.includeView.setSrc("/content/verrechnung.zul");
    }

    /* ***************** Taetigkeitsfeld ******************* */
    @Listen("onClick=#miTaetigkeitsfelderVerwaltung")
    public void onLoadNewEntryTaetigkietsfeld() {
        this.transferModel.clearKlientInSearch();
        this.includeView.setSrc("/content/taetigkeitsfeldVerwaltung.zul");
    }

    @Listen("onClick=#miTaetigkeitsfelderKonfiguration")
    public void onLoadTaetigkeitsfelderKonfiguration() {
        this.transferModel.clearKlientInSearch();
        this.includeView.setSrc("/content/taetigkeitsfeldKonfiguration.zul");
    }


    /* ************************ Info *********************** */

    @Listen("onClick=#miReleasenotes")
    public void onReleasenotes() {
        this.transferModel.clearKlientInSearch();
        this.includeView.setSrc("/content/releasenotes.zul");
    }

    @Listen("onClick=#miAbout")
    public void onAbout() {
        this.transferModel.clearKlientInSearch();
        this.includeView.setSrc("");
        Executions.createComponents("/content/about.zul", null, null);
    }

    /* *********************** Logout ********************** */
    @Listen("onClick=#miLogout")
    public void onLogout() {
        this.sessionModel.clearAll();
        this.transferModel.clearAll();
        Sessions.getCurrent().setAttribute("sessionUser", null);
        Executions.sendRedirect("/login.zul");
    }

    private void applyUserRoleAccessRestrictions() {
        MokiUserRole mokiUserRole = this.sessionModel.getMokiUser().getMokiUserRole();

        if (mokiUserRole == MokiUserRole.ADMINISTRATOR) {
            this.miPersonalData.detach();
        } else if (mokiUserRole == MokiUserRole.KRANKENPFLEGERIN) {
            this.menuAdministration.detach();
            this.miEntryVerwalung.detach();
            this.miEntryVerrechnung.detach();
            this.miImport.detach();
        } else if (mokiUserRole == MokiUserRole.VERWALTUNG) {
            // no functions concerning krankenpflegerin
            this.miPersonalData.detach();
            this.miUserdata.detach();
            this.miEntryVerwalung.detach();
            this.miSearchKrankenpflegerin.detach();
            this.miEntryKrankenpflegerin.detach();
        } else {
            throw new UnsupportedOperationException(String.format("es existieren keine Rechte fuer den eingeloggten MokiUser %s", this.sessionModel.getMokiUser().getUsername()));
        }
    }

    private void initializeTaetigkeitsfelder() {
        this.taetigkeitsfeldRepo.findAll().forEach(taetigkeitsfeld -> {
            Menuitem menuitem = new Menuitem();
            menuitem.setId("miEntryKlientIn" + taetigkeitsfeld.getBezeichnung());
            menuitem.setLabel(taetigkeitsfeld.getBezeichnung());
            menuitem.setParent(this.mpEntryKlientIn);
            menuitem.addEventListener("onClick", new EventListener<Event>() {
                @Override
                public void onEvent(Event event) {
                    transferModel.clearAll();
                    includeView.setSrc("");

                    Menuitem taetigkeitsfeldMenuitem = (Menuitem) event.getTarget();
                    String taetigkeitsfeld = taetigkeitsfeldMenuitem.getLabel();
                    transferModel.setKlientInTaetigkeitsfeld(taetigkeitsfeld);

                    if (StringUtils.equals("FK", taetigkeitsfeld)) {
                        includeView.setSrc("/content/klientInFk.zul");
                    } else if (StringUtils.equals("KJH", taetigkeitsfeld)) {
                        includeView.setSrc("/content/klientInKjh.zul");
                    } else {
                        includeView.setSrc("/content/klientIn.zul");
                    }
                }
            });
        });
    }
}
