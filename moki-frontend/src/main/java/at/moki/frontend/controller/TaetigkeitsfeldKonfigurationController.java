package at.moki.frontend.controller;

import at.moki.dbrepo.einstellungen.EinstellungenRepo;
import at.moki.dbrepo.excel.dateien.ExcelDateinRepo;
import at.moki.dbrepo.klientin.TaetigkeitsfeldRepo;
import at.moki.domainmodel.excel.dateien.ExcelDatei;
import at.moki.domainmodel.klientin.Taetigkeitsfeld;
import at.moki.einstellungen.Einstellungen;
import at.moki.einstellungen.EinstellungenBereich;
import at.moki.einstellungen.TaetigkeitsfeldJson;
import at.moki.frontend.constants.MokiFrontendConstants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.media.Media;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.*;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Green Arrow
 * @date 08.08.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class TaetigkeitsfeldKonfigurationController extends SelectorComposer<Component> {

    private static final long serialVersionUID = -1712249635939207925L;

    private static Logger logger = LoggerFactory.getLogger(TaetigkeitsfeldKonfigurationController.class);

    @Wire("#divTaetigkeitsfeld")
    private Div divTaetigkeitsfeld;

    @WireVariable("taetigkeitsfeldRepoImpl")
    private TaetigkeitsfeldRepo taetigkeitsfeldRepo;

    @WireVariable("einstellungenRepoImpl")
    private EinstellungenRepo einstellungenRepo;

    @WireVariable("excelDateinRepoImpl")
    private ExcelDateinRepo excelDateinRepo;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        logger.info("Lade alle Taetigkeitsfelder aus der Datenbank");

        Einstellungen einstellungen = this.einstellungenRepo.findByTaetigkeitsfeld();

        if (einstellungen != null) {
            loadPaneldFromDatabase(einstellungen);
        } else {
            createPanels();
        }
    }

    @Listen("onClick=#btnSave")
    public void onSave() {
        List<TaetigkeitsfeldJson> taetigkeitsfeldList = new LinkedList<>();
        this.divTaetigkeitsfeld.getChildren()
                .forEach(component -> {
                    Panel panel = (Panel) component;
                    System.out.println("Titel: " + panel.getTitle());
                    List<String> list = panel.getChildren().get(0).getChildren()
                            .stream()
                            .filter(zkComponent -> zkComponent instanceof Div)
                            .map(item -> {
                                Textbox textbox = (Textbox) item.getChildren().get(1);
                                return textbox.getValue();
                            }).collect(Collectors.toList());

                    list.forEach(System.out::println);

                    Textbox textboxOrgProt = (Textbox) panel.getChildren().get(2).getChildren().get(0).getChildren().get(0).getChildren().get(1);

                    Textbox textboxStammblatt = (Textbox) panel.getChildren().get(2).getChildren().get(0).getChildren().get(1).getChildren().get(1);

                    TaetigkeitsfeldJson taetigkeitsfeldJson = new TaetigkeitsfeldJson();
                    taetigkeitsfeldJson.setTaetigkietsfeld(panel.getTitle());
                    taetigkeitsfeldJson.setOrgprot(textboxOrgProt.getValue());
                    taetigkeitsfeldJson.setStammblatt(textboxStammblatt.getValue());
                    taetigkeitsfeldJson.setBlattList(list);

                    taetigkeitsfeldList.add(taetigkeitsfeldJson);
                });

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            String string = objectMapper.writeValueAsString(taetigkeitsfeldList);
            Einstellungen einstellungen = new Einstellungen();

            Einstellungen einstellungenFromDb = this.einstellungenRepo.findByTaetigkeitsfeld();

            if (einstellungenFromDb == null) {
                einstellungen.setId(UUID.randomUUID().toString());
            } else {
                einstellungen.setId(einstellungenFromDb.getId());
            }

            einstellungen.setInhalt(string);
            einstellungen.setFachlichkeit(EinstellungenBereich.TAETIGKEITSFELDER.getValue());

            this.einstellungenRepo.merge(einstellungen);

            Include include = (Include) Path.getComponent(MokiFrontendConstants.INCLUDE);
            include.setSrc("");// reset include

            Messagebox.show(Labels.getLabel("taetigkietsfeld.display.info.updatecomplete"), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);

        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }

    private void loadPaneldFromDatabase(Einstellungen einstellungen) throws java.io.IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        List<Taetigkeitsfeld> taetigkeitsfeldList = taetigkeitsfeldRepo.findAll();

        List<TaetigkeitsfeldJson> taetigkeitsfeldJsonList = objectMapper.readValue(einstellungen.getInhalt(),
                new TypeReference<List<TaetigkeitsfeldJson>>() {
                });

        List<Taetigkeitsfeld> addedTaetigkeitsfelder = taetigkeitsfeldList.stream()
                .filter(taetigkeitsfeld -> taetigkeitsfeldJsonList.stream()
                        .noneMatch(taetigkeitsfeldJson -> StringUtils.equals(taetigkeitsfeld.getBezeichnung(), taetigkeitsfeldJson.getTaetigkietsfeld())))
                .collect(Collectors.toList());

        addedTaetigkeitsfelder.forEach(addedTaetigkeitsfeld -> {
            TaetigkeitsfeldJson taetigkeitsfeldJson = new TaetigkeitsfeldJson();
            taetigkeitsfeldJson.setTaetigkietsfeld(addedTaetigkeitsfeld.getBezeichnung());
            taetigkeitsfeldJsonList.add(taetigkeitsfeldJson);
        });

        taetigkeitsfeldJsonList
                .stream()
                .sorted(Comparator.comparing(TaetigkeitsfeldJson::getTaetigkietsfeld))
                .forEach(taetigkeitsfeldJson -> {
                    Panel panel = getPanel(taetigkeitsfeldJson.getTaetigkietsfeld());

                    Panelchildren panelchildren = new Panelchildren();
                    panelchildren.setParent(panel);

                    if (taetigkeitsfeldJson.getBlattList() != null) {
                        taetigkeitsfeldJson.getBlattList().forEach(blattName -> addBlatt(panelchildren, blattName));
                    } else {
                        addBlatt(panelchildren, null);
                    }

                    addBottomToolbar(panel);

                    Vlayout vlayout = addFootToolbar(taetigkeitsfeldJson.getOrgprot(), taetigkeitsfeldJson.getStammblatt(), panel);

                    Taetigkeitsfeld taetigkeitsfeld = taetigkeitsfeldRepo.findByBezeichnung(taetigkeitsfeldJson.getTaetigkietsfeld());
                    ExcelDatei excelDatei = excelDateinRepo.findByTaetigkeitsfeldId(taetigkeitsfeld.getId());
                    addExcelBar(taetigkeitsfeld, vlayout, excelDatei);
                });
    }

    private void createPanels() {
        List<Taetigkeitsfeld> list = this.taetigkeitsfeldRepo.findAll();

        list.forEach(taetigkeitsfeld -> {
            Panel panel = getPanel(taetigkeitsfeld.getBezeichnung());

            Panelchildren panelchildren = new Panelchildren();
            panelchildren.setParent(panel);

            addBlatt(panelchildren, null);
            addBottomToolbar(panel);

            Vlayout vlayout = addFootToolbar(null, null, panel);
            addExcelBar(taetigkeitsfeld, vlayout, null);
        });
    }

    private Panel getPanel(String taetigkeitsfeld) {
        Panel panel = new Panel();
        panel.setParent(this.divTaetigkeitsfeld);
        panel.setTitle(taetigkeitsfeld);
        panel.setBorder("normal");
        panel.setWidth("100%");
        panel.setCollapsible(true);
        panel.setOpen(false);
        return panel;
    }

    private void addBlatt(Panelchildren panelchildren, String blattName) {
        Div div = new Div();
        div.setAlign("center");
        div.setParent(panelchildren);

        Label label = new Label();
        label.setParent(div);
        label.setValue("Blatt: ");

        Textbox textbox = new Textbox();
        textbox.setParent(div);
        textbox.setMaxlength(50);
        textbox.setHflex("1");
        textbox.setValue(blattName);
    }

    private void addBottomToolbar(Panel panel) {
        Toolbar bottomToolbar = new Toolbar();
        bottomToolbar.setAlign("end");
        bottomToolbar.setParent(panel);

        addButtonDelete(bottomToolbar);
        addButtonAdd(bottomToolbar);
    }

    private Vlayout addFootToolbar(String orgProt, String stammblatt, Panel panel) {
        Toolbar footToolbar = new Toolbar();
        footToolbar.setAlign("center");
        footToolbar.setParent(panel);

        Vlayout vlayout = new Vlayout();
        vlayout.setParent(footToolbar);

        Hlayout hlayoutOrgprot = new Hlayout();
        hlayoutOrgprot.setValign("middle");
        hlayoutOrgprot.setParent(vlayout);

        Label labelOrgProt = new Label();
        labelOrgProt.setParent(hlayoutOrgprot);
        labelOrgProt.setValue("Org.Prot.: ");

        Textbox textboxOrgProt = new Textbox();
        textboxOrgProt.setParent(hlayoutOrgprot);
        textboxOrgProt.setMaxlength(50);
        textboxOrgProt.setHflex("1");
        textboxOrgProt.setValue(orgProt);

        Hlayout hlayoutStammblatt = new Hlayout();
        hlayoutStammblatt.setValign("middle");
        hlayoutStammblatt.setParent(vlayout);

        Label labelStammblatt = new Label();
        labelStammblatt.setParent(hlayoutStammblatt);
        labelStammblatt.setValue("Stammblatt:");

        Textbox textboxStammblatt = new Textbox();
        textboxStammblatt.setParent(hlayoutStammblatt);
        textboxStammblatt.setMaxlength(50);
        textboxStammblatt.setHflex("1");
        textboxStammblatt.setValue(stammblatt);
        return vlayout;
    }

    private void addExcelBar(Taetigkeitsfeld taetigkeitsfeld, Vlayout vlayout, ExcelDatei excelDatei) {
        Hlayout hlayoutExcel = new Hlayout();
        hlayoutExcel.setValign("middle");
        hlayoutExcel.setParent(vlayout);

        Label labelDatei = new Label();
        labelDatei.setParent(hlayoutExcel);
        labelDatei.setValue("Excel-Vorlage:");

        Textbox textboxDateiName = new Textbox();
        textboxDateiName.setParent(hlayoutExcel);
        textboxDateiName.setDisabled(true);
        textboxDateiName.setHflex("1");
        textboxDateiName.setValue(excelDatei == null ? null : excelDatei.getDateiname());

        addButtonUpload(hlayoutExcel, textboxDateiName, taetigkeitsfeld, excelDatei);
    }

    private void addButtonDelete(Toolbar bottomToolbar) {
        Button buttonDelete = new Button();
        buttonDelete.setParent(bottomToolbar);
        buttonDelete.setIconSclass("z-icon-minus-circle");
        buttonDelete.addEventListener("onClick", new EventListener<Event>() {
            @Override
            public void onEvent(Event event) {
                Button targetButton = (Button) event.getTarget();
                List<Component> children = targetButton.getParent().getParent().getChildren().get(0).getChildren();
                try {
                    children.remove(children.size() - 1);
                } catch (IndexOutOfBoundsException e) {
                    // do nothing
                }
            }
        });
    }

    private void addButtonAdd(Toolbar bottomToolbar) {
        Button buttonAdd = new Button();
        buttonAdd.setParent(bottomToolbar);
        buttonAdd.setIconSclass("z-icon-plus-circle");
        buttonAdd.addEventListener("onClick", new EventListener<Event>() {
            @Override
            public void onEvent(Event event) {
                Button targetButton = (Button) event.getTarget();

                Panelchildren panelchildren = (Panelchildren) targetButton.getParent().getParent().getChildren().get(0);

                Div div = new Div();
                div.setParent(panelchildren);

                Label label = new Label();
                label.setParent(div);
                label.setValue("Blatt: ");

                Textbox textbox = new Textbox();
                textbox.setParent(div);
                textbox.setMaxlength(50);
                textbox.setHflex("1");
            }
        });
    }

    private void addButtonUpload(Hlayout hlayoutDatei, Textbox textboxDateiName, Taetigkeitsfeld taetigkietsfeld, ExcelDatei excelDatei) {
        Button buttonUpload = new Button();
        buttonUpload.setParent(hlayoutDatei);
        buttonUpload.setIconSclass("z-icon-upload");
        buttonUpload.setUpload("true");
        buttonUpload.setAttribute("value", textboxDateiName);
        buttonUpload.addEventListener("onUpload", new EventListener<UploadEvent>() {
            @Override
            public void onEvent(UploadEvent event) {
                Media media = event.getMedia();

                if (!StringUtils.equals("xlsx", media.getFormat())) {
                    throw new UnsupportedOperationException("Die Datei ist kein gültiges Excel Format");
                }

                Button button = (Button) event.getTarget();
                Textbox textbox = (Textbox) button.getAttribute("value");

                String dateiname = media.getName();
                textbox.setValue(dateiname);
                textbox.setTooltiptext(dateiname);

                ExcelDatei excelDateiNeu = new ExcelDatei();

                if (excelDatei != null) {
                    excelDateiNeu.setId(excelDatei.getId());
                } else {
                    excelDateiNeu.setId(UUID.randomUUID().toString());
                }

                excelDateiNeu.setDateiname(dateiname);
                excelDateiNeu.setDatei(media.getByteData());
                excelDateiNeu.setTaetigkeitsfeldId(taetigkietsfeld.getId());

                excelDateinRepo.merge(excelDateiNeu);
            }
        });
    }
}
