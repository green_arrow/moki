package at.moki.frontend.validation;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.regex.Pattern;

/**
 * Created by Green Arrow on 19.02.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public final class MokiValidation {

    private static Pattern INVAID_CHARACTER_PATTERN = Pattern.compile("^[A-Za-z0-9ÄäÜüÖöß \\\\-\\\\/\\\\(\\\\)\\\\=\\\\.\\\\,\\\\:\\\\;\\\\§]+$");

    public static boolean isOnlyNumbers(String numberAsString) {
        numberAsString = StringUtils.replace(numberAsString, ".", "");
        numberAsString = StringUtils.replace(numberAsString, ",", "");

        return NumberUtils.isCreatable(numberAsString);
    }

    public static boolean isInputValid(String text) {
        return INVAID_CHARACTER_PATTERN.matcher(text).matches();
    }

    private MokiValidation() {
        // empty constructor to prevent JVM from creating one
    }
}
