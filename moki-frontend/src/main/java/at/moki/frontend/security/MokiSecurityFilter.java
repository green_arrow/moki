package at.moki.frontend.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Green Arrow on 06.01.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class MokiSecurityFilter implements Filter {

    private static final String REQUEST = "request";
    private static Logger logger = LoggerFactory.getLogger(MokiSecurityFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("initialisiere filter");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        logger.info("filtere user");

        if (servletRequest instanceof HttpServletRequest) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
            final HttpSession session = httpServletRequest.getSession();

            session.setAttribute(REQUEST, httpServletRequest.getServletPath());

            if (session.getAttribute("sessionUser") != null) {
                filterChain.doFilter(servletRequest, servletResponse);
                return;
            }

            // log remote ip address since at that point he or she is not in the session
            logger.info(String.format("Moki Aufruf von: %s", httpServletRequest.getRemoteAddr()));

            final String url = httpServletRequest.getRequestURI();
            if (url == null) {
                return;
            }

            if (url.contains("/login.zul")) {
                filterChain.doFilter(servletRequest, servletResponse);
                session.setAttribute(REQUEST, "/index.zul");
            } else {
                session.setAttribute(REQUEST, "/index.zul");
                servletRequest.getRequestDispatcher("/login.zul").forward(servletRequest, servletResponse);
            }
        }
    }

    @Override
    public void destroy() {
        logger.info("zerstoere filter");
    }
}
