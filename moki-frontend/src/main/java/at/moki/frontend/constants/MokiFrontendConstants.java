package at.moki.frontend.constants;

/**
 * Created by Green Arrow on 15.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public final class MokiFrontendConstants {

    // used as an identifiert for the selected klientin object
    public static final String KLIENTIN_SELECTED_FROM_SEARCH = "klientInSelected";
    public static final String KLIENTIN_SELECTED_FROM_SEARCH_ORG_PROT = "klientInSelectedOrgProt";

    // used for message box constants
    public static final String INFORMATION = "Information";

    // used for resolving zk paths
    public static final String INCLUDE = "/winMokiIndex/includeView";

    public static final String ORGANISATIONSPROTOKOLL_SEARCH_WINDOW = "/winOrganisationsprotokollSearchWindow";

    private MokiFrontendConstants() {
        // empty constructor to prevent JVM from creating one
    }
}
