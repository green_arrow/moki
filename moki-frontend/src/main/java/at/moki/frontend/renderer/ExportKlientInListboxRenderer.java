package at.moki.frontend.renderer;

import at.moki.business.excel.ExcelExportBusiness;
import at.moki.business.klientin.KlientInBusiness;
import at.moki.domainmodel.klientin.KlientIn;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.session.SessionModel;
import at.moki.util.MokiFileUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hpsf.ClassIDPredefined;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zkplus.cdi.DelegatingVariableResolver;
import org.zkoss.zul.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Green Arrow on 30.04.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class ExportKlientInListboxRenderer implements ListitemRenderer {

    private static Logger logger = LoggerFactory.getLogger(ExportKlientInListboxRenderer.class);

    private DelegatingVariableResolver cdiBeanResolver = new DelegatingVariableResolver();

    private SessionModel sessionModel = (SessionModel) cdiBeanResolver.resolveVariable("sessionModel");

    private KlientInBusiness klientInBusiness = (KlientInBusiness) cdiBeanResolver.resolveVariable("klientInBusinessImpl");

    private ExcelExportBusiness excelExportBusiness = (ExcelExportBusiness) cdiBeanResolver.resolveVariable("excelExportBusinessImpl");

    @Override
    public void render(Listitem item, Object data, int index) {

        if (!(data instanceof KlientIn)) {
            throw new UnsupportedOperationException("Konnte keinen Renderer fuer Objekt " + data.toString() + " erstellen.");
        } else {

            KlientIn klientIn = (KlientIn) data;

            logger.debug("Starte KlientIn render prozess");

            // map klientIn date into Listcells
            Listcell listcellKlientenNummer = new Listcell();
            listcellKlientenNummer.setLabel(klientIn.getKlientenNummer());
            listcellKlientenNummer.setParent(item);

            Listcell listcellVorname = new Listcell();
            listcellVorname.setLabel(klientIn.getVorname());
            listcellVorname.setParent(item);

            Listcell listcellNachname = new Listcell();
            listcellNachname.setLabel(klientIn.getNachname());
            listcellNachname.setParent(item);

            Listcell listcellButton = new Listcell();
            listcellButton.setStyle("text-align: center; background: white;border-top: 1px solid #f2f2f2;");
            listcellButton.setParent(item);

            Button btnEdit = new Button();
            btnEdit.setIconSclass("z-icon-file-excel-o");
            btnEdit.setTooltiptext(Labels.getLabel("display.excel.export"));
            btnEdit.setStyle("padding: 0px; width: 25px;");
            btnEdit.setParent(listcellButton);
            btnEdit.setAttribute(MokiFrontendConstants.KLIENTIN_SELECTED_FROM_SEARCH, klientIn);

            btnEdit.addEventListener("onClick", new EventListener<Event>() {
                @Override
                public void onEvent(Event event) {
                    KlientIn klientIn = (KlientIn) btnEdit.getAttribute(MokiFrontendConstants.KLIENTIN_SELECTED_FROM_SEARCH);
                    String klientInTaetigkeitsfeld = klientIn.getTaetigkeitsfeld();

                    if (StringUtils.equalsIgnoreCase("FK", klientInTaetigkeitsfeld)) {
                        List<KlientIn> klientInListFk = klientInBusiness.getKlientInGeschwister(klientIn);

                        KlientIn zwilling = klientInListFk.get(0);
                        KlientIn drilling = klientInListFk.get(1);

                        ByteArrayOutputStream byteArrayOutputStreamFk = excelExportBusiness.exportFk(
                                sessionModel.getKrankenpflegerin(), klientIn, zwilling, drilling);

                        performFiledownload(klientIn, byteArrayOutputStreamFk);
                    } else if (StringUtils.equalsIgnoreCase("KJH", klientInTaetigkeitsfeld)) {
                        List<KlientIn> klientInListKjh = klientInBusiness.getKlientInWithKinder(klientIn);

                        KlientIn zweitesKind = klientInListKjh.get(0);
                        KlientIn drittesKind = klientInListKjh.get(1);
                        KlientIn viertesKind = klientInListKjh.get(2);

                        ByteArrayOutputStream byteArrayOutputStreamKjh = excelExportBusiness.exportKjh(
                                sessionModel.getKrankenpflegerin(),
                                klientIn, zweitesKind, drittesKind, viertesKind);

                        performFiledownload(klientIn, byteArrayOutputStreamKjh);
                    } else {
                        ByteArrayOutputStream byteArrayOutputStreamIchFrzMptTs = excelExportBusiness.exportSingleKlientIn(
                                sessionModel.getKrankenpflegerin(), klientIn);

                        performFiledownload(klientIn, byteArrayOutputStreamIchFrzMptTs);
                    }
                }
            });
        }
    }

    private void performFiledownload(KlientIn klientIn, ByteArrayOutputStream byteArrayOutputStreamKjh) {
        Filedownload.save(byteArrayStreamToInputstream(byteArrayOutputStreamKjh),
                ClassIDPredefined.EXCEL_V11.getContentType(), MokiFileUtil.getTaetigkeitsfeldFileName(klientIn.getTaetigkeitsfeld()));
    }

    private InputStream byteArrayStreamToInputstream(ByteArrayOutputStream excelAsOutputStream) {
        return new ByteArrayInputStream(excelAsOutputStream.toByteArray());
    }
}
