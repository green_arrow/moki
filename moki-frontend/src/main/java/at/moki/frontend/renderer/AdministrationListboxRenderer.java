package at.moki.frontend.renderer;

import at.moki.business.user.UserBusiness;
import at.moki.domainmodel.user.MokiUser;
import at.moki.encryption.MokiEncryption;
import at.moki.exception.MokiBusinessException;
import at.moki.frontend.constants.MokiFrontendConstants;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zkplus.cdi.DelegatingVariableResolver;
import org.zkoss.zul.*;

/**
 * Created by Green Arrow on 29.01.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class AdministrationListboxRenderer implements ListitemRenderer {

    private static Logger logger = LoggerFactory.getLogger(AdministrationListboxRenderer.class);

    private Include include = (Include) Path.getComponent(MokiFrontendConstants.INCLUDE);

    private DelegatingVariableResolver cdiBeanResolver = new DelegatingVariableResolver();
    private UserBusiness userBusiness = (UserBusiness) cdiBeanResolver.resolveVariable("userBusinessImpl");
    private MokiEncryption mokiEncryption = (MokiEncryption) cdiBeanResolver.resolveVariable("mokiEncryption");

    @Override
    public void render(Listitem item, Object data, int index) throws Exception {

        if (data instanceof MokiUser) {
            MokiUser mokiUser = (MokiUser) data;
            // map mokiUser date into Listcells
            Listcell listcellUsername = new Listcell();
            listcellUsername.setLabel(mokiUser.getUsername());
            listcellUsername.setParent(item);

            Listcell listcellUserRole = new Listcell();
            listcellUserRole.setLabel(mokiUser.getMokiUserRole().getRolename());
            listcellUserRole.setParent(item);

            Listcell listcellReset = new Listcell();
            listcellReset.setStyle("text-align: center; background: white;border-top: 1px solid #f2f2f2;");

            addButtonReset(listcellReset, mokiUser);

            addButtonDelete(listcellReset, mokiUser);

            listcellReset.setParent(item);
        } else {
            throw new UnsupportedOperationException("Konnte keinen Renderer fuer Objekt erstellen.");
        }
    }

    private void addButtonReset(Listcell listcellReset, MokiUser mokiUser) {
        Button buttonReset = new Button();
        buttonReset.setIconSclass("z-icon-undo");
        buttonReset.setStyle("padding: 0px; width: 25px;");
        buttonReset.setTooltiptext(Labels.getLabel("administration.display.table.reset.tooltip"));
        buttonReset.setParent(listcellReset);

        buttonReset.addEventListener("onClick", new EventListener<Event>() {
            @Override
            public void onEvent(Event event) {
                try {
                    mokiUser.setPassword(mokiEncryption.generateHashPassword("s3cret",
                            RandomStringUtils.random(15, true, true)));
                    userBusiness.update(mokiUser);

                    Messagebox.show(String.format(
                            "Das Passwort für Username %s wurde erfolgreich zurükgesetzt", mokiUser.getUsername()),
                            MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);
                } catch (MokiBusinessException e) {
                    logger.error("", e);
                    Messagebox.show(e.getTrueCause().getMessage(), "Error", Messagebox.OK, Messagebox.ERROR);
                }
            }
        });
    }

    private void addButtonDelete(Listcell listcellButton, MokiUser mokiUser) {
        Button btnDelete = new Button();
        btnDelete.setIconSclass("z-icon-trash");
        btnDelete.setTooltiptext(Labels.getLabel("display.delete"));
        btnDelete.setStyle("padding: 0px; width: 25px; margin-left: 4px;");
        btnDelete.setParent(listcellButton);
        // store mokiUser for every row in button
        btnDelete.setAttribute(MokiFrontendConstants.KLIENTIN_SELECTED_FROM_SEARCH, mokiUser);

        btnDelete.addEventListener("onClick", new EventListener<Event>() {
            @Override
            public void onEvent(Event event) {
                MokiUser mokiUserDelete = (MokiUser) btnDelete.getAttribute(MokiFrontendConstants.KLIENTIN_SELECTED_FROM_SEARCH);
                userBusiness.delete(mokiUserDelete);
                include.setSrc("");
                Messagebox.show(Labels.getLabel("krankenpflegerin.display.deletecomplete"), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);
            }
        });
    }
}
