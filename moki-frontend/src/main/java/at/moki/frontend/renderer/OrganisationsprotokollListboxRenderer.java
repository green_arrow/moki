package at.moki.frontend.renderer;

import at.moki.domainmodel.organisationsprotokoll.Organisationsprotokoll;
import at.moki.formatter.MokiDateFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import java.time.LocalDateTime;

/**
 * Created by Green Arrow on 21.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class OrganisationsprotokollListboxRenderer implements ListitemRenderer {

    private static Logger logger = LoggerFactory.getLogger(OrganisationsprotokollListboxRenderer.class);

    @Override
    public void render(Listitem item, Object data, int index) {

        if (data instanceof Organisationsprotokoll) {

            logger.debug("Starte organisationsprotokoll render prozess");

            Organisationsprotokoll organisationsprotokoll = (Organisationsprotokoll) data;

            // map organisationsprotokoll date into Listcells
            LocalDateTime aenderungszeitpunkt = organisationsprotokoll.getAenderungszeitpunkt();

            Listcell listcellDatum = new Listcell();
            listcellDatum.setLabel(aenderungszeitpunkt.format(MokiDateFormatter.DATE_FORMATER));
            listcellDatum.setParent(item);

            Listcell listcellUhrzeit = new Listcell();
            listcellUhrzeit.setLabel(aenderungszeitpunkt.format(MokiDateFormatter.TIME_FORMATER));
            listcellUhrzeit.setParent(item);

            Listcell listcellEintrag = new Listcell();
            listcellEintrag.setLabel(organisationsprotokoll.getText());
            listcellEintrag.setParent(item);

            Listcell listcellHandschrift = new Listcell();
            listcellHandschrift.setLabel(organisationsprotokoll.getHandschriftDgkp());
            listcellHandschrift.setParent(item);
        } else {
            throw new UnsupportedOperationException("konnte object nicht rendern");
        }
    }
}
