package at.moki.frontend.renderer;

import at.moki.domainmodel.verrechnung.Verrechnung;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

/**
 * @author Green Arrow
 * @date 21.07.19
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class VerrechnungListboxRenderer implements ListitemRenderer {

    @Override
    public void render(Listitem item, Object data, int index) {

        if (data instanceof Verrechnung) {
            Verrechnung verrechnung = (Verrechnung) data;
            // map verrechnung date into Listcells
            Listcell listcellVerrechnungBezeichnung = new Listcell();
            listcellVerrechnungBezeichnung.setLabel(verrechnung.getBezeichnung());
            listcellVerrechnungBezeichnung.setParent(item);
        } else {
            throw new UnsupportedOperationException("Konnte keinen Renderer fuer Objekt erstellen.");
        }
    }
}
