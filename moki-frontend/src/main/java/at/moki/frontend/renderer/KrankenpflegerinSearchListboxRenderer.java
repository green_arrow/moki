package at.moki.frontend.renderer;

import at.moki.domainmodel.krankenpflegerin.Krankenpflegerin;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.session.SessionModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zkplus.cdi.DelegatingVariableResolver;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

/**
 * Created by Green Arrow on 10.02.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class KrankenpflegerinSearchListboxRenderer implements ListitemRenderer {

    private static Logger logger = LoggerFactory.getLogger(KrankenpflegerinSearchListboxRenderer.class);
    private DelegatingVariableResolver cdiBeanResolver = new DelegatingVariableResolver();

    private SessionModel sessionModel = (SessionModel) cdiBeanResolver.resolveVariable("sessionModel");


    @Override
    public void render(Listitem item, Object data, int index) {

        if (data instanceof Krankenpflegerin) {

            Krankenpflegerin krankenpflegerin = (Krankenpflegerin) data;

            logger.debug("Starte Krankenpflegerin render prozess");

            // map krankenpflegerin date into Listcells
            Listcell listcellVorname = new Listcell();
            listcellVorname.setLabel(krankenpflegerin.getVorname());
            listcellVorname.setParent(item);

            Listcell listcellNachname = new Listcell();
            listcellNachname.setLabel(krankenpflegerin.getNachname());
            listcellNachname.setParent(item);

            Listcell listcellTelefonnumer = new Listcell();
            listcellTelefonnumer.setLabel(krankenpflegerin.getTelefonnummer());
            listcellTelefonnumer.setParent(item);

            Listcell listcellEmail = new Listcell();
            listcellEmail.setLabel(krankenpflegerin.getEmail());
            listcellEmail.setParent(item);

            //map only if admin is logged in
            if (MokiUserRole.KRANKENPFLEGERIN == sessionModel.getMokiUser().getMokiUserRole()) {

                Listcell listcellBezeichnung = new Listcell();
                listcellBezeichnung.setLabel(krankenpflegerin.getBezeichnung());
                listcellBezeichnung.setParent(item);

                Listcell listcellStrasse = new Listcell();
                listcellStrasse.setLabel(krankenpflegerin.getStrasse());
                listcellStrasse.setParent(item);

                Listcell listcellPostleitzahl = new Listcell();
                listcellPostleitzahl.setLabel(krankenpflegerin.getPostleitzahl());
                listcellPostleitzahl.setParent(item);

                Listcell listcellBankverbindung = new Listcell();
                listcellBankverbindung.setLabel(krankenpflegerin.getBankverbindung());
                listcellBankverbindung.setParent(item);

                Listcell listcellIban = new Listcell();
                listcellIban.setLabel(krankenpflegerin.getIban());
                listcellIban.setParent(item);

                Listcell listcellBic = new Listcell();
                listcellBic.setLabel(krankenpflegerin.getBic());
                listcellBic.setParent(item);
            }
        } else {
            throw new UnsupportedOperationException("konnte object nicht rendern");
        }
    }
}
