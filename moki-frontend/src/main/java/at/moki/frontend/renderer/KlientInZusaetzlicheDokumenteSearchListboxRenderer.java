package at.moki.frontend.renderer;

import at.moki.business.klientin.KlientInZusaetzlicheDokumenteBusiness;
import at.moki.domainmodel.klientin.KlientInZusaetzlicheDokumente;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.formatter.MokiDateFormatter;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.session.SessionModel;
import org.apache.poi.hpsf.ClassIDPredefined;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zkplus.cdi.DelegatingVariableResolver;
import org.zkoss.zul.*;

/**
 * Created by Green Arrow on 03.02.2019.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class KlientInZusaetzlicheDokumenteSearchListboxRenderer implements ListitemRenderer {

    private static Logger logger = LoggerFactory.getLogger(KlientInZusaetzlicheDokumenteSearchListboxRenderer.class);

    private DelegatingVariableResolver cdiBeanResolver = new DelegatingVariableResolver();
    private SessionModel sessionModel = (SessionModel) cdiBeanResolver.resolveVariable("sessionModel");

    @Override
    public void render(Listitem item, Object data, int index) {

        if (data instanceof KlientInZusaetzlicheDokumente) {

            KlientInZusaetzlicheDokumente klientInZusaetzlicheDokumente = (KlientInZusaetzlicheDokumente) data;

            logger.debug("Starte KlientInZusaetzlicheDokumente render prozess");

            // map klientInZusaetzlicheDokumente date into Listcells
            Listcell listcellErstellungsdatum = new Listcell();
            listcellErstellungsdatum.setLabel(klientInZusaetzlicheDokumente.getErstellungszeitpunkt()
                    .format(MokiDateFormatter.DATE_TIME_FORMATTER));
            listcellErstellungsdatum.setParent(item);

            Listcell listcellDatei = new Listcell();
            listcellDatei.setLabel(klientInZusaetzlicheDokumente.getDateiname());
            listcellDatei.setParent(item);

            Listcell listcellButton = new Listcell();
            listcellButton.setStyle("text-align: center; background: white;border-top: 1px solid #f2f2f2;");
            listcellButton.setParent(item);

            addButtonDownload(listcellButton, klientInZusaetzlicheDokumente);

            if (MokiUserRole.KRANKENPFLEGERIN != this.sessionModel.getMokiUser().getMokiUserRole()) {
                addButtonDelete(listcellButton, klientInZusaetzlicheDokumente);
            }

        } else {
            throw new UnsupportedOperationException("Konnte KlientInZusaetzlicheDokumente object nicht rendern.");
        }
    }

    private void addButtonDownload(Listcell listcellButton, KlientInZusaetzlicheDokumente klientInZusaetzlicheDokumente) {
        Button btnDownload = new Button();
        btnDownload.setIconSclass("z-icon-download");
        btnDownload.setTooltiptext(Labels.getLabel("display.download"));
        btnDownload.setStyle("padding: 0px; width: 25px; margin-left: 4px;");
        btnDownload.setParent(listcellButton);
        // store klientInZusaetzlicheDokumente for every row in button
        btnDownload.setAttribute(MokiFrontendConstants.KLIENTIN_SELECTED_FROM_SEARCH, klientInZusaetzlicheDokumente);

        btnDownload.addEventListener("onClick", new EventListener<Event>() {
            @Override
            public void onEvent(Event event) {
                KlientInZusaetzlicheDokumenteBusiness klientInZusaetzlicheDokumenteBusiness = (KlientInZusaetzlicheDokumenteBusiness) cdiBeanResolver
                        .resolveVariable("klientInZusaetzlicheDokumenteBusinessImpl");

                byte[] dataAsByteArray = klientInZusaetzlicheDokumenteBusiness.getById(klientInZusaetzlicheDokumente.getId());
                Filedownload.save(dataAsByteArray, ClassIDPredefined.PDF.getContentType(), klientInZusaetzlicheDokumente.getDateiname());
            }
        });
    }

    private void addButtonDelete(Listcell listcellButton, KlientInZusaetzlicheDokumente klientInZusaetzlicheDokumente) {
        Button btnDelete = new Button();
        btnDelete.setIconSclass("z-icon-trash");
        btnDelete.setTooltiptext(Labels.getLabel("display.delete"));
        btnDelete.setStyle("padding: 0px; width: 25px; margin-left: 4px;");
        btnDelete.setParent(listcellButton);
        // store klientInZusaetzlicheDokumente for every row in button
        btnDelete.setAttribute(MokiFrontendConstants.KLIENTIN_SELECTED_FROM_SEARCH, klientInZusaetzlicheDokumente);

        btnDelete.addEventListener("onClick", new EventListener<Event>() {
            @Override
            public void onEvent(Event event) {

                KlientInZusaetzlicheDokumenteBusiness klientInZusaetzlicheDokumenteBusiness = (KlientInZusaetzlicheDokumenteBusiness) cdiBeanResolver
                        .resolveVariable("klientInZusaetzlicheDokumenteBusinessImpl");

                klientInZusaetzlicheDokumenteBusiness.deleteById(klientInZusaetzlicheDokumente.getId());

                Messagebox.show(Labels.getLabel("klientin.zusaetzliche.dokumente.successful"), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);

                // listcellButton.getParent() -> Listitem -> detach whole row
                listcellButton.getParent().detach();
            }
        });
    }
}
