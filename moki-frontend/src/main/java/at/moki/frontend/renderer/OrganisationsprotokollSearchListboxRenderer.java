package at.moki.frontend.renderer;

import at.moki.domainmodel.klientin.KlientIn;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.session.TransferModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zkplus.cdi.DelegatingVariableResolver;
import org.zkoss.zul.*;

/**
 * Created by Green Arrow on 21.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class OrganisationsprotokollSearchListboxRenderer implements ListitemRenderer {

    private static Logger logger = LoggerFactory.getLogger(OrganisationsprotokollSearchListboxRenderer.class);

    private Include include = (Include) Path.getComponent(MokiFrontendConstants.INCLUDE);
    private DelegatingVariableResolver cdiBeanResolver = new DelegatingVariableResolver();
    private TransferModel transferModel = (TransferModel) cdiBeanResolver.resolveVariable("transferModel");

    @Override
    public void render(Listitem item, Object data, int index) {

        if (data instanceof KlientIn) {

            KlientIn klientIn = (KlientIn) data;

            logger.debug("Starte KlientIn render prozess");

            // map klientIn date into Listcells
            Listcell listcellKlientenNummer = new Listcell();
            listcellKlientenNummer.setLabel(klientIn.getKlientenNummer());
            listcellKlientenNummer.setParent(item);

            Listcell listcellVorname = new Listcell();
            listcellVorname.setLabel(klientIn.getVorname());
            listcellVorname.setParent(item);

            Listcell listcellNachname = new Listcell();
            listcellNachname.setLabel(klientIn.getNachname());
            listcellNachname.setParent(item);

            Listcell listcellButton = new Listcell();
            listcellButton.setStyle("text-align: center; background: white;border-top: 1px solid #f2f2f2;");
            listcellButton.setParent(item);

            Button btnSelect = new Button();
            btnSelect.setIconSclass("z-icon-check-circle");
            btnSelect.setTooltiptext(Labels.getLabel("display.choose"));
            btnSelect.setStyle("padding: 0px; width: 25px;");
            btnSelect.setParent(listcellButton);
            btnSelect.setAttribute(MokiFrontendConstants.KLIENTIN_SELECTED_FROM_SEARCH_ORG_PROT, klientIn);

            btnSelect.addEventListener("onClick", new EventListener<Event>() {
                @Override
                public void onEvent(Event event) {
                    KlientIn klientInSelected = (KlientIn) btnSelect.getAttribute(MokiFrontendConstants.KLIENTIN_SELECTED_FROM_SEARCH_ORG_PROT);
                    transferModel.setKlientInOrgProt(klientInSelected);
                    include.setSrc("");
                    include.setSrc("/content/organisationsprotokoll.zul");
                    // detach window after selecting new orgprot
                    Window winOrganisationsprotokollSearchWindow = (Window) Path.getComponent(MokiFrontendConstants.ORGANISATIONSPROTOKOLL_SEARCH_WINDOW);
                    winOrganisationsprotokollSearchWindow.detach();
                }
            });
        } else {
            throw new UnsupportedOperationException("konnte object nicht rendern");
        }
    }
}
