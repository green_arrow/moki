package at.moki.frontend.renderer;

import at.moki.business.klientin.KlientInBusiness;
import at.moki.domainmodel.klientin.KlientIn;
import at.moki.domainmodel.user.MokiUserRole;
import at.moki.frontend.constants.MokiFrontendConstants;
import at.moki.session.SessionModel;
import at.moki.session.TransferModel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zkplus.cdi.DelegatingVariableResolver;
import org.zkoss.zul.*;

/**
 * Created by Green Arrow on 10.03.2018.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class KlientInSearchListboxRenderer implements ListitemRenderer {

    private static Logger logger = LoggerFactory.getLogger(KlientInSearchListboxRenderer.class);

    private DelegatingVariableResolver cdiBeanResolver = new DelegatingVariableResolver();
    private SessionModel sessionModel = (SessionModel) cdiBeanResolver.resolveVariable("sessionModel");

    @Override
    public void render(Listitem item, Object data, int index) {

        if (data instanceof KlientIn) {

            KlientIn klientIn = (KlientIn) data;

            logger.debug("Starte KlientIn render prozess");

            // map klientIn date into Listcells
            Listcell listcellKlientenNummer = new Listcell();
            listcellKlientenNummer.setLabel(klientIn.getKlientenNummer());
            listcellKlientenNummer.setParent(item);

            Listcell listcellVorname = new Listcell();
            listcellVorname.setLabel(klientIn.getVorname());
            listcellVorname.setParent(item);

            Listcell listcellNachname = new Listcell();
            listcellNachname.setLabel(klientIn.getNachname());
            listcellNachname.setParent(item);

            Listcell listcellTaetigkeitsfeld = new Listcell();
            listcellTaetigkeitsfeld.setLabel(klientIn.getTaetigkeitsfeld());
            listcellTaetigkeitsfeld.setParent(item);

            Listcell listcellButton = new Listcell();
            listcellButton.setStyle("text-align: center; background: white;border-top: 1px solid #f2f2f2;");
            listcellButton.setParent(item);

            addButtonEdit(listcellButton, klientIn);

            if (MokiUserRole.KRANKENPFLEGERIN != this.sessionModel.getMokiUser().getMokiUserRole()) {
                addButtonDelete(listcellButton, klientIn);
            }

        } else {
            throw new UnsupportedOperationException("Konnte KlientIn object nicht rendern.");
        }
    }

    private void addButtonEdit(Listcell listcellButton, KlientIn klientIn) {
        Button btnEdit = new Button();
        btnEdit.setIconSclass("z-icon-check-circle");
        btnEdit.setTooltiptext(Labels.getLabel("display.choose"));
        btnEdit.setStyle("padding: 0px; width: 25px;");
        btnEdit.setParent(listcellButton);
        // store klientIn for every row in button
        btnEdit.setAttribute(MokiFrontendConstants.KLIENTIN_SELECTED_FROM_SEARCH, klientIn);

        btnEdit.addEventListener("onClick", new EventListener<Event>() {
            @Override
            public void onEvent(Event event) {
                Include include = (Include) Path.getComponent(MokiFrontendConstants.INCLUDE);
                TransferModel transferModel = (TransferModel) cdiBeanResolver.resolveVariable("transferModel");

                // get klientIn from specific row
                KlientIn klientIn = (KlientIn) btnEdit.getAttribute(MokiFrontendConstants.KLIENTIN_SELECTED_FROM_SEARCH);

                transferModel.setKlientInSearch(klientIn);

                // set klientFile as attribute in order to set the correct label such as "FRZ"-NR
                String klientInTaetigkeitsfeld = klientIn.getTaetigkeitsfeld();
                transferModel.setKlientInTaetigkeitsfeld(klientInTaetigkeitsfeld);

                if (StringUtils.equals(klientInTaetigkeitsfeld, "FK")) {
                    include.setSrc("/content/klientInFk.zul");
                } else if (StringUtils.equals(klientInTaetigkeitsfeld, "KJH")) {
                    include.setSrc("/content/klientInKjh.zul");
                } else {
                    include.setSrc("/content/klientIn.zul");
                }
            }
        });
    }

    private void addButtonDelete(Listcell listcellButton, KlientIn klientIn) {
        Button btnDelete = new Button();
        btnDelete.setIconSclass("z-icon-trash");
        btnDelete.setTooltiptext(Labels.getLabel("display.delete"));
        btnDelete.setStyle("padding: 0px; width: 25px; margin-left: 4px;");
        btnDelete.setParent(listcellButton);
        // store klientIn for every row in button
        btnDelete.setAttribute(MokiFrontendConstants.KLIENTIN_SELECTED_FROM_SEARCH, klientIn);

        btnDelete.addEventListener("onClick", new EventListener<Event>() {
            @Override
            public void onEvent(Event event) {
                Include include = (Include) Path.getComponent(MokiFrontendConstants.INCLUDE);

                // get klientIn from specific row
                KlientIn klientIn = (KlientIn) btnDelete.getAttribute(MokiFrontendConstants.KLIENTIN_SELECTED_FROM_SEARCH);

                KlientInBusiness klientInBusiness = (KlientInBusiness) cdiBeanResolver
                        .resolveVariable("klientInBusinessImpl");

                klientInBusiness.delete(klientIn);

                include.setSrc("");

                Messagebox.show(Labels.getLabel("newentry.display.info.deletecomplete"), MokiFrontendConstants.INFORMATION, Messagebox.OK, Messagebox.INFORMATION);
            }
        });
    }
}
