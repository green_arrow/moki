package at.moki.frontend.generator;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.metainfo.Property;
import org.zkoss.zk.ui.sys.IdGenerator;

import java.util.List;

/**
 * @author Green Arrow
 * @date 26.04.2019
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class MokiHtmlIdGenerator implements IdGenerator {

    @Override
    public String nextComponentUuid(Desktop desktop, Component comp, ComponentInfo compInfo) {
        int i = Integer.parseInt(desktop.getAttribute("Id_Num").toString());
        i++;// Start from 1

        StringBuilder uuid = new StringBuilder("");

        desktop.setAttribute("Id_Num", String.valueOf(i));
        if (compInfo != null) {
            String id = getId(compInfo);
            if (id != null) {
                uuid.append(id).append("_");
            }
            String tag = compInfo.getTag();
            if (tag != null) {
                uuid.append(tag).append("_");
            }
        }
        return uuid.length() == 0 ? "zkcomp_" + i : uuid.append(i).toString();
    }

    @Override
    public String nextPageUuid(Page page) {
        return null;
    }

    @Override
    public String nextDesktopId(Desktop desktop) {
        if (desktop.getAttribute("Id_Num") == null) {
            String number = "0";
            desktop.setAttribute("Id_Num", number);
        }
        return null;
    }

    private String getId(ComponentInfo compInfo) {
        List<Property> properties = compInfo.getProperties();
        for (Property property : properties) {
            if ("id".equals(property.getName())) {
                return property.getRawValue();
            }
        }
        return null;
    }
}
