# [![alt text](https://noe.moki.at/images/Logo_MOKI_NOE.png "MOKI NÖ")](https://noe.moki.at/)

This Project was developed with the intention to assist and help the diligent people of MOKI NÖ in Lower Austria. It is basically a rich client application with user authentication to manage data of the paediatric nurses as users and the clients under the care of MOKI NÖ.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Following Software is required to install MOKI NÖ locally

```
Build automation tool:          apache-maven-3.3.9
Database:                       mariadb-10.1.40
Database managing tool:         MySQL Workbench 6.3 CE
Java Platform:                  java-8-openjdk-amd64
Java EE application server:     payara-5.192
```

### Installing

1) Setup mariadb and add a user with admin privileges to it.
2) Afterwards, install the mokidb.sql file in the resource path of moki-installation maven module.
3) Configure Payara Server for a connection pool and datasource (user this manual for help: https://blog.payara.fish/an-intro-to-connection-pools-in-payara-server-5) NOTE: the jta-data-source must be named jdbc/moki since it is configured in the persistence.xml
4) Username and Password may be set as wished since the application server provides all the necessary configuration.
5) Deploy the application

After successfully configuration of Payara and mariaDb you should see the login display under http://localhost:8080/moki-frontend/

## Running the tests

Automated tests are running during maven build.

## Limitations

However there are test limitations. Since this application is used for a public charity it also contains private information that are not a part of the gitlab repository. The test classes ExcelImportBusinessImplTest.java and ExcelExportBusinessImplTest.java will not work for you locally because they depend on excel sheets provided by MOKI NÖ only. Therefore, you have to ignore these tests manually, furthermore, it is not possible for you to use anything concerning excel import and export.

Even though the excel functionality is for anyone but MOKI NÖ not available, the code on how I achived the importing and exporting mechanism is still open source.

## Built With

[Maven](https://maven.apache.org/) - Dependency Management

## Versioning

I use [git](https://gitlab.com/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/green_arrow/moki). 

## Authors

[![alt text](https://gitlab.com/uploads/-/system/user/avatar/2597448/avatar.png?width=400 "Green Arrow")](https://gitlab.com/green_arrow)


I have developed this work completely free and without any charge. Also my technical support is free and without charge. My mission was to contribute to the great work of the MOKI NÖ team and assist them with this web application. Also, I have made some new friends.

## License

This project is licensed under the GNU General Public License, Version 3 - see the [LICENSE](LICENSE) file for details